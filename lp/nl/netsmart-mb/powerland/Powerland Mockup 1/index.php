<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Powerland</title>
		<meta name="description" content="สาว VDO คลิป" />
		<meta name="keywords" content="สาว VDO คลิป, คลิป VDO, ผู้หญิงเซ็กซี่" />
		<meta name="viewport" content="width=device-width, initial scale=1.0" />

		<link href="style/css/styles.css" rel="stylesheet" type="text/css" /> 

		<script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
		<script type="text/javascript" src="/vendor/jquery/jquery-3.2.1.min.js"></script>

	</head>
	<body>

	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="" >
	<input type="hidden" id="shortcode" name="shortcode"  value="8877" >
	<input type="hidden" id="keyword" name="keyword"  value="" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="NL" >
	<input type="hidden" id="landing_page" name="landing_page"  value="powerland1" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="Teracomm" >
	<input type="hidden" id="carryover" name="carryover" value="" >

	<div class="lp-container">
		<header class="header">
			<div class="head">
				<div class="head-left">
					<p>Neem een QNET abonnement op games, apps, beltonen en</p>
				</div>
				<div class="head-right">
					<ul>
						<li>Abonnement</li>
						<li>5.00€/Week</li>
						<li>Advertentie</li>
					</ul>
				</div>
			</div>
		</header>
		<main class="wrapper">
			<section class="lp-image">
				<img src="img/image.png" alt="box with content image">
			</section>
			<div class="button">
				<button id="btn-continue"><img src="img/button.gif" alt="KLIK HIER"></button>
			</div>
		</main>
		<footer class="lp-footer">
			<p>
				<a href="http://tnc.mobitechsolutions.net/#1" target="_blank">Diensten & Prijzen</a> /
				<a href="http://tnc.mobitechsolutions.net/#2" target="_blank">ONDERSTEUNDE TOESTELLEN</a> /
				<a href="http://tnc.mobitechsolutions.net/#3" target="_blank">VOOWARDEN</a> /
				<a href="https://www.payinfo.nl/" target="_blank">SMS-Gedragscode</a> /
				<a href="http://tnc.mobitechsolutions.net/#4" target="_blank">CONTACTS</a> /
				<a href="http://tnc.mobitechsolutions.net/#5" target="_blank">PRIVACYBELEID</a>
			</p>
			<p>Dit is een betaalde abonnementendienst tot u het opzegt. €5.00/week, 3sms/week (+SMS-en downloadkosten). De kosten van de eerste week bedragen €5.00. Alle aangegeven prijzen zijn inclusief BTW. Standaard besturings/datakosten zijn van toepassing. Om te abonneren vult u uw mobiele telefoonnummer in en antwoort u met het woord BETALEN. Om het abonnement stop te zetten stuurt u de tekst STOP naar 8877. De minimale abonnementsduur is 1 week. U kunt onbeperkt content downloaden uit ons assortiment bijv. ringtones, games, wallpapers, applicaties (apps). Telefoon met WAP/GPRS vereist. Applicaties zijn allen voor eigen gebruik. Alle ringtones zijn gecoverd en niet goedgekeurd door de artiest. Handelsmerken, dienstmerken, logo's (inclusief die zonder beperking en de individuele namen van producten en retailers) zijn eigendom van hun eigenaren. Klanten moeten boven de 18 zijn en goedkeuring geven om wettelijke betaler te zijn. Er geldt geen herroepingrecht of vergoeding. Voor hulp belt u 09000400430, €0.00 per minuut (maandag t/m vrijdag 8:00-17:00 met uitzondering van feestdagen) of stuurt u een email naar: nl_cs@mobitechsolutions.net. Deze dienst wordt aangeboden door Mobitech Solutions. Adres Bmobil (adres SMS aanbieder): Unit No.6, 2nd Floor, Block B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713. BTW nummer: P/83736/2010. De Reclamecode SMS-Dienstverlening Code en de SMS-Dienstverlening Gedragscode zijn van toepassing. Bij het abonneren op deze dienst, gaat u akkoord met de Algemene Voorwaarden.
			</p>
			<ul>
				<li><img src="img/iconSmall1.png" alt="icon1"></li>
				<li><img src="img/iconSmall2.png" alt="icon1"></li>
				<li><img src="img/iconSmall3.png" alt="icon1"></li>
				<li><img src="img/iconSmall4.png" alt="icon1"></li>
				<li><img src="img/iconSmall5.png" alt="icon1"></li>
				<li><img src="img/iconSmall6.png" alt="icon1"></li>
			</ul>
		</footer>
	</div>
		
	</body>
</html>
