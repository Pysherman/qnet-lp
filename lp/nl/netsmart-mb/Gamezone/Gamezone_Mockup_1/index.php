<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Gamezone_mockup_1</title>
    <meta name="description" content="สาว VDO คลิป" />
    <meta name="keywords" content="สาว VDO คลิป, คลิป VDO, ผู้หญิงเซ็กซี่" />
    <meta name="viewport" content="width=device-width, initial scale=1.0" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen and (max-width:599px)" href="css/mobile.css" />
    <link rel="stylesheet" type="text/css" media="screen and (min-width:600px) and (max-width:1023px)" href="css/tablet.css"/>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700" rel="stylesheet">
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
</head>

<body>
    <input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
    <input type="hidden" id="operator_code" name="operator_code" value="">
    <input type="hidden" id="shortcode" name="shortcode" value="8877">
    <input type="hidden" id="keyword" name="keyword" value="KEYWORD">
    <input type="hidden" id="user_ip" name="user_ip" value="">
    <input type="hidden" id="affiliate_code" name="affiliate_code" value="">
    <input type="hidden" id="country_code" name="country_code" value="rs">
    <input type="hidden" id="landing_page" name="landing_page" value="Gamezone_mockup_1">
    <input type="hidden" id="gateway_code" name="gateway_code" value="Teracomm">
    <input type="hidden" id="carryover" name="carryover" value="">

    <div class="lp-container">
        <section class="section">
            <div class="main">
                <header class="header">
                    <img src="assets/banner.png" alt="">
                </header>
                <div class="content">
                    <div>
                        <div class="primary">Speel de nieuwste html5- en apk-games</div>
                        <div class="button"><a href="sms:8877?body=" id="btn-continue"><img src="assets/button.png" alt="NU AFSPELEN"></a></div>
                        <div class="primary">of verzenden</div>
                        <div class="animated">
                            <span style="vertical-align:middle;">KEYWORD</span>
                            <small style="vertical-align:middle;">naar</small>
                            <span style="vertical-align:middle;">8877</span>
                        </div>
                        <div class="secondary">Abonnement: 0.00 €/Hour</div>
                        <div class="tertiary">Verenigbaar</div>
                        <div class="icons">
                            <img src="assets/android-icon.png" alt="android-icon">
                            <img src="assets/crome-icon.png" alt="chrome-icon">
                            <img src="assets/firefox-icon.png" alt="firefox-icon">
                            <img src="assets/ie-icon.png" alt="ie-icon">
                            <img src="assets/opera-icon.png" alt="opera-icon">
                        </div>
                    </div>
                </div>
                <footer class="footer">
                    <p>Sed eu euismod neque. Pellentesque fringilla orci vitae mattis semper. Curabitur sed imperdiet eros.
                        Quisque libero nisl, laoreet nec ligula id, blandit convallis enim. Vivamus vel ante rhoncus, condimentum
                        risus vitae, accumsan turpis. Praesent pulvinar felis dui, sit amet dignissim nibh elementum in.
                        Quisque et ante ut ipsum tincidunt porta. Nunc vestibulum purus eu sem commodo tempus eget sit amet
                        turpis. Sed non velit nulla. Suspendisse nunc mi, vehicula eget tellus sed, sodales tristique nisl.
                        Proin placerat eget nibh a tempus. In hac habitasse platea dictumst. Morbi ut mauris a lectus bibendum
                        gravida. Nunc tellus eros, mollis eget nulla non, aliquam facilisis ligula. Aenean eget metus pulvinar,
                        pulvinar felis eget, elementum orci.
                    </p>
                    <ul>
                        <li><a target="_blank" href="#">Sed eu | </a></li>
                        <li><a target="_blank" href="#">Euismod | </a></li>
                        <li><a target="_blank" href="#">Neque Pellentesque | </a></li>
                        <li><a target="_blank" href="#">Fringilla</a></li>
                    </ul>
                </footer>
            </div>
        </section>
    </div>
</body>

</html>