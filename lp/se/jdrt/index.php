<?php require_once '../../../resources/se/netsmart/header/header.php' ?>
<?php 
    $service = array(
    
     "FPS" => array(
         "lptitle" => 'Funopolis',
         "css" => 'style.css',
         "keyword" => 'FPS',
         "shortcode" => '72688',
         "lpheader" => 'image/header-imga.jpg',
         "description" => 'FUNOPOLIS är en mobil prenumeration som kostar 50 kr/sms vecka under 52 veckor för att gå
                  med att gå med och få tillgång till portal. Spela obegränsat antal spel, ringsignaler, videor och
                  Android-applikationer! Den som vill registrera sig för tjänsten
                  måste ha fyllt 18 år. Denna tjänst gäller bara för följande operatörer: Tre, Tele2, Telia och Telenor. Du kan
                  avbryta prenumerationen genom att skicka STOPP FPS till 72688. Kontrollera att din telefon är aktiverad för
                  GPRS för att använda tjänsten. Behöver du hjälp? 0200898322 eller cs@jdnetworks.mobi. Varumärken,
                  registrerade varumärken och logotyper tillhör respektive företag. Alla innehåll på den här webbplatsen är bara
                  avsedda för underhållning. Den här tjänsten tillhandahålls av JD Tech Solutions, Unit No. 6, 2nd Floor, Block B, Spg
                  82, Pg Haji Tajuddin Complex, KG Delima Satu Serusop, Jalan Muara Bandar Seri Begawan, BB4713 Brunei Darussalam.',
         "webref" => 'http://funopolis.jdnetworks.mobi'
         ), 
    
     "GZD" => array(
         "lptitle" => 'Gamezoid',
         "css" => 'stylegz.css',
         "keyword" => 'GZD',
         "shortcode" => '72688',
         "lpheader" => 'image/gmzoid.jpg',
         "description" => 'GAMEZOID är en mobil prenumeration som kostar 50 kr/sms vecka under 52 veckor för att gå
					med att gå med och få tillgång till portal. Spela obegränsat antal Android- och HTML5-spel! Den som
					vill registrera
					sig för tjänsten
					måste ha fyllt 18 år. Denna tjänst gäller bara för följande operatörer: Tre, Tele2, Telia och
					Telenor. Du kan
					avbryta prenumerationen genom att skicka STOPP GZD till 72688. Kontrollera att din telefon är
					aktiverad för
					GPRS för att använda tjänsten. Behöver du hjälp? 0200898322 eller cs@jdnetworks.mobi. Varumärken,
					registrerade varumärken och logotyper tillhör respektive företag. Alla innehåll på den här
					webbplatsen är bara
					avsedda för underhållning. Den här tjänsten tillhandahålls av JD Tech Solutions, Unit No. 6, 2nd
					Floor, Block B, Spg
					82, Pg Haji Tajuddin Complex, KG Delima Satu Serusop, Jalan Muara Bandar Seri Begawan, BB4713 Brunei
					Darussalam.',
         "webref" => 'http://gamezoid.jdnetworks.mobi'
         ),
    
     );

    if (!empty($_POST['keyword'])) {
        $randIndex = $_POST['keyword'];
    } else {
        $randIndex = array_rand($service);
    }

    $lptitle = $service[$randIndex]['lptitle']; 
    $css = $service[$randIndex]['css']; 
    $keyword = $service[$randIndex]['keyword']; 
    $shortcode = $service[$randIndex]['shortcode']; 
    $lpheader = $service[$randIndex]['lpheader']; 
    $description = $service[$randIndex]['description']; 
    $webref = $service[$randIndex]['webref']; 
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="<?php echo $css; ?>">
      <title><?php echo $lptitle; ?></title>
      <script type="text/javascript" src="http://www.adsqnt.com/resources/js/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="http://www.adsqnt.com/resources/se/netsmart/handler/handler.js"></script>
   </head>
   <body>
      <form method="post">
         <input type="hidden" id="shortcode" name="shortcode" value="<?php echo $shortcode; ?>">
         <input type="hidden" id="keyword" name="keyword" value="<?php echo $keyword; ?>">
         <input type="hidden" id="user_ip" name="user_ip" value="">
         <input type="hidden" id="user_agent" name="user_agent" value="">
         <input type="hidden" id="view_code" name="view_code" value="<?php echo $view_code;?>">
         <input type="hidden" id="affiliate_code" name="affiliate_code" value="<?php echo $affiliate_code;?>">
         <input type="hidden" id="landing_page_url" name="landing_page_url" value="<?php echo $landing_page_url;?>">
         <input type="hidden" id="country_code" name="country_code" value="SE">
         <input type="hidden" id="landing_page" name="landing_page" value="<?php echo $lptitle; ?>">
         <input type="hidden" id="gateway_code" name="gateway_code" value="Netsmart">
         <input type="hidden" id="start-doi" name="start-doi" value="1">
         <input type="hidden" id="web_reference"  value="<?php echo $webref; ?>">
         
         <main class="lp-container">
            <header class="title">
               <img src="<?php echo $lpheader;?>">
            </header>

            <section class="details">
               <?php if($display_style == 'display:none'): ?>
               <div id="section__detailsuno">
                  <p><?php echo $lptitle;?> för en prenumerationsavgift på 50 kr/sms vecka under 52 veckor. Behöver du hjålp? 0200898322</p>
                  <h3>Ange ditt mobilnummer</h3>
                  <input type="text" class="input" name="msisdn" id="msisdn" placeholder="07">
                  <section>
                     <button class="button input contract-overlay" style="z-index: 10;" id="btn-continue">
                     <span class="label">FORTSATT</span> <span class="spinner"></span>
                     </button>
                  </section>
               </div>

               <?php endif; ?>
               <div id="section__detailsdos" style="<?=$display_style?>">
                  <p><?php echo $lptitle;?> för en prenumerationsavgift på 50 kr/sms vecka under 52 veckor. Behöver du hjålp? 0200898322
                  </p>
                  <h3>Skickat</h3>
                  <div class="span">
                     <span><?php echo $keyword; ?></span>
                     <h3>till</h3>
                     <span><?php echo $shortcode; ?></span>
                  </div>
               </div>

               <p>
                  <?php echo $description; ?>
               </p>
               </p>
            </section>

            <section class="details2">
               <img src="image/icons.png">
            </section>
            <a class="policy" href="http://seads.jdnetworks.mobi/prev/">Integritetspolicy</a>
         </main>
      </form>
      <script src="js/script.js"></script>
   </body>
</html>