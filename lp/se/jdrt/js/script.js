var buttons = document.querySelectorAll(".button");
   
Array.prototype.slice.call(buttons).forEach(function (button) {
  var resetTimeout;

  button.addEventListener(
    "click",
    function () {
      if (typeof button.getAttribute("data-loading") === "string") {
        button.removeAttribute("data-loading");
      } else {
        button.setAttribute("data-loading", "");
      }

      clearTimeout(resetTimeout);
        resetTimeout = setTimeout(function () {
          button.removeAttribute("data-loading");
         document.getElementById("section__detailsuno").style.display = "none";
         document.getElementById("section__detailsdos").style.display = "block";
      }, 2000);
    },
    false
  );
});
