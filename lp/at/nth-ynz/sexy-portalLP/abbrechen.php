<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/style.css">
	
	<script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
    <title>Naughtybabe</title>
</head>
<body>
	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="" >
	<input type="hidden" id="shortcode" name="shortcode"  value="4010" >
	<input type="hidden" id="keyword" name="keyword"  value="con" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="at" >
	<input type="hidden" id="landing_page" name="landing_page"  value="" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="Teracomm" >
	<input type="hidden" id="carryover" name="carryover" value="" >

    <main class="container">
        <section class="main">
            <div class="ximage">
                <img src="assets/abbrechen.png" alt="">
            </div>
            <div class="heading">
                <h3>ABBRECHEN</h3>
            </div>
            <div class="paragh1">
                <p class="weight500">Sehr geehrte/r, Sie haben den Kaufvorgang abgebrochen. Vielen Dank</p>
            </div>
        </section>
    </main>
	
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
</body>
</html>