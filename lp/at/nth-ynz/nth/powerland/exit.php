<!--?php require_once '../../resources/rs/teracomm/header/header.php' ?-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Powerland_LP</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
</head>

<body>
    <input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
    <input type="hidden" id="operator_code" name="operator_code" value="">
    <input type="hidden" id="shortcode" name="shortcode" value="8877">
    <input type="hidden" id="keyword" name="keyword" value="">
    <input type="hidden" id="user_ip" name="user_ip" value="">
    <input type="hidden" id="affiliate_code" name="affiliate_code" value="">
    <input type="hidden" id="country_code" name="country_code" value="nl">
    <input type="hidden" id="landing_page" name="landing_page" value="Healthland_LP2">
    <input type="hidden" id="gateway_code" name="gateway_code" value="Teracomm">
    <input type="hidden" id="carryover" name="carryover" value="">

    <header class="header">
        <div class="header-div">
            <div class="logo">
            <div  id="btn-continue"><img src="assets/abbrechen.png"/></div>
            </div>
        </div>
    </header>
    <div id="content" style="border:none;">
        <div class="content-main">
            <div class="content-top">
                <p style="color:#DC5D08;font-size: 22px;font-weight: 600;margin-top: 1.5rem;">ABBRECHEN</p>
                <p style="font-weight:600;margin-top: 1rem;">Sehr geehrte/r, Sie haben den Kaufvorgang
abgebrochen. Vielen Dank</p>
            </div>
        </div>
    </div>
</body>

</html>