var sub_btn = document.getElementById("subBTN");
var check_box = document.getElementById("CheckBox");
var tncLink = document.getElementById("TNC");

var tncWrapper = document.querySelector(".wrapper");
var tncClose = document.querySelector(".close");
var termsWrapper =  document.querySelector(".termsContainer");

check_box.addEventListener('click',function(){
    if(check_box.checked == true){
        sub_btn.removeAttribute("disabled");
        sub_btn.classList.remove("disabled");
        sub_btn.classList.add("enabled");
    }else{
        sub_btn.setAttribute("disabled","");
        sub_btn.classList.remove("enabled");
        sub_btn.classList.add("disabled");
    }
});


tncLink.addEventListener('click', function(e){
    e.preventDefault();
    tncWrapper.style.display = "block";
    termsWrapper.classList.add("animateZomein");
    //setTimeout(function(){
    //    termsWrapper.classList.remove("animateZomein");
    //}, 500);
});

tncClose.addEventListener('click', function(e){
    e.preventDefault();
    termsWrapper.classList.add("animateZomeout");
    setTimeout(function(){
        tncWrapper.style.display = "none";
        termsWrapper.classList.remove("animateZomeout");
    }, 400);
});