let getSubscribeBtn = document.querySelector(".sub-btn");
let getCheckBox = document.querySelector("#CheckBox");
let getTncLink = document.querySelector("#TNC");
let getCloseBtn = document.querySelector(".close");
let getTncWrapper = document.querySelector(".wrapper");
let getTermsWrapper = document.querySelector(".termsContainer");

getCheckBox.addEventListener('click', function(){
    if(getCheckBox.checked == true){
        getSubscribeBtn.removeAttribute("disabled");
        getSubscribeBtn.style.cssText = "opacity:1;";
    }else if(getCheckBox.checked == false){
        getSubscribeBtn.setAttribute("disabled", "");
        getSubscribeBtn.style.cssText = "opacity:.5;";
    }
});

getTncLink.addEventListener('click', function(e){
    e.preventDefault();
    activateTncLink.tncfunction();
});

getCloseBtn.addEventListener('click', function(e){
    e.preventDefault();
    closeTnc.tncfunction();
});

function DisplayTNC(tncbutton, tncwrapper, termswrapper, tncdisplayclass, tnctimer){
    this.tncbutton = tncbutton;
    this.tncwrapper = tncwrapper;
    this.termswrapper = termswrapper;
    this.tncdisplayclass = tncdisplayclass;
    this.thnctimer = tnctimer;
    this.tncfunction = function(){
        if(tncbutton.classList.contains("close")){
            termswrapper.classList.add("animateslideUp");
            setTimeout(function(){
                tncwrapper.style.display = tncdisplayclass;
                termswrapper.classList.remove("animateslideUp");
            }, tnctimer);
        }else if(tncbutton.id == "TNC"){
            tncwrapper.style.display = tncdisplayclass;
            termswrapper.classList.add("animateslideDown");
            setTimeout(function(){
                termswrapper.classList.remove("animateslideDown");
            }, tnctimer);
        }
    };
};

let activateTncLink = new DisplayTNC(getTncLink, getTncWrapper, getTermsWrapper, "block", 800);
let closeTnc = new DisplayTNC(getCloseBtn, getTncWrapper, getTermsWrapper, "none", 700);