<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>APK</title>
		<meta name="description" content="สาว VDO คลิป" />
		<meta name="keywords" content="สาว VDO คลิป, คลิป VDO, ผู้หญิงเซ็กซี่" />
		<meta name="viewport" content="width=device-width, initial scale=1.0" />

<!--<link rel="stylesheet" type="text/css" href="vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css" />-->
<!--<link href="style/css/serbia.css" rel="stylesheet" type="text/css" />-->

		<link href="style/css/styles.css" rel="stylesheet" type="text/css" /> 

		<script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>

	</head>
	<body>

	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="" >
	<input type="hidden" id="shortcode" name="shortcode"  value="1298" >
	<input type="hidden" id="keyword" name="keyword"  value="VID PLAY FUN" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="bgr" >
	<input type="hidden" id="landing_page" name="landing_page"  value="apk" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="Teracomm" >
	<input type="hidden" id="carryover" name="carryover" value="" >
	

	<header>
		<img src="img/title.png" alt="">
	</header>

	<main>
		<section class="sect1">
			<p><span>1000+ игри по избор!</span> Аркадни игри, пъзели, забавни игри и др.!</p>
		</section>
		<section class="sect2">
			<img src="img/design 1.png" alt="">
		</section>
		<section class="sect3">
			<article class="art1">
				<p>ПОЛУЧЕТЕ НЕОГРАНИЧЕН ДОСТЪП ДО ИГРИ!</p>
				<span>6.12лв/седмично</span>
			</article>
			<article class="art2">
				<button id="subButton">ИЗПРАТИ SMS</button>
			</article>
			<article class="art3">
					<span>ИЗПРАТИ</span>
					<p><span>PLAY</span>&nbsp&nbsp&nbsp to<span> 1298</span></p>
			</article>
			<article class="art4">
				<p>НЕОГРАНИЧЕНО МОБИЛНО СЪДЪРЖАНИЕ</p>
				<ul>
					<li><img src="img/1.png" alt=""></li>
					<li><img src="img/2.png" alt=""></li>
					<li><img src="img/3.png" alt=""></li>
					<li><img src="img/4.png" alt=""></li>
					<li><img src="img/5.png" alt=""></li>
					<li><img src="img/6.png" alt=""></li>
					<li><img src="img/7.png" alt=""></li>
					<li><img src="img/8.png" alt=""></li>
					<li><img src="img/9.png" alt=""></li>
					<li><img src="img/10.png" alt=""></li>
					<li><img src="img/11.png" alt=""></li>
					<li><img src="img/12.png" alt=""></li>
				</ul>
			</article>
		</section>
	</main>

	<footer>
		<span id="trigger">Общи условия</span>
		<p id="displayON">Със стартирането на абонамента, се съгласявате с условията на услугата, а именно да бъдете таксувани 6 лв. с ДДС / седмично. Стойността на услугата ще се начислява върху вашето месечно потребление за мобилен телефон. За да използвате услугата трябва да имате поне 18 години, а вашия телефон трябва да поддържа интернет услуги. За повече информация и въпроси, моля пишете на: mobitech.bg@silverlines.info</p>
	</footer>
		
	
	</body>
	<script type="text/javascript" src="style/js/script.js"></script>
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
</html>
