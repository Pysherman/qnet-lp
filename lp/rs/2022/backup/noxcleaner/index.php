<?php require_once '../../../resources/rs/nth/header/header.php' ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-147360504-3');
    </script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Noxcleaner</title>
<meta name="viewport" content="width=device-width, initial scale=1.0" />

<link rel="stylesheet" type="text/css" href="vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css" />
<link href="style/css/serbia.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../../../resources/rs/nth/handler/handler.js?v=5"></script> 

</head>

<body>

<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
<input type="hidden" id="operator_code" name="operator_code"  value="TELENOR" >
<input type="hidden" id="shortcode" name="shortcode"  value="5060" >
<input type="hidden" id="keyword" name="keyword"  value="ON" >
<input type="hidden" id="user_ip" name="user_ip"  value="" >
<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
<input type="hidden" id="country_code" name="country_code"  value="rs" >
<input type="hidden" id="landing_page" name="landing_page"  value="Noxcleaner" >
<input type="hidden" id="gateway_code" name="gateway_code"  value="Nth" >
<input type="hidden" id="carryover" name="carryover" value="" >
<input type="hidden" id="endpage" name="endpage"  value="<?php echo "http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/rs/p2/noxcleaner/" ?>" >

<div id="bar">Lagan i brz čistač za pametne telefone</div>
<div id="wrap" class="container">
    <div id="wrapper" style="text-align:center;">
        <div style="text-align:center; ">
            
            <img id="img-header" src="assets/header.png" alt="" width="100%" />
            <div class="row">
                <p>Zavrsni Korak:</p>
                <div id="klikni" class="col-md-6 col-xs-12">
                        <a id="btn-continue" class="btn2">
                             <img id="img-btn" src="assets/btn.png" alt="" width="100%"  />
                        </a>
                </div>
                
                <div id="code" class="col-md-4" >
                    <div id="text2">ili pošalji</div>
                    <div id="textP" class="animated pulse">ON</div>&nbsp;&nbsp;
                    <div id="text2" >na</div>&nbsp;&nbsp;
                    <div id="textP" class="animated pulse">5060</div>
                </div>
            </div>

            <div class="compatibility">
                <p>Neograničeni pristup sadrzajima za mobilne telefone</p>
                    <img  src="assets/icon/1.png" alt="Android" width="17%" />
                    <img  src="assets/icon/2.png" alt="Opera" width="17%"/>
                    <img  src="assets/icon/3.png" alt="Internet Explorer" width="17%" />
                    <img  src="assets/icon/4.png" alt="Firefox" width="17%"/>
                    <img  src="assets/icon/5.png" alt="Chrome" width="17%" />
            </div><br>

            <div class="compatibility">
                <br>
                <p>Kompatibilno sa</p>
                    <img src="assets/icon/6.png" alt="Android" width="17%" />
                    <img src="assets/icon/7.png" alt="Opera" width="17%" />
                    <img src="assets/icon/8.png" alt="Internet Explorer" width="17%" />
                    <img src="assets/icon/9.png" alt="Firefox" width="17%" />
                    <img src="assets/icon/10.png" alt="Chrome" width="17%" />
            </div>
            <br><br>
            <div id="desc" class="col-xs-12 text-center">
            <p style="text-align:justify; font-size:11px;"> <br><br>
            Ovo je zabavna usluga koja pruža pristup različitim videima, igrama, slikama, aplikacijama i
melodijama. Pošalji ON na 5060. Pružalac usluge: Mobitech Solutions. Sav GPRS/WAP
promet ce biti naplacen od strane operatora. Usluga kosta 360 DIN nedeljno (plus cena
osnovnog SMSa + PDV: Telekom 6,0 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel
besplatno). Za odjavu sa servisa posaljite STOP ON za Telenor, Globaltel i VIP ili STOPON
na 5060 za Telekom. Cena odjave servisa je: Telekom 6,0 din + PDV, Telenor 3,60 din +
PDV, VIP 3,48 din + PDV, Globaltel besplatno. Kontakt podrška:
mobitech.rs@silverlines.info hotline: 00381113216815. Tehnički pružalac usluge NTH
Media d.o.o. Beograd.
                    </p>
            </div>

            <div id="footer">
                    <a href="tnc.html" target="blank">Opšti uslovi korišćenja</a> <span style="color:white">|</span>
                    <a href="help.html" target="blank">Pomoć</a> <span style="color:white">|</span>
                    <a href="support.html" target="blank">Podržavani telefoni</a> <span style="color:white">|</span>
                    <a href="contact.html" target="blank">Kontakt</a>
            </div><br/>
</div>
</div>

</body>

</html>