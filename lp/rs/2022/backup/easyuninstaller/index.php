<?php require_once '../../../resources/rs/nth/header/header.php' ?>
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Easy Uninstaller</title>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-3"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag() { dataLayer.push(arguments); }
		gtag('js', new Date());
		gtag('config', 'UA-147360504-3');
	</script>

	<link href="style/css/styles.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../../../resources/rs/nth/handler/handler.js?v=5"></script>

</head>

<body>

	<input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
	<input type="hidden" id="operator_code" name="operator_code" value="">
	<input type="hidden" id="shortcode" name="shortcode" value="5060">
	<input type="hidden" id="keyword" name="keyword" value="OK">
	<input type="hidden" id="user_ip" name="user_ip" value="">
	<input type="hidden" id="affiliate_code" name="affiliate_code" value="">
	<input type="hidden" id="country_code" name="country_code" value="rs">
	<input type="hidden" id="landing_page" name="landing_page" value="easy_uninstaller">
	<input type="hidden" id="gateway_code" name="gateway_code" value="nth">
	<input type="hidden" id="carryover" name="carryover" value="">

	<header class="headertop"></header>
	<div class="lp-container">
		<div class="lp-top">
			<div class="lp-header">
				<img src="img/unnamed.png" alt="" class="class-anim" />
			</div>
		</div>
		<div class="lp-body">
			<div class="lp-details">
				<span>Zavrsni Korak:</span>
				<button class="subscribe-btn btn" id="btn-continue2">
					PREUZMI SADA
				</button>
				<span>ili pošalji</span>
				<p><span class="class-anim2">OK</span> na <span class="class-anim2">5060</span></p>
			</div>
			<div class="lp-gamesicon">
				<span>Neograničeni pristup sadrzajima za mobilne telefone</span>
				<ul>
					<li><img src="img/1.png" alt="Game 1" /></li>
					<li><img src="img/2.png" alt="Game 2" /></li>
					<li><img src="img/3.png" alt="Game 3" /></li>
					<li><img src="img/4.png" alt="Game 4" /></li>
					<li><img src="img/5.png" alt="Game 5" /></li>
					<li><img src="img/6.png" alt="Game 6" /></li>
					<li><img src="img/7.png" alt="Game 5" /></li>
					<li><img src="img/8.png" alt="Game 5" /></li>
					<li><img src="img/9.png" alt="Game 5" /></li>
					<li><img src="img/10.png" alt="Game 5" /></li>
				</ul>
			</div>
			<div class="lp-footer">
				<p>Ovo je zabavna usluga koja pruža pristup različitim videima, igrama, slikama, aplikacijama i
melodijama. Pošalji OK na 5060. Pružalac usluge: Mobitech Solutions. Sav GPRS/WAP
promet ce biti naplacen od strane operatora. Usluga kosta 360 DIN nedeljno (plus cena
osnovnog SMSa + PDV: Telekom 6,0 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel
besplatno). Za odjavu sa servisa posaljite STOP OK za Telenor, Globaltel i VIP ili STOPOK
na 5060 za Telekom. Cena odjave servisa je: Telekom 6,0 din + PDV, Telenor 3,60 din +
PDV, VIP 3,48 din + PDV, Globaltel besplatno. Kontakt podrška:
mobitech.rs@silverlines.info hotline: 00381113216815. Tehnički pružalac usluge NTH
Media d.o.o. Beograd.</p>
			</div>
			<div id="footer">
				<a href="tnc.html" target="blank">Opšti uslovi korišćenja</a> <span style="color:#000">|</span>
				<a href="help.html" target="blank">Pomoć</a> <span style="color:#000">|</span>
				<a href="support.html" target="blank">Podržavani telefoni</a> <span style="color:#000">|</span>
				<a href="contact.html" target="blank">Kontakt</a>
			</div>
		</div>
	</div>

</body>
<!-- <script type="text/javascript" src="style/js/script.js"></script> -->
<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<script>
	const getLis = document.querySelectorAll(".lp-header ul li");
	let x = 0;
	let interVal = setInterval(() => {
		getLis[x].classList.add('class-anim');
		x++;
		if (x >= getLis.length) {
			console.log('yes');
			clearInterval(interVal);
		}
	}, 300);
</script>

</html>