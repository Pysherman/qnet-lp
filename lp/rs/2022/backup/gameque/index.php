<?php require_once '../../../resources/rs/nth/header/header.php' ?>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-147360504-3');
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Gameque</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../../resources/rs/nth/handler/handler.js?v=5"></script>

</head>

<body>

    <input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
    <input type="hidden" id="operator_code" name="operator_code" value="">
    <input type="hidden" id="shortcode" name="shortcode" value="5060">
    <input type="hidden" id="keyword" name="keyword" value="YES">
    <input type="hidden" id="user_ip" name="user_ip" value="">
    <input type="hidden" id="affiliate_code" name="affiliate_code" value="">
    <input type="hidden" id="country_code" name="country_code" value="rs">
    <input type="hidden" id="landing_page" name="landing_page" value="Gameque">
    <input type="hidden" id="gateway_code" name="gateway_code" value="nth">
    <input type="hidden" id="carryover" name="carryover" value="">

    <section class="lp-header">
        <p>Igre za koje nije potrebna ogromna količina ili RAM memorija.</p>
    </section>
    <main class="lp-container">
        <section class="lp-img-container">
            <img src="img/lp-banner.gif" alt="">
        </section>

        <section class="lp-details">
            <p>Korisnik ima pristup najboljim igrama. Preuzmite sadržaj na svoj portal i uživajte u iskustvu.</p>
            <a href="#" class="href"  id="btn-continue2">KLIKNI OVDE</a>
            <p>ili posalji</p>
            <p class="sms"><span>YES</span> na <span>5060</span></p>
        </section>

        <section class="lp-tnc">
            <div class="tnc">
                <p>Ovo je zabavna usluga koja pruža pristup različitim videima, igrama, slikama, aplikacijama i
                    melodijama. Pošalji ON na 5060. Pružalac usluge: Mobitech Solutions. Sav GPRS/WAP promet ce biti
                    naplacen od strane operatora. Usluga kosta 360 DIN nedeljno (plus cena osnovnog SMSa + PDV: Telekom
                    6,0 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel besplatno). Za odjavu sa servisa posaljite STOP
                    YES za Telenor, Globaltel i VIP ili STOPYES na 5060 za Telekom. Cena odjave servisa je: Telekom 6,0
                    din + PDV, Telenor 3,60 din + PDV, VIP 3,48 din + PDV, Globaltel besplatno. Kontakt podrška:
                    mobitech.rs@silverlines.info hotline: 00381113216815. Tehnički pružalac usluge NTH Media d.o.o.
                    Beograd.</p>
                <ul>
                    <li><a href="tnc.html" target="_blank">Opšti uslovi korišćenja</a> |</li>
                    <li><a href="help.html" target="_blank">Pomoć</a> |</li>
                    <li><a href="support.html" target="_blank">Podržavani telefoni</a> |</li>
                    <li><a href="help.html" target="_blank">Kontakt</a></li>
                </ul>
            </div>
        </section>
    </main>
</body>

</html>