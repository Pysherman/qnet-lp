<?php require_once '../../../resources/rs/nth/header/header.php' ?>
<?php
    $service = array(
    
     "FUN" => array(
         "keyword" => 'FUN',
         "shortcode" => '5060',
         "landingpage" => 'Download Fun',
         ), 

     "ON" => array(
         "keyword" => 'ON',
         "shortcode" => '5060',
         "landingpage" => 'Download On',
         ), 

     "VID" => array(
         "keyword" => 'VID',
         "shortcode" => '5060',
         "landingpage" => 'Download Video',
         ), 

     "YES" => array(
         "keyword" => 'YES',
         "shortcode" => '5060',
         "landingpage" => 'Download Yes',
         ), 
    
     "OK" => array(
         "keyword" => 'OK',
         "shortcode" => '5060',
        "landingpage" => 'Download OK',
         ),
     );

    $randIndex = array_rand($service);
    $keyword = $service[$randIndex]['keyword']; 
    $shortcode = $service[$randIndex]['shortcode']; 
    $landingpage = $service[$randIndex]['landingpage']; 
    ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css" />
    <script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../../resources/rs/nth/handler/handler.js?v=5"></script> 
    <title>Download Cotent</title>
  </head>
  <body>
    <input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
    <input type="hidden" id="operator_code" name="operator_code" value="">
    <input type="hidden" id="shortcode" name="shortcode" value="<?php echo $shortcode;?>">
    <input type="hidden" id="keyword" name="keyword" value="<?php echo $keyword;?>">
    <input type="hidden" id="user_ip" name="user_ip" value="">
    <input type="hidden" id="affiliate_code" name="affiliate_code" value="">
    <input type="hidden" id="country_code" name="country_code" value="rs">
    <input type="hidden" id="landing_page" name="landing_page" value="<?php echo $landingpage;?>">
    <input type="hidden" id="gateway_code" name="gateway_code" value="NTH">
    <input type="hidden" id="carryover" name="carryover" value="">
    <input type="hidden" id="endpage" name="endpage"  value="<?php echo "http://adsqnt.com/rs/mb/dwnload1/?affiliate_code=QNET" ?>" >

    <main class="main-container">
      <section class="top">
        <img src="assets/img.png" alt="img" />
      </section>
      <section class="middle">
        <p>Zavrsni Korak:</p>
        <a href="#" id="btn-continue" class="btn">PREUZIMANJE</a>
        <p>ili pošalji</p>
        <div class="details">
          <p>
            <span class="big pulse"><?php echo $keyword;?></span> na
            <span class="big pulse"><?php echo $shortcode;?></span>
          </p>
        </div>
      </section>
      <section class="bottom">
        <div class="compatibility">
          <p>Kompatibilno sa</p>
          <div>
            <img src="assets/6.png" alt="" />
            <img src="assets/7.png" alt="" />
            <img src="assets/8.png" alt="" />
            <img src="assets/9.png" alt="" />
            <img src="assets/10.png" alt="" />
          </div>
        </div>
        <p>
          Ovo je <?php echo $keyword;?> usluga koja pruža pristup različitim videima, igrama,
          slikama, aplikacijama i melodijama. Pošalji <?php echo $keyword;?> na <?php echo $shortcode;?>. Pružalac
          usluge: Mobitech Solutions. Sav GPRS/WAP promet ce biti naplacen od
          strane operatora. Usluga kosta 360 DIN nedeljno (plus cena osnovnog
          SMSa + PDV: Telekom 6,0 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel
          besplatno). Za odjavu sa servisa posaljite STOP <?php echo $keyword;?> za Telenor,
          Globaltel i VIP ili STOP<?php echo $keyword;?> na <?php echo $shortcode;?> za Telekom. Cena odjave servisa
          je: Telekom 6,0 din + PDV, Telenor 3,60 din + PDV, VIP 3,48 din + PDV,
          Globaltel besplatno. Kontakt podrška: mobitech.rs@silverlines.info
          hotline: 00381113216815. Tehnički pružalac usluge NTH Media d.o.o.
          Beograd.
        </p>
        <div class="tnc">
          <p>
            <a href="tnc.html" target="blank">Opšti uslovi korišćenja</a> |
            <a href="help.html" target="blank">Pomoć</a> |
            <a href="support.html" target="blank">Podržavani telefoni</a> |
            <a href="contact.html" target="blank">Kontakt</a>
          </p>
        </div>
      </section>
    </main>
  </body>
</html>
