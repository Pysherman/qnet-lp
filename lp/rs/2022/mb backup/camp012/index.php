<?php require_once '../../../resources/rs/nth/header/header.php' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-3"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-147360504-3');
        </script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Apex</title>
		<meta name="viewport" content="width=device-width, initial scale=1.0" />
		<link href="style/css/styles.css" rel="stylesheet" type="text/css" /> 
		<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../../../resources/rs/nth/handler/handler.js?v=5"></script> 
	</head>
	<body>

	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="" >
	<input type="hidden" id="shortcode" name="shortcode"  value="5060" >
	<input type="hidden" id="keyword" name="keyword"  value="FUN" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="rs" >
	<input type="hidden" id="landing_page" name="landing_page"  value="rs_camp012" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="nth" >
	<input type="hidden" id="carryover" name="carryover" value="" >

		<div class="content-wrapper">
			<div class="grid-container">	
				<div class="image-heading">
					<img src="img/header.png" alt="NAJTOPLIJE MOBILNE IGRE" />
				</div>
				<div class="final-step">
					<div class="step-heading">
						<span>Čestitamo! Zavrsni Korak:</span>
					</div>
					<div class="step-button">
						<!-- <button class="click-btn" id="btn-continue2"><img src="img/button.png" alt="Preuzmi Sada"></button> -->
					</div>
					<div class="step-keynum">
						<span>SMS</span>
						<p><span>FUN</span> na <span>5060</span></p>
					</div>
				</div>
			</div>
		</div>

		<div class="otherGames">
			<p>Neograničeni pristup sadrzajima za mobilne telefone</p>
			<ul>
				<li><img src="img/cod.png" alt="Android" /></li>
				<li><img src="img/fortnite.png" alt="Opera" /></li>
				<li><img src="img/moderncombat.png" alt="Internet Explorer" /></li>
				<li><img src="img/pubg.png" alt="Firefox" /></li>
				<li><img src="img/ros.png" alt="Chrome" /></li>
			</ul>
		</div>

		<div class="compatibility">
			<p>Kompatibilno sa</p>
			<ul>
				<li><img src="img/android.png" alt="Android" /></li>
				<li><img src="img/opera.png" alt="Opera" /></li>
				<li><img src="img/ie.png" alt="Internet Explorer" /></li>
				<li><img src="img/firefox.png" alt="Firefox" /></li>
				<li><img src="img/chrome.png" alt="Chrome" /></li>
			</ul>
		</div>
		<div class="words">
			<p>Apex je zabavna usluga koja pruža pristup različitim videima, igrama, slikama, aplikacijama i
melodijama. Pošalji FUN na 5060. Pružalac usluge: Mobitech Solutions. Sav GPRS/WAP
promet ce biti naplacen od strane operatora. Usluga kosta 360 DIN nedeljno (plus cena
osnovnog SMSa + PDV: Telekom 6,0 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel
besplatno). Za odjavu sa servisa posaljite STOP FUN za Telenor, Globaltel i VIP ili
STOPFUN na 5060 za Telekom. Cena odjave servisa je: Telekom 6,0 din + PDV, Telenor
3,60 din + PDV, VIP 3,48 din + PDV, Globaltel besplatno. Kontakt podrška:
mobitech.rs@silverlines.info hotline: 00381113216815. Tehnički pružalac usluge NTH
Media d.o.o. Beograd.</p>
			<div id="footer">
                    <a href="tnc.html" target="blank">Opšti uslovi korišćenja</a> <span style="color:white">|</span>
                    <a href="help.html" target="blank">Pomoć</a> <span style="color:white">|</span>
                    <a href="support.html" target="blank">Podržavani telefoni</a> <span style="color:white">|</span>
                    <a href="contact.html" target="blank">Kontakt</a>
            </div><br/>
		</div>
	</div>
		
	</body>
	<!--<script type="text/javascript" src="style/js/script.js"></script>
	<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>-->
</html>
