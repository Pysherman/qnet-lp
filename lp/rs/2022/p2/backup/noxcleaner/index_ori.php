<?php require_once '../../../resources/rs/teracomm/header/header.php' ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Noxcleaner</title>
<meta name="viewport" content="width=device-width, initial scale=1.0" />

<link rel="stylesheet" type="text/css" href="vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css" />
<link href="style/css/serbia.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>

</head>

<body>

<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
<input type="hidden" id="operator_code" name="operator_code"  value="" >
<input type="hidden" id="shortcode" name="shortcode"  value="5060" >
<input type="hidden" id="keyword" name="keyword"  value="ON" >
<input type="hidden" id="user_ip" name="user_ip"  value="" >
<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
<input type="hidden" id="country_code" name="country_code"  value="rs" >
<input type="hidden" id="landing_page" name="landing_page"  value="Noxcleaner" >
<input type="hidden" id="gateway_code" name="gateway_code"  value="NTH" >
<input type="hidden" id="carryover" name="carryover" value="" >

<div id="bar">Lagan i brz čistač za pametne telefone</div>
<div id="wrap" class="container">
    <div id="wrapper" style="text-align:center;">
        <div style="text-align:center; ">
            
            <img id="img-header" src="assets/header.png" alt="" width="100%" />
            <div class="row">
                <p>Zavrsni Korak:</p>
                <div id="klikni" class="col-md-6 col-xs-12">
                        <a id="btn-continue" class="btn2">
                             <img id="img-btn" src="assets/btn.png" alt="" width="100%"  />
                        </a>
                </div>
                
                <div id="code" class="col-md-4" >
                    <div id="text2">ili pošalji</div>
                    <div id="textP" class="animated pulse">ON</div>&nbsp;&nbsp;
                    <div id="text2" >na</div>&nbsp;&nbsp;
                    <div id="textP" class="animated pulse">5060</div>
                </div>
            </div>

            <div class="compatibility">
                <p>Neograničeni pristup sadrzajima za mobilne telefone</p>
                    <img  src="assets/icon/1.png" alt="Android" width="17%" />
                    <img  src="assets/icon/2.png" alt="Opera" width="17%"/>
                    <img  src="assets/icon/3.png" alt="Internet Explorer" width="17%" />
                    <img  src="assets/icon/4.png" alt="Firefox" width="17%"/>
                    <img  src="assets/icon/5.png" alt="Chrome" width="17%" />
            </div><br>

            <div class="compatibility">
                <br>
                <p>Kompatibilno sa</p>
                    <img src="assets/icon/6.png" alt="Android" width="17%" />
                    <img src="assets/icon/7.png" alt="Opera" width="17%" />
                    <img src="assets/icon/8.png" alt="Internet Explorer" width="17%" />
                    <img src="assets/icon/9.png" alt="Firefox" width="17%" />
                    <img src="assets/icon/10.png" alt="Chrome" width="17%" />
            </div>
            <br><br>
            <div id="desc" class="col-xs-12 text-center">
            <p style="text-align:justify; font-size:11px;"> <br><br>
                 Ovo je zabavni pretplatnički servis. Usluga košta 2*180 DIN na nedeljnom nivou za MTS ,Telenor, VIP i  Globaltel korisnike (plus cena osnovnog SMS-a: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel besplatno) (Sve navedene cene sa uracunatim PDV-om). Pretplata
na uslugu će biti automatski obnavljana sve dok ne pošaljete STOPON na 5060 po ceni od 3.60 DIN za MTS i Telenor
korisnike i 3,48 DIN za VIP korisnike. Registracijom na ovu uslugu potvrđujete da ste saglasni sa svim važećim odredbama
i uslovima -a. Svi video snimci i slike na ovoj stranici su u svrhu zabave. Za dodatne informacije u vezi naplate pozovite
 na 011/888888. Ovu uslugu nudi vam Mobitech Solutions Unit No.6, 2nd Floor, Block B, Spg82, Pg Haji Tajuddin
Complex, Kg Delima Satu Serusop, Jalan Muara BB4713
                    </p>
            </div>

            <!--<div id="footer">
                    <a href="tnc.html" target="blank">Opšti uslovi korišćenja</a> <span style="color:white">|</span>
                    <a href="help.html" target="blank">Pomoć</a> <span style="color:white">|</span>
                    <a href="support.html" target="blank">Podržavani telefoni</a> <span style="color:white">|</span>
                    <a href="contact.html" target="blank">Kontakt</a>
            </div><br/>-->
</div>
</div>

</body>

</html>