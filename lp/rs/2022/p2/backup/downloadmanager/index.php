<?php require_once '../../../resources/rs/nth/header/header.php' ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-3"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-147360504-3');
        </script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>downloadManager</title>
	<meta name="viewport" content="width=device-width, initial scale=1.0" />
	<link href="style/css/styles.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
 <script type="text/javascript" src="../../../resources/rs/nth/handler/handler.js"></script> 

	</head>
	<body>

	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="TELENOR" >
	<input type="hidden" id="shortcode" name="shortcode"  value="5060" >
	<input type="hidden" id="keyword" name="keyword"  value="YES" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="rs" >
	<input type="hidden" id="landing_page" name="landing_page"  value="downloadManager" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="Nth" >
	<input type="hidden" id="carryover" name="carryover" value="" >
<input type="hidden" id="endpage" name="endpage"  value="<?php echo "http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/rs/p2/downloadmanager/" ?>" >
	<div class="lp-container">
		<div class="lp-top">
			<div class="lp-header">
				<img src="img/logo-animation.gif" alt=""/>
				<h1><span class="Tred">ADVANCE</span>&nbsp&nbsp<span class="Tgreen">DOWNLOAD</span>&nbsp&nbsp<span class="Tblue">MANAGER</span></h1>
			</div>
			<div class="lp-details">
				<span>Zavrsni Korak:</span>
				<button id="btn-continue" class="btn">PREUZIMANJE</button>
				<span>ili pošalji</span>
				<p><span>YES</span> na <span>5060</span></p>
			</div>
		</div>
		<div class="lp-gamesicon">
			<span>Neograničeni pristup sadrzajima za mobilne telefone</span>
			<ul>
				<li><img src="img/icon6.png" alt="Game 1" /></li>
				<li><img src="img/icon7.png" alt="Game 2" /></li>
				<li><img src="img/icon8.png" alt="Game 3" /></li>
				<li><img src="img/icon9.png" alt="Game 4" /></li>
				<li><img src="img/icon10.png" alt="Game 5" /></li>
			</ul>
		</div>
		<div class="lp-compatibility">
			<span>Kompatibilno sa</span>
			<ul>
				<li><img src="img/android.png" alt="Android" /></li>
				<li><img src="img/opera.png" alt="Opera" /></li>
				<li><img src="img/ie.png" alt="Internet Explorer" /></li>
				<li><img src="img/firefox.png" alt="Firefox" /></li>
				<li><img src="img/chrome.png" alt="Chrome" /></li>
			</ul>
		</div>
		<div class="lp-footer">
			<p>Ovo je zabavna pretplatnička usluga. Usluga košta 2 * 180 DIN nedeljno za MTS, Telenor, VIP i Globaltel korisnike 
				(plus cenu osnovnih SMS poruka: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel besplatno) Sa PDV-om). 
				Pretplata na uslugu automatski se ažurira dok ne pošaljete STOPYES na 5060 za Telekom i STOP YES na 5060 za Telenor, 
				VIP i Globaltel po ceni od 3,60 DIN za MTS i Telenor korisnike i 3,48 DIN za VIP korisnike. 
				Prijavom na ovu uslugu potvrdite da ste u skladu sa svim važećim uslovima i odredbama -a. 
				Svi video snimci i slike na ovoj stranici su za zabavu. Za više informacija o obračunu pozovite +381113216815.
				Ova usluga nudi Jedinica Mobitech Solutions br.6, 2. sprat, Blok B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713.
				Хелпдеск Емаил:mobitech.rs@silverlines.info</p>
			<!--<ul>
				<li><a href="tnc.html">Opšti uslovi korišćenja</a></li>
				<li><a href="help.html">Pomoć</a></li>
				<li><a href="support.html">Podržavani telefoni</a></li>
				<li><a href="contact.html">Kontakt</a></li>
			</ul>-->
		</div>
	</div>
		
	</body>
	<!--<script type="text/javascript" src="style/js/script.js"></script>-->

</html>
