<?php require_once '../../../resources/rs/nth/header/header.php' ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-3"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-147360504-3');
        </script>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <title>RS Baby</title>
	    <meta name="viewport" content="width=device-width, initial scale=1.0" />

<!--<link href="https://fonts.googleapis.com/css?family=Open+Sans|Press+Start+2P" rel="stylesheet">-->
<!--<link rel="stylesheet" type="text/css" href="vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css" />-->
<!--<link href="style/css/serbia.css" rel="stylesheet" type="text/css" />-->
 		<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../../../resources/rs/nth/handler/handler.js"></script>

		<link rel="stylesheet" href="dist/css/swiper.min.css" type="text/css">
		<link href="style/css/styles.css" rel="stylesheet" type="text/css" />		
	</head>
	<body>

	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="" >
	<input type="hidden" id="shortcode" name="shortcode"  value="5060" >
	<input type="hidden" id="keyword" name="keyword"  value="VID" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="rs" >
	<input type="hidden" id="landing_page" name="landing_page"  value="rs_baby" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="nth" >
	<input type="hidden" id="carryover" name="carryover" value="" >
	
	<div class="border"></div>
		<div class="lp-container">
	
			<div class="lp-details">
				<div class="image-holder">
					<img src="img/babe.gif" alt="babe">
				</div>
				<div class="details-wrapper">
					<div class="details-top">
						<span>Privlačne i vrele dame</span>
						<span>Video klipovi zasamo 18+</span>
					</div>
					<div class="details-mid">
						<button class="subscribe-btn" id="btn-continue2"><h2>KLIKNI OVDI</h2></button>
					</div>
					<div class="details-bot">
						<span>ili pošalji</span>
						<p><span>VID</span> na <span>5060</span></p>
					</div>
				</div>
			</div>
			<div class="lp-footer">
				<p>Ovo je zabavna pretplatnička usluga. Usluga košta 360 DIN nedeljno za MTS, Telenor, VIP i Globaltel
                            korisnike (plus cenu osnovnih SMS poruka: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel
                            besplatno) Sa PDV-om). Pretplata na uslugu automatski se ažurira dok ne pošaljete STOP VID na 5060
                            za Telekom i STOP VID na 5060 za Telenor, VIP i Globaltel po ceni od 3,60 DIN za MTS i Telenor
                            korisnike i 3,48 DIN za VIP korisnike. Prijavom na ovu uslugu potvrdite da ste u skladu sa svim važećim
                            uslovima i odredbama -a. Svi video snimci i slike na ovoj stranici su za zabavu. Za više informacija o
                            obračunu pozovite +381113216815. Ova usluga nudi Jedinica Mobitech Solutions br.6, 2. sprat, Blok B,
                            Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713. Хелпдеск
                            Емаил:mobitech.rs@silverlines.info</p>

			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
	</body>
	<!--<script type="text/javascript" src="style/js/script.js"></script>-->
</html>
