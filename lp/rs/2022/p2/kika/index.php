<?php require_once '../../../resources/rs/nth/header/header.php' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-147360504-3');
    </script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kika </title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/kzstyle.css" media="screen"  />
<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
 <script type="text/javascript" src="../../../resources/rs/nth/handler/handler.js"></script> 

<style type="text/css">
@media only screen and (max-width: 480px){
  .iconImage{
    height:auto !important;
    max-width:50px !important;
    width: 100% !important;
  }
}
</style>
</head>

<body>

<input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>" >
<input type="hidden" id="operator_code" name="operator_code"  value="TELENOR" >
<input type="hidden" id="shortcode" name="shortcode"  value="5060" >
<input type="hidden" id="keyword" name="keyword"  value="FUN" >
<input type="hidden" id="user_ip" name="user_ip"  value="" >
<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
<input type="hidden" id="country_code" name="country_code"  value="rs" >
<input type="hidden" id="landing_page" name="landing_page"  value="Kika" >
<input type="hidden" id="gateway_code" name="gateway_code"  value="NTH" >
<input type="hidden" id="carryover" name="carryover" value="" >
<input type="hidden" id="endpage" name="endpage"  value="<?php echo "http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/rs/p2/kika/" ?>" >

<div class="content">
    <div class="wrap flex apps2">
    	<div class="item flex-item" style="text-align:center">
            <div class="hidden"><div style="padding:5px;" id="scnd" class="text2">Čestitamo! Zavrsni Korak:</div>
            <a id="btn-continue" href="#" ><img src="img/button.png" width="284" height="63" /></a>
            <div style="margin:0px;"> <p class="text2">ili SMS </p>
            <span class="srvcs">FUN</span></div>
            <div style="padding:5px; margin:0px;"> <p class="text2">na </p> <span class="srvcs">5060</span></div></div>
            
        	<div class="hidden2"><img src="img/emoji-bg.png" class="iconLogo" width="250" height="250" /></div>
        </div>
    	<div class="item flex-item" style="text-align:center">  
            <div class="hidden"><img src="img/emoji-bg.png" class="iconLogo" width="250" height="250" /></div> 
        	<div class="hidden2"><div style="padding:5px;" id="scnd">Čestitamo! Zavrsni Korak:</div>
            <a id="btn-continue2" href="#" ><img src="img/button.png" width="284" height="63" /></a>
            <div style="padding:5px; margin:0px;">ili SMS
            <div class="srvcs">FUN</div></div>
            <div style="padding:5px; margin:0px;"> <font class="text2"> na </font> <div class="srvcs">5060</div></div></div>
        </div>
    </div>
    <div class="wrap flex apps">
    	<div class="item flex-item">
        	<div style="padding:4px; text-align:center" class="text2">Neograničeni pristup sadrzajima za mobilne telefone</div>
            <div style="padding:4px; text-align:center">
           		<img src="img/icon1.png" class="iconImage" width="90" height="73" />
           		<img src="img/icon2.png" class="iconImage" width="90" height="73" />
           		<img src="img/icon3.png" class="iconImage" width="90" height="73" />
           		<img src="img/icon4.png" class="iconImage" width="90" height="73" />
           		<img src="img/icon5.png" class="iconImage" width="90" height="73" />
            </div>
      </div>
    	<div class="item flex-item">
        	<div style="padding:4px; text-align:center" class="text2">Kompatibilno sa</div>
            <div style="padding:4px; text-align:center">
           		<img src="img/icon6.png" class="iconImage" width="90" height="73" />
           		<img src="img/icon7.png" class="iconImage" width="90" height="73" />
           		<img src="img/icon8.png" class="iconImage" width="90" height="73" />
           		<img src="img/icon9.png" class="iconImage" width="90" height="73" />
           		<img src="img/icon10.png" class="iconImage" width="90" height="73" />
            </div>
        </div>
    </div>
    
    <div id="tnc">
                 Ovo je zabavna pretplatnička usluga. Usluga košta 360 DIN nedeljno za MTS, Telenor, VIP i Globaltel korisnike 
                 (plus cenu osnovnih SMS poruka: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel besplatno) Sa PDV-om). 
                 Pretplata na uslugu automatski se ažurira dok ne pošaljete STOPFUN na 5060 za Telekom i STOP FUN na 5060 za Telenor, 
                 VIP i Globaltel po ceni od 3,60 DIN za MTS i Telenor korisnike i 3,48 DIN za VIP korisnike. 
                 Prijavom na ovu uslugu potvrdite da ste u skladu sa svim važećim uslovima i odredbama -a. 
                 Svi video snimci i slike na ovoj stranici su za zabavu. Za više informacija o obračunu pozovite +381113216815. 
                 Ova usluga nudi Jedinica Mobitech Solutions br.6, 2. sprat, Blok B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713.
                 Хелпдеск Емаил:mobitech.rs@silverlines.info
    </div>
    <!--<div id="btmnav">
        <a href="tnc.html" target="blank">Opšti uslovi korišćenja</a> |
        <a href="help.html" target="blank">Pomoć</a> | 
        <a href="support.html" target="blank">Podržavani telefoni</a> |
        <a href="contact.html" target="blank">Kontakt</a>
    </div>-->
    
</div>
</body>
</html>