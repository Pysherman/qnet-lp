<?php require_once '../../../resources/rs/nth/header/header.php' ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="vendor/jquery/jquery-confirm.js"></script>
 <script type="text/javascript" src="../../../resources/rs/nth/handler/handler2.js"></script> 

<title>Dragon Kingdom</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/kzstyle.css" media="screen"  />
<link rel="stylesheet" type="text/css" href="css/jquery-confirm.css" media="screen"  />



<style type="text/css">
    @media only screen and (max-width: 480px){
        .iconImage{
            height:auto !important;
            max-width:50px !important;
            width: 100% !important;
        }
        .iconLogo1{
            height:auto !important;
            max-width:200px !important;
            width: 100% !important;
        }
        .iconLogo2{
            height:auto !important;
            max-width:100px !important;
            width: 100% !important;
        }
    }
</style>
</head>

<body>
<form action="" name="indexfrm">
<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
<input type="hidden" id="operator_code" name="operator_code"  value="TELENOR" >
<input type="hidden" id="shortcode" name="shortcode"  value="5060" >
<input type="hidden" id="keyword" name="keyword"  value="OK" >
<input type="hidden" id="user_ip" name="user_ip"  value="" >
<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
<input type="hidden" id="country_code" name="country_code"  value="rs" >
<input type="hidden" id="landing_page" name="landing_page"  value="DragonKingdom" >
<input type="hidden" id="gateway_code" name="gateway_code"  value="Nth" >
<input type="hidden" id="carryover" name="carryover" value="" >
<input type="hidden" id="endpage" name="endpage"  value="<?php echo "http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/rs/p2/dragonkingdom/" ?>" >

<div class="header">&nbsp;</div>

<div class="content">
    <div class="wrap flex">
    	<div class="item flex-item" style="text-align:center">
        	<img src="img/logo-icon.png" class="iconLogo2" width="110" height="110" />
        	<img src="img/logo.png" class="iconLogo1" width="300" height="127" />
        </div>
    	<div class="item flex-item apps2" style="text-align:center">
        	<div style="padding:5px;" class="text2" id="scnd">Zavrsni Korak:</div>
            
      <input type="button" class="btn btn-primary" id="btn-continue" value="PREUZMI SADA" />

            <p style="padding:5px; margin:0px;" class="text2">ili pošalji</p>
            <div >
              <div class="srvcs">OK</div>
              <font class="text2" style="">na</font>
            <div class="srvcs">5060</div></div>
        </div>
    </div>
    <div class="wrap flex">
    	<div class="item flex-item">
          <div class="apps">
          	<div style="padding:4px; text-align:center" class="text2">Neograničeni pristup sadrzajima za mobilne telefone</div>
              <div style="padding:4px; text-align:center">
             		<img src="img/icon1.png" class="iconImage" width="60" height="60" />
             		<img src="img/icon2.png" class="iconImage" width="60" height="60" />
             		<img src="img/icon3.png" class="iconImage" width="60" height="60" />
             		<img src="img/icon4.png" class="iconImage" width="60" height="60" />
             		<img src="img/icon5.png" class="iconImage" width="60" height="60" />
             		<img src="img/icon6.png" class="iconImage" width="60" height="60" />
             		<img src="img/icon7.png" class="iconImage" width="60" height="60" />
             		<img src="img/icon8.png" class="iconImage" width="60" height="60" />
             		<img src="img/icon9.png" class="iconImage" width="60" height="60" />
             		<img src="img/icon10.png" class="iconImage" width="60" height="60" />
              </div>
            </div>
      </div>
    	<div class="item flex-item">
        	<div style="padding:4px; text-align:center" class="text2">Kompatibilno sa</div>
            <div style="padding:4px; text-align:center">
              <img src="img/android.png" class="iconImage" width="60" height="60" />
           		<img src="img/chrome.png" class="iconImage" width="60" height="60" />
           		<img src="img/firefox.png" class="iconImage" width="60" height="60" />
           		<img src="img/ie.png" class="iconImage" width="60" height="60" />
           		<img src="img/opera.png" class="iconImage" width="60" height="60" />
            </div>
        </div>
    </div>
  
  <div id="footer">
    <div id="tnc">
    	Ovo je zabavni pretplatnički servis. Usluga košta 360 DIN na nedeljnom nivou za MTS ,Telenor, VIP i  Globaltel korisnike (plus cena osnovnog SMS-a: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel besplatno) (Sve navedene cene sa uracunatim PDV-om). Pretplata
na uslugu će biti automatski obnavljana sve dok ne pošaljete STOPYES na 5060 po ceni od 3.60 DIN za MTS i Telenor
korisnike i 3,48 DIN za VIP korisnike. Registracijom na ovu uslugu potvrđujete da ste saglasni sa svim važećim odredbama
i uslovima -a. Svi video snimci i slike na ovoj stranici su u svrhu zabave. Za dodatne informacije u vezi naplate pozovite
 na 011/888888. Ovu uslugu nudi vam Mobitech Solutions Unit No.6, 2nd Floor, Block B, Spg82, Pg Haji Tajuddin
Complex, Kg Delima Satu Serusop, Jalan Muara BB4713
    </div>
    <!--<div id="btmnav">
        <a href="tnc.html" target="blank">Opšti uslovi korišćenja</a> |
        <a href="help.html" target="blank">Pomoć</a> | 
        <a href="support.html" target="blank">Podržavani telefoni</a> |
        <a href="contact.html" target="blank">Kontakt</a>
    </div>-->
  </div>
    
</div>

<div class="footer">&nbsp;</div>
</form>
</body>
</html>