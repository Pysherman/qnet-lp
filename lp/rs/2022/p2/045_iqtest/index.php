
<?php 

$shortcode = "5060";
$landing_page = "045_iqtest";

require_once '../../../resources/rs/nth/header/rs_header_redis.php';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-3"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-147360504-3');
        </script>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>IQ Test</title>
		<meta name="description" content="IQ Test" />
		<meta name="viewport" content="width=device-width, initial scale=1.0" />

		<link href="css/style.css" rel="stylesheet" type="text/css" /> 

		<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../../../resources/rs/nth/handler/handler.js"></script> 

	</head>
	<body>

	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="" >
	<input type="hidden" id="shortcode" name="shortcode"  value="5060" >
	<input type="hidden" id="keyword" name="keyword"  value="<?php echo $keyword;?>" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="rs" >
	<input type="hidden" id="landing_page" name="landing_page"  value="045_iqtest" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="nth" >
	<input type="hidden" id="carryover" name="carryover" value="" >
	
	<div class="container">

		<header class="logo-country">
			<div class="logo">
				<img src="assets/IQ_logo.jpg" alt="IQ_logo">
			</div>
			<div class="country">
				<img src="assets/flag.jpg" alt="flag">
			</div>
		</header>

		<section class="numbers">
			<ul>
				<li>1</li>
				<li>2</li>
				<li>3</li>
				<li>4</li>
				<li>5</li>
				<li>6</li>
				<li>7</li>
				<li>8</li>
				<li>9</li>
			</ul>
		</section>

		<main class="wordings">
			<h3>Последњи корак</h3>
			<p>Пошаљите смс поруку која садржи</p>
			<span><?php echo strtoupper($keyword); ?></span>
			<p>на број</p>
			<span>5060</span>
			<h4>и сазнајте колики је ваш IQ</h2>
			<button class="click-btn" id="btn-continue2">ПОШАЉИТЕ СМС</button>
		</main><br>
		<p class="footer">Ovo je zabavna pretplatnička usluga. Usluga košta 360 DIN nedeljno za MTS, Telenor, VIP i Globaltel korisnike (plus cenu osnovnih SMS poruka: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel besplatno) Sa PDV-om). Pretplata na uslugu automatski se ažurira dok ne pošaljete STOP<?php echo strtoupper($keyword); ?> na 5060 za Telekom i STOP <?php echo strtoupper($keyword); ?> na 5060 za Telenor, VIP i Globaltel po ceni od 3,60 DIN za MTS i Telenor korisnike i 3,48 DIN za VIP korisnike. Prijavom na ovu uslugu potvrdite da ste u skladu sa svim važećim uslovima i odredbama -a. Svi video snimci i slike na ovoj stranici su za zabavu. Za više informacija o obračunu pozovite +381113216815. Ova usluga nudi Jedinica Mobitech Solutions br.6, 2. sprat, Blok B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713. Хелпдеск <br>Емаил:mobitech.rs@silverlines.info </p>
	</div>
	
	<!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>-->
	</body>
</html>
