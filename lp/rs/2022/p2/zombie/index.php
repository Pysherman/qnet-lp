<?php require_once '../../../resources/rs/nth/header/header.php' ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-147360504-3');
    </script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Zombie</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/kzstyle.css" media="screen"  />

<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="../../../resources/rs/nth/handler/handler.js"></script> 

<style type="text/css">
	@media only screen and (max-width: 480px) {
		.iconLogo {
			height: auto !important;
			max-width: 280px !important;
			width: 100% !important;
		}
		
		.wrap {
			width: 300px;
		}
		
		.iconImage{height:auto !important;max-width:50px !important;width: 100% !important;
	}
}
</style></head>

<body>

<input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>" >
<input type="hidden" id="operator_code" name="operator_code"  value="TELENOR" >
<input type="hidden" id="shortcode" name="shortcode"  value="5060" >
<input type="hidden" id="keyword" name="keyword"  value="OK" >
<input type="hidden" id="user_ip" name="user_ip"  value="" >
<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
<input type="hidden" id="country_code" name="country_code"  value="rs" >
<input type="hidden" id="landing_page" name="landing_page"  value="zombie" >
<input type="hidden" id="gateway_code" name="gateway_code"  value="NTH" >
<input type="hidden" id="carryover" name="carryover" value="" >
<input type="hidden" id="endpage" name="endpage"  value="<?php echo "http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/rs/p2/zombie/" ?>" >

<div class="content">
    <div class="wrap flex">
        <div class="item flex-item" style="text-align:center; width: 285px;">
        	<img src="assets/image.png" class="iconLogo" width="264" height="90" />
        </div>
    	<div class="item flex-item" style="text-align:center; width: 285px;">
       		<div style="margin-bottom: 10px; font-weight: bold">Zavrsni Korak:</div>
      		  <a id="btn-continue2" href="#" ><img src="assets/button.gif" width="200" height="87" /></a>
       		  <div>ili pošalji</div> 
        	<span class="srvcs pulse">OK</span> 
        	<strong>&nbsp; na &nbsp; </strong>
			<span class="srvcs pulse">5060</span>
        </div>
        <div class="wrap flex">
    	<div class="item flex-item">
        	<div style="padding:4px; text-align:center">Neograničeni pristup sadrzajima za mobilne telefone</div>
            <div style="padding:4px; text-align:center">
           		<img src="assets/ark-icon.png" class="iconImage" width="90" height="80" />
           		<img src="assets/battleboom-icon.png" class="iconImage" width="90" height="80" />
           		<img src="assets/creativedest-icon.png" class="iconImage" width="90" height="80" />
           		<img src="assets/darkrise-png.png" class="iconImage" width="90" height="80" />
           		<img src="assets/dreamleag-icon.png" class="iconImage" width="90" height="80" />
            </div>
      </div>
    	<div class="item flex-item">
        	<div style="padding:4px; text-align:center">Kompatibilno sa</div>
            <div style="padding:4px; text-align:center">
           		<img src="assets/android-icon.png" class="iconImage" width="90" height="80" />
           		<img src="assets/crome-icon.png" class="iconImage" width="90" height="80" />
           		<img src="assets/firefox-icon.png" class="iconImage" width="90" height="80" />
           		<img src="assets/ie-icon.png" class="iconImage" width="90" height="80" />
           		<img src="assets/opera-icon.png" class="iconImage" width="90" height="80" />
            </div>
        </div>
    </div>
    	<div class="item flex-item st1" style="padding: 10px; text-align: center">Ovo je zabavna pretplatnička usluga. Usluga košta 360 DIN nedeljno za MTS, Telenor, VIP i Globaltel korisnike (plus cenu osnovnih SMS poruka: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel besplatno) Sa PDV-om). Pretplata na uslugu automatski se ažurira dok ne pošaljete STOPOK na 5060 za Telekom i STOP OK na 5060 za Telenor, VIP i Globaltel po ceni od 3,60 DIN za MTS i Telenor korisnike i 3,48 DIN za VIP korisnike. Prijavom na ovu uslugu potvrdite da ste u skladu sa svim važećim uslovima i odredbama -a. Svi video snimci i slike na ovoj stranici su za zabavu. Za više informacija o obračunu pozovite +381113216815. Ova usluga nudi Jedinica Mobitech Solutions br.6, 2. sprat, Blok B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713. Хелпдеск <br> Емаил:mobitech.rs@silverlines.info</div>
    </div>
    <div class="space">&nbsp;</div>
    
</div>
</body>
</html>