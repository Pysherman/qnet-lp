
<?php 

$shortcode = "5060";
$landing_page = "rs_camp012";

require_once '../../../resources/rs/nth/header/rs_header_redis.php';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-3"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-147360504-3');
        </script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Apex</title>
		<meta name="viewport" content="width=device-width, initial scale=1.0" />

<!--<link href="https://fonts.googleapis.com/css?family=Open+Sans|Press+Start+2P" rel="stylesheet">-->
<!--<link rel="stylesheet" type="text/css" href="vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css" />-->
<!--<link href="style/css/serbia.css" rel="stylesheet" type="text/css" />-->

		<link href="style/css/styles.css" rel="stylesheet" type="text/css" /> 
		<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../../../resources/rs/nth/handler/handler.js"></script> 

	</head>
	<body>

	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="" >
	<input type="hidden" id="shortcode" name="shortcode"  value="5060" >
	<input type="hidden" id="keyword" name="keyword"  value="<?php echo $keyword;?>" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="rs" >
	<input type="hidden" id="landing_page" name="landing_page"  value="rs_camp012" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="nth" >
	<input type="hidden" id="carryover" name="carryover" value="" >

		<div class="content-wrapper">
			<div class="grid-container">	
				<div class="image-heading">
					<img src="img/header.png" alt="NAJTOPLIJE MOBILNE IGRE" />
				</div>
				<div class="final-step">
					<div class="step-heading">
						<span>Čestitamo! Zavrsni Korak:</span>
					</div>
					<div class="step-button">
						<button class="click-btn" id="btn-continue2"><img src="img/button.png" alt="Preuzmi Sada"></button>
					</div>
					<div class="step-keynum">
						<span>ili SMS</span>
						<p><span><?php echo strtoupper($keyword); ?></span> na <span>5060</span></p>
					</div>
				</div>
			</div>
		</div>

		<div class="otherGames">
			<p>Neograničeni pristup sadrzajima za mobilne telefone</p>
			<ul>
				<li><img src="img/cod.png" alt="Android" /></li>
				<li><img src="img/fortnite.png" alt="Opera" /></li>
				<li><img src="img/moderncombat.png" alt="Internet Explorer" /></li>
				<li><img src="img/pubg.png" alt="Firefox" /></li>
				<li><img src="img/ros.png" alt="Chrome" /></li>
			</ul>
		</div>

		<div class="compatibility">
			<p>Kompatibilno sa</p>
			<ul>
				<li><img src="img/android.png" alt="Android" /></li>
				<li><img src="img/opera.png" alt="Opera" /></li>
				<li><img src="img/ie.png" alt="Internet Explorer" /></li>
				<li><img src="img/firefox.png" alt="Firefox" /></li>
				<li><img src="img/chrome.png" alt="Chrome" /></li>
			</ul>
		</div>
		<div class="words">
			<p>Ovo je zabavna pretplatnička usluga. Usluga košta 360 DIN nedeljno za MTS, Telenor, VIP i Globaltel korisnike (plus cenu osnovnih SMS poruka: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel besplatno) Sa PDV-om). Pretplata na uslugu automatski se ažurira dok ne pošaljete STOPFUN na 5060 za Telekom i STOP FUN na 5060 za Telenor, VIP i Globaltel po ceni od 3,60 DIN za MTS i Telenor korisnike i 3,48 DIN za VIP korisnike. Prijavom na ovu uslugu potvrdite da ste u skladu sa svim važećim uslovima i odredbama -a. Svi video snimci i slike na ovoj stranici su za zabavu. Za više informacija o obračunu pozovite +381113216815. Ova usluga nudi Jedinica Mobitech Solutions br.6, 2. sprat, Blok B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713. Хелпдеск <br>Емаил:mobitech.rs@silverlines.info </p>
		</div>
	</div>
		
	</body>
	<!--<script type="text/javascript" src="style/js/script.js"></script>
	<script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>-->
</html>
