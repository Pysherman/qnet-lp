var tnc_link=document.getElementById("tncLink");
var tnc_section=document.getElementById("tncSection");
var contact_link=document.getElementById("contactLink");
var contact_section=document.getElementById("contactSection");
var help_link=document.getElementById("helpLink");
var help_section=document.getElementById("helpSection");
var support_link=document.getElementById("supportLink");
var support_section=document.getElementById("supportSection");


tnc_link.addEventListener('click', function(e){
	e.preventDefault();
	if (tnc_section.style.display === "none"){
		tnc_section.style.display = "block";
		help_section.style.display = "none";
		contact_section.style.display = "none";
		support_section.style.display = "none";
	}else{
		tnc_section.style.display = "none";
	}
});

contact_link.addEventListener('click', function(e){
	e.preventDefault();
	if (contact_section.style.display === "none"){
		contact_section.style.display = "block";
		help_section.style.display = "none";
		tnc_section.style.display = "none";
		support_section.style.display = "none";
	}else{
		contact_section.style.display = "none";
	}
});

help_link.addEventListener('click', function(e){
	e.preventDefault();
	if (help_section.style.display === "none"){
		help_section.style.display = "block";
		contact_section.style.display = "none";
		tnc_section.style.display = "none";
		support_section.style.display = "none";
	}else{
		help_section.style.display = "none";
	}
});

support_link.addEventListener('click', function(e){
	e.preventDefault();
	if (support_section.style.display === "none"){
		support_section.style.display = "block";
		contact_section.style.display = "none";
		tnc_section.style.display = "none";
		help_section.style.display = "none";
	}else{
		support_section.style.display = "none";
	}
});