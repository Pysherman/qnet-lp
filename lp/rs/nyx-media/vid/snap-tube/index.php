<!--?php require_once '../../resources/rs/teracomm/header/header.php' ?-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SnapTube</title>
    <meta name="description" content="สาว FB คลิป" />
    <meta name="keywords" content="สาว FB คลิป, คลิป FB, ผู้หญิงเซ็กซี่" />
    <meta name="viewport" content="width=device-width, initial scale=1.0" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,900" rel="stylesheet">
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
</head>

<body>
    <input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
    <input type="hidden" id="operator_code" name="operator_code" value="">
    <input type="hidden" id="shortcode" name="shortcode" value="1553">
    <input type="hidden" id="keyword" name="keyword" value= FB">
    <input type="hidden" id="user_ip" name="user_ip" value="">
    <input type="hidden" id="affiliate_code" name="affiliate_code" value="">
    <input type="hidden" id="country_code" name="country_code" value="rs">
    <input type="hidden" id="landing_page" name="landing_page" value="SnapTube">
    <input type="hidden" id="gateway_code" name="gateway_code" value="Teracomm">
    <input type="hidden" id="carryover" name="carryover" value="">

    <div class="lp-container">
        <section class="section">
            <div class="lp-main">
                <img src="assets/mobile.png" alt="Snap Tube Mobile">
                <div class="lp-main-content">
                    <div class="lp-text">Čestitamo! Zavrsni Korak:</div>
                    <div class="button">
                        <a href="sms:1553?body=FB" id="btn-continue"><img src="assets/Group 1.png" alt=""></a>
                    </div>
                    <div class="lp-text text-two">ili SMS</div>
                    <div class="animated">
                        <span style="vertical-align:middle" FB</span>
                        <small style="vertical-align:middle; font-size: 24px;">na</small>
                        <span style="vertical-align:middle">1553</span>
                    </div>
                    <div class="lp-text">neograničeni pristup sadrzajima za mobilne telefone</div>
                    <div class="icons">
                        <img src="assets/1.png" alt="">
                        <img src="assets/2.png" alt="">
                        <img src="assets/3.png" alt="">
                        <img src="assets/4.png" alt="">
                        <img src="assets/5.png" alt="">
                    </div>
                    <div class="lp-text">Kompatibilno sa</div>
                    <div class="icons">
                        <img src="assets/android-icon.png" alt="">
                        <img src="assets/crome-icon.png" alt="">
                        <img src="assets/firefox-icon.png" alt="">
                        <img src="assets/ie-icon.png" alt="">
                        <img src="assets/opera-icon.png" alt="">
                    </div>
                </div>

                <div class="lp-footer">
                    <p>Ovo je zabavni pretplatnički servis. Usluga košta 360 DIN na nedeljnom nivou za MTS ,Telenor i VIP
                        korisnike (plus cena osnovnog SMS-a: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN) (Sve navedene
                        cene sa uracunatim PDV-om). Pretplata na uslugu će biti automatski obnavljana sve dok ne pošaljete
                        STOP FB na 1553 po ceni od 3.60 DIN za MTS i Telenor korisnike i 3,48 DIN za VIP korisnike. Registracijom
                        na ovu uslugu potvrđujete da ste saglasni sa svim važećim odredbama i uslovima -a. Svi FBeo snimci
                        i slike na ovoj stranici su u svrhu zabave. Za dodatne informacije u vezi naplate pozovite. Ovu uslugu nudi vam Nyx Media Networks br.58A, Spg 60, Jalan Dato Ratna Kg Kiarong, Brunei-Muara, BE1318, Brunei Darussalam. Хелпдеск Емаил:nikki@nyxmedianetworks.net </p>
                    <ul>
                        <li><a target="_blank" href="pages/tnc.htm">Opšti uslovi korišćenja |</a></li>
                        <li><a target="_blank" href="pages/help.htm">Pomoć |</a></li>
                        <li><a target="_blank" href="pages/support.htm">Podržavani telefoni |</a></li>
                        <li><a target="_blank" href="pages/contact.htm">Kontakt</a></li>
                    </ul>
                </div>
            </div>
        </section>
    </div>

</body>

</html>