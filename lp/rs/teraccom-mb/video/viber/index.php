<!--?php require_once '../../resources/rs/teracomm/header/header.php' ?-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Viber</title>
    <meta name="description" content="สาว VDO คลิป" />
    <meta name="keywords" content="สาว VDO คลิป, คลิป VDO, ผู้หญิงเซ็กซี่" />
    <meta name="viewport" content="width=device-width, initial scale=1.0" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,700" rel="stylesheet">
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
</head>

<body>
    <input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
    <input type="hidden" id="operator_code" name="operator_code" value="">
    <input type="hidden" id="shortcode" name="shortcode" value="1311">
    <input type="hidden" id="keyword" name="keyword" value="video">
    <input type="hidden" id="user_ip" name="user_ip" value="">
    <input type="hidden" id="affiliate_code" name="affiliate_code" value="">
    <input type="hidden" id="country_code" name="country_code" value="rs">
    <input type="hidden" id="landing_page" name="landing_page" value="Viber">
    <input type="hidden" id="gateway_code" name="gateway_code" value="Teracomm">
    <input type="hidden" id="carryover" name="carryover" value="">
    
    <div class="container">
        <main class="main">
            <header class="header"></header>
            <section class="section">
                <div class="banner"><img src="images/viber.png" alt=""></div>
                <div class="primary">
                    <span>Čestitamo</span>
                </div>
                <div class="secondary">
                    <span>Zavrsni Korak:</span>
                </div>
                <div class="button">
                    <a href="sms:1311?body=YES"  class="btn btn-green" id="btn-continue">DOBITI VIBER</a>
                </div>
                <div class="tertiary">
                    <span>ili SMS</span>
                </div>
                <div class="bounce bounce-animated">
                    <span style="vertical-align:middle;">VIDEO</span>
                    <small style="vertical-align:middle;">na</small>
                    <span style="vertical-align:middle;">1311</span>
                </div>
                <div class="forth">
                    <span>Neograničeni pristup sadrzajima za mobilne telefone</span>
                </div>
                <div class="icons">
                    <img src="images/icons/1.png" alt="">
                    <img src="images/icons/2.png" alt="">
                    <img src="images/icons/3.png" alt="">
                    <img src="images/icons/4.png" alt="">
                    <img src="images/icons/5.png" alt="">
                </div>
                <div class="forth">
                    <span>Kompatibilno sa</span>
                </div>
                <div class="icons">
                    <img src="images/icons/android-icon.png" alt="">
                    <img src="images/icons/chrome-icon.png" alt="">
                    <img src="images/icons/firefox-icon.png" alt="">
                    <img src="images/icons/ie-icon.png" alt="">
                    <img src="images/icons/opera-icon.png" alt="">
                </div>
                <div class="pre-footer">
                    <p>Ovo je zabavni pretplatnički servis. Usluga košta 360 DIN na nedeljnom nivou za MTS ,Telenor i VIP
                        korisnike (plus cena osnovnog SMS-a: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN) (Sve navedene
                        cene sa uracunatim PDV-om). Pretplata na uslugu će biti automatski obnavljana sve dok ne pošaljete
                        STOPVIDEO na 1311 po ceni od 3.60 DIN za MTS i Telenor korisnike i 3,48 DIN za VIP korisnike. Registracijom
                        na ovu uslugu potvrđujete da ste saglasni sa svim važećim odredbama i uslovima -a. Svi igre i/ili
                        aplikacije , video snimci i slike na ovoj stranici su u svrhu zabave. Za dodatne informacije u vezi
                        naplate pozovite EDS na 011/7702342. Ovu uslugu nudi vam Mobitech Solutions Unit No.6, 2nd Floor,
                        Block B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713
                    </p>
                    <ul>
                        <li><a target="_blank" href="pages/tnc.htm">Opšti uslovi korišćenja |</a></li>
                        <li><a target="_blank" href="pages/help.htm">Pomoć |</a></li>
                        <li><a target="_blank" href="pages/support.htm">Podržavani telefoni |</a></li>
                        <li><a target="_blank" href="pages/contact.htm">Kontakt</a></li>
                    </ul>
                </div>
            </section>
        </main>
        <footer class="footer"></footer>
    </div>
</body>

</html>