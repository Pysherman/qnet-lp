<!--?php require_once '../../resources/rs/teracomm/header/header.php' ?-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>DragonCraft</title>
    <meta name="description" content="สาว VDO คลิป" />
    <meta name="keywords" content="สาว VDO คลิป, คลิป VDO, ผู้หญิงเซ็กซี่" />
    <meta name="viewport" content="width=device-width, initial scale=1.0" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,900|Roboto+Slab" rel="stylesheet">
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
</head>

<body>
    <input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
    <input type="hidden" id="operator_code" name="operator_code" value="">
    <input type="hidden" id="shortcode" name="shortcode" value="1311">
    <input type="hidden" id="keyword" name="keyword" value="YES">
    <input type="hidden" id="user_ip" name="user_ip" value="">
    <input type="hidden" id="affiliate_code" name="affiliate_code" value="">
    <input type="hidden" id="country_code" name="country_code" value="rs">
    <input type="hidden" id="landing_page" name="landing_page" value="DragonCraft">
    <input type="hidden" id="gateway_code" name="gateway_code" value="Teracomm">
    <input type="hidden" id="carryover" name="carryover" value="">

    <div class="container">
        <div class="header"><img src="images/header.png" alt=""></div>
    </div>
    <div class="container-main">
        <div class="items left">
            <div class="primary-text">
                <span>Zavrsni Korak:</span>
            </div>
            <div class="banner">
                <a href="sms:1311?body=YES" id="btn-continue"><img src="images/banner.png" alt=""></a>
            </div>
            <div class="primary-text second">
                <span>ili pošalji</span>
            </div>
            <div class="bounce">
                <span style="vertical-align:middle;">YES</span>
                <small style="vertical-align:middle;">na</small>
                <span style="vertical-align:middle;">1311</span>
            </div>
        </div>
        <div class="items right">
            <div class="secondary">Neograničeni pristup sadrzajima za mobilne telefone</div>
            <div class="icon-1">
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
            </div>
            <div class="icon-1">
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
            </div>
            <div class="secondary sec-2"> Kompatibilno sa </div>
            <div class="icon-1">
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
                <div class="images"><img src="images/icons/icon1.png" alt=""></div>
            </div>
        </div>
    </div>
    <div class="footer">
        <p>Ovo je zabavni pretplatnički servis. Usluga košta 360 DIN na nedeljnom nivou za MTS ,Telenor i VIP korisnike (plus
            cena osnovnog SMS-a: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN) (Sve navedene cene sa uracunatim PDV-om).
            Pretplata na uslugu će biti automatski obnavljana sve dok ne pošaljete STOPYES na 1311 po ceni od 3.60 DIN za
            MTS i Telenor korisnike i 3,48 DIN za VIP korisnike. Registracijom na ovu uslugu potvrđujete da ste saglasni
            sa svim važećim odredbama i uslovima -a. Svi video snimci i slike na ovoj stranici su u svrhu zabave. Za dodatne
            informacije u vezi naplate pozovite EDS na 011/7702342. Ovu uslugu nudi vam Mobitech Solutions Unit No.6, 2nd
            Floor, Block B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713</p>
        <ul>
            <li><a target="_blank" href="pages/tnc.htm">Opšti uslovi korišćenja |</a></li>
            <li><a target="_blank" href="pages/help.htm">Pomoć |</a></li>
            <li><a target="_blank" href="pages/support.htm">Podržavani telefoni |</a></li>
            <li><a target="_blank" href="pages/contact.htm">Kontakt</a></li>
        </ul>
    </div>
</body>

</html>