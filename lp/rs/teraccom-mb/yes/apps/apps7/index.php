<?php require_once '../../resources/rs/teracomm/header/header.php' ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>батерия</title>
		<meta name="description" content="สาว VDO คลิป" />
		<meta name="keywords" content="สาว VDO คลิป, คลิป VDO, ผู้หญิงเซ็กซี่" />
		<meta name="viewport" content="width=device-width, initial scale=1.0" />

<!--<link href="https://fonts.googleapis.com/css?family=Open+Sans|Press+Start+2P" rel="stylesheet">-->
<!--<link rel="stylesheet" type="text/css" href="vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css" />-->
<!--<link href="style/css/serbia.css" rel="stylesheet" type="text/css" />-->

		<link href="style/css/styles.css" rel="stylesheet" type="text/css" /> 
		<!--<link rel="stylesheet" type="text/css" href="vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css" />-->
		<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>

	</head>
	<body>

	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="" >
	<input type="hidden" id="shortcode" name="shortcode"  value="1311" >
	<input type="hidden" id="keyword" name="keyword"  value="yes" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="rs" >
	<input type="hidden" id="landing_page" name="landing_page"  value="apps7" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="Teracomm" >
	<input type="hidden" id="carryover" name="carryover" value="" >

<!--Preloader-->
	<div id="overlay">
		<h3>Povećavajući Vašu Bateriju....</h3>
		<div class="spinner"></div> 
		<div class="lightning"></div>
	</div>

	<div class="site-container">
		<div class="upper-wrapper">
			<div class="image-container">
				<div class="image-wrapper">
					<img src="img/phone2.gif" alt="" />
				</div>
			</div>
			<div class="keyword-container">
				<h3>Baterija Sačuvati</h3>
				<h4>Povecati životni vek baterije</h4>
				<p>Čestitamo</p>
				<p>Zavrsni Korak:</p>
				<button id="btn-continue"><img src="img/battery2-button.gif" alt="" /></button>
				<p>ili SMS</p>
				<p><span>YES</span> na <span>1311</span></p>
			</div>
		</div>
		<div class="lower-wrapper">
			<p>Ovo je zabavni pretplatnički servis. Usluga košta 360 DIN na nedeljnom nivou za MTS ,Telenor i VIP korisnike (plus cena osnovnog SMS-a: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN) (Sve navedene cene sa uracunatim PDV-om). Pretplata na uslugu će biti automatski obnavljana sve dok ne pošaljete STOPYES na 1311 po ceni od 3.60 DIN za MTS i Telenor korisnike i 3,48 DIN za VIP korisnike. Registracijom na ovu uslugu potvrđujete da ste saglasni sa svim važećim odredbama i uslovima -a. Svi igre i/ili aplikacije , video snimci i slike na ovoj stranici su u svrhu zabave. Za dodatne informacije u vezi naplate pozovite EDS na 011/7702342. Ovu uslugu nudi vam Mobitech Solutions Unit No.6, 2nd Floor, Block B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713</p>
		</div>
		<div class="footer">
			<ul>
				<li><a href="tnc.html" target="blank">Opšti uslovi korišćenja</a></li>
				<li><a href="help.html" target="blank">Pomoć</a></li>
				<li><a href="support.html" target="blank">Podržavani telefoni</a></li>
				<li><a href="contact.html" target="blank">Kontakt</a></li>
			</ul>		
		</div>
	</div>
	</body>
	<!--<script type="text/javascript" src="style/js/script.js"></script>-->
	
	
	<script type="text/javascript">

		setTimeout(function hidediv(){$("#overlay").addClass('hideD');}, 2000);

	</script>
</html>
