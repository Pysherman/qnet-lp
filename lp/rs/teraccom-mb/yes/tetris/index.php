<!--?php require_once '../../resources/rs/teracomm/header/header.php' ?-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Tetris</title>
    <meta name="description" content="สาว VDO คลิป" />
    <meta name="keywords" content="สาว VDO คลิป, คลิป VDO, ผู้หญิงเซ็กซี่" />
    <meta name="viewport" content="width=device-width, initial scale=1.0" />
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link href="https://fonts.googleapis.com/css?family=Baloo+Paaji|Open+Sans" rel="stylesheet ">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
</head>

<body>
    <input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
    <input type="hidden" id="operator_code" name="operator_code" value="">
    <input type="hidden" id="shortcode" name="shortcode" value="5060">
    <input type="hidden" id="keyword" name="keyword" value="YES">
    <input type="hidden" id="user_ip" name="user_ip" value="">
    <input type="hidden" id="affiliate_code" name="affiliate_code" value="">
    <input type="hidden" id="country_code" name="country_code" value="rs">
    <input type="hidden" id="landing_page" name="landing_page" value="Tetris">
    <input type="hidden" id="gateway_code" name="gateway_code" value="nth">
    <input type="hidden" id="carryover" name="carryover" value="">
<body>
<div class="container">
    <main class="main">
        <div class="top">
            <img src="assets/header.png" alt="">
        </div>
        <div class="bottom">
            <div class="bottom-main">
                <div class="bottom-main_item"><h1>Zavrsni Korak:</h1></div>
                <div class="bottom-main_item"><a href="sms:5060?body=YES" id="btn-continue"><img class='btn' src="assets/button.gif" alt=""></a></div>
                <div class="bottom-main_item"><h2>ili pošalji </h2></div>
                <div class="bottom-main_item animated infinite fadeIn delay-1s"><span>yes</span><small>na</small><span>5060</span></div>
                <div class="bottom-main_item"><h2>Neograničeni pristup sadrzajima za mobilne telefone</h2></div>
                <div class="bottom-main_item">
                    <img src="assets/1.png" alt="">
                    <img src="assets/2.png" alt="">
                    <img src="assets/3.png" alt="">
                    <img src="assets/4.png" alt="">
                    <img src="assets/6.png" alt="">
                </div>
                <div class="bottom-main_item">
                    <img src="assets/7.png" alt="">
                    <img src="assets/8.png" alt="">
                    <img src="assets/9.png" alt="">
                    <img src="assets/11.png" alt="">
                    <img src="assets/12.png" alt="">
                </div>
                <div class="bottom-main_item"><h2>Kompatibilno sa</h2></div>
                <div class="bottom-main_item">
                    <img src="assets/android.png" alt="">
                    <img src="assets/chrome.png" alt="">
                    <img src="assets/firefox.png" alt="">
                    <img src="assets/ie.png" alt="">
                    <img src="assets/opera.png" alt="">
                </div>
                <div class="bottom-main_item">
                    <p>    	Ovo je zabavna pretplatnička usluga. Usluga košta 360 DIN nedeljno za MTS, Telenor, VIP i Globaltel korisnike 
                        (plus cenu osnovnih SMS poruka: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN, Globaltel besplatno) Sa PDV-om). 
                        Pretplata na uslugu automatski se ažurira dok ne pošaljete STOP YES na 5060 za Telekom i STOP YES na 5060 za Telenor, 
                        VIP i Globaltel po ceni od 3,60 DIN za MTS i Telenor korisnike i 3,48 DIN za VIP korisnike. 
                        Prijavom na ovu uslugu potvrdite da ste u skladu sa svim važećim uslovima i odredbama -a. 
                        Svi video snimci i slike na ovoj stranici su za zabavu. Za više informacija o obračunu pozovite +381113216815. 
                        Ova usluga nudi Jedinica Mobitech Solutions br.6, 2. sprat, Blok B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713.
                        Хелпдеск Емаил:mobitech.rs@silverlines.info
                    </p>
                </div>
            </div>
        </div>
    </main>    
</div>

</body>
</html>