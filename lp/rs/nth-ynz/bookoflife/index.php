<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>The Book of life</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/kzstyle.css" media="screen"  />
<style type="text/css">
	@media only screen and (max-width: 480px) {
		.iconImage {
			height: auto !important;
			max-width: 80% !important;
			width: 100% !important;
		}
	}
</style>
</head>

<body>
<div class="content">
    <div style="text-align:center"><img src="assets/logo.png" class="iconImage" /></div>
    
    <div class="wrap flex" style="text-align:center; background: rgba(243, 240, 230, 0.3); color: #000;">     
      <div style="text-align:center; padding:5px; font-weight: bold">Zavrsni Korak:</div>
   	  <div class="item flex-item" style="text-align:center"><a class="button" href="#">PREUZMI SADA</a></div>
    	<div class="item flex-item st1" style="text-align:center; font-weight: bold">
            <div>ili posalji<br /><span class="srvcs pulse">CON</span><br /> na <br /><span class="srvcs pulse">4010</span></div>
      </div>      
    </div>
    
  <div id="tnc">
    	Ovo je zabavna pretplatnička usluga. Usluga košta 360 RSD nedeljno za MTS, Telenor, VIP i Globaltel
korisnike (plus cena osnovnih SMS poruka: MTS 3.60 RSD, Telenor 3.60 RSD, Vip 3.48 RSD, Globaltel
besplatno) sa PDV-om. Pretplata na uslugu automatski se ažurira dok ne pošaljete STOPCON na 4010
za Telekom i STOP CON na 4010 za Telenor, Globaltel i VIP. Prijavom na ovu uslugu potvrđujete da ste u
skladu sa svim važećim uslovima i odredbama. Sve igrice snimci i slike na ovoj stranici su za zabavu. Za
više informacija o obračunu pozovite +381113216815. Ovu uslugu nudi Jedinica Yunimaz Enterprise
No.28B, @nd Floor, Serusop Complex, Jalan Muara, Brunei/Muara. Helpdesk
Email:yunimaz.rs@silverlines.info
    </div>
    
</div>
</body>
</html>