<!--?php require_once '../../resources/rs/teracomm/header/header.php' ?-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Roll the Ball</title>
    <meta name="description" content="สาว VDO คลิป" />
    <meta name="keywords" content="สาว VDO คลิป, คลิป VDO, ผู้หญิงเซ็กซี่" />
    <meta name="viewport" content="width=device-width, initial scale=1.0" />
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link href="https://fonts.googleapis.com/css?family=Baloo+Paaji|Open+Sans" rel="stylesheet ">
    <link rel="stylesheet" href="animate.min.css">
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
</head>

<body>
    <input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
    <input type="hidden" id="operator_code" name="operator_code" value="">
    <input type="hidden" id="shortcode" name="shortcode" value="4010">
    <input type="hidden" id="keyword" name="keyword" value="MOB">
    <input type="hidden" id="user_ip" name="user_ip" value="">
    <input type="hidden" id="affiliate_code" name="affiliate_code" value="">
    <input type="hidden" id="country_code" name="country_code" value="rs">
    <input type="hidden" id="landing_page" name="landing_page" value="Roll the Ball">
    <input type="hidden" id="gateway_code" name="gateway_code" value="nth">
    <input type="hidden" id="carryover" name="carryover" value="">
<body>
<div class="container">
    <main class="main">
        <div class="top">
            <img src="assets/header.png" alt="">
        </div>
        <div class="bottom">
            <div class="bottom-main">
                <div class="bottom-main_item"><h1>Zavrsni Korak:</h1></div>
                <div class="bottom-main_item"><a class="button" href="sms:4010?body=MOB" id="btn-continue">PREUZMI SADA</a></div>
                <div class="bottom-main_item"><h1>ili pošalji </h1></div>
                <div class="keyword animated  infinite flash delay-1s slow"><span>mob</span><small>na</small><span>4010</span></div>

                <div class="bottom-main_item">
                    <p>Ovo je zabavna pretplatnička usluga. Usluga košta 360 RSD nedeljno za MTS, Telenor, VIP i Globaltel
korisnike (plus cena osnovnih SMS poruka: MTS 3.60 RSD, Telenor 3.60 RSD, Vip 3.48 RSD, Globaltel
besplatno) sa PDV-om. Pretplata na uslugu automatski se ažurira dok ne pošaljete STOPCON na 4010
za Telekom i STOP CON na 4010 za Telenor, Globaltel i VIP. Prijavom na ovu uslugu potvrđujete da ste u
skladu sa svim važećim uslovima i odredbama. Sve igrice snimci i slike na ovoj stranici su za zabavu. Za
više informacija o obračunu pozovite +381113216815. Ovu uslugu nudi Jedinica Yunimaz Enterprise
No.28B, @nd Floor, Serusop Complex, Jalan Muara, Brunei/Muara. Helpdesk
Email:yunimaz.rs@silverlines.info
                    </p>
                </div>
            </div>
        </div>
    </main>    
</div>

</body>
</html>