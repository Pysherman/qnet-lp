<!--?php require_once '../../resources/rs/teracomm/header/header.php' ?-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/style.css">
	
	<script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
    <title>Black Ops 3</title>
</head>
<body>
	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="" >
	<input type="hidden" id="shortcode" name="shortcode"  value="4010" >
	<input type="hidden" id="keyword" name="keyword"  value="MOB" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="rs" >
	<input type="hidden" id="landing_page" name="landing_page"  value="Black Ops 3" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="Teracomm" >
	<input type="hidden" id="carryover" name="carryover" value="" >
	
    <div class="lp-wrapper">
        <section class="lp-details">
            <span>Zavrsni Korak:</span>
            <a href="sms:4010?body=MOB" id="btn-continue" class="button">PREUZMI SADA</a>
            <span>ili posalji</span>
            <h2>MOB</h2>
            <span>na</span>
            <h2>4010</h2>
        </section>
        <section class="lp-footer">
            <p>Ovo je zabavna pretplatnička usluga. Usluga košta 360 RSD nedeljno za MTS, Telenor, VIP i Globaltel korisnike (plus cena osnovnih SMS poruka: MTS 3.60 RSD, Telenor 3.60 RSD, 
                Vip 3.48 RSD, Globaltel besplatno) sa PDV-om. Pretplata na uslugu automatski se ažurira dok ne pošaljete STOPMOB na 4010 za Telekom i STOP MOB na 4010 za Telenor, Globaltel i 
                VIP. Prijavom na ovu uslugu potvrđujete da ste u skladu sa svim važećim uslovima i odredbama. Sve igrice snimci i slike na ovoj stranici su za zabavu. Za više informacija o 
                obračunu pozovite +381113216815. Ovu uslugu nudi Jedinica Yunimaz Enterprise No.28B, @nd Floor, Serusop Complex, Jalan Muara, Brunei/Muara. Helpdesk Email:yunimaz.rs@silverlines.info</p>
        </section>
    </div>
	
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
</body>
</html>