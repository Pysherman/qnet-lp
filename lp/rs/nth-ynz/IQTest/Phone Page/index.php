<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>да</title>
		<meta name="description" content="สาว VDO คลิป" />
		<meta name="keywords" content="สาว VDO คลิป, คลิป VDO, ผู้หญิงเซ็กซี่" />
		<meta name="viewport" content="width=device-width, initial scale=1.0" />

		<link href="css/style.css" rel="stylesheet" type="text/css" /> 

		<script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>

	</head>
	<body>

	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="" >
	<input type="hidden" id="shortcode" name="shortcode"  value="" >
	<input type="hidden" id="keyword" name="keyword"  value="" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="rs" >
	<input type="hidden" id="landing_page" name="landing_page"  value="IQTest" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="Teracomm" >
	<input type="hidden" id="carryover" name="carryover" value="" >
	
	<div class="container">

		<header class="logo-country">
			<div class="logo">
				<img src="assets/IQ_logo.jpg" alt="IQ_logo">
			</div>
			<div class="country">
				<img src="assets/flag.jpg" alt="flag">
			</div>
		</header>

		<section class="numbers">
			<ul>
				<li>1</li>
				<li>2</li>
				<li>3</li>
				<li>4</li>
				<li>5</li>
				<li>6</li>
				<li>7</li>
				<li>8</li>
				<li>9</li>
			</ul>
		</section>

		<main class="wordings">
			<h3>Твој IQ резултат је спреман!</h3>
			<p>Да бисте добили резултате, унесите број свог мобилног телефона овде:</p>
			<input type="text" value="06">
			<button>Добиј IQ резултате</button>
		</main>

	</div>
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=" crossorigin="anonymous"></script>
	</body>
</html>
