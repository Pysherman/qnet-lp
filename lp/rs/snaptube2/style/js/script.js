const getContainer = document.querySelector('.lp-container');
let numCir = 10;

setInterval(() => {
	let randomNumLeft = Math.floor((Math.random() * 150) + 1);
	let randomNumTop = Math.floor((Math.random() * 200) + 1);
	let randomNumSize = Math.floor((Math.random() * 100) + 50);

	let elemTest = document.createElement('div');

	elemTest.classList.add('circle');
	elemTest.style.width = ""+randomNumSize+"px";
	elemTest.style.height = ""+randomNumSize+"px";
	elemTest.style.top = ""+randomNumTop+"px";
	elemTest.style.left = ""+randomNumLeft+"px";
	getContainer.append(elemTest);
	setTimeout(() => {
		getContainer.removeChild(getContainer.lastElementChild);
	}, 2500);
}, 2000);

console.log('yes');