<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/style.css">
	
	<script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
    <title>Gamingroom</title>
</head>
<body>
	<input type="hidden" id="API_PATH" name="API_PATH"  value="<?php echo API_PATH;?>" >
	<input type="hidden" id="operator_code" name="operator_code"  value="" >
	<input type="hidden" id="shortcode" name="shortcode"  value="4010" >
	<input type="hidden" id="keyword" name="keyword"  value="con" >
	<input type="hidden" id="user_ip" name="user_ip"  value="" >
	<input type="hidden" id="affiliate_code" name="affiliate_code"  value="" >
	<input type="hidden" id="country_code" name="country_code"  value="bd" >
	<input type="hidden" id="landing_page" name="landing_page"  value="lifestyleguru" >
	<input type="hidden" id="gateway_code" name="gateway_code"  value="Teracomm" >
	<input type="hidden" id="carryover" name="carryover" value="" >

    <div class="mainContainer">
        <header class="logoHeader">
            <img src="assets/gamingroom_logo-01.png" alt="">
        </header>
        <main class="container">
            <section class="upperPart">
                <h4>Best HTML5 Games</h4>
                <p>Play & download unlimited games</p>
                <a href="#" class="subBtn">SUBSCRIBE</a>
            </section>
            <section class="lowerPart">
                <img src="assets/bg.png" alt="">
            </section>
        </main>
        <footer class="tcFooter">
            <a href="#" id="modal_open">Terms & Conditions</a>
        </footer> 
        <div class="tcModal" id="the_modal">
            <a href="" class="modalClose" id="modal_close">close</a>
            <h4>Terms & Conditions</h4>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquid cupiditate quo, dolores harum enim velit fugit voluptatum recusandae molestias excepturi aut, deleniti suscipit perspiciatis earum placeat. Facilis sit sequi ratione maiores, a ut quo, eum laboriosam soluta, aspernatur quae! Error rerum perspiciatis natus incidunt illum. Exercitationem eum minus vel, alias molestiae cumque quia voluptas, tempore repellat distinctio porro? Ad assumenda ab consequuntur culpa alias et natus minima nostrum nulla quod reprehenderit architecto esse blanditiis laborum dolores quam, soluta dolorem aspernatur rerum unde est vero sed corrupti quia. Rerum provident dolor quia porro, sed, neque reiciendis consequatur, voluptatem fugiat nobis hic minus distinctio harum. Eius assumenda optio, dolorem atque dolorum accusantium quas inventore asperiores natus eos consequatur repudiandae dolores aut? Neque aliquam velit dolores, unde vero id dolor fugiat voluptatem! Debitis culpa reprehenderit, unde, soluta perspiciatis veniam suscipit dolores sequi rem quod ex consectetur maxime autem voluptatum fugit cupiditate magni amet quos! Labore modi enim explicabo voluptate perferendis doloribus illum libero fugiat quas velit qui quam commodi optio voluptas dolor quasi, dignissimos vero at harum laborum deleniti eveniet nihil. Libero repellendus nam ullam quod exercitationem, architecto iusto neque minus vel facere ipsam beatae nostrum totam aliquam numquam ducimus et earum sequi!</p>
        </div>   
    </div>

    <script>
        var open = document.getElementById('modal_open');
        var close = document.getElementById('modal_close');
        var modal = document.getElementById('the_modal');

        open.onclick = function(){  
            modal.style.display = "block";
        };

        close.onclick = function(){  
            modal.style.display = "none";
        };
    </script>
	
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
</body>
</html>