<?php require_once '../../resources/cz/comgate/header/header.php' ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/style.css">

    <script type="text/javascript" src="http://www.adsqnt.com/resources/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="http://www.adsqnt.com/resources/cz/comgate/handler/handler.js"></script>
    <title>czech-sexy-2</title>
</head>

<body>
    <input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
    <input type="hidden" id="operator_code" name="operator_code" value="">
    <input type="hidden" id="shortcode" name="shortcode" value="90930">
    <input type="hidden" id="keyword" name="keyword" value="AP">
    <input type="hidden" id="user_ip" name="user_ip" value="">
    <input type="hidden" id="affiliate_code" name="affiliate_code" value="">
    <input type="hidden" id="country_code" name="country_code" value="cz">
    <input type="hidden" id="landing_page" name="landing_page" value="Sexysense">
    <input type="hidden" id="gateway_code" name="gateway_code" value="Comgate">
    <input type="hidden" id="carryover" name="carryover" value="">

    <main class="container">
        <header class="hd-img">
            <img src="assets/header.gif" alt="logo">
        </header>

        <section class="details" id="continue_div">
            <p>Chcete-li získat kód pro stažení,zadejte své mobilní číslo</p>
            <input type="text" name="msisdn" id="msisdn"  placeholder="0">
            <a href="#" class="btn" id="btn-continue">zaplatit pomoci sms</a>
            <p style="padding-top: 10px;">Předplatné 99 Kč / týden. Pro ukončení pošlete STOP AP na 90930</p>
        </section>

        <section class="details" id="thanks_div" style="display:none">
            <p>Odeslat ANO AP na 90930</p>
        </section>

        <footer class="footer">
            <p>Jedná se o předplacenou službu. Cena služby je 99 Kč za každou SMS. Frekvence zasílání je 1 SMS každých 7
                dní. Operátorské poplaty včetně dat GPRS nejsou v ceně zahrnuty. Pro více informací pošli HELP na 90930.
                Pro ukončení předplatného pošli STOP AP na 90930. Službu je možné využívat pouze v případě, že
                máte aktivní připojení k síti Internet. Zkontrolujte si prosím vaše nastavení. Služby se mohou zúčastnit
                pouze osoby starší 18 let. Tato pravidla se vztahují i na soutěž. Není potřeba nic kupovat, ale
                vědomostní otázka je povinná. Služby společnosti JD Media Networks se řídí zákony České republiky. JD Media Networks
                dodává interaktivní a zábavný mobilní obsah zahrnující vyzvánění, tapety, aplikace a témata. JD Media Networks
                nabízí originální a kvalitní služby. Pro více informací navštivte <label class="btntnc"
                    for="modal-1">Pravidla a podmínky</label>. Zákaznická podpora:
                infolinka@comgate nebo na infolince 228224291 Podmínky soutěže naleznete. Službu technicky
                zajišťuje ComGate Payments a.s.</p>
        </footer>
    </main>

    <input class="modal-state" id="modal-1" type="checkbox" />
    <div class="modal">
        <label class="modal__bg" for="modal-1"></label>
        <div class="modal__inner">
            <label class="modal__close" for="modal-1"></label>
            <h2>Všeobecné</h2>
            <h2>Podmínky</h2>
            <br>
            <div class="modalp">
                <p>
                    Následující předpisy se týkají všech služeb organizovaných JD Media Networks pod následujícími značkami
                    služeb: JD Media Networks. Různých produktů se mohou příležitostně týkat specifické předpisy. V případě
                    rozporu mezi všeobecnými předpisy a specifickými předpisy pro konkrétní službu platí specifické
                    předpisy
                    pro konkrétní službu.
                </p>
                <p>
                    Oprávnění k účasti
                    Účastníci musí mít platné spojení prostřednictvím operátora mobilní sítě a mobilní telefon.
                </p>
                <p>
                    Věk předplatitele musí být alespoň 18 let.
                </p>
                <p>
                    Všichni předplatitelé musí před vstupem do služby požádat o povolení plátce účtu za mobilní
                    telefon.
                </p>
                <p>
                    Předplatitel musí před vstupem do klubu předplatitelů pochopit a zavázat se k dodržování
                    všeobecných a
                    specifických pravidel soutěže, podmínek a ujednání.
                </p>
                <p>
                    Vstupem do předplacené služby se předplatitel zaručuje, že má právo do služby
                    vstupovat a že chápe a
                    přijímá podmínky a ujednání JD Media Networks služby.
                </p>
                <p>
                    Reklama
                    JD Media Networks si vyhrazuje právo zasílat Prémiové reklamní SMS zprávy do své
                    databáze uživatelů.
                    Uživatelé, kteří si nepřejí dostávat reklamní sdělení, by měli zaslat STOP
                    AP na 90930 k
                    zastavení zasílání reklamy. K povolení reklamy musí uživatel zaslat heslo
                    AP na 90930
                </p>
                <p>
                    Předplatné
                    Ke vstupu do JD Media Networks klubu musí účastník zaslat heslo předplatitele na
                    udané krátké číslo
                    pomocí SMS, např. AP na číslo 90930 nebo poskytnout číslo svého
                    mobilního telefonu na ukládací
                    stránce. Poté je uživateli zasláno heslo PIN, které uživatel musí zadat na
                    webové stránce k potvrzení
                    předplacení.
                </p>
                <p>
                    Zasláním hesla pro předplatné nebo zadáním hesla PIN uživatel přijímá
                    službu T&C a platební podmínky.
                </p>
                <p>
                    Předplatitel obdrží uvítací zprávu s informacemi o službě.
                </p>
                <p>
                    Předplatiteli bude zaslán WAP odkaz na stránku se stahovatelným
                    obsahem, kde si předplatitel může zvolit
                    položky, které si přeje stáhnout.
                </p>
                <p>
                    Klub předplatitelů JD Media Networks nabízí personalizaci
                    mobilního telefonu a položky zábavy jako např.
                    vyzváněcí tóny, tapety, melodie, aplikace, zábavné zvuky,
                    videa a animace.
                </p>
                <p>
                    Předplatitel si může objednat více kreditů zasláním SMS
                    zprávy AP na číslo 90930.
                </p>
                <p>
                    Předplacená služba je průběžně zpoplatňována Kč 99 každých 7 dnu. Každý 7. den budou na mobilní
                    číslo
                    předplatitele odeslány nové kredity, pakliže nebyla
                    předplacená služba ukončena.
                </p>
                <p>
                    Současně může probíhat několik předplatných.
                    Veškeré předplatné jsou nahlášena na webovou
                    stránku
                    JD Media Networks, a také do reklamních kampaní a
                    seznamů pošty. Každý nový předplatitel vstupuje
                    do
                    specifického předplatného podle své volby.
                    Předplatitelé jsou automaticky převedeni do
                    dalšího období
                    předplatného bez ohledu na to, zda využili své
                    kredity v předchozím období předplatného.
                </p>
                <p>
                    Je možné účastnit se více předplacených služeb
                    současně.
                </p>
                <p>
                    Nedodržení všeobecných nebo specifických
                    pravidel, podmínek a ujednání
                    předplatného může vést k vyřazení
                    uživatele z probíhajících a budoucích
                    předplatných období.
                </p>
                <p>
                    JD Media Networks si vyhrazuje právo
                    kdykoli a z jakéhokoli důvodu změnit
                    pravidla předplatného, zrušit
                    nebo pozastavit předplatné.
                </p>
                <p>
                    JD Media Networks neponese odpovědnost
                    za žádné ztracené nebo zpožděné
                    kredity následkem technické
                    závady, chyb způsobených
                    operátory mobilní sítě,
                    poskytovateli připojení, selhání
                    přístroje nebo
                    mobilního zařízení, selhání
                    počítače nebo následkem
                    jakýchkoli chyb či závad
                    způsobených technickými
                    problémy týkajícími se hardwaru
                    a softwaru, které by mohly
                    zabránit hráči v účasti v
                    soutěži.
                </p>
                <p>
                    Uživatel se před
                    předplacením musí ujistit,
                    že jeho mobilní telefon je
                    kompatibilní se službou. JD
                    Networks neponese
                    odpovědnost za to, že
                    produkty nefungují na
                    mobilním telefonu uživatele,
                    pokud tento
                    uživatel předem
                    nezkontroloval jeho
                    kompatibilitu.
                </p>
                <p>
                    Uživateli je účtována
                    každá obdržená SMS
                    samostatně. Platí
                    standardní poplatky
                    operátora.
                    Uživatelé mohou ukončit
                    své předplatné zasláním
                    hesla STOP AP na
                    číslo 90930.
                </p>
                <p>
                    Služba JD Media Networks
                    může pořádat
                    příležitostné
                    soutěže o ceny pro
                    své předplatitele.
                    Každá soutěž
                    bude mít vlastní
                    specifická pravidla,
                    která lze nalézt na
                    webové stránce a v
                    reklamním materiálu
                    pro
                    danou soutěž.
                </p>
                <p>
                    Pokud není
                    uvedeno jinak
                    vítězové soutěže
                    nesou
                    odpovědnost za
                    platbu všech
                    daní a pojištění
                    vyplývajících ze
                    soutěže nebo s
                    ní spojených.
                </p>
                <p>
                    S osobními
                    údaji
                    účastníků
                    bude
                    nakládáno
                    jako s
                    důvěrnými
                    informacemi
                    a budou
                    použity
                    výhradně pro
                    účely
                    služby.
                    Žádné osobní
                    údaje
                    nebudou
                    zpřístupněny
                    třetím
                    osobám bez
                    předchozího
                    svolení
                    účastníka.
                </p>
                <p>
                    V
                    případě
                    sporu
                    vzniklého
                    v
                    souvislosti
                    s těmito
                    podmínkami
                    a
                    ujednáními,
                    pravidly
                    a
                    vedením
                    předplatných
                    je
                    rozhodnutí
                    JD
                    Networks
                    konečné.
                    Nebudou
                    zahajována
                    žádná
                    další
                    jednání.
                </p>
                <p>
                    Služby
                    JD
                    Networks
                    podléhají
                    zákonům
                    Brunei.
                </p>
                <p>
                    Kontaktovat
                    nás
                    můžete
                    na
                    adrese
                    fc.cz@mobisupport.net.
                    Do
                    e-mailu
                    nezapomeňte
                    zadat
                    i
                    číslo
                    svého
                    mobilního
                    telefonu.
                    Naše
                    linka
                    nabízející
                    pomoc
                    je
                    k
                    dispozici
                    na
                    čísle:
                    228224291.
                    Dostupná
                    od
                    úterý
                    do
                    pátku
                    14.00-17.00.
                </p>
            </div>
        </div>
    </div>
<div>&nbsp;</div>
    <script>
        document.querySelector('[type="text"]').addEventListener('input', () => {
            if (isNaN(document.querySelector('[type="text"]').value))
                document.querySelector('[type="text"]').value = ""
        })
    </script>
</body>

</html>