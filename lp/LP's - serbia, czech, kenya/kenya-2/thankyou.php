<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial scale=1.0" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Content Download</title>
      <style type="text/css">
      	@media only screen and (max-width: 480px) {
      		.iconLogo {
      			height: auto !important;
      			max-width: 280px !important;
      			width: 100% !important;
      		}
      		
      	}
      </style>
  </head>

<body>


  <div class="content">
      <div class="wrap flex" style="font-size: 30px;">
        Thank you!
      </div>
  </div>

</body>
</html>