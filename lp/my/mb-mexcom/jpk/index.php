<?php require_once '../../../resources/my/mexcomm/header/aoc-redirect-header.php' ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-147360504-2');
        </script>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Mobile Content Download</title>
        <link rel="stylesheet" type="text/css" href="css/kzstyle.css" media="screen"  />
        <script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="/resources/my/mexcomm/handler/handler.js"></script>

        <style type="text/css">
        	@media only screen and (max-width: 480px) {
        		.iconLogo {
        			height: auto !important;
        			max-width: 280px !important;
        			width: 100% !important;
        		}
        	}
        </style>
    </head>

<body>

    <input type="hidden" id="shortcode" name="shortcode"  value="32339" >
    <input type="hidden" id="endpage" name="endpage"  value="" >
    <input type="hidden" id="keyword" name="keyword"  value="JPK" >
    <input type="hidden" id="country_code" name="country_code"  value="my" >
    <input type="hidden" id="landing_page" name="landing_page"  value="jpk">
    <input type="hidden" id="gateway_code" name="gateway_code"  value="Mexcomm" >

<div class="content">    
    <div class="wrap flex">
    	<div class="item flex-item" style="text-align:center">
            <div class="kwtitle"><div class="animated pulse">Mobile Content Subscription</div></div>
            <img src="assets/images.png" id="logBan"/><br/>
            <a href="sms:32339?body=ON JPK <?=$carryover?>"  id="btn-continue-a"><img id="scnd" src="assets/button.png" alt=""/></a>
            
            <div class="kw">
                <p>OR send SMS</p>
                <div class="animated pulse">ON JPK</div> 
                <span> &nbsp; TO &nbsp; </span>
                <div class="animated pulse">32339</div>
            </div>

        </div>
    </div>
    
    
    <div id="tnc">
        This is an ongoing subscription service until you unsubscribe the services. All subscription contents are compatible with 
        3G/GPRS/WAP-enabled mobile phones and applicable to both postpaid and prepaid users. The content will Product name 
        Funny Videos. Each message will be charged at RM3/message (excl. GST). Maxis users will receive maximum 20 SMS/month. 
        This service only available for Maxis users. Data charges are billed separately by mobile operators. Please seek parental 
        and or guardian approval if you are 18 years old and below. Upon sending in the registration SMS to the shortcode as per 
        advertisement, you are acknowledged that you have read and understood the “General Terms and Conditions”. To subscribe the 
        service, send JPK to 32339 and to cancel this service, send “STOP JPK” to 32339. Helpline: 1-300-22-3228. 9am – 6pm, Mon-Fri. 
        This service is operated according Malaysia Code of Conduct for the SMS Services. Powered by Mexcomm Sdn Bhd.
    </div>
    <div class="space">&nbsp;</div>
</div>
</body>
</html>

