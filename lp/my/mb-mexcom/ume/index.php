<?php require_once '../../../resources/my/mexcomm/header/aoc-redirect-header.php' ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147360504-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-147360504-2');
        </script>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <meta name="viewport" content="width=device-width, initial scale=1.0" />
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Mobile Content Download</title>
		<link rel="stylesheet" type="text/css" href="css/kzstyle.css" media="screen"  />
        <script type="text/javascript" src="/resources/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="/resources/my/mexcomm/handler/handler.js"></script>
		<style type="text/css">
			@media only screen and (max-width: 480px) {
				.keyword p{
					font-size:1rem;
					margin-top:5px;
					margin-bottom:2px;
				}
				.keyword p span{
					font-size: 2rem;
				}
				.item h2{
					margin-bottom:0;
				}
				
				.iconLogo {
					height: auto !important;
					max-width: 280px !important;
					width: 100% !important;
				}
				
				#flt1, #flt2
				{
					text-align:center;
				}
				#flt2{
					float:none;
				}
				#flt2 img{
					width:70%;
				}
				#flt1 img{
					width:50%;
				}
				
			}
		</style>
	</head>

<body>

    <input type="hidden" id="shortcode" name="shortcode"  value="33391" >
    <input type="hidden" id="endpage" name="endpage"  value="" >
    <input type="hidden" id="keyword" name="keyword"  value="UME" >
    <input type="hidden" id="country_code" name="country_code"  value="my" >
    <input type="hidden" id="landing_page" name="landing_page"  value="ume">
    <input type="hidden" id="gateway_code" name="gateway_code"  value="Mexcomm" >

    <div class="wrap flex">
    	<div class="item flex-item" style="text-align:center;">
    		<h2>Mobile Content Subscription
    		UNLIMITED DOWNLOADS</h2>
            <div id="flt2">
            <img src="assets/image.png" alt=""/>
            </div>
             <div id="flt1">
            	<div class="keyword">
					<p>send sms <span class="pulse">ON UME</span> to <span span class="pulse">33391</span> <br> or</p>
            	</div>
				<a href="sms:33391?body=ON UME <?=$carryover?>" id="btn-continue-a"><img id="btn-continue" src="assets/button.gif" alt=""/></a>
           </div>
        </div>
    	<div class="tnc">
        This is an ongoing subscription service until you unsubscribe the services. All subscription contents are compatible with 
        3G/GPRS/WAP-enabled mobile phones and applicable to both postpaid and prepaid users. The content will Product name 
        Funny Videos. Each message will be charged at RM3/message (excl. GST). Maxis users will receive maximum 20 SMS/month. 
        This service only available for Maxis users. Data charges are billed separately by mobile operators. Please seek parental 
        and or guardian approval if you are 18 years old and below. Upon sending in the registration SMS to the shortcode as per 
        advertisement, you are acknowledged that you have read and understood the “General Terms and Conditions”. To subscribe the 
        service, send UME to 33391 and to cancel this service, send “STOP UME” to 33391. Helpline: 1-300-22-3228. 9am – 6pm, Mon-Fri. 
        This service is operated according Malaysia Code of Conduct for the SMS Services. Powered by Mexcomm Sdn Bhd.
    </div>

</body>
</html>