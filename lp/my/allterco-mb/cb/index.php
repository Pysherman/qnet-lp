<!--?php require_once '../../resources/rs/teracomm/header/header.php' ?-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CB Download</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/resources/rs/teracomm/handler/handler.js"></script>
</head>

<body>

    <input type="hidden" id="API_PATH" name="API_PATH" value="<?php echo API_PATH;?>">
    <input type="hidden" id="operator_code" name="operator_code" value="">
    <input type="hidden" id="shortcode" name="shortcode" value="33070">
    <input type="hidden" id="keyword" name="keyword" value="CB">
    <input type="hidden" id="user_ip" name="user_ip" value="">
    <input type="hidden" id="affiliate_code" name="affiliate_code" value="">
    <input type="hidden" id="country_code" name="country_code" value="ml">
    <input type="hidden" id="landing_page" name="landing_page" value="CB_Landing__page">
    <input type="hidden" id="gateway_code" name="gateway_code" value="Allterco_my">
    <input type="hidden" id="carryover" name="carryover" value="">

    <div class="main">
        <div class="container">
            <nav class="header">
                <img class="images" src="img/image.png" alt="">
            </nav>
            <div class="subscription">
                <span class="normal-1">send sms</span>
                <div class="subscription_bounce-1"><span class="subs__text">on cb</span></div>
                <span class="normal-2">to</span>
                <div class="subscription_bounce-2"><span class="subs__text" >33070</span></div>
                <span class="normal-3">or</span>
            </div>
            <section class="section">
                <a href="sms:33070?body=CB" id="btn-continue"><img class="button" src="img/button.gif" alt=""></a>
            </section>
            <footer class="footer">
                <p class="paragraph">
                This is an ongoing subscription for unlimited download content until you quit. The content is compatible with most mobile phones with multimedia support. This service only available for Tunetalk users only. Tunetalk: RM5/SMS(incl. 0% GST), 4SMS/week. GPRS/WAP-enabled phone required. Standard Operator SMS/MMS/WAP/GPRS/UMTS charges apply. To opt out send STOP CB to 33070. Customers must be 18+ and have bill payer's permission. This service operates according to the Malaysia Code of Conduct for SMS services. Trademarks, service marks, logos (including, without limitation, the individual names of products and retailers) are the property of their respective owners. Helpline: 0xxxxxxxx (local rate), 9am to 6pm Monday to Friday. Service Provider: QuickCell Technologies. No. B.28, Block B, Serusop Complex, Simpang 82, Delima Satu, Serusop, Jalan Muara, BB4713, Brunei/Muara, Brunei Darussalam.
                </p>
            </footer>
        </div>
    </div>

</body>

</html>