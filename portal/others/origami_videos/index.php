<?php
session_start();
include('includes/inc.php');
include('includes/Consts.php');
if(isset($_GET["varx"])){
    $cat = $_GET["varx"]; 
	$main_category_id = $_GET["main_category_id"];


    if($cat=="Origami_room_decoration")
	{
        $btnImg_1 = "Origami_room_decoration.png";
        $CatCondition = "and (b.sub_category='Origami_room_decoration')";
	}
    if($cat=="Origami_flower_bouquet")
	{
        $btnImg_2= "Origami_flower_bouquet.png";
        $CatCondition = "and (b.sub_category='Origami_flower_bouquet')";
    }
    if($cat=="3D_origami_diamond_pattern_swan")
	{
        $btnImg_3= "3D_origami_diamond_pattern_swan.png";
        $CatCondition = "and (b.sub_category='3D_origami_diamond_pattern_swan')";
    }
    if($cat=="3D_origam3D_origami_small_swani_diamond_pattern_swan")
	{
        $btnImg_4= "3D_origami_small_swan.png";
        $CatCondition = "and (b.sub_category='3D_origami_small_swan')";
    }
    if($cat=="origami_christmas_tree")
	{
        $btnImg_5= "origami_christmas_tree.png";
        $CatCondition = "and (b.sub_category='origami_christmas_tree')";
    }
    if($cat=="paper_boat_origami")
	{
        $btnImg_6= "paper_boat_origami.png";
        $CatCondition = "and (b.sub_category='paper_boat_origami')";
    }
    if($cat=="origami_stick_paper_flower")
	{
        $btnImg_7= "origami_stick_paper_flower.png";
        $CatCondition = "and (b.sub_category='origami_stick_paper_flower')";
    }
    if($cat=="kids_craft_toddlers")
	{
        $btnImg_8= "kids_craft_toddlers.png";
        $CatCondition = "and (b.sub_category='kids_craft_toddlers')";
    }
    if($cat=="educational_crafts")
	{
        $btnImg_9= "educational_crafts.png";
        $CatCondition = "and (b.sub_category='educational_crafts')";
    }
    if($cat=="toilet_paper_roll")
	{
        $btnImg_10= "toilet_paper_roll.png";
        $CatCondition = "and (b.sub_category='toilet_paper_roll')";
    }
    if($cat=="decorative_paper_basket")
	{
        $btnImg_11= "decorative_paper_basket.png";
        $CatCondition = "and (b.sub_category='decorative_paper_basket')";
    }
    if($cat=="kidscraft_newspaper_cycle_Pen")
	{
        $btnImg_12= "kidscraft_newspaper_cycle_Pen.png";
        $CatCondition = "and (b.sub_category='kidscraft_newspaper_cycle_Pen')";
    }
    if($cat=="diorama_ultra_realistic_trees")
	{
        $btnImg_13= "diorama_ultra_realistic_trees.png";
        $CatCondition = "and (b.sub_category='diorama_ultra_realistic_trees')";
    }
    if($cat=="diorama_wire_tree")
	{
        $btnImg_14= "diorama_wire_tree.png";
        $CatCondition = "and (b.sub_category='diorama_wire_tree')";
    }
    if($cat=="diorama_realistic_forest")
	{
        $btnImg_15= "diorama_realistic_forest.png";
        $CatCondition = "and (b.sub_category='diorama_realistic_forest')";
    }
    if($cat=="diorama_train_from_scratch")
	{
        $btnImg_16= "diorama_train_from_scratch.png";
        $CatCondition = "and (b.sub_category='diorama_train_from_scratch')";
    }
    if($cat=="diorama_dinousaur")
	{
        $btnImg_17= "diorama_dinousaur.png";
        $CatCondition = "and (b.sub_category='diorama_dinousaur')";
    }
    if($cat=="diorama_ariplane_crash")
	{
        $btnImg_18= "diorama_ariplane_crash.png";
        $CatCondition = "and (b.sub_category='diorama_ariplane_crash')";
    }
    if($cat=="diorama_waterfalls")
	{
        $btnImg_19= "diorama_waterfalls.png";
        $CatCondition = "and (b.sub_category='diorama_waterfalls')";
    }
    if($cat=="diorama_jurassic")
	{
        $btnImg_20= "diorama_jurassic.png";
        $CatCondition = "and (b.sub_category='diorama_jurassic')";
    }
}else{
    $btnImg_1 = "Origami_room_decoration.png";
    $btnImg_2= "Origami_flower_bouquet.png";
    $btnImg_3 = "3D_origami_diamond_pattern_swan.png";
    $btnImg_4 = "3D_origami_small_swan.png";
    $btnImg_5 = "origami_christmas_tree.png";
    $btnImg_6 = "paper_boat_origami.png";
    $btnImg_7 = "origami_stick_paper_flower.png";
    $btnImg_8 = "kids_craft_toddlers.png";
    $btnImg_9 = "educational_crafts.png";
    $btnImg_10 = "toilet_paper_roll.png";
    $btnImg_11= "decorative_paper_basket.png";
    $btnImg_12= "kidscraft_newspaper_cycle_Pen.png";
    $btnImg_13 = "diorama_ultra_realistic_trees.png";
    $btnImg_14 = "diorama_wire_tree.png";
    $btnImg_15 = "diorama_realistic_forest.png";
    $btnImg_16 = "diorama_train_from_scratch.png";
    $btnImg_17= "diorama_dinousaur.png";
    $btnImg_18= "diorama_ariplane_crash.png";
    $btnImg_19 = "diorama_waterfalls.png";
    $btnImg_20 = "diorama_jurassic.png";
    $CatCondition = "and (b.sub_category='Origami_room_decoration') or (b.sub_category='Origami_flower_bouquet') or (b.sub_category='3D_origami_diamond_pattern_swan') or (b.sub_category='3D_origami_small_swan') or (b.sub_category='origami_christmas_tree') or (b.sub_category='paper_boat_origami') or (b.sub_category='origami_stick_paper_flower') or (b.sub_category='kids_craft_toddlers') or (b.sub_category='educational_crafts') or (b.sub_category='toilet_paper_roll') or (b.sub_category='decorative_paper_basket') or (b.sub_category='kidscraft_newspaper_cycle_Pen') or (b.sub_category='diorama_ultra_realistic_trees') or (b.sub_category='diorama_wire_tree') or (b.sub_category='diorama_realistic_forest') or (b.sub_category='diorama_train_from_scratch') or (b.sub_category='diorama_dinousaur') or (b.sub_category='diorama_ariplane_crash') or (b.sub_category='diorama_jurassic') or (b.sub_category='diorama_waterfalls')";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <title>Origami Portal</title>
</head>
<body>
    <div class="container">
        <header class="logo">
            <img src="assets/head_banner_2.jpg" alt="Logo">
        </header>
        <main class="content">

        <div class="btn "><a href="view.php?varx=48&main_cat=12"><img src='assets/<?php echo $btnImg_1; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=51&main_cat=12"><img src='assets/<?php echo $btnImg_2; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=49&main_cat=12"><img src='assets/<?php echo $btnImg_3; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=50&main_cat=12"><img src='assets/<?php echo $btnImg_4; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=52&main_cat=12"><img src='assets/<?php echo $btnImg_5; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=53&main_cat=12"><img src='assets/<?php echo $btnImg_6; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=68&main_cat=12"><img src='assets/<?php echo $btnImg_7; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=54&main_cat=12"><img src='assets/<?php echo $btnImg_8; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=55&main_cat=12"><img src='assets/<?php echo $btnImg_9; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=56&main_cat=12"><img src='assets/<?php echo $btnImg_10; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=57&main_cat=12"><img src='assets/<?php echo $btnImg_11; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=67&main_cat=12"><img src='assets/<?php echo $btnImg_12; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=58&main_cat=12"><img src='assets/<?php echo $btnImg_13; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=59&main_cat=12"><img src='assets/<?php echo $btnImg_14; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=60&main_cat=12"><img src='assets/<?php echo $btnImg_15; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=61&main_cat=12"><img src='assets/<?php echo $btnImg_16; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=62&main_cat=12"><img src='assets/<?php echo $btnImg_17; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=63&main_cat=12"><img src='assets/<?php echo $btnImg_18; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=66&main_cat=12"><img src='assets/<?php echo $btnImg_19; ?>' border="0"></a></div>
        <div class="btn "><a href="view.php?varx=64&main_cat=12"><img src='assets/<?php echo $btnImg_20; ?>' border="0"></a></div>


        </main>
        <footer>
            <ul>
                <li><a href="#">General terms of use</a></li>
                <li><a href="#">Help</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <p>
            This is a fun origami service. The service costs 360 DIN per week for MTS, Telenor and VIP users (plus the price of basic SMS: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN) (All quoted prices with VAT included) . Subscription to the service will be automatically updated until you send STOPVIDEO to 1311 at 3.60 DIN price for MTS and Telenor users and 3.48 DIN for VIP users. By registering to this service you confirm that you are in compliance with all applicable terms and conditions -a. All games and / or applications, videos, and pictures on this site are for fun. For more information on billing, please call EDS at 011/7702342. This service is offered by Mobitech Solutions Unit No.6, 2nd Floor, Block B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713
            </p>
        </footer>
    </div>

    <script type="text/javascript" src="script/jquery.js" ></script>
</body>
</html>