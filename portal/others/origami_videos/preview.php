<?php
ob_start();
session_start();
include('includes/inc.php');
if(isset($_GET["varx"])){
	$cat = $_GET["varx"]; 
}else{
	$cat = 'Craft';
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <title>DOWNLOAD</title>
</head>
<body>
    <div class="container">
        <header class="logo">
        <img src="assets/head_banner_2.jpg" alt="Logo">
        </header>
        <main class="dl-container">
        <?php
            if(isset($_GET['varx'])){
                $prevSesID = $_GET['contid'];
                $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";

                if($_GET['varx']==""){
                    header("location:index.php");
                }else{
                    $getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
                    $data = array();
                    if($getContent){
                        while($items = mysqli_fetch_array($getContent)){
                            $ContentID 		= $items['id'];
							$itemTitle 		= $items['title'];
							//$description 	= $items['description'];
							//$contentRate	= $items['rate'];
							$file_name 		= $items['file_name'];
							$original_file_name = $items['original_file_name'];
							$mime 			= $items['mime'];
							$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
							$filename = $contenrdir.$file_name;	
                            $file = pathinfo($file_name, PATHINFO_FILENAME);
                            
                            if($ext=="mp4" || $ext=="mp3")
                            {
                            $thumbimg = $file.'.png';
                            $preview ='<video width="100%" class="video_preview" controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
                            <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
                            </video>';
                            }
                            else
                            {
                            $thumbimg = $file.'.png';
                            $preview ='<img src="'.$filename.'" alt="'.$itemTitle.'">';
                            }
                            }
                            }
                            }
                            }
                            else
                            {
                            header("location:index.php");
                            }
        ?>
            <section>
                <div class="dl-name">
                    <h5><?php echo $itemTitle; ?></h5>
                </div>
                <div class="dl-image">
                    <?php echo $preview; ?>
                </div>
                <div class="dl-download">
                    <a href="<?php echo $filename; ?>" download >DOWNLOAD</a>
                </div>
            </section>
        </main>
        <footer>
            <ul>
                <li><a href="#">General terms of use</a></li>
                <li><a href="#">Help</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
            <p>
            This is a fun origami service. The service costs 360 DIN per week for MTS, Telenor and VIP users (plus the price of basic SMS: MTS 3,60 DIN, Telenor 3,60 DIN, Vip 3,48 DIN) (All quoted prices with VAT included) . Subscription to the service will be automatically updated until you send STOPVIDEO to 1311 at 3.60 DIN price for MTS and Telenor users and 3.48 DIN for VIP users. By registering to this service you confirm that you are in compliance with all applicable terms and conditions -a. All games and / or applications, videos, and pictures on this site are for fun. For more information on billing, please call EDS at 011/7702342. This service is offered by Mobitech Solutions Unit No.6, 2nd Floor, Block B, Spg82, Pg Haji Tajuddin Complex, Kg Delima Satu Serusop, Jalan Muara BB4713
            </p>
        </footer>
    </div>

    <script type="text/javascript" src="script/jquery.js" ></script>
</body>
</html>