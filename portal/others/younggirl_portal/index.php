<?php require ('connection/header.php');
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Young Girl</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="js/jquery.js"></script>

        <script type="text/javascript" language="javascript">
            var hash = (window.location.hash).replace('#', '');
            var str2 = hash != '' ? str2 = hash.substr(0) : str2 = '';

            //map back to subcategory after preview closed
            if (str2 == 8 || str2 == 16 || str2 == 4) {
                str2 = '';
            } //First Subcategory Action, Car Racing, Antivirus
            else {
                str2 = '#a' + str2;
            } //else the rest of Subcategory

            $(document).ready(function() {
                $('.m_c_content div.acco2:eq(0)').find('div.expand:eq(0)').addClass('openAd').end()
                    .find('div.collapse:gt(0)').hide().end()
                    .find('div.expand').click(function() {
                        $(this).toggleClass('openAd').siblings().removeClass('openAd').end()
                            .next('div.collapse').slideToggle().siblings('div.collapse:visible').slideUp();
                        return false;
                    });

                //set subcategory tab to click to fire  event
                if (str2 != '') {
                    $(str2 + " a").click();
                }
            })
        </script>
    </head>

    <body>

        <section class="header-nav">
            <div class="nav-burger" id="NavBurger">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <a href="./"><img src="img/younggirl_logo.png" alt="Young Girl"></a>
        </section>
        <section class="side-nav" id="sideNav">
            <div class="logo">
                <a href="./"><img src="img/younggirl_logo.png" alt="Young Girl"></a>
            </div>
            <div id="content_menu_1">
                <div class="btn "><a href="?varx=videos"><img src='img/<?php echo $vdoImg; ?>' border="0"></a></div>
                <div class="btn "><a href="?varx=wallpaper"><img src='img/<?php echo $btnImg; ?>' border="0"></a></div>
            </div>
        </section>

        <div class="container">
            <div class="wrap">
                <div class="extra_left"></div>
                <div class="main_container" id="main">
                    <div class="main_container_content">
                        <div class="m_c_content">
                            <div class="acco2">
                                <?php

                        $queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");                        
                        if($queryJoin)
                        {
                        while ($DateRow = mysqli_fetch_assoc($queryJoin))
                        {
                        $resCatgID	= $DateRow['id'];
                        $resName= $DateRow['category'];
                        $resSbCatgID= $DateRow['sc_id'];
                        $resSName= $DateRow['sub_category'];
                        $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";

                        if($resSName=="Application")
                        {
                        $resSName = "Antivirus";
                        }

                        echo'
                        <div id="a'.$resSbCatgID.'" class="expand"><a title="expand/collapse" href="#" style="display: block;">'.ucfirst($resSName).'</a></div>';
                        echo"<div class=\"collapse\">
                        <div class=\"accCntnt\" style=\"color:#000000\">
                        <ul class=\"container_of_holder\">";
                        if(isset($_GET["varx"]))
                        {
                        $cat = $_GET["varx"]='videos'; 
                        }
                        else
                        {
                        $cat = 'videos';
                        }

                        if(!empty($_SESSION['categref']))
                        {
                        $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
                        $categref 	= $_SESSION['categref'];
                        $sesCatg 	= $_SESSION['sesCatg'];

                        $getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and $categref order by id asc limit 20");
                        $data = array();

                        
                        if($getContent)
                        {
                        while($items = mysqli_fetch_array($getContent))
                        {
                        $contentID 		= $items['id'];
                        $itemTitle 		= $items['title'];
                        $description 	= $items['description'];
                        $file_name 		= $items['file_name'];
                        $original_file_name = $items['original_file_name'];
                        $mime 			= $items['mime'];
                        $ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
                        $filename = $contenrdir.$file_name;
                        $subcat = $items['sub_category_id'];

                        $file = pathinfo($file_name, PATHINFO_FILENAME);

                        if($ext=="mp4")
                        {
                        $thumbimg = $file.'.png';
                        $preview ='<video style="z-index:-2;" width="100%" height="100"  controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
                        <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
                        </video>';
                        }
                        elseif($ext=="mp3")
                        {
                        $preview = '<img id="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
                        }
                        elseif($ext=="jpg")
                        {
                            $thumbimg = $file.'.jpg';
                            $preview ='<img id="img-thumbnail" src="' .$filename. '" alt="'.$itemTitle.'">';
                        }
                        else
                        {
                        $thumbimg = $file.'.png';
                        $preview ='<img id="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
                        }

                        echo"<li id=\"content_items\" style=\"z-index:0;\">						  
                        <a style='z-index:100; width:6.17rem' href=\"preview.php?varx=$sesCatg&contid=$contentID&subcat=$subcat\"><div style='z-index:100;'>$preview</div></a>
                            <div class='content_item_description'>
                                <div id=\"title\"><a href='preview.php?varx=".$sesCatg."&contid=".$contentID."'>".$itemTitle."</a></div>
                                <div id=\"btnalign\"><a href=\"$filename\" id=\"btn\" download>DOWNLOAD</a></div>
                            </div>
                        </li>";
                        }
                        }
                        }		  

                        echo"</ul>                
                        </div>
                        </div>";

                        }
                        }
                        ?>
                            </div>
                        </div>
                    </div>
                    <script>
                        function openNav() {
                            document.getElementById("mySidenav").style.height = "auto";

                            document.getElementById("main_container_content").style.marginRight = "80%";
                            document.getElementById("main_container_content").style.margin = "auto";
                            document.getElementById("box4").style.marginRight = "80%";
                            /*document.getElementById("nvmg").style.marginRight = "auto";*/
                            document.getElementById("proj").style.display = "block";

                        }

                        function closeNav() {
                            document.getElementById("mySidenav").style.height = "0";
                            /* document.getElementById("main_container_content").style.margin = "margin-top:200px";*/
                            document.getElementById("main_container_content").style.margin = "auto";
                            document.getElementById("box4").style.margin = "0";
                            /*document.getElementById("nvmg").style.margin = "auto";*/
                            document.getElementById("proj").style.display = "none";
                            /*document.getElementById("block").style.display = "none";*/
                        }

                        var navBurger = document.getElementById('NavBurger');
                        var sideMenu = document.getElementById('sideNav');
                        var toggle = false;
                        navBurger.addEventListener('click', slideSideMenu);

                        if (window.innerWidth >= 900) {
                            sideMenu.classList.remove('slide-out');
                        }

                        function slideSideMenu() {
                            //slides the menu in or out
                            if (toggle === true) {
                                //slide out
                                sideMenu.classList.remove('slide-in');
                                sideMenu.classList.add('slide-out');
                                toggle = false;
                            } else {
                                //slide in
                                sideMenu.classList.add('slide-in');
                                sideMenu.classList.remove('slide-out');
                                toggle = true;
                            }

                            navBurger.classList.toggle('tog-burger');

                        };
                    </script>

                </div>
            </div>
        </div>
    </body>

    </html>