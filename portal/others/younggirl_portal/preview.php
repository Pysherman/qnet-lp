<?php
ob_start();
session_start();
include('connection/inc.php');
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"];
	@$sub = $_GET["subcat"];

	if($cat=="videos")
	{
		$btnImg = "wallpaper1.png";
		$vdoImg = "vdo2.png";
	}
	if($cat=="wallpaper")
	{
		$btnImg = "wallpaper2.png";
		$vdoImg = "vdo1.png";
	}

}
else
{
	$cat = 'videos';
	$sub = $_GET["subcat"];
	$btnImg = "wallpaper1.png";
	$vdoImg = "vdo2.png";
}
$_SESSION['sesCatg'] = $cat;
?>
    <!DOCTYPE html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Preview</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="js/jquery.js"></script>
        <!--link href="./css/toggle.css?2" rel="stylesheet" type="text/css">
        <script src="./js/jquery-1.12.4.min.js"></script>
        <script src="./js/toggle.js?2" type="text/javascript"></script-->
    </head>

    <body>

        <section class="header-nav">
            <div class="nav-burger" id="NavBurger">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <a href="./"><img src="img/younggirl_logo.png" alt="Young Girl"></a>
        </section>
        <section class="side-nav" id="sideNav">
            <div class="logo">
                <a href="./"><img src="img/younggirl_logo.png" alt="Young Girl"></a>
            </div>
            <div id="content_menu_1">
                <div class="btn "><a href="./?varx=videos"><img src='img/<?php echo $vdoImg; ?>' border="0"></a></div>
                <div class="btn "><a href="./?varx=wallpaper"><img src='img/<?php echo $btnImg; ?>' border="0"></a></div>
            </div>
        </section>

        <div class="wrap">
            <div class="extra_left"></div>
            <div class="main_container">
                <div class="main_container_content">
                    <div class="m_c_content">
                        <div class="accordion_head">PREVIEW
                            <a href="<?php echo './?varx='.$cat.'#'.$sub; ?>"><span class='close'><i class="fa fa-close"></i></span></a>
                        </div>
                        <div class="panitankonaimongitlogron">
                            <?php
						if(isset($_GET['varx']))
						{
						$prevSesID = $_GET['contid'];
						$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";

						if($_GET['varx']=="")
						{
						header("location:index.php");
						}
						else
						{
						$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
						$data = array();
						if($getContent)
						{
						while($items = mysqli_fetch_array($getContent))
						{
						$ContentID 		= $items['id'];
						$itemTitle 		= $items['title'];
						$description 	= $items['description'];
						$contentRate	= $items['rate'];
						$file_name 		= $items['file_name'];
						$original_file_name = $items['original_file_name'];
						$mime 			= $items['mime'];
						$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
						$filename = $contenrdir.$file_name;

						$file = pathinfo($file_name, PATHINFO_FILENAME);

						if($ext=="mp4" || $ext=="mp3")
						{
						$thumbimg = $file.'.png';
						$preview ='<video width="100%"  controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
						<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
						</video>';
						}
						else
						{
						$thumbimg = $file.'.png';
						$preview ='<img src="'.$filename.'" alt="'.$itemTitle.'">';
						}
						}
						}
						}
						}
						else
						{
						header("location:index.php");
						}
						?>

                                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr class="mobile-tr">
                                        <td width="43%" align="center" style="padding:5px;" class="prevw">
                                            <?php echo $preview; ?>
                                        </td>
                                        <td width="57%" align="center" valign="middle">
                                            <div id="title" style="text-align:center; font-weight:bold; font-size:13px;">
                                                <?php echo $itemTitle; ?>
                                            </div>
                                            <div><img src="img/5.png" border="0" /></div>
                                            <div style="margin-top:10px; padding-left:8px;"><a href="<?php echo $filename; ?>" id="btn">DOWNLOAD</a></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding:8px; text-align:left">
                                            <?php echo stripslashes($description); ?>
                                        </td>
                                    </tr>

                                    <?php
							if($ext=="apk" || $ext=="xapk")
							{
							$prevFile = $conn->query("SELECT file_name FROM cms.preview WHERE content_id='$ContentID'");
							//if($prevFile)
							if(mysqli_affected_rows($conn))
							{
							echo'<tr>
							<td colspan="2" style="padding:8px; font-weight:bold; text-align:left">Screenshots:</td>
							</tr>
							<tr>
								<td colspan="2"><div style="margin-top:5px;">
									<div class="w3-content w3-display-container" style="background:#f0ca4d; padding:5px;">
										<div style="width:221px; margin:auto">';
											while($PrevItems = mysqli_fetch_array($prevFile))
											{
											$scrFiles	= $PrevItems['file_name'];
											$screenshot = $contenrdir.$scrFiles;

											echo"<img src=\"".$screenshot."\" width=\"221\" height=\"191\" class=\"mySlides\">";
											}
											echo"</div>
											<button class=\"w3-button w3-black w3-display-left\" onclick=\"plusDivs(-1)\"><img src=\"img/btn_left.png\" width=\"19\" height=\"14\" /></button>

											<button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs(1)\"><img src=\"img/btn_right.png\" width=\"19\" height=\"14\" /></button>
										</div>
										<script>
										var slideIndex = 1;
										showDivs(slideIndex);

										function plusDivs(n) {
										showDivs(slideIndex += n);
										}

										function showDivs(n) {
										var i;
										var x = document.getElementsByClassName(\"mySlides\");
										if (n > x.length) {slideIndex = 1}    
										if (n < 1) {slideIndex = x.length}
										for (i = 0; i < x.length; i++) {
										x[i].style.display = \"none\";  
										}
										x[slideIndex-1].style.display = \"block\";  
										}
										</script>
									</div>
								</td>
							</tr>";
							}
							}
						?>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function openNav() {
                document.getElementById("mySidenav").style.height = "auto";

                document.getElementById("main_container_content").style.marginRight = "80%";
                document.getElementById("main_container_content").style.margin = "auto";
                document.getElementById("box4").style.marginRight = "80%";
                /*document.getElementById("nvmg").style.marginRight = "auto";*/
                document.getElementById("proj").style.display = "block";

            }

            function closeNav() {
                document.getElementById("mySidenav").style.height = "0";
                /* document.getElementById("main_container_content").style.margin = "margin-top:200px";*/
                document.getElementById("main_container_content").style.margin = "auto";
                document.getElementById("box4").style.margin = "0";
                /*document.getElementById("nvmg").style.margin = "auto";*/
                document.getElementById("proj").style.display = "none";
                /*document.getElementById("block").style.display = "none";*/
            }

            var navBurger = document.getElementById('NavBurger');
            var sideMenu = document.getElementById('sideNav');
            var toggle = false;
            navBurger.addEventListener('click', slideSideMenu);

            if (window.innerWidth >= 900) {
                sideMenu.classList.remove('slide-out');
            }

            function slideSideMenu() {
                //slides the menu in or out
                if (toggle === true) {
                    //slide out
                    sideMenu.classList.remove('slide-in');
                    sideMenu.classList.add('slide-out');
                    toggle = false;
                } else {
                    //slide in
                    sideMenu.classList.add('slide-in');
                    sideMenu.classList.remove('slide-out');
                    toggle = true;
                }

                navBurger.classList.toggle('tog-burger');

            };
        </script>
    </body>

    </html>