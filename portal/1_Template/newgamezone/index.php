<?php
session_start();
include('inc.php');
require('prop/Consts.php');
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"]; 
	$btn_play="";
	if($cat=="games-apk")
	{
		$btnImg = "apkgames.png";
		$htmlImg = "htmlgames1.png";
		$CatCondition = " and (b.sub_category='Adventure'  or b.sub_category='Action'   or b.sub_category='Arcade' or b.sub_category='Strategy')";
		$btn_play="DOWNLOAD";
	}
	if($cat=="html5")
	{
		$btnImg = "apkgames1.png";
		$htmlImg = "htmlgames.png";
		$CatCondition = " and (b.sub_category='Embed-Games')";
		$btn_play="PLAY";
	}

}
else
{
	$cat = 'games-apk';
		$btnImg = "apkgames.png";
		$htmlImg = "htmlgames1.png";
		$CatCondition = " and (b.sub_category='virtual-reality'  or b.sub_category='Action'  or b.sub_category='Arcade' or b.sub_category='Strategy')";
		$btn_play="DOWNLOAD";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Game Zone</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/portal.css" media="screen" />
<link rel="stylesheet" type="text/css" href="styles.css" media="screen"  />
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
<script type="text/javascript" src="js/jquery.js" ></script>
<script  type="text/javascript" language="javascript">
	$(document).ready(function(){
    $('.adCntnr div.acco2:eq(0)').find('div.expand:eq(0)').addClass('openAd').end()
    .find('div.collapse:gt(0)').hide().end()
    .find('div.expand').click(function() {
        $(this).toggleClass('openAd').siblings().removeClass('openAd').end()
        .next('div.collapse').slideToggle().siblings('div.collapse:visible').slideUp();
        return false;
    });
})
</script>
<style>
* {
    box-sizing: border-box;
}

.column {
    float: left;
    /*width: 33.33%;*/
    width: 50%;
    padding: 5px;
}

.column img, .item img { max-width: 100% !important; width: auto !important; height: auto !important; }

/* Clearfix (clear floats) */
.row::after {
    content: "";
    clear: both;
    display: table;
}

/* ============================== */
.container {
  list-style:none;
  margin: 0;
  padding: 0;
}
.item {
  /*background: tomato;*/
  border:1px solid #CCC;
  padding: 5px;
  width: 200px;
  /*height: 150px;*/
  margin: 10px;
  font-size:12px;
  font-weight:normal;
  /*line-height: 150px;*/
  color: #000;
  border-radius:10px;
  /*text-align: center;*/
}

.flex {
  padding: 0;
  margin: 0;
  list-style: none;
  
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  flex-wrap:wrap;
  -webkit-flex-flow: row wrap;
  justify-content: space-around;
}

#title
{
	padding:10px;
	text-align:center;
	color:#0094ca;
}

#title a
{
	text-decoration:none;
	color:#0094ca;
}

#title a:hover
{
	text-decoration:underline;
}

.btn{
	width:100%;
	text-align:center;
border:1px solid #006b92; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px; padding: 10px 10px 10px 10px; text-decoration:none; display:inline-block;text-shadow: -1px -1px 0 rgba(0,0,0,0.3);font-weight:bold; color: #FFFFFF;
 background-color: #b95e0f; background-image: -webkit-gradient(linear, left top, left bottom, from(#b95e0f), to(#b95e0f));
 background-image: -webkit-linear-gradient(top, #b95e0f, #b95e0f);
 background-image: -moz-linear-gradient(top, #b95e0f, #b95e0f);
 background-image: -ms-linear-gradient(top, #b95e0f#940705, #b95e0f);
 background-image: -o-linear-gradient(top, #b95e0f, #b95e0f);
 background-image: linear-gradient(to bottom, #b95e0f, #b95e0f);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#b95e0f, endColorstr=#b95e0f);
}

.btn:hover{
 border:1px solid #004964;
 background-color: #940705; background-image: -webkit-gradient(linear, left top, left bottom, from(#940705), to(#940705));
 background-image: -webkit-linear-gradient(top, #940705, #940705);
 background-image: -moz-linear-gradient(top, #940705, #940705);
 background-image: -ms-linear-gradient(top, #940705, #940705);
 background-image: -o-linear-gradient(top, #940705, #940705);
 background-image: linear-gradient(to bottom, #940705, #940705);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#940705, endColorstr=#940705);
}

#block{
    width: 100%;
    height: 100%;
    position: absolute;
    visibility:hidden;
    display:none;
    background-color: rgba(22,22,22,0.5);
	top:0;
	left:0;
}

#block:target {
    visibility: visible;
    display: block;
}

@media (min-width: 320px) and (max-width: 479px)
{
	.item {
  border:1px solid #CCC;
  padding: 5px;
  width: 100px;
  margin: 10px;
  font-size:12px;
  font-weight:normal;
  color: #000;
  border-radius:10px;
}
}

</style>
</head>

<body id="body"> 

<div id="header">
    <div id="box1">  
        <div style="float:right; padding:20px;"><span onclick="openNav()" style="cursor:pointer"><a href="#block"><img src="img/menu.png" id="img" border="0" /></a></span></div>
        <div > <img src="img/gzlogo.png" id="ptslogo" border="0" />  	
        </div>  	
        </div>
    </div>
</div>

<div id="mySidenav" class="sidenav">
    <div id="box1" style="padding-bottom:0px;">
    	<div> <!-- style="padding:17px 0 15px 15px;"-->
        <a href="#" class="closebtn" onclick="closeNav()" style="padding-top:1%; padding-right:2%;"><img src="img/btn_close.png" width="32" height="32" border="0" /></a></div>
        <div style="background-image:url(img/header.png); background-repeat:repeat-x; padding-bottom:17px;"> <img src="img/gzlogo.png" id="ptslogo" border="0" /> </div>
    </div>
    <div id="menu_link">
    	<a href="?varx=games-apk">Games</a>
        <a href="?varx=html5">HtmlGames</a>
    </div>
</div>

<div id="iconimg">
    <div class="row">
      <div class="column"><a href="?varx=html5"><img src='img/<?php echo $htmlImg; ?>' border="0"></a></div>
      <div class="column"><a href="?varx=games-apk"><img src='img/<?php echo $btnImg; ?>' border="0"></a></div>
    </div>
</div>

<div id="contentWrap" style="margin-top:auto;">
	<div class="adCntnr">
    	<div class="acco2">
        	<?php

            $queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");
            //echo "SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'";
            if($queryJoin)
            {
			
                while ($DateRow = mysqli_fetch_assoc($queryJoin))
                {
                    $resCatgID	= $DateRow['id'];
                    $resName= $DateRow['category'];
                    $resSbCatgID= $DateRow['sc_id'];
                    $resSName= $DateRow['sub_category'];
                    $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";
                    
                    if($resSName=="Application")
                    {
                        $resSName = "Antivirus";
                    }
               echo'<div style="padding:2px;"></div>
				<div class="expand"><a title="expand/collapse" href="#" style="display: block;">'.ucfirst($resSName).'</a></div>';
                   echo"<div class=\"collapse\">
					<div class=\"accCntnt\" style=\"background-color:#f2f2f2; color:#000000\">
						<ul class=\"container flex\">";


	if(!empty($_SESSION['categref']))
	{
		$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt-portal/portal/";
		$categref 	= $_SESSION['categref'];
		$sesCatg 	= $_SESSION['sesCatg'];
		//echo $html5stat;

		$getContent = $conn->query("SELECT * FROM cms.portal_content WHERE id!=1 and $categref  order by id desc limit 20");

		if($getContent)
		{
			while($items = mysqli_fetch_array($getContent))
			{
				$contentID 		= $items['id'];
				$itemTitle 		= $items['title'];
				$description 	= $items['description'];
				$icon_file_name 		= $items['icon_file_name'];
				$content_file_name = $items['content_file_name'];
				$mime 			= $items['content_file_mime'];
				$ext 			= pathinfo($icon_file_name, PATHINFO_EXTENSION);
					
				if($cat=="games-apk")
				{ $filename = $contenrdir.$cat."/".strtolower($resSName)."/".str_replace(' ', '+',strtolower($itemTitle))."/".str_replace(' ', '+',($content_file_name));
				$file = pathinfo($icon_file_name, PATHINFO_FILENAME);
				
				}
				
				if($cat=="html5")
				{ $filename = $contenrdir.$cat."/".strtolower($resSName)."/".str_replace(' ', '+',strtolower($itemTitle))."/".str_replace(' ', '+',($content_file_name));
				$file = pathinfo($icon_file_name, PATHINFO_FILENAME);
				}
				//debug("$file");
				
				if($ext=="mp4")
				{
					$thumbimg = $file.'.png';
					$preview ='<video width="100%" height="100"  controls preload="metadata">
							<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
						  </video>';
				}
				elseif($ext=="mp3")
				{
					$preview = '<img class="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt-portal/portal/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
				}
				else
				{
					
					$thumbimg = $file.'.png';
					$preview ='<img class="img-thumbnail" src="'.$contenrdir.$cat.'/'.strtolower($resSName).'/'.str_replace(' ', '+',strtolower($itemTitle)).'/'.$thumbimg.'" alt="'.$itemTitle.'">';

				}
				//debug($preview);

					if($cat=="html5")
					{
					 echo"<li class=\"item flex-item\">						  
					 <a href=\"preview.php?varx=$sesCatg&contid=$contentID&category=$resSName\">$preview</a>
					 <div id=\"title\"><a href=\"preview.php?varx=$sesCatg&contid=$contentID\">$itemTitle</a></div>
					 <a href=\"$filename\" class=\"btn\">$btn_play</a>
						  </li>";
					}else{

					echo"<li class=\"item flex-item\">						  
							<a href=\"preview.php?varx=$sesCatg&contid=$contentID&category=$resSName\">$preview</a>
							<div id=\"title\"><a href=\"preview.php?varx=$sesCatg&contid=$contentID\">$itemTitle</a></div>
							<a href=\"$filename\" class=\"btn\">$btn_play</a>
						  </li>";
						}
			} // 2nd while loop
		}
	}	

                echo"</ul>                
					</div>
				</div>";

                } // End of 1st while loop

            } // End of if condtion

			?>
        </div>
    </div>
</div>

<div style="margin-top:70px;"></div>
<script>
function openNav() {
    document.getElementById("mySidenav").style.height = "auto";
	
	document.getElementById("contentWrap").style.marginRight = "80%";
	document.getElementById("contentWrap").style.margin = "auto";
	document.getElementById("box4").style.marginRight = "80%"; 
	/*document.getElementById("nvmg").style.marginRight = "80%";*/
	document.getElementById("proj").style.display = "block";
	/*document.getElementById("block").style.display = "block";
	
	
	*/
}
function closeNav() {
    document.getElementById("mySidenav").style.height = "0";
	
    document.getElementById("contentWrap").style.margin = "auto";
    document.getElementById("box4").style.margin = "0";
	/*document.getElementById("nvmg").style.margin = "auto";*/
	document.getElementById("proj").style.display = "none";
	/*document.getElementById("block").style.display = "none";*/
}

</script>
<div id="block">&nbsp;</div>

</body>
</html>
