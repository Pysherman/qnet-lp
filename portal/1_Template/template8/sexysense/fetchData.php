<?php
	include('inc.php');
	
	class FetchData {
		public $cat;
		public $catCond;
		public $catId;
		public $subCatId = [];
		public $contentId;

		function __construct(){
			if(isset($_GET['cat'])){
				$this->cat = $_GET['cat'];
			}else{
				$this->cat = "Videos";
			}

			if($this->cat === "Videos"){
				$this->catCond = "and (b.sub_category='Asian' or b.sub_category='Blonde' or b.sub_category='Cosplay' or b.sub_category='European')";
			}else if($this->cat === "Wallpaper"){
				$this->catCond = "and (b.sub_category='Asian' or b.sub_category='Cosplay' or b.sub_category='PornStar')";
			}else{
				$this->cat = "Videos";
				$this->catCond = "and (b.sub_category='Asian' or b.sub_category='Blonde' or b.sub_category='Cosplay' or b.sub_category='European')";
			}

			if(isset($_GET['id'])){
				$this->contentId = $_GET['id'];
			}
		}

		function getSubCats($conn){
			$cat = "{$this->cat}%";
			$catCond = $this->catCond;

			$stmt = $conn->prepare("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $catCond and category like ?");
			$stmt->bind_param("s", $cat);
			$stmt->execute();
			$stmt->store_result();
			$stmt->num_rows();

			$stmt->bind_result($catId, $category, $subCatId, $subCategory);

			$data = [];

			while($stmt->fetch()){
				$dataAssoc = [
					"catId" => $catId,
					"subCatId" => $subCatId,
					"subCategory" => $subCategory,
					"category" => $category
				];

				$this->catId = $catId;

				array_push($data, $dataAssoc);
				array_push($this->subCatId, $subCatId);
			}

			$stmt->close();
			return $data;
		}

		function getContents($conn){
			$data = [];
			foreach ($this->subCatId as $subId) {
				$stmt = $conn->prepare("SELECT id, title, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and category_id=? and sub_category_id=? order by id desc limit 30");
				$stmt->bind_param("ii", $this->catId, $subId);
				$stmt->execute();
				$stmt->store_result();
				$stmt->num_rows();

				$stmt->bind_result($id, $title, $filename, $originalFilename, $mime, $subCatId);

				while($stmt->fetch()){
					$dataAssoc = [
						"contentId" => $id,
						"title" => $title,
						"filename" => $filename,
						"originalFileName" => $originalFilename,
						"subCatId" => $subCatId
					];

					array_push($data, $dataAssoc);
				}

				$stmt->close();
			}

			return $data;
		}

		function setContent($conn){
			$stmt = $conn->prepare("SELECT id, title, description, file_name, original_file_name FROM cms.contents WHERE id=?");
			$stmt->bind_param("i", $this->contentId);
			$stmt->execute();
			$stmt->store_result();
			$stmt->num_rows();

			$stmt->bind_result($id, $title, $description, $filename, $originalFilename);

			$data = [];

			while($stmt->fetch()){
				$data = [
					"id" => $id,
					"title" => $title,
					"description" => $description,
					"filename" => $filename,
					"originalFilename" => $originalFilename
				];
			}

			$stmt->close();
			return $data;
		}

		// function setScreenShots($conn){
		// 	$stmt = $conn->prepare("SELECT file_name FROM cms.preview WHERE content_id=?");
		// 	$stmt->bind_param("i", $this->contentId);
		// 	$stmt->execute();
		// 	$stmt->store_result();
		// 	$stmt->get_result();
		// 	$stmt->num_rows();
		// 	$stmt->bind_result($filename);

		// 	$data = [];

		// 	while($stmt->fetch()){
		// 		array_push($data, $filename);
		// 	}

		// 	$stmt->close();
		// 	return $data;
		// }

	}

	$fetchData = new FetchData();
?>