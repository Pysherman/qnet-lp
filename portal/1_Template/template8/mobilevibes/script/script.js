$(function () {

	$('.sideNav').height(($('body').height() - $('.headerWrapper').height()) - 20).css({ top: $('.headerWrapper').height() });
	$('.mainWrapper').height(($('body').height() - $('.headerWrapper').height()) - 60);

	// $('.navLinks li a:first').addClass("active");
	// $('.sideNavLinks li a:first').addClass("active");
	// $('.sideNavLinks li a.active').parent().addClass("active");

	class Navigation {
		constructor() {
			this.toggle = false;
			this.newTL = gsap.timeline()
			this.navTL = this.newTL.to($('.navLinks').children(), {
				duration: .3, x: 100, opacity: 0, stagger: { from: "end", each: .1 },
				onComplete: function () {
					$('.navLinks').css({ display: "none" })
					$('.navMenuBurg').css({ display: "block" })
				}
			})
				.from($('.navMenuBurg').children(), {
					duration: .3, x: 100, opacity: 0, stagger: { from: 1, each: .1 },
					onReverseComplete: function () {
						$('.navLinks').css({ display: "block" })
						$('.navMenuBurg').css({ display: "none" })
					}
				})
			this.navSvgEclipse = gsap.to($('.navBg').children(), { duration: .3, attr: { rx: 100 } })
			this.navLinkUl = gsap.to($('.navLinks'), { duration: .3, paddingRight: 0 })
		}

		toggleActiveLink() {
			const url = $(location).attr("href");

			$.each($('.navLinks li a'), function (i, navLink) {
				const name = url.includes($(navLink).text())
				if (name) {
					$.each($('.navLinks li a'), function (i, navLink) {
						$(navLink).removeClass("active");
						$($('.sideNavLinks li a')[i]).removeClass("active");
						$($('.sideNavLinks li')[i]).removeClass("active");
					})

					$(navLink).addClass("active");
					$($('.sideNavLinks li a')[i]).addClass("active");
					$($('.sideNavLinks li')[i]).addClass("active");
				}
			})
		}

		navBgAnimation(windowDom, headWrapHeight) {
			const width620 = windowDom.matchMedia("(max-width: 620px)");

			if (width620.matches) {

				gsap.to($('.navBg'), { duration: .3, attr: { width: 230, height: headWrapHeight, viewBox: '0 0 200 65' } });

				this.navSvgEclipse.play()

				this.navLinkUl.play()

				this.navTL.play()

			} else {

				gsap.to($('.navBg'), { duration: .3, attr: { width: 451, height: headWrapHeight, viewBox: '0 0 451 65' } });

				this.navSvgEclipse.reverse()

				this.navLinkUl.reverse()

				this.navTL.reverse()
			}
		}

		showSideMenu() {
			$('.sideNav').css({ display: "block" })
			gsap.to($('.navBg'), {
				duration: .15, attr: { width: 1000, viewBox: '0 0 1000 65' },
				onComplete: function () {
					$('.headerWrapper').css({ background: "#1A659E" });
					gsap.to($('.navBg'), { duration: .15, delay: .1, attr: { width: 230, viewBox: '0 0 200 65' } })
				}
			})
			gsap.to('.sideNav', { duration: .3, right: 0 });
			gsap.to($('.navBg').children(), {
				duration: .15, attr: { rx: 1000, cx: 1000 },
				onComplete: function () {
					gsap.to($('.navBg').children(), { duration: .15, delay: .1, attr: { rx: 100, cx: 225.5, fill: "#004E89" } })
				}
			})

			gsap.to($('.navMenuBurg').children('span:nth-child(1)'), {
				duration: .15, top: 10,
				onComplete: function () {
					gsap.to($('.navMenuBurg').children('span:nth-child(1)'), { duration: .15, rotation: 45 })
					gsap.to($('.navMenuBurg').children('span:nth-child(2)'), { duration: .15, opacity: 0 })
				}
			});
			gsap.to($('.navMenuBurg').children('span:nth-child(3)'), {
				duration: .15, top: 10,
				onComplete: function () {
					gsap.to($('.navMenuBurg').children('span:nth-child(3)'), { duration: .15, rotation: -45 })
				}
			});
		}

		hideSideMenu() {
			gsap.to($('.navBg'), {
				duration: .3, attr: { width: 1000, viewBox: '0 0 1000 65' },
				onComplete: function () {
					$('.headerWrapper').css({ background: "#004E89" });
					gsap.to($('.navBg'), { duration: .15, delay: .1, attr: { width: 230, viewBox: '0 0 200 65' } })
				}
			})
			gsap.to('.sideNav', {
				duration: .3, right: -200,
				onComplete: function () {
					$('.sideNav').css({ display: "none" })
				}
			});
			gsap.to($('.navBg').children(), {
				duration: .3, attr: { rx: 1000, cx: 1000 },
				onComplete: function () {
					gsap.to($('.navBg').children(), { duration: .15, delay: .1, attr: { rx: 100, cx: 225.5, fill: "#1A659E" } })
				}
			})

			gsap.to($('.navMenuBurg').children('span:nth-child(1)'), {
				duration: .15, rotation: 0,
				onComplete: function () {
					gsap.to($('.navMenuBurg').children('span:nth-child(1)'), { duration: .15, top: 0 })
					gsap.to($('.navMenuBurg').children('span:nth-child(2)'), { duration: .15, opacity: 1 })
				}
			})

			gsap.to($('.navMenuBurg').children('span:nth-child(3)'), {
				duration: .15, rotation: 0,
				onComplete: function () {
					gsap.to($('.navMenuBurg').children('span:nth-child(3)'), { duration: .15, top: 20 })
				}
			})
		}

		events() {

			$(window).on('resize', (e) => {
				const headWrapHeight = $('.headerWrapper').height()

				$('body').height("100vh")
				$('.sideNav').height(($('body').height() - $('.headerWrapper').height()) - 20).css({ top: $('.headerWrapper').height() });

				if (this.toggle) {
					gsap.to('.sideNav', {
						duration: .3, right: -200,
						onComplete: function () {
							$('.sideNav').css({ display: "none" })
						}
					});
					gsap.to($('.navMenuBurg').children('span:nth-child(1)'), {
						duration: .15, rotation: 0,
						onComplete: function () {
							gsap.to($('.navMenuBurg').children('span:nth-child(1)'), { duration: .15, top: 0 })
							gsap.to($('.navMenuBurg').children('span:nth-child(2)'), { duration: .15, opacity: 1 })
						}
					})

					gsap.to($('.navMenuBurg').children('span:nth-child(3)'), {
						duration: .15, rotation: 0,
						onComplete: function () {
							gsap.to($('.navMenuBurg').children('span:nth-child(3)'), { duration: .15, top: 20 })
						}
					})

					gsap.to('.headerWrapper', { duration: .3, background: "#004E89" })
					gsap.to($('.navBg').children(), { duration: .3, attr: { fill: "#1A659E" } })
					this.toggle = false;
				}

				this.navBgAnimation(e.target, headWrapHeight);
			})

			$('.navMenuBurg').on('click', (e) => {
				if (!this.toggle) {
					this.showSideMenu();
					$('.sideNav').height(($('body').height() - $('.headerWrapper').height()) - 20);
					this.toggle = true
				} else {
					this.hideSideMenu();
					this.toggle = false;
				}
			})
		}
	}


	const navigation = new Navigation;

	navigation.events();
	navigation.navBgAnimation(window, $('.headerWrapper').height());
	navigation.toggleActiveLink();

	$('#accordion').accordion({
		classes: {
			"ui-accordion-header-active": "highlight",
			"ui-accordion-header": "whiteText"
		},
		header: "h3",
		heightStyle: "content"
	});
})