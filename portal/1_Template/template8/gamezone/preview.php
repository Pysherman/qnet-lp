<!DOCTYPE html>
<html lang="ES">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Powerland</title>
	<link href="https://fonts.googleapis.com/css2?family=Cabin:wght@400;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" />
	<link rel="stylesheet" href="stylesheets/styles.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fontisto@v3.0.4/css/fontisto/fontisto.min.css">

	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous" defer></script>
	<script src="script/script.js" defer></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<?php
	$strstatus=0;
	if(isset($_GET['status'])){
		$strstatus=$_GET['status'];
	}else{
		$strstatus=0;
	}

	if($strstatus <> 0){
		header("Location: http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/nl/gamezone/gm1/errorstatus.php");
    	die();
	}

	include('fetchData.php'); 

	$setContent = $fetchData->setContent($conn);

	$screenshots = $fetchData->setScreenShots($conn);

	$conn->close();
?>
<body>
	<header class="headerWrapper">
		<section class="header">
			<div class="logo">
				<h2>POWERLAND</h2>
			</div>
			<nav class="navigation">
				<ul class="navLinks">
					<li><a href="index.php?cat=Android" class="active">Android</a></li>
					<li><a href="index.php?cat=Html5">Html5</a></li>
				</ul>
				<div class="navMenuBurg">
					<span></span>
					<span></span>
					<span></span>
				</div>
				<svg class="navBg" width="330" height="69" viewBox="0 0 451 65" fill="none" xmlns="http://www.w3.org/2000/svg">
					<ellipse cx="225.5" cy="-8.5" rx="225.5" ry="120" fill="#3C3F41"/>
				</svg>
			</nav>
		</section>
	</header>
	<div class="lang">
	<span class='langson'>Idioma:</span>
				<a href="#googtrans(en|en)" class="lang-select langson langson2" data-lang="en">English</a>
				<a href="#googtrans(en|es)" class="lang-select langson langson2" data-lang="es">Spanish</a>
	</div>
	<section class="mainWrapper">
		<aside class="sideNav">
			<ul class="sideNavLinks">
				<li class="active"><a href="index.php?cat=Android" class="active">Android</a></li>
				<li><a href="index.php?cat=Html5">Html5</a></li>
			</ul>
		</aside>
		<main class="previewWrapper">
			<?php
					if(isset($setContent)):
						$contentName = pathinfo($setContent['filename'], PATHINFO_FILENAME);
						$ext = pathinfo($setContent['filename'], PATHINFO_EXTENSION);
						$thumbnail = "";

						switch($ext){
							case "mp4":
								$thumbnail = "<video width='100%' height='100%' preload='metadata'><source src='https://s3-ap-southeast-1.amazonaws.com/qcnt/{$setContent['filename']}#t=10' type='video/mp4'></video>";
								break;
							case "apk":
							case "xapk":
								$thumbnail = "<img src='https://s3-ap-southeast-1.amazonaws.com/qcnt/content/{$contentName}.png' alt='thumbnail' />";
								break;
							default:
								$thumbnail = "<img src='https://s3-ap-southeast-1.amazonaws.com/qcnt/content/{$contentName}.png' alt='thumbnail' />";
								break;
						}

			?>
						<div class="previewHeader">
							<a href="index.php?cat=<?php echo $fetchData->cat; ?>">
								<i class="fi fi-arrow-left"></i>
								<span>Back</span>
							</a>
						</div>
						<div class="previewBody">
							<div class="previewDetails">
								<div class="thumbWrapper">
									<div class="thumbnail">
										<?php echo $thumbnail; ?>
									</div>
									<div class="titleCta">
										<span><?php echo $setContent['title']; ?></span>
										<a href="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/<?php echo $contentName.'.'.$ext; ?>" class="previewDownload">
											<i class="fi fi-download"></i>
											Download
										</a>
									</div>
								</div>
								<div class="descripWrapper">
									<h5>Information</h5>
									<p><?php echo $setContent['description']; ?></p>
								</div>
							</div>
							<div class="previewPreview">
								<?php
									if($ext === "apk" || $ext === "xapk"):
								?>
										<h5>Screenshots</h5>
										<div class="screensCont">
								<?php
											foreach ($screenshots as $img):
											// while(mysqli_stmt_fetch($stmtScreens)):
								?>
												<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/<?php echo $img; ?>">
								<?php
											// endwhile;
											endforeach;
								?>
										</div>
								<?php
									endif;
								?>
							</div>
						</div>
			<?php
					endif;
			?>
		</main>
	</section>
	<svg class="svgBg1" width="519" height="518" viewBox="0 0 519 518" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M390.684 426.926C313.645 478.689 209.573 475.771 103.471 485.796C-2.10736 496.276 -109.681 520.188 -181.039 479.004C-252.922 437.365 -288.099 330.594 -286.931 229.601C-285.762 128.607 -247.758 33.3554 -185.303 -50.3485C-122.883 -134.542 -35.5565 -207.713 46.9312 -203.748C129.454 -199.294 207.173 -117.214 293.149 -43.5996C379.615 29.9803 473.848 95.1306 495.735 181.19C518.147 267.705 467.722 375.163 390.684 426.926Z" fill="#C9C8C8"/>
	</svg>
	<svg class="svgBg2" width="484" height="479" viewBox="0 0 484 479" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M708.684 661.926C631.645 713.689 527.573 710.771 421.471 720.796C315.893 731.276 208.319 755.188 136.961 714.004C65.0778 672.365 29.9006 565.594 31.0692 464.601C32.2379 363.607 70.242 268.355 132.697 184.652C195.117 100.458 282.444 27.2872 364.931 31.2518C447.454 35.7061 525.173 117.786 611.149 191.4C697.615 264.98 791.848 330.131 813.735 416.19C836.147 502.705 785.722 610.163 708.684 661.926Z" fill="#C2BFBF"/>
	</svg>

	<!-- Translate Custom JS-->
	<script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }
	function triggerHtmlEvent(element, eventName) {
	  var event;
	  if (document.createEvent) {
		event = document.createEvent('HTMLEvents');
		event.initEvent(eventName, true, true);
		element.dispatchEvent(event);
	  } else {
		event = document.createEventObject();
		event.eventType = eventName;
		element.fireEvent('on' + event.eventType, event);
	  }
	}
	jQuery('.lang-select').click(function() {
	  var theLang = jQuery(this).attr('data-lang');
	  jQuery('.goog-te-combo').val(theLang);
	  //alert(jQuery(this).attr('href'));
	  window.location = jQuery(this).attr('href');
	  location.reload();

	});
</script>
<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</body>
</html>