<!DOCTYPE html>
<html lang="sk">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>
	<link rel="stylesheet" href="css/style.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous" defer></script>
	<script src="js/script.js" defer></script>
	<script src="js/jquery.slides.min.js" defer></script>
	<title>Zdroj Zábavy</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
	<main class="mainContainer">
		<!-- main header -->
		<header class="mainHeader">
			<!-- logo container -->
			<section class="logoContainer">
				<h1>Zdroj Zábavy</h1>
			</section>
			<!-- seacrh nav container -->
			<section class="searchNavContainer">
				<!-- search container -->
				<div class="searchContainer">
					<input type="search" class="searchField" id="searchField" name="search" placeholder="Obsah hľadania"/>
					<button type="button" class="searchBtn" id="searchBtn">
						<i class='bx bx-search-alt'></i>
					</button>
				</div>
				<!-- menu container -->
				<div class="menuContainer">
					<!-- menu button container -->
					<div class="menuBtnContainer">
						<!-- menu button -->
						<button class="menuBtn" id="menuBtn">
							<span></span>
							<span></span>
							<span></span>
						</button>
					</div>
					<!-- category container -->
					<div class="categoryContainer">
						<!-- games-apk -->
						<div class="catBtn" data-category="games-apk">
							<i class='bx bxl-android' ></i>
							<span>Android</span>
						</div>
						<!-- html5 -->
						<div class="catBtn catActive" data-category="html5">
							<i class='bx bxl-html5' ></i>
							<span>HTML</span>
						</div>
					</div>
				</div>
			</section>
			<!-- subcategory container -->
			<div class="subcatheader">
			<section class="subcategoryContainer" id="subcategoryContainer"></section>
			<section class="subcategoryContainer sublang2" id="subcategoryContainer">
				<span>Jazyk:</span>
				<a href="#googtrans(en|en)" class="lang-sk lang-select langsk languageSk1" data-lang="en">EN</a>
				<a href="#googtrans(en|sk)" class="lang-sk lang-select langsk languageSk" data-lang="sk">SK</a>
			</section>	
			</div>
		</header>
		<section class="contentContainer" id="contentContainer"></section>
	</main>


<!-- Translate Custom JS-->
	<script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }
	function triggerHtmlEvent(element, eventName) {
	  var event;
	  if (document.createEvent) {
		event = document.createEvent('HTMLEvents');
		event.initEvent(eventName, true, true);
		element.dispatchEvent(event);
	  } else {
		event = document.createEventObject();
		event.eventType = eventName;
		element.fireEvent('on' + event.eventType, event);
	  }
	}
	jQuery('.lang-select').click(function() {
	  var theLang = jQuery(this).attr('data-lang');
	  jQuery('.goog-te-combo').val(theLang);
	  //alert(jQuery(this).attr('href'));
	  window.location = jQuery(this).attr('href');
	  location.reload();

	});
</script>
<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</body>
</html>