<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>
	<link rel="stylesheet" href="css/style.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous" defer></script>
	<script src="js/script.js" defer></script>
	<script src="js/jquery.slides.min.js" defer></script>
	<title>Gamezone 7</title>
</head>
<body>
	<main class="mainContainer">
		<!-- main header -->
		<header class="mainHeader">
			<!-- logo container -->
			<section class="logoContainer">
				<h1>Gamezone</h1>
			</section>
			<!-- seacrh nav container -->
			<section class="searchNavContainer">
				<!-- search container -->
				<div class="searchContainer">
					<input type="search" class="searchField" id="searchField" name="search" placeholder="Search Content"/>
					<button type="button" class="searchBtn" id="searchBtn">
						<i class='bx bx-search-alt'></i>
					</button>
				</div>
				<!-- menu container -->
				<div class="menuContainer">
					<!-- menu button container -->
					<div class="menuBtnContainer">
						<!-- menu button -->
						<button class="menuBtn" id="menuBtn">
							<span></span>
							<span></span>
							<span></span>
						</button>
					</div>
					<!-- category container -->
					<div class="categoryContainer">
						<!-- games-apk -->
						<div class="catBtn" data-category="games-apk">
							<i class='bx bxl-android' ></i>
							<span>Android</span>
						</div>
						<!-- html5 -->
						<div class="catBtn catActive" data-category="html5">
							<i class='bx bxl-html5' ></i>
							<span>HTML</span>
						</div>
					</div>
				</div>
			</section>
			<!-- subcategory container -->
			<section class="subcategoryContainer" id="subcategoryContainer"></section>
		</header>
		<section class="contentContainer" id="contentContainer"></section>
	</main>
</body>
</html>