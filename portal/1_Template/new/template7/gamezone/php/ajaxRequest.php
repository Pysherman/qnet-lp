<?php
	require('connection.php');

	function getSubCategories($cat, $conn){
		if($cat == "games-apk"){
			$catCondition = "and (b.sub_category='Action' or b.sub_category='Arcade' or b.sub_category='Strategy')";
		}else if($cat == "html5"){
			$catCondition = "and (b.sub_category='Embed-Games')";
		}else{
			$cat = "games-apk";
			$catCondition = "and (b.sub_category='Action' or b.sub_category='Arcade' or b.sub_category='Strategy')";
		}

		$querySubMenu = "SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $catCondition and category like ?";
		$sanitizeCat = filter_var($cat, FILTER_SANITIZE_SPECIAL_CHARS);
		$bindCat = "{$sanitizeCat}%";

		$stmt = mysqli_stmt_init($conn);
		mysqli_stmt_prepare($stmt, $querySubMenu);
		$bindParam = mysqli_stmt_bind_param($stmt, "s", $bindCat);

		if($bindParam){
			mysqli_stmt_execute($stmt);
			$subCat = [];
			$result = mysqli_stmt_get_result($stmt);
			while($data = mysqli_fetch_assoc($result)){
				$catId = $data['id'];
				$subCatId = $data['sc_id'];
				$subCategory = $data['sub_category'];

				$dataAssoc = ["catId" => $catId, "subCatId" => $subCatId, "subCategory" => ucwords($subCategory)];
				array_push($subCat, $dataAssoc);
			}
			echo json_encode($subCat);
			mysqli_stmt_close($stmt);
		}
	}

	function getContents($catId, $subCatId, $conn){
		$catId = filter_var($catId, FILTER_SANITIZE_SPECIAL_CHARS);
		$subCatId = filter_var($subCatId, FILTER_SANITIZE_SPECIAL_CHARS);

		$getContent = "SELECT id, title, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and category_id=? and sub_category_id=? order by id desc limit 27";

		$stmt = mysqli_stmt_init($conn);
		mysqli_stmt_prepare($stmt, $getContent);
		mysqli_stmt_bind_param($stmt, "ii", $catId, $subCatId);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_store_result($stmt);

		$result = mysqli_stmt_num_rows($stmt);
		if($result > 0){
			mysqli_stmt_bind_result($stmt, $id, $title, $fileName, $originalFileName, $mime, $subCategoryId);
			$data = [];
			while(mysqli_stmt_fetch($stmt)){
				$dataAssoc = [
					"contentId" => $id,
					"title" => $title,
					"fileName" => pathinfo($fileName, PATHINFO_FILENAME),
					"fileExt" => pathinfo($fileName, PATHINFO_EXTENSION),
                    "origFileName" => pathinfo($originalFileName, PATHINFO_FILENAME),
                    "playFile" => $fileName
				];

				array_push($data, $dataAssoc);
			}

			echo json_encode($data);
			mysqli_stmt_close($stmt);
		}
	}

	function showContentDetails($contentId, $conn){
		$contentId = filter_var($contentId, FILTER_SANITIZE_SPECIAL_CHARS);
		$getDetails = "SELECT title, file_name, original_file_name, description FROM cms.contents WHERE id = ?";
		$data = [];

		$stmt = mysqli_stmt_init($conn);
		mysqli_stmt_prepare($stmt, $getDetails);
		mysqli_stmt_bind_param($stmt, "i", $contentId);
		mysqli_stmt_execute($stmt);

		$resultDetails = mysqli_stmt_get_result($stmt);

		while($dataDetails = mysqli_fetch_assoc($resultDetails)){
			$dataAssoc = [
				"title" => $dataDetails['title'],
				"fileName" => pathinfo($dataDetails['file_name'], PATHINFO_FILENAME),
				"fileExt" => pathinfo($dataDetails['file_name'], PATHINFO_EXTENSION),
				"origFileName" => pathinfo($dataDetails['original_file_name'], PATHINFO_FILENAME),
                "description" => $dataDetails['description'],
                "playFile" => $dataDetails['file_name']
			];

			$ext = pathinfo($dataDetails['file_name'], PATHINFO_EXTENSION);

			if($ext == "apk" || $ext == "xapk"){
				$screenshots = "SELECT file_name FROM cms.preview WHERE content_id = ?";
				mysqli_stmt_prepare($stmt, $screenshots);
				mysqli_stmt_bind_param($stmt, "i", $contentId);
				mysqli_stmt_execute($stmt);

				$resultScreens = mysqli_stmt_get_result($stmt);
				$imgContainer = [];

				while($dataScreens = mysqli_fetch_assoc($resultScreens)){
					array_push($imgContainer, $dataScreens['file_name']);
				}

				$imageArr = ["screenshots" => $imgContainer];
			}

			if(!empty($imageArr)){
				$merged = array_merge($dataAssoc, $imageArr);
				array_push($data, $merged);
			}else{
				array_push($data, $dataAssoc);
			}
		}

		echo json_encode($data);
		mysqli_stmt_close($stmt);
	}

	if(isset($_POST['cat'])){
		getSubCategories($_POST['cat'], $conn);
		mysqli_close($conn);
	}else if(isset($_POST['contentId'])){
		showContentDetails($_POST['contentId'], $conn);
		mysqli_close($conn);
	}else{
		$contentType = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : "";
		if(stripos($contentType, 'application/json') === false){
			throw new Exception('Content-Type must be application/json');
		}

		$body = file_get_contents("php://input");
		$bodyData = json_decode($body, true);

		if(!is_array($bodyData)){
			throw new Exception('Failed to decode JSON object');
		}

		if(isset($bodyData['subCatId'])){
			$catId = $bodyData['catId'];
			$subCatId = $bodyData['subCatId'];
			getContents($catId, $subCatId, $conn);
			mysqli_close($conn);
		}
	}
?>