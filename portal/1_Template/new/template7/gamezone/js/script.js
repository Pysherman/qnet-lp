$(function(){
    // set search input width
	$('#searchField').innerWidth(`${$('.searchContainer').width() - ($('#menuBtn').width() + 10)}`);
	let catTog = "close";
	let searchTog = "open";
    let searchFieldWidth = $('#searchField').innerWidth();

    class Search{
		// set the shape of the search input to circle
		maintainSizeToCircle = () => {
			let sumOf =  ($('#searchField').innerWidth() - (($('.catBtn').width() + 10) * $('.catBtn').length) - $('.catBtn').width());
			// value for the right position of search button
			let rightValue =  ($('.catBtn').width() + 10) * $('.catBtn').length + (65 + (sumOf));

			$('#searchField').css({width:`${$('.catBtn').width()}px`});
			$('#searchBtn').css({right:`${rightValue}px`});
		}

		showResult = (inputValue) => {
			// declare array containers for result of the search
			const [contentNames, inputValResult, contentResult] = [[], [], []];
			// convert text to lowercase
			const inputVal = inputValue.toLowerCase();
			// get all the title of the contents, store in to contentNames array
			$('.contentPage .content .contentName p').each(function(key, value){
				contentNames.push($(value).text().toLowerCase());
			})
			// loop through all the contentNames array, if there is a match in the string then get the index and store it to inputValResult array
			$.each(contentNames, function(index, value){
				if(value.includes(inputVal)){
					inputValResult.push(index);
				}
			})
			// lop through all the contentBlock and assign the corresponding index then store them in contentResult array
			$.each(inputValResult, function(index, value){
				// clone content
				contentResult.push($('.contentBlock').eq(value).clone(true, true))
			})

			if($('.searchResultContainer') != undefined){
				$('.searchResultContainer').remove();
			}

			// hide all the main div in the contentContainer
			$('.contentHeader').css({display:'none'});
			$('#contentWrapper').css({display:'none'});
			$('#contentPagination').css({display:'none'});

			$('#contentContainer').append(`
				<div class="searchResultContainer">
					<div class="searchHeader">
						<span>Search Result</span>
					</div>
					<div class="searchResult"></div>
				</div>`)

			if(inputValResult == undefined || inputValResult.length == 0){
				// show no result if no match in the search
				$('.searchResult').append(`<h1>There are no results that match your search</h1>`)

			}else{
				// hide all content
				$('.contentPage .contentBlock').each(function(key, elem){
					$(this).css({display:"none"});
				})

				// show all content that matched the search
				$.each(contentResult, function(key, elem){
					// $(elem).css({display:'block'});
					$('.searchResult').append(elem);
					$(elem).fadeIn(1000);
				})
			}
		}
	}
    
    class Category{
        openCategory = () => {
			// initial width of the menu button
			let catWidth = 60;
			// add inside shadow
			$('.menuBtn').css({boxShadow: "inset -1px -1px 2px #FFFFFF, inset 1px 1px 2px #C2CDDC, inset -1px -1px 1px rgba(255, 255, 255, 0.6)"});
			// get initial width of search field | get width of the first cat btn then add 10 then multiply the sum by how many cat btn elements 
			let sumOf =  ($('#searchField').innerWidth() - (($('.catBtn').width() + 10) * $('.catBtn').length) - $('.catBtn').width());
			// value for the right position of search button
			let rightValue =  ($('.catBtn').width() + 10) * $('.catBtn').length + (65 + (sumOf));
			// animate top property
			$('.menuBtn span').animate({top: "22px"}, 200,
				function(){
					// after animate top
					// set width
					$('#searchField').animate({width:`${$('.catBtn').width()}px`}, 300);
					// set right property
					$('#searchBtn').animate({right:`${rightValue}px`}, 300);

					// set menu burger icon to circles
					$('.menuBtn span').css({
						backgroundColor: "#5C677D",  
					}).animate({
						borderRadius: "5px",
						width:"16%",
						left: "42%",
						height:"8px"
					}, 50, function(){
						$('.menuBtn span:nth-child(1)').animate({left: "10px"}, 200);
						$('.menuBtn span:nth-child(2)').animate({left: "21px"}, 200);
						$('.menuBtn span:nth-child(3)').animate({left: "32px"}, 200);
					});
				}
			);
			// animate right pos of all category btn
			$('.catBtn').each(function(){
				$(this).fadeIn(300).animate({right:`${catWidth}px`}, 300);
				// increment variable catWidth
				catWidth += 60;
				// set var catTog to open
				catTog = "open";
			})

			searchTog = "close";
        }

        closeCategory = () => {
            // animate menu burger
            $('.menuBtn span').animate({left: "42%"}, 200,
                function(){
                    // se outside shadow
                    $('.menuBtn').css({boxShadow: "-2px -2px 8px #FFFFFF, 2px 2px 8px #C2CDDC, -1px -1px 1px rgba(255, 255, 255, 0.6)"});
                    // set menu burger to bars
                    $('.menuBtn span').css({backgroundColor: "#BF0603"}).animate({width:"50%", height:"4px", left: "25%"}, 200);
                    $('.menuBtn span:nth-child(1)').animate({top: "14px"}, 200);
                    $('.menuBtn span:nth-child(2)').animate({top: "24px"}, 200);
                    $('.menuBtn span:nth-child(3)').animate({top: "34px"}, 200);
    
                    if(searchTog == "close"){
                        // execute if search input is close
                        // reassign the width of search input 
                        let searchContainerWidth = $('.searchContainer').width() - ($('#menuBtn').width() + 10);
                        $('#searchField').animate({width:`${searchContainerWidth}px`}, 300);
                        $('#searchBtn').animate({right:"65px"}, 300);
                        // set to open
                        searchTog = "open";
                    }else if(searchTog == "open"){
                        $('#searchField').animate({width:`${searchFieldWidth}px`}, 300);
                        $('#searchBtn').animate({right:"65px"}, 300);
                    }
                    // hide all category buttons
                    $('.catBtn').each(function(){
                        $(this).animate({right:"0px"}, 300).fadeOut(300);
                        catTog = "close";
                    })
    
                }
            );
        }
    }

    class SubCategory{
        showSubCategories = (catName) => {
			// make request send caterogy name as argument
			$.ajax({
				type: 'POST',
				url: 'php/ajaxRequest.php',
				data: `cat=${catName}`,
				dataType: "json",
				success: (data) => {
					$.each(data, (key, value) => {
						// destructure value
						const {catId, subCatId, subCategory} = value;
						// create new elements and append to subcategory container
						$('#subcategoryContainer').append(`
							<div class="subCategory" data-subCategory="${subCategory.replace("-", " ")}">
								<input type="hidden" value="${catId}" />
								<input type="hidden" value="${subCatId}" />
								<span>${subCategory.replace("-", " ")}</span>
							</div>
						`)
						// add class active to first sub category
						$('.subCategory:first').addClass("subCatActive");

					})
					// slide to left
					$('.subCategory').animate({
						marginLeft: "0px",
						opacity: 1
					}, 300)
					// add click event
					this.clickSubCategory(catName);
					// drag x functionalities
					this.subCategoriesScroll();

					const subCatParams = {
						catId: $('.subCategory:first').children('input:nth-child(1)').val(),
						subCatId: $('.subCategory:first').children('input:nth-child(2)').val(),
						catName: catName,
						subCatName: $('.subCategory:first').children('span').text()
					}

					const content = new Content;

					// content.removeContents();
					// show all the contents of current sub category
					content.showContent(subCatParams);
				},
				error: (err) => console.log(err)

			})
        }
        
        clickSubCategory = (catName) => {
			// click event
			$('.subCategory').on('click', function(e){
				e.stopPropagation()
				// clear search field
				if($('#searchField').val() != ""){
					$('#searchField').val("");
				}
				// remove class to previous sub category
				$('.subCategory').each(function(){
					$(this).removeClass("subCatActive");
				})
				// add class to current sub category
				$(this).addClass("subCatActive");
				// close the category if its open
				if(catTog == "open"){
					const category = new Category;
					category.closeCategory();
				}

				const subCatParams = {
					catId: $(this).children('input:nth-child(1)').val(),
					subCatId: $(this).children('input:nth-child(2)').val(),
					catName: catName,
					subCatName: $(this).children('span').text()
				}

				const content = new Content;
				// show all contents of current sub category
				content.showContent(subCatParams);
			})
        }
        
        subCategoriesScroll = () => {
			// mouse down toggle
			let isDown = false;
			// initial x position
			let startX;
			// initial scroll
			let scrollLeft;

			// $('#subcategoryContainer').on('click', (e) => {
			// 	this.clickSubCategory();
			// })

			$('#subcategoryContainer').on('mousedown', function(e){
				isDown = true;
				startX = e.pageX - $(this).offset().left;
				scrollLeft = $(this).scrollLeft();
			})

			$('#subcategoryContainer').on('mousemove', function(e){
				if(!isDown) return;
				e.preventDefault();
				const x = e.pageX - $(this).offset().left;
				const walk = (x - startX);
				$(this).scrollLeft(scrollLeft - walk);
				// $('.subCategory').off('click');
			})

			$('#subcategoryContainer').on('mouseup', function(){
				isDown = false;
			})

			$('#subcategoryContainer').on('mouseleave', function(){
				isDown = false;
			})

			$('#subcategoryContainer').on('touchstart', function(e){
				isDown = true;
				startX = e.originalEvent.touches[0].pageX - $(this).offset().left;
				scrollLeft = $(this).scrollLeft();
				console.log("touchStart")
			})

			$('#subcategoryContainer').on('touchmove', function(e){
				if(!isDown) return;
				e.preventDefault();

				const x = e.originalEvent.touches[0].pageX - $(this).offset().left;
				const walk = (x - startX);
				$(this).scrollLeft(scrollLeft - walk);
				// $('.subCategory').off('click');

			})

			$('#subcategoryContainer').on('touchend', function(){
				isDown = false;
				console.log("touchEnd")
			})

			$('#subcategoryContainer').on('touchcancel', function(){
				isDown = false;
			})
		}
    }

    class Content{
        pagination = () => {
			// pages count
			const pages = $('.contentPage').length;
			// initial number of bullets
			let bulletNum = 1;
			// append container for page bullets
			$('#contentPagination').append(`<div class="pageBulletWrapper" id="pageBulletWrapper"></div>`)
			// create bullets for each pages
			for(let i = 0; i < pages; i++){
				$('#pageBulletWrapper').append(`
					<div class="pageBullet${bulletNum} pageBullet" data-bullet-name="contentPage${bulletNum}">
						<span></span>
					</div>
				`)
				// increment number of bullets
				bulletNum += 1;
			}
			// add class to first bullet
			$('.pageBullet:first').addClass('bulletActive');

			$('.pageBullet').on('click', function(){
				// remove class of previous bullet
				$('.pageBullet').removeClass('bulletActive');
				// add class to current bullet
				$(this).addClass('bulletActive');
				// hide previous page
				$('.contentPage').css({display: "none"});
				// hide content of previous page
				$('.contentBlock').css({display: "none"});
				// show current page
				$(`.${$(this).data("bulletName")}`).css({display: "block"});
				// show content of current page
				$(`.${$(this).data("bulletName")} .contentBlock`).fadeIn(1000);
				// const width = $(`.${$(this).data("bulletName")}`).innerWidth();
				// const index = $(`.${$(this).data("bulletName")}`).index();
				// const indexNeg = $(`.${$(this).data("bulletName")}`).index() - 1;
				// $(`.${$(this).data("bulletName")}`).css({left:`-${width * index}px`});
				// $('.contentPage1').css({left:`-${width * index}px`})
			})
        }
        
        removeContents = () => {
			// replace all contents and header
			if($('#contentContainer').children().length > 0){
				$('#contentContainer').empty();
			}
		}

        showContent = ({catId, subCatId, catName, subCatName}) => {

			const contentDir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
			const thumbExt = ".png";
			// thumbnail container
            let thumbnail = "";
            // link container
            let link = "";
			// fetch contents
			$.ajax({
				type:'POST',
				url:'php/ajaxRequest.php',
				contentType:'application/json',
				data: JSON.stringify({
					catId: catId,
					subCatId: subCatId
				}),
				dataType: 'json',
				beforeSend: () => {
					// replace all contents before fetch content
					this.removeContents();
					// load spinner before adding contents
					$('#contentContainer').append(`
						<div class="loader">
							<span></span>
						</div>
					`)
				},
				success: (data, textStatus, xhr) => {
					if(xhr.status == 200){
						// set first character to uppercase
                        let catNameCaps = catName.charAt(0).toUpperCase() + catName.slice(1);
                        
                        if(catNameCaps == "Games-apk"){
                            catNameCaps = "Android";
                        }else{
                            catNameCaps = "HTML";
                        }
						// append header, pagination and wrapper
						$('#contentContainer').append(`
							<section class="contentHeader">
								<span>${catNameCaps} / ${subCatName}</span>
							</section>
							<section class="contentWrapper" id="contentWrapper"></section>
							<section class="contentPagination" id="contentPagination"></section>
						`);
						// content limit per page
						let contentLimit;
						// how many pages
						let divLimit;
						// container for the converted array
						const newData = [];
						// page number
						let pageNum = 1;

						// pagination based on the width of the browser
						if(window.matchMedia('(min-width:600px)').matches){
							contentLimit = 10;
							divLimit = data.length / contentLimit
						}else if(window.matchMedia('(max-width:599px)').matches){
							contentLimit = 7;
							divLimit = data.length / contentLimit
						}

						for(let i = 0; i < divLimit; i++){
							// divide the contents then push it to the container
							newData.push(data.splice(0, contentLimit))
						}
						// loop through to the new array
						$.each(newData, function(key, data){
							// add pageNum to class property
							$('#contentWrapper').append(`
								<div class="contentPage${pageNum} contentPage" data-page-name="contentPage${pageNum}"></div>
							`)
							// loop through to the newData values
							$.each(data, function(key, content){
								// destructure the content
								const {contentId, title, fileName, fileExt, origFileName, playFile} = content;
								// check for catName and assign the thumbnail
								switch(catName){
									case 'games-apk':
                                        thumbnail = `<img src="${contentDir}content/${fileName}${thumbExt}" alt="${title}" />`;
                                        link= `
                                            <a href="${contentDir}content/${fileName}.${fileExt}" class="ctaDownload ctaBtn">
                                                <i class='bx bxs-download' ></i>
                                            </a>                                        
                                        `
										break;
									case 'html5':
                                        thumbnail = `<img src="${contentDir}content/${origFileName}${thumbExt}" alt="${title}" />`;
                                        link= `
                                            <a href="${playFile}" class="ctaDownload ctaBtn">
                                                <i class='bx bxs-joystick' ></i>
                                            </a>                                        
                                        `
										break;
									default:
                                        thumbnail = `<img src="${contentDir}content/${fileName}${thumbExt}" alt="${title}" />`;
                                        link= `
                                            <a href="${contentDir}content/${fileName}.${fileExt}" class="ctaDownload ctaBtn">
                                                <i class='bx bxs-download' ></i>
                                            </a>                                        
                                        `
										break;
								}
								// append content
								$(`.contentPage${pageNum}`).append(`
									<div class="contentBlock">
										<div class="content" data-category="${catName}" data-subcategory="${subCatName}">
											<div class="contentThumb">
												${thumbnail}
											</div>
											<div class="contentName">
							 					<p>${title}</p>
							 				</div>
							 				<div class="contentCta">
							 					<a href="" class="ctaInfo ctaBtn">
							 						<input type="hidden" value=${contentId} />
							 						<i class='bx bx-info-square' ></i>
							 					</a>
							 					${link}
							 				</div>
										</div>
									</div>
								`)
							})
							// increment page number
							pageNum += 1;
						})
						// display first page
						$('.contentPage:first').css({display:"block"});
						// add animation on all content
						$('.contentPage:first .contentBlock').fadeIn(800);
						// pagination
						this.pagination();
						// show preview when info btn clicked
						this.contentPreview(subCatName, catName);

						// remove loader spinner
						$('.loader').remove();

						// $('#searchField').innerWidth(`${$('.searchContainer').width() - ($('#menuBtn').width() + 10)}`);
						
					}
				},
				error: (err) => console.log(err) 
			})
        }
        
        contentPreview = (subCatName, catName) => {
			$('.ctaInfo').on('click', function(e){
				e.preventDefault();
				// get id of current content
				// const contentId = $(this).children('input').val();
				const contentId = $(e.currentTarget).children('input').val();

				$.ajax({
					type:'POST',
					url: 'php/ajaxRequest.php',
					data: `contentId=${contentId}`,
					dataType:'json',
					beforeSend: () => {
						// remove previewContainer if it exist
						if($('#previewContainer') != undefined){
							$('#previewContainer').remove();
						}
					},
					success: (data, textStatus, xhr) => {
						if(xhr.status == 200){
							// hide menu and contents
							$('.mainHeader').css({display: "none"});
							$('#contentContainer').css({display: "none"});

							// destructure data
							const {title, description, fileName, fileExt, origFileName, playFile, screenshots} = data[0];
							// create and append previewContainer
							$('.mainContainer').append(`<section class="previewContainer" id="previewContainer"></section>`);
							// thumbnail file format
							const thumbExt = ".png";
							// directory
							const contentDir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
							
							switch(catName){
								case 'games-apk':
									// screenshots container
									let images = "";
									// loop and assign all screenshots
									$.each(screenshots, function(key, image){
										images += `<img src="${contentDir}${image}" />`
									})

									$('#previewContainer').append(`
										<div class="previewClose">
											<button class="closeBtn">
												<i class='bx bx-x'></i>
											</button>
										</div>
										<div class="previewThumbImage">
											<div class="previewImage">
												<img src="${contentDir}content/${fileName}${thumbExt}" alt="${title}" />
											</div>
											<div class="previewNameCta">
												<p>${title}</p>
												<span>${subCatName}</span>
												<a href="${contentDir}content/${fileName}.${fileExt}" class="previewDownload ctaBtn">
							 						<i class='bx bxs-download'></i>
							 						<span>DOWNLOAD</span>
							 					</a>
											</div>
										</div>
										<div class="previewDescription">
											<p>Description</p>
											<p>${description}</p>
										</div>
										<div class="previewScreenshots">
											<p>Screenshots</p>
											<div class="sliderContainer" id="slides">
												${images}
												<a class="slidesjs-previous slidesjs-navigation sliderBtn" href="#">
													<i class='bx bx-skip-previous'></i>
												</a>
												<a class="slidesjs-next slidesjs-navigation sliderBtn" href="#">
													<i class='bx bx-skip-next'></i>
												</a>
											</div>
										</div>
									`)
									// slider plugin
									$('#slides').slidesjs({
										navigation: {
											active: false,
											effect: "slide" 
										},
										pagination: {
											active: false,
											effect: "slide"
										}
									})

									// show button clicked effect
									$('.sliderBtn').on('mousedown', function(){
										$(this).css({boxShadow: "inset -1px -1px 2px #FFFFFF, inset 1px 1px 2px #A9B5C7, inset -1px -1px 1px rgba(255, 255, 255, 0.6)"});
										$(this).children("i").css({color: "#5C677D"});
									}).on('mouseup', function(){
										$(this).css({boxShadow: "-2px -2px 8px #FFFFFF, 2px 2px 8px #A9B5C7, -1px -1px 1px rgba(255, 255, 255, 0.6)"})
										$(this).children("i").css({color:"#0466C8"});
									})

									break;
                                
                                    case 'html5':
                                        // loop and assign all screenshots
                                        $.each(screenshots, function(key, image){
                                            images += `<img src="${contentDir}${image}" />`
                                        })
    
                                        $('#previewContainer').append(`
                                            <div class="previewClose">
                                                <button class="closeBtn">
                                                    <i class='bx bx-x'></i>
                                                </button>
                                            </div>
                                            <div class="previewThumbImage">
                                                <div class="previewImage">
                                                    <img src="${contentDir}content/${origFileName}${thumbExt}" alt="${title}" />
                                                </div>
                                                <div class="previewNameCta">
                                                    <p>${title}</p>
                                                    <span>${subCatName}</span>
                                                    <a href="${playFile}" class="previewDownload ctaBtn">
                                                         <i class='bx bxs-joystick'></i>
                                                         <span>Play Game</span>
                                                     </a>
                                                </div>
                                            </div>
                                            <div class="previewDescription">
                                                <p>Description</p>
                                                <p>${description}</p>
                                            </div>
                                        `)
                                        break;

							}
							// opacity animation previewContainer
							$('#previewContainer').animate({opacity:1}, 800);
							// click event on closeBtn | remove previewContainer and show mainHeader and contentContainer
							$('.closeBtn').on('click', function(){
								$('#previewContainer').remove()
								$('.mainHeader').fadeIn(800);
								$('#contentContainer').fadeIn(800);
							})
						}
					},
					error: (err) => console.log(err)
				})
			}.bind(this))
        }
        
        videoAudioControls = (fileId) => {
			// get audio or video
			const mediaFile = document.getElementById(fileId);
			let playStatus = "notplaying";
			let volumeStatus = "unmute";
			// set media volume
			$('#volumeRange').css({background:`linear-gradient(to right, #0466C8 ${mediaFile.volume}00%, #E9EDF0 ${mediaFile.volume}00%)`}).attr('value', mediaFile.volume);
			// change css styles and volume of the volume component
			$('#volumeRange').on('input', function(){
				// assign value of the input to the media volume
				mediaFile.volume = $(this).val();
				$(this).css({background:`linear-gradient(to right, #0466C8 ${($(this).val() * 100).toFixed(2)}%, #E9EDF0 ${($(this).val() * 100).toFixed(2)}%)`})

				if(mediaFile.volume < .3){
					// changes icon if volume below .3
					$('#volumeBtn').children("i").attr('class', 'bx bxs-volume-low').css({color: "#0466C8"});
				}else if(mediaFile.volume > .3){
					// changes icon if volume above .3
					$('#volumeBtn').children("i").attr('class', 'bx bxs-volume-full').css({color: "#0466C8"});
				}
				// media is muted status
				if(volumeStatus == "muted"){
					// unmute media
					mediaFile.muted = false;
					// change icon color
					$('#volumeBtn').children("i").attr('class', 'bx bxs-volume-full').css({color: "#0466C8"});
					// add pressed button effect
					$('#volumeBtn').css({boxShadow: "-2px -2px 8px #FFFFFF, 2px 2px 8px #A9B5C7, -1px -1px 1px rgba(255, 255, 255, 0.6)"});
					// set status to unmute
					volumeStatus = "unmute";
				}

			});

			$('#mediaRange').on('change', function(){
				// change media current time
				mediaFile.currentTime = ($('#mediaRange').val() * mediaFile.duration) / 100;
			})


			$('#playBtn').on('click', function(){
				if(playStatus == "notplaying"){
					// play media
					mediaFile.play();
					// change icon and color
					$(this).children('i').attr('class', 'bx bx-pause-circle').css({color: "#5C677D"});
					// add button effect
					$(this).css({boxShadow: "inset -1px -1px 2px #FFFFFF, inset 1px 1px 2px #A9B5C7, inset -1px -1px 1px rgba(255, 255, 255, 0.6)"})
					// chnage status to playing
					playStatus = "playing";
				}else if(playStatus == "playing"){
					// pause media
					mediaFile.pause();
					// change icon and color
					$(this).children('i').attr('class', 'bx bx-play-circle').css({color: "#0466C8"});
					// add button effect
					$(this).css({boxShadow: "-2px -2px 8px #FFFFFF, 2px 2px 8px #A9B5C7, -1px -1px 1px rgba(255, 255, 255, 0.6)"})
					// set status to notplaying
					playStatus = "notplaying";
				}
			})

			$('#volumeBtn').on('click', function(){
				if(volumeStatus == "unmute"){
					// mute media
					mediaFile.muted = true;
					// change volumeRange color
					$('#volumeRange').css({background:`linear-gradient(to right, #5C677D ${(mediaFile.volume * 100).toFixed(2)}%, #E9EDF0 ${(mediaFile.volume * 100).toFixed(2)}%)`})
					// change icon and color
					$(this).children('i').attr('class', 'bx bxs-volume-mute').css({color: "#5C677D"});
					// add pressed button effect
					$(this).css({boxShadow: "inset -1px -1px 2px #FFFFFF, inset 1px 1px 2px #A9B5C7, inset -1px -1px 1px rgba(255, 255, 255, 0.6)"});
					// set status to muted
					volumeStatus = "muted";
				}else if(volumeStatus == "muted"){
					// unmute media
					mediaFile.muted = false;
					// change volumen range color
					$('#volumeRange').css({background:`linear-gradient(to right, #0466C8 ${(mediaFile.volume * 100).toFixed(2)}%, #E9EDF0 ${(mediaFile.volume * 100).toFixed(2)}%)`})
					// change icon and color
					$(this).children('i').attr('class', 'bx bxs-volume-full').css({color: "#0466C8"});
					// add button effect
					$(this).css({boxShadow: "-2px -2px 8px #FFFFFF, 2px 2px 8px #A9B5C7, -1px -1px 1px rgba(255, 255, 255, 0.6)"});
					// set status to unmute
					volumeStatus = "unmute";
				}
			})
			// display media duration and current time
			$(mediaFile).on('timeupdate', function(){
				let timeUpdate = (this.currentTime / this.duration) * 100;
				let durationSec = Math.floor(mediaFile.duration) % 60;
				let durationMin = Math.floor(Math.floor(mediaFile.duration) / 60);
				let currentMin = Math.floor(Math.floor(mediaFile.currentTime) / 60);
				let currentSec = Math.floor(mediaFile.currentTime) % 60;

				if(durationMin < 10){
					durationMin = `0${durationMin.toString()}`
				}

				if(durationSec < 10){
					durationSec = `0${durationSec.toString()}`
				}

				if(currentMin < 10){
					currentMin = `0${currentMin.toString()}`
				}

				if(currentSec < 10){
					currentSec = `0${currentSec.toString()}`
				}
				// set current time to linear gradient position
				$('#mediaRange').attr("value", timeUpdate).css({background: `linear-gradient(to right, #5C677D ${timeUpdate}%, #E9EDF0 ${timeUpdate}%)`});
				// display duration and current time
				$('#durationIndicator').text(`${currentMin}:${currentSec} / ${durationMin}:${durationSec}`);

			})

			// set 0 to current time when the media playtime ended 
			$(mediaFile).on('ended', function(){
				this.currentTime = 0;
				// change icon and color
				$('#playBtn').children("i").attr('class', 'bx bx-play-circle').css({color: "#0466C8"});
				// add button effect
				$('#playBtn').css({boxShadow: "-2px -2px 8px #FFFFFF, 2px 2px 8px #A9B5C7, -1px -1px 1px rgba(255, 255, 255, 0.6)"})
				playStatus = "notplaying";
			})
		}
    }

    $('#menuBtn').on('click', function(){
		const category = new Category;
		if(catTog == "close"){
			// show all categories
			category.openCategory();
		}else if(catTog == "open"){
			// hide categories
			category.closeCategory();
		}
    })

    $(window).on('resize', function(){
		// reassign width of search input if the window resizes
		$('#searchField').innerWidth(`${$('.searchContainer').width() - ($('#menuBtn').width() + 10)}`);
		searchFieldWidth = $('#searchField').innerWidth();
		
		if(searchTog == "close"){
			const search = new Search;
			// execute if search input is close
			search.maintainSizeToCircle();
		}
	})
	// search button clicked
	$('#searchBtn').on('click', function(){
		if(searchTog == "open"){
			// if the search input is not hidden, then make a request
			if($('#searchField').val() == ""){
				return;
			}else{
				// search input visible
				const search = new Search;
				search.showResult($('#searchField').val());
			}
		}else if(searchTog == "close"){
			const category = new Category;
			// show search input and hide categories
			category.closeCategory();
		}
	})
	// enter key event
	$('#searchField').on('keypress', function(e){
		if(e.code == "Enter"){
			const search = new Search;
			search.showResult($('#searchField').val());
		}
	})
    
    $('.catBtn').on('click', function(){
		// clear search field
		if($('#searchField').val() != ""){
			$('#searchField').val("");
		}
		// remove active class
		$('.catBtn').each(function(){
			$(this).removeClass("catActive");
		})
		// add class on current category
		$(this).addClass("catActive");

		const category = new Category;
		// then hide the categories
		category.closeCategory();
		// get the name of category
		let currentCatBtn = $(this).attr('data-category')

		if($('#subcategoryContainer').children().length > 0){
			// hide all sub categories
			$('.subCategory').animate({
				marginLeft: "30px",
				opacity: 0
			}, 300, function(){

				// replace the previous sub categories if condition is true
				if($('.subCategory:last').css('marginLeft') == "30px"){
					$('#subcategoryContainer').empty();
					const subCategory = new SubCategory;

					subCategory.showSubCategories(currentCatBtn)
				}
			})
		}
	})

	const subCategory = new SubCategory;
	// show sub categories and contents on window load
	subCategory.showSubCategories($('.catBtn:last').attr('data-category'));
})