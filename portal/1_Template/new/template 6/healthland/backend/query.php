<?php
    require('connect.php');

    function getDescAndScreens($contentId, $conn){
        $contentId = filter_var($contentId, FILTER_SANITIZE_SPECIAL_CHARS);

        $getDesc = "SELECT description, file_name FROM cms.contents WHERE id = ?";

        $data = [];

        $stmt = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt, $getDesc);
        mysqli_stmt_bind_param($stmt, "i", $contentId);
        mysqli_stmt_execute($stmt);
        $resultDesc = mysqli_stmt_get_result($stmt);

        while($dataDesc = mysqli_fetch_assoc($resultDesc)){
            $dataAssoc = ["description" => $dataDesc['description'], "fileName" => pathinfo($dataDesc['file_name'], PATHINFO_FILENAME), "fileExtension" => pathinfo($dataDesc['file_name'], PATHINFO_EXTENSION)];
            array_push($data, $dataAssoc);
        }
        
        echo json_encode($data);
        mysqli_stmt_close($stmt);
    }

    function getContents($catId, $subId, $conn){
        $catId = filter_var($catId, FILTER_SANITIZE_SPECIAL_CHARS);
        $subId = filter_var($subId, FILTER_SANITIZE_SPECIAL_CHARS);

        $getContent = "SELECT id, title, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and category_id=? and sub_category_id=? order by id desc limit 30";

        $stmt = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt, $getContent);
        mysqli_stmt_bind_param($stmt, "ii", $catId, $subId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);

        $result = mysqli_stmt_num_rows($stmt);
        if($result > 0){
            
            mysqli_stmt_bind_result($stmt, $id, $title, $fileName, $originalFileName, $mime, $subCategoryId);
            $data = [];
            while(mysqli_stmt_fetch($stmt)){
                $dataAssoc = ["contentId" => $id, "title" => $title, "fileName" => pathinfo($fileName, PATHINFO_FILENAME), "fileExtension" => pathinfo($fileName, PATHINFO_EXTENSION), "originalFileName" => pathinfo($originalFileName, PATHINFO_FILENAME)];
                array_push($data, $dataAssoc);
            }
            echo json_encode($data);
            mysqli_stmt_close($stmt);
        }
    }

    function getSubCategories($cat, $conn){
        if($cat == "videos"){
            $catCondition = "and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
        }else if($cat == "apps"){
            $catCondition = "and (b.sub_category='Fitness' or b.sub_category='LifeStyle' or b.sub_category='Relaxation')";
        }else{
            $cat = "videos";
            $catCondition = "and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
        }

        $querySubMenu = "SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $catCondition and category like ?";
        $sanitizeCat = filter_var($cat, FILTER_SANITIZE_SPECIAL_CHARS);
        $bindCat = "{$sanitizeCat}%";

        $stmt = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt, $querySubMenu);
        $bindParam = mysqli_stmt_bind_param($stmt, "s", $bindCat);
        if($bindParam){
            mysqli_stmt_execute($stmt);
            $subCat = [];
            $result = mysqli_stmt_get_result($stmt);
            while($data = mysqli_fetch_assoc($result)){
                $catId = $data['id'];
			    $category = $data['category'];
			    $subCatId = $data['sc_id'];
                $subCategory = $data['sub_category'];
                
                $dataAssoc = ["catId" => $catId, "category" => $category, "subId" => $subCatId, "subCategory" => ucwords($subCategory)];
                array_push($subCat, $dataAssoc);
            }
            echo json_encode($subCat);
            mysqli_stmt_close($stmt);
        }
        
    }

    if(isset($_POST['cat'])){
        getSubCategories($_POST['cat'], $conn);
        mysqli_close($conn);
    }else if(isset($_POST['contentId'])){
        getDescAndScreens($_POST['contentId'], $conn);
        mysqli_close($conn);
    }else{
        // check the content type if json
        $contentType = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : "";
        if(stripos($contentType, 'application/json') === false){
            throw new Exception('Content-Type must be application/json');
        }
        // get all data from input
        $body = file_get_contents("php://input");
        // convert data to associative array
        $bodyData = json_decode($body, true);
        // check if array
        if(!is_array($bodyData)){
            throw new Exception('Failed to decode JSON object');
        }

        if(isset($bodyData['subId'])){
            $catId = $bodyData['catId'];
            $subId = $bodyData['subId'];
            getContents($catId, $subId, $conn);
            mysqli_close($conn);
        }
    }
?>