<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="style/style.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous" defer></script>
    <script src="script/waitForImages.js" defer></script>
    <script src="script/main.js" defer></script>
    <title>Healthland Template 6</title>
</head>
<body>
    <main class="mainContainer">
        <div class="headerBg"></div>
        <section class="scrollContainer">
            <section class="headerContainer">
                <section class="headerWrapper">
                    <h1>HEALTHLAND</h1>
                    <div class="subMenu">
                    </div>
                    <div class="subMenusContainer">
                        <ul class="subMenuLinksContainer"></ul>
                    </div>
                </section>
            </section>
            <section class="contentContainer"></section>
        </section>
        <footer class="footerContainer">
            <nav class="navContainer">
                <div class="menuButton menuButtonActive" data-cat="videos">
                    <i class="las la-film"></i>
                </div>
                <div class="menuButton" data-cat="tips">
                    <i class="las la-comment-medical"></i>
                </div>
                <div class="menuButton" data-cat="apps">
                    <i class="las la-box"></i>
                </div>
                <div class="menuActive"></div>
            </nav>
        </footer>
    </main>
</body>
</html>