<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="style/style.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous" defer></script>
    <script src="script/waitForImages.js" defer></script>
    <script src="script/main.js" defer></script>
    <title>YesWorld</title>
</head>
<body>
    <main class="mainContainer">
        <div class="headerBg"></div>
        <section class="scrollContainer">
            <section class="headerContainer">
                <section class="headerWrapper">
                <div class="logo">
                <img src="logo.png" alt="YesWorld Logo">
                <h1>YesWorld</h1>
                </div>
                    <div class="subMenu">
                    </div>
                    <div class="subMenusContainer">
                        <ul class="subMenuLinksContainer"></ul>
                    </div>
                </section>
            </section>
            <section class="contentContainer"></section>
        </section>
        <footer class="footerContainer">
            <nav class="navContainer">
                <div class="menuButton menuButtonActive" data-cat="games-apk">
                    <i class="lab la-android"></i>
                </div>
                <div class="menuButton" data-cat="html5">
                    <i class="lab la-html5"></i>
                </div>
                <div class="menuActive"></div>
            </nav>
        </footer>
    </main>
</body>
</html>