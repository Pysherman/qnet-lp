$(document).ready(() => {
    class Misc{
        // sets height of headerBG
        static setHeight(){
            $('.headerBg').height(`${$('.headerContainer').height() + 30}`);
        }
        // set top position of contentContainer
        static setTopPos(posVal){
            $('.contentContainer').css({top:`${$('.headerContainer').height() + posVal}px`})
        }
    }

    class BackEndRequest{
        constructor(){
            // toggle indicator for the sub cat menu
            this.subMenuSwitch = true;
            // toggle indicator for the show more events
            this.showMoreSwitch = true;

            this.contentDir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
            this.thumbExt = ".png";
            // set thumbnail
            this.thumbNail = "";
            // set type of button
            this.ActBtn = "";
        }

        ajaxReqShowMore(e, contentDir){
            $.ajax({
                type: 'POST',
                url: 'backend/query.php',
                data: `contentId=${$(e.currentTarget).parent().siblings('input').val()}`,
                dataType: 'json',
                success: (data, textStatus, xhr) => {
                    if(xhr.status == 200){
                        let showDiv = "";
                        const {description, screenshots, fileName, fileExtension} = data[0];
                        switch($('.contentContainer').attr('data-category')){
                            case "games-apk":
                                let images = "";
                                // loop through all the image filename then assign them to img tag
                                $.each(screenshots, (i, image) => {
                                    images += `<img src="${contentDir}${image}" />`
                                });

                                showDiv = ` <div class="showDiv">
                                                <p>${description}</p>
                                                <div class="screenshots">
                                                    ${images}
                                                </div>
                                            </div>`;
                                break;
                            case "html5":
                                showDiv = ` <div class="showDiv">
                                                <p>${description}</p>
                                            </div>`;
                                break;
                        }
                        // append the data to the content div set a data attribute
                        $(e.currentTarget).parent().parent().append(showDiv).attr("data-state", "showing");

                        // set height of the content once all images are loaded
                        $('.showDiv').waitForImages(function(){
                            $(e.currentTarget).parent().parent().height($(e.currentTarget).parent().prev().outerHeight() + $(e.currentTarget).parent().height() + $('.showDiv').outerHeight());                            
                        })

                        // rotate up the chevron down
                        $(e.currentTarget).children("i").css({transform: "rotate(-180deg)"});
                        // change text
                        $(e.currentTarget).children("span").text("Show Less");
                    }
                },
                error: (errorThrown) => console.log(errorThrown)
            })
        }

        showMore(){
            $('.showBtn').on('click', (e) => {
                if(this.showMoreSwitch == true){
                    // fetch data
                    this.ajaxReqShowMore(e, this.contentDir);
                    // reassign the value to false
                    this.showMoreSwitch = false;
                }else if(this.showMoreSwitch == false){
                    // if content has no data close the content that has a data attribute
                    if(e.currentTarget.parentElement.parentElement.hasAttribute("data-state") == false){
                        // loop through all the content then remove the data attribute from the previous open content
                        $('.content').each((i, elem) => {
                            if($(elem).data('state') == "showing"){
                                // remove data attrib
                                $(elem).removeAttr("data-state");
                                // remove showDiv
                                $('.showDiv').remove();
                                // reassign height of content
                                $(elem).height($(e.currentTarget).parent().prev().outerHeight() + $(e.currentTarget).parent().height())
                                // set to the original rotation
                                $(elem).children(".lowerSection").children(".showBtn").children("i").css({transform: "rotate(0deg)"});
                                // change text
                                $(elem).children(".lowerSection").children(".showBtn").children("span").text("Show More");
                            }
                        })
                        // request for the content
                        this.ajaxReqShowMore(e, this.contentDir);
                        // reassign the value to false
                        this.showMoreSwitch = false;
                    }else{
                        // remove showDiv
                        $('.showDiv').remove();
                        // reassign height of content
                        $(e.currentTarget).parent().parent().height($(e.currentTarget).parent().prev().outerHeight() + $(e.currentTarget).parent().height())
                        // if content has data attribute, remove the attribute
                        $(e.currentTarget).parent().parent().removeAttr("data-state");
                        // set to the original rotation
                        $(e.currentTarget).children("i").css({transform: "rotate(0deg)"});
                        // change text
                        $(e.currentTarget).children("span").text("Show More");
                        // reassign the value to true
                        this.showMoreSwitch = true;
                    }
                }
            })
        }

        setContent(catId, subId){
            // remove all content div
            if($('.contentContainer').children().length > 0){
                $('.content').fadeOut(200, () => $('.contentContainer').empty());
            }
            $.ajax({
                type:'POST',
                url: 'backend/query.php',
                contentType: 'application/json',
                data: JSON.stringify({
                                        "catId": catId,
                                        "subId": subId
                                    }),
                dataType: 'json',
                beforeSend: () => {
                    // add a loader
                    $('.contentContainer').append(`<div class="loader"></div>`); 
                },
                complete: () => {
                    // remove the loader
                    $('.contentContainer').children('.loader').remove();
                },
                success: (data, textStatus, xhr) => {
                    if(xhr.status == 200){
                        $.each(data, (key, value) => {
                            const {fileName, fileExtension, contentId, title, originalFileName, playFileName} = value;
                             // set thumbnails based from category
                            switch($('.contentContainer').attr('data-category')){
                                case "games-apk":
                                    this.thumbNail = `<img src="${this.contentDir}content/${fileName}${this.thumbExt}" alt="${title}" />`;
                                    this.ActBtn =   `
                                                        <i class="las la-cloud-download-alt"></i>
                                                        <a href="${this.contentDir}content/${fileName}.${fileExtension}">Download</a>
                                                    `;
                                    break;
                                case "html5":
                                    this.thumbNail = `<img src="${this.contentDir}content/${originalFileName}${this.thumbExt}" alt="${title}" />`;
                                    this.ActBtn =   `
                                                        <i class="las la-gamepad"></i>
                                                        <a href="${playFileName}">Play Game</a>
                                                    `;
                                    break;
                                default:
                                    break;
                            }
                            // fecth data then append to contentContainer
                            $('.contentContainer').append(`
                                <div class="content">
                                    <input type="hidden" name="contentId" value="${contentId}" />
                                    <div class="upperSection">
                                        <div class="thumbContainer">
                                            ${this.thumbNail}
                                        </div>  
                                        <div class="nameContainer">
                                            <p>${title}</p>
                                            <div class="contentBtn">
                                                ${this.ActBtn}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="lowerSection">
                                        <div class="showBtn">
                                            <span>Show More</span>
                                            <i class="las la-angle-down"></i>
                                        </div>
                                    </div>
                                </div`
                            );
                        })
                        // fadeIn animation content
                        $('.content').fadeIn(200);
                        // set height of content div, will be using the height for the animation of show more btn
                        $('.content').height($('.content').height())
                        // call showMore method
                        this.showMore();
                        // hide sub categories
                        if(this.subMenuSwitch == false){
                            // call this.toggleTopMenu method and set subMenu as argument
                            this.toggleTopMenu(".subMenu");
                        }
                    }
                },
                error: (errorThrown) => {
                    console.log(errorThrown)
                }
            })
        }

        toggleTopMenu(currentTarget){
            if(this.subMenuSwitch == true){
                // show sub categories
                $('.subMenusContainer').fadeIn(200);
                // rotate up the chevron down
                $(currentTarget).children("i").css({transform: "rotate(-180deg)"});
                // reassign the height of headerBg
                Misc.setHeight();
                // reassign the top position and 30 of contentContainer
                Misc.setTopPos(30);
                // reassign the value to false
                this.subMenuSwitch = false;
            }else if(this.subMenuSwitch == false){
                // hide sub categories
                $('.subMenusContainer').fadeOut(200);
                // set to the original rotation
                $(currentTarget).children("i").css({transform: "rotate(0deg)"});
                // reset the height of headerBg, deduct the height of sub category div
                $('.headerBg').height(`${($('.headerContainer').height() + 28) - $('.subMenusContainer').innerHeight()}`);
                // reset the top position of contentContainer, deduct the height of sub category div
                $('.contentContainer').css({top:`${($('.headerContainer').height() + 28) - $('.subMenusContainer').innerHeight()}px`});
                // reassign the value to true
                this.subMenuSwitch = true;
            }
        }

        setMenuTop(catName){
            // assign data attribute on contentContainer
            $('.contentContainer').attr('data-category', catName);
            $.ajax({
                type:'POST',
                url: 'backend/query.php',
                data: `cat=${catName}`,
                dataType: 'json',
                success: (data, textStatus, xhr) => {
                    $.each(data, (key, value) => {
                        const {subCategory, subId, catId} = value;
                        // fetch data
                        $('.subMenuLinksContainer').append(`
                            <li>
                                <input type="hidden" name="subId" value="${subId}" />
                                <input type="hidden" name="catId" value="${catId}" />
                                <span>${subCategory.replace("-", " ")}</span>
                            </li>`
                        );
                    })
                    // add class on the first sub category and append check mark
                    $('.subMenuLinksContainer li:first').addClass('subActive').append(`<i class="las la-check"></i>`);
                    // change the text on the selected Sub Category
                    $('.subMenu').children('span').text($('.subMenuLinksContainer li:first').children('span').text());
                    // fetch all content
                    this.setContent(
                        $(".subMenuLinksContainer li:first").children('input:last').val(),
                        $(".subMenuLinksContainer li:first").children('input:first').val()
                    );
                    // add click event on sub categories
                    $('.subMenuLinksContainer li').on('click', (e) => {
                        // show content of the selected sub category
                        this.setContent(
                            e.currentTarget.children[1].value,
                            e.currentTarget.children[0].value
                        );
                        
                        // remove the check mark and the active class on the previous active sub category
                        $('.subMenuLinksContainer li').each(function(){
                            if($(this).hasClass("subActive")){
                                $(this).removeClass("subActive").children('i').remove();
                            }
                        })
                        // add class and check mark on the selected sub category
                        $(e.currentTarget).addClass("subActive").append(`<i class="las la-check"></i>`);

                        // assign the text of the selected sub category to the subMenu
                        $('.subMenu').children('span').text($(e.currentTarget).text());

                    });

                },
                error: (errorThrown) => {
                    console.log(errorThrown);
                }
            })
        }

        subCategories(){
            
        }

        events(){
            $(window).on('load', () => {
                // call setMenuTop method
                this.setMenuTop($('.menuButton:first').attr('data-cat'));
                // append first sub category
                $('.subMenu').append(`
                                            <span>${$('.subMenuLinksContainer li:first').text()}</span>
                                            <i class="las la-angle-down"></i>
                                        `);
                // set top position of contentContainer
                Misc.setTopPos(48);
                // sets height of headerBG
                Misc.setHeight();
            });
            
            $('.menuButton').on('click', (e) => {
                // remove sub categories from previous category
                $('.subMenuLinksContainer').empty();
                // show sub categories from selected category
                this.setMenuTop(e.currentTarget.dataset.cat);
                // remove active class of the previous category
                $('.menuButton').each(function(){
                    if($(this).hasClass("menuButtonActive")){
                        $(this).removeClass("menuButtonActive");
                    }
                });
                // add class to the current category
                $(e.currentTarget).addClass("menuButtonActive");
                // add active indicator to the current category
                $('.menuActive').animate({left:`${$(e.currentTarget).outerWidth() * $(e.currentTarget).index()}px`}, 200);
                // hide sub category menu if its open
                if(this.subMenuSwitch == false){
                    this.toggleTopMenu(".subMenu");
                }
            });

            $('.subMenu').on('click', (e) => {
                // show sub categories
                this.toggleTopMenu(e.currentTarget);
            })
        }
    }

    const backEndRequest = new BackEndRequest();
    backEndRequest.events();
})