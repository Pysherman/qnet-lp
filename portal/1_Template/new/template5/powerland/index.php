<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" />
	<title>Powerland Animation Version 2</title>
</head>
<body>
	<header class="header">
		<h1>Anim V2</h1>
	</header>
	<main class="main-container" id="mainContainer">
		<section class="group group-menu">
			<section class="main-menu heading-style">
				<ul>
					<li><a href="#" class="main-link active" data-category="games">Games</a></li>
					<li><a href="#" class="main-link" data-category="videos">Videos</a></li>
					<li><a href="#" class="main-link" data-category="tones">Tones</a></li>
					<li><a href="#" class="main-link" data-category="apps">Apps</a></li>
				</ul>
			</section>
			<section class="sub-menu pad-content" id="subMenu">
			</section>
		</section>
		<section class="group group-content" id="groupContent"></section>
		<section class="group group-preview" id="groupPreview"></section>
	</main>
<script src="js/scriptMain.js"></script>
<!-- <script src="js/scriptHistory.js"></script> -->
<!-- <script src="js/scriptClass.js"></script> -->
</body>
</html>

<!-- need to integrate history api -->