// initialize DOM elements
const mainLinks = document.querySelectorAll('.main-link');
const subMenu = document.getElementById('subMenu');
const groups = document.querySelectorAll('.group');
const mainContainer = document.getElementById('mainContainer');

// initialize ajax request
const xhr = new XMLHttpRequest();

// initialize string data type
const contentDir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
const thumbExt = ".png";

class Height{
	constructor(_indx1, _indx2){
		this.indx1 = _indx1;
		this.indx2 = _indx2;
	};
	// set the height of the group menu equal to the viewport height of div-body
	setHeightGroupMenu(){
		// get the height value of the div-body
		let bodyHeight = document.body.clientHeight;
		// set the height of group menu by subtracting the height of the body to the offset top of the group menu parent element
		groups[0].style.height = `${bodyHeight - mainContainer.offsetTop}px`;
	};
	// set the height to all the remaining cards
	// indx1 is equal to the index of the current group
	// indx2 is equal to the index of the previous group
	setHeightRemainingGroups(){
		// get the top position value of the group content as a reference
		let topValue = groups[1].style.top.replace("px", "");
		// get the height of the div-header
		let divHeaderHeight = groups[this.indx1].firstElementChild.clientHeight;
		// get the height of the div-body-inner
		let divBodyInnerHeightOffsetTop = groups[this.indx1].lastElementChild.firstElementChild.offsetTop;
		// set height for group indx1 by subtracting the height of group indx2 to the top position value of group content
		groups[ths.indx1].style.height = `${groups[this.indx2].clientHeight - parseInt(topValue)}px`;
		// set height for group indx1 div-body
		groups[this.indx1].lastElementChild.style.height = `${(groups[this.indx1].clientHeight - (divBodyInnerHeightOffsetTop * 2) - divHeaderHeight)}px`;
	};
};

class Remove{
	constructor(_parentElem){
		this.parentElem = _parentElem;
	};
	// remove children of each group to avoid duplication
	removeChildren(){
		while(this.parentElem.children.length > 0)
			this.parentElem.removeChild(this.parentElem.firstElementChild);
		// trigger the transitionOpacity css on submenu
		if(this.parentElem.id == "subMenu")
			this.parentElem.style.opacity = "0";
	};
	// remove double heading if user double click item or subMenu
	removeDoubleHeading(){
		let divHeaders = document.querySelectorAll(`#${this.parentElem.id} .div-header`);
		// get how many divHeaders element in the group div and then subtract it by 1
		let i = divHeaders.length - 1
		// check the length if its greater than 1, if its true then remove all the duplicate header div
		while(divHeaders.length > 1){
			// if index reaches 0 break out of the loop
			if(i == 0){
				break;
			}else{
				this.parentElem.removeChild(divHeaders[i]);
				i--;
			}
		};
	};
};

class XhrRequest{
	constructor(_sendQuery, _mainLinkData){
		this.sendQuery = _sendQuery;
		this.mainLinkData = _mainLinkData;
	};
	// send ajax request 
	sendRequest(){
		xhr.open("POST", "connection/query.php", true);
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		// send the data-category value in the query
		xhr.send(this.sendQuery);
	};
	// send a ajax request to fetch the sub category of the current main link in the database
	showSubMenu(){
		this.sendRequest();
		xhr.onreadystatechange = function(){
			if(this.readyState == 4 && this.status == 200){
				// convert result to object
				let data = JSON.parse(this.responseText);
				const ul = document.createElement('ul');
				let outputBody = "";
				let i = 0;
				for(i in data){
					// remove the all the white spaces
					let dataSubCat = data[i].subCategory.replace(" ", "");
					// remove the hyphens
					dataSubCat = dataSubCat.replace("-", "");
					// replace hyphen to white space
					let subCategoryNoHyphen = data[i].subCategory.replace("-", " ");

					outputBody += `	<li>
										<a href="#" class="sub-link blue-button" data-subCat="${dataSubCat}" data-cat="${this.mainLinkData}" data-catId="${data[i].catId}" data-subCatId="${data[i].subId}">${subCategoryNoHyphen}</a>
									</li>`;
				};

				ul.innerHTML = outputBody;
				//insert all data in the section that have an id of subMenu
				subMenu.append(ul);
				subMenu.style.opacity = "1";

				// send a ajax request to fetch all the contents in the database
				this.showContentList();
			};
		};

	};
	// method for submenu button events
	// send a ajax request to fetch all the contents in the database
	showContentList(){
		const subMenuLinks = document.querySelectorAll('.sub-link');
		this.sendQuery = "query";
		console.log(this.sendQuery);
		console.log(subMenuLinks);
	};
};

mainLinks.forEach(mainLink => {
	mainLink.addEventListener('click', e => {
		e.preventDefault();
		// get the data-category value
		let catData = mainLink.dataset.category;
		// create new object for class XhrRequest
		let subCatMenu = new XhrRequest(`cat=${catData}`, catData);
		// create new object for class Remove
		let remove = new Remove(subMenu);
		/// remove all children of sub menu
		remove.removeChildren();
		// show all the current main link submenu items
		// pass the current mainnlink data-category value as parameter
		subCatMenu.showSubMenu();
	}, false);
});

window.addEventListener('load', () => {
	// create new object for class Height
	let height = new Height();
	// create new object for class XhrRequest
	let subCatMenu = new XhrRequest(`cat=${mainLinks[0].dataset.category}`, mainLinks[0].dataset.category);
	// invoke method setHeightGroupMenu to set height on the group menu
	height.setHeightGroupMenu();
	// show all the current main link submenu items
	// pass the current mainnlink data-category value as parameter
	subCatMenu.showSubMenu();
});