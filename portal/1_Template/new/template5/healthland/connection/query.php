<?php

require('inc/header.php');

if(isset($_POST['cat'])):

	$cat = $_POST['cat'];

	if($cat == "videos"):
		$catCondition = "and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
	elseif($cat == "apps"):
		$catCondition = "and (b.sub_category='Fitness' or b.sub_category='LifeStyle' or b.sub_category='Relaxation')";
	elseif($cat == "tips"):
		return;
	else:
		$cat = "games";
		$catCondition = "and(b.sub_category='Action' or b.sub_category='Adventure' or b.sub_category='Arcade' or b.sub_category='Puzzle' or b.sub_category='Simulation' or b.sub_category='Strategy')";
	endif;

	$querySubMenu = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $catCondition  and category like '$cat%'");

	$subData = [];

	if($querySubMenu):
		while($fetchedData = mysqli_fetch_assoc($querySubMenu)):

			$catId = $fetchedData['id'];
			$category = $fetchedData['category'];
			$subCatId = $fetchedData['sc_id'];
			$subCategory = $fetchedData['sub_category'];

			// $subCategory = str_replace(" ", "", $subCategory);

			$dataAssoc = ["catId" => $catId, "category" => $category, "subId" => $subCatId, "subCategory" => ucwords($subCategory)];

			array_push($subData, $dataAssoc);
		endwhile;

		echo json_encode($subData);
		exit();

	endif;

endif;

if(isset($_POST['subcat'])):
	$catId = $_POST['catid'];
	$subCatId = $_POST['subcatid'];

	$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and category_id='$catId' and sub_category_id='$subCatId' order by id desc limit 30");

	$contentData = [];

	if($getContent):
		while($items = mysqli_fetch_assoc($getContent)):
			$contentId = $items['id'];
			$itemTitle = $items['title'];
			$description = $items['description'];
			$fileName = $items['file_name'];
			$originalFileName = $items['original_file_name'];

			// $subCategoryId = $items['sub_category_id'];

			$dataAssoc = ["contentId" => $contentId, "title" => $itemTitle, "description" => $description, "filename" => pathinfo($fileName, PATHINFO_FILENAME), "fileExtension" => pathinfo($fileName, PATHINFO_EXTENSION), "originalFileName" => pathinfo($originalFileName, PATHINFO_FILENAME)];

			array_push($contentData, $dataAssoc);
		endwhile;

		echo json_encode($contentData);
		exit();

	endif;

endif;

if(isset($_POST['itemid'])):
	$itemId = $_POST['itemid'];

	$getItem = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id='$itemId'");

	$itemData = [];

	if($getItem):
		while($item = mysqli_fetch_assoc($getItem)):
			$itemId = $item['id'];
			$itemName = $item['title'];
			$itemDescription = $item['description'];
			$itemRate = $item['rate'];
			$itemOriginFileName = $item['original_file_name'];
			$itemMime = $item['mime'];
			$itemFileName = $item['file_name'];
			$itemExt = pathinfo($itemFileName, PATHINFO_EXTENSION);

			$dataAssoc = ['itemId' => $itemId, 'itemName' => $itemName, 'itemDescription' => $itemDescription, 'itemRate' => $itemRate, 'fileName' => pathinfo($itemFileName, PATHINFO_FILENAME), 'itemExt' => $itemExt];

			$imageArr = [];

			if($itemExt == "apk" || $itemExt == "xapk"):
				$getImages = $conn->query("SELECT file_name FROM cms.preview WHERE content_id='$itemId'");

				if($getImages):
					$imageContainer = [];

					while($image = mysqli_fetch_assoc($getImages)):
						array_push($imageContainer, $image['file_name']);

					endwhile;
					$imageArr = ["images" => $imageContainer];

				endif;

			endif;

			if(!empty($imageArr)):
				$merged = array_merge($dataAssoc, $imageArr);
				array_push($itemData, $merged);
			else:
				array_push($itemData, $dataAssoc);

			endif;

		endwhile;

		echo json_encode($itemData);
		exit();

	endif;

endif;

?>