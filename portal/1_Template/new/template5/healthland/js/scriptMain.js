// initialize DOM elements
const mainLinks = document.querySelectorAll('.main-link');
const subMenu = document.getElementById('subMenu');
const groups = document.querySelectorAll('.group');
const mainContainer = document.getElementById('mainContainer');

// initialize ajax request
const xhr = new XMLHttpRequest();

// initialize string data type
const contentDir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
const thumbExt = ".png";

// set the height of the group menu equal to the viewport height of div-body
function setHeightGroupMenu(){
	// get the height value of the div-body
	let bodyHeight = document.body.clientHeight;
	// set the height of group menu by subtracting the height of the body to the offset top of the group menu parent element
	groups[0].style.height = bodyHeight - mainContainer.offsetTop+"px";
}

// set the height to all the remaining cards
// indx1 is equal to the index of the current group
// indx2 is equal to the index of the previous group
function setHeightRemainingGroups(indx1, indx2){
	// get the top position value of the group content as a reference
	let topValue = groups[1].style.top.replace("px", "");
	// get the height of the div-header
	let divHeaderHeight = groups[indx1].firstElementChild.clientHeight;
	// get the height of the div-body-inner
	let divBodyInnerHeightOffsetTop = groups[indx1].lastElementChild.firstElementChild.offsetTop;

	// set height for group indx1 by subtracting the height of group indx2 to the top position value of group content
	groups[indx1].style.height = (groups[indx2].clientHeight - parseInt(topValue))+"px";
	// set height for group indx1 div-body
	groups[indx1].lastElementChild.style.height = (groups[indx1].clientHeight - (divBodyInnerHeightOffsetTop * 2) - divHeaderHeight)+"px";
	// set height for group indx1 div-body-inner
	groups[indx1].lastElementChild.firstElementChild.style.height = (groups[indx1].lastElementChild.clientHeight - (divBodyInnerHeightOffsetTop * 2))+"px";
}

// remove children of each group to avoid duplication
function removeChildren(parentElem){
	// loop to all child and remove them
	while(parentElem.children.length > 0)
			parentElem.removeChild(parentElem.firstElementChild);

	// trigger the transitionOpacity css on submenu
	if(parentElem.id == "subMenu")
		parentElem.style.opacity = "0";
}
// remove double heading if user double click item or subMenu
function removeDoubleHeading(parentElem){
	let divHeaders = document.querySelectorAll(`#${parentElem.id} .div-header`);
	// get how many divHeaders element in the group div and then deduct it by 1
	let i = divHeaders.length - 1
	// check the length if its greater than 1, if its true then remove all the duplicate header div
	while(divHeaders.length > 1){
		// if index reaches 0 break out of the loop
		if(i == 0){
			break;
		}else{
			parentElem.removeChild(divHeaders[i]);
			i--;
		}
	}
}

// function to hide the current card once the user click the close button
function closeBtnFn(groupInd){
	// get all elements that have a class of .close-btn
	let closeBtn = document.querySelectorAll('.close-btn');
	
	// set a click event close btn, to get the current button that we clicked we need to subtract the groupInd by 1 because node index is zero based and our groupInd ends with number 2 index and our closeBtn index ends with number 1 
	closeBtn[groupInd - 1].addEventListener('click', function(e){
		switch(groupInd){
			case 1:
				// set top position value to 999px of group content to hide the card
				groups[1].style.top = "999px";
				// reassign width and top position value of group menu and then remove the class heading-not-active in div header
				groups[0].style.cssText = "width:100%; top:10px;";
				groups[0].firstElementChild.classList.remove("heading-not-active");

				// invoke this function to set the height of the group menu
				setHeightGroupMenu();
				// remove all children of the group content
				removeChildren(groups[1]);
				break;
			case 2:
				// set top position value to 999px of group preview to hide the card
				groups[2].style.top = "999px";

				// reassign width and top position value of group content and then remove the class heading-not-active in div header
				groups[1].style.cssText = "width:100%; top:10px;";
				groups[1].firstElementChild.classList.remove("heading-not-active");

				// reassign width and left position value of group menu and then set the top position valu to 0 
				groups[0].style.cssText = "width:92%; left:4%; top:0;";
		
				// invoke this function to set the height of the group menu
				setHeightGroupMenu();
				// remove all children of the group preview
				removeChildren(groups[2]);
				break;
			default:
				break;
		}

	}, false);
};
// function for the group content and group preview
function scrollFunctionGroups(divBodyInner){
	let mouseDown =  false;
	let divPosY;
	let scrollToTop;

	function scrollEvents(evt1, evt2, evt3, evt4){
		// listen to mousedown/touchstart
		divBodyInner.addEventListener(evt1, function(e){
			mouseDown = true;
			divPosY = e.pageY - divBodyInner.offsetLeft;
			scrollToTop = divBodyInner.scrollTop;
			divBodyInner.style.cursor = 'grabbing';
		}, true);
		// listen to mousemove/touchmove
		divBodyInner.addEventListener(evt2, function(e){
			e.preventDefault();
			if(!mouseDown)
				return;
			const posY = e.pageY - divBodyInner.offsetTop;
			const walk = (posY - divPosY);
			divBodyInner.scrollTop = scrollToTop - walk;
		}, true);
		// listen to mouseup/touchend
		divBodyInner.addEventListener(evt3, function(e){
			mouseDown = false;
			scrollToTop = 0;
			divBodyInner.style.cursor = 'grab';
		}, true);
		// listen to mouseleave/touchcancel
		divBodyInner.addEventListener(evt4, function(e){
			mouseDown = false;
			scrollToTop = 0;
			divBodyInner.style.cursor = 'grab';
		}, true);
	}
	// mouse events
	scrollEvents('mousedown', 'mousemove', 'mouseup', 'mouseleave');
	// touch events
	scrollEvents('touchstart', 'touchmove', 'touchend', 'touchcancel');
};
// function for the preview card screenshots
function scrollFunctionScreenshot(){
	const screenshotImages = document.getElementById('screenshotImages');
	let mouseDown = false;
	let divPosX;
	let scrollToLeft;

	function scrollEvents(evt1, evt2, evt3, evt4){
		// listen to mousedown/touchstart
		screenshotImages.addEventListener(evt1, function(e){
			mouseDown = true;
			divPosX = e.pageX - screenshotImages.offsetLeft;
			scrollToLeft = screenshotImages.scrollLeft;
			screenshotImages.style.cursor = 'grabbing';
		}, false);
		// listen to mousemove/touchmove
		screenshotImages.addEventListener(evt2, function(e){
			e.preventDefault();
			if(!mouseDown)
				return;
			const posX = e.pageX - screenshotImages.offsetLeft;
			const walk = (posX - divPosX) * 2;
			screenshotImages.scrollLeft = scrollToLeft - walk;
		}, false);
		// listen to mouseup/touchend
		screenshotImages.addEventListener(evt3, function(e){
			mouseDown = false;
			scrollToLeft = 0;
			screenshotImages.style.cursor = 'grab';
		}, false);
		// listen to mouseleave/touchcancel
		screenshotImages.addEventListener(evt4, function(e){
			mouseDown = false;
			scrollToLeft = 0;
			screenshotImages.style.cursor = 'grab';
		}, false);
	}
	// mouse events
	scrollEvents('mousedown', 'mousemove', 'mouseup', 'mouseleave');
	// touch events
	scrollEvents('touchstart', 'touchmove', 'touchend', 'touchcancel');
}

// function for the video and tones item
function mediaPlayerFn(idControl){
	const mediaElement = document.getElementById(idControl);
	const play = document.getElementById('play');
	const stop = document.getElementById('stop');
	const progress = document.getElementById('progress');
	const timestamp = document.getElementById('timestamp');
	const volume = document.getElementById('volume');
	const volumeRange = document.getElementById('volumeRange');

	// toggle the state of the mediaElement to play or pause and then toggle the icons
	function toggleStatus(){
		if(mediaElement.paused)
			mediaElement.play();	
		else
			mediaElement.pause();
		
	};

	// toggle the icon of the mediaElement
	function toggleStatusIcon(){
		if(mediaElement.paused)
			play.innerHTML = `<i class="fas fa-play"></i>`;		
		else
			play.innerHTML = `<i class="fas fa-pause"></i>`;
		
	};

	// update the progress time of the video/audio
	function updateProgress(){
		progress.value = (mediaElement.currentTime / mediaElement.duration) * 100;

		let mins = Math.floor(mediaElement.currentTime / 60);
		let secs = Math.floor(mediaElement.currentTime % 60);

		if(mins < 10)
			mins = '0' + String(mins);

		if(secs < 10)
			secs = '0' + String(secs);

		timestamp.innerHTML = `${mins}:${secs}`;
	}

	// set the current progress of the video/audio
	function setVideoProgress(){
		mediaElement.currentTime = (parseInt(progress.value) * mediaElement.duration) / 100;
	}

	// stop the video/audio
	function stopVideo(){
		mediaElement.currentTime = 0;
		mediaElement.pause();
	}

	// decrease/increase volume
	function updateVolume(){
		mediaElement.volume = volumeRange.value;

		if(mediaElement.volume == 0)
			volume.innerHTML = `<i class="fas fa-volume-mute"></i>`;
		else
			volume.innerHTML = `<i class="fas fa-volume-up"></i>`;
	}

	mediaElement.addEventListener('play', toggleStatusIcon);
	mediaElement.addEventListener('click', toggleStatus);
	mediaElement.addEventListener('pause', toggleStatusIcon);
	mediaElement.addEventListener('timeupdate', updateProgress);

	play.addEventListener('click', toggleStatus);
	stop.addEventListener('click', stopVideo);

	progress.addEventListener('change', setVideoProgress);

	volumeRange.addEventListener('input', updateVolume);
	volume.addEventListener('click', function(){
		volumeRange.classList.toggle('hide-vol');
	})
};

// function for the selected item that will show all the details about the item
function showPreview(){
	const items =  document.querySelectorAll('.item');

	items.forEach(function(item){
		item.addEventListener('click', function(e){
			e.preventDefault();
			e.stopPropagation();

			// get the item data attribute
			let itemId = item.dataset.itemid;
			let catOrigin = item.dataset.catorigin;
			let subCatOrigin = item.dataset.subcatorigin;

			// create new div elements
			const divBody = document.createElement('div');
			const divBodyInner = document.createElement('div');
			const divHeader = document.createElement('div');

			// insert value from the switch condition
			let outputHeaderH4 = "";

			switch(catOrigin){
				case "videos":
					outputHeaderH4 = "Video Preview";
					break;
				case "apps":
					outputHeaderH4 = "App Preview";
					break;
				default:
					outputHeaderH4 = "Preview";
					break;
			}

			// create html markup for the div-header
			let outputHeader = `	<h4>${outputHeaderH4}</h4>
									<a href="#" class="close-btn">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 8.586L2.929 1.515 1.515 2.929 8.586 10l-7.071 7.071 1.414 1.414L10 11.414l7.071 7.071 1.414-1.414L11.414 10l7.071-7.071-1.414-1.414L10 8.586z"/></svg>
									</a>`;

			// append the outputHeader into divHeader and assign a class
			divHeader.innerHTML = outputHeader;
			divHeader.classList.add("div-header", "heading-style");

			// append div header t group preview
			groups[2].append(divHeader);

			xhr.open("POST", "connection/query.php", true);
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			// send the data-itemId value in the query
			xhr.send("itemid="+itemId);

			xhr.onreadystatechange = function(){
				if(this.readyState == 4 && this.status == 200){
					// convert result to object
					let data = JSON.parse(this.responseText);

					// var for all the screenshot images
					let screenShots = "";
					// var for the thumbnail image
					let previewThumb = "";
					// var for the video controls and audio
					let idControl = "";
					let videoControls = "";
					let audioControls = "";

					switch(catOrigin){
						case "apps":
							// assign img tag if category is equal to games or apss
							previewThumb = `<img src="${contentDir}content/${data[0].fileName}${thumbExt}" alt="${data[0].itemName}">`;
							break;
						case "videos":
							// set id value
							idControl = "video";
							// assign video tag if category is equal to videos
							previewThumb = `<video preload="metadata" id="${idControl}">
												<source src="${contentDir}content/${data[0].fileName}.${data[0].itemExt}" type='video/mp4; odecs="avcl.42E01E, mp4.40.2"'>
											</video>`;
							// create html elements for video
							videoControls = `	<div class="controls">
													<button class="btn" id="play">
														<i class="fas fa-play"></i>
													</button>
													<button class="btn" id="stop">
														<i class="fas fa-stop"></i>
													</button>
													<input type="range" id="progress" class="progress" min="0" max="100" step="0.1" value="0" />
													<div class="volume-controls">
														<input type="range" id="volumeRange" class="volume-range hide-vol" min="0" max="1" step="0.01" value="1" />
														<button class="btn" id="volume">
															<i class="fas fa-volume-up"></i>
														</button>
													</div>
													<span class="timestamp" id="timestamp">00:00</span>
												</div>`;
							break;
						default:
							previewThumb = `<img src="${contentDir}content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="${data[0].itemName}" />`;
							break;
					}

					// get all the images and assign them on an img tag and then assign it to sceenShots  variable
					for(i in data[0].images)
						screenShots += `<img src="${contentDir+data[0].images[i]}">`;

					let outputBody = `	<div class="preview-thumb">
											${previewThumb}
											${catOrigin == "videos" ? videoControls : ""}
											${catOrigin == "tones" ? audioControls : ""}
										</div>
										<div class="preview-name-dl">
											<div class="preview-name">
												<h3>${data[0].itemName}</h3>
												<span>${subCatOrigin}</span>
											</div>
											<a href="${contentDir}content/${data[0].fileName}.${data[0].itemExt}" class="dl-btn green-button">DOWNLOAD</a>
										</div>
										<div class="preview-description">
											<h3>Information</h3>
											<p>${data[0].itemDescription}</p>
										</div>
										<div class="preview-screenshot">
											<h3>Screenshots</h3>
											<div class="screenshot-images" id="screenshotImages">
												${screenShots}
											</div>
										</div>`;
					
					// append the outputBody into divBodyInner and assign a class
					divBodyInner.innerHTML = outputBody;
					divBodyInner.classList.add("div-body-inner");

					// append the divBodyInner into divBody and assign a class
					divBody.append(divBodyInner);
					divBody.classList.add("div-body", "pad-content");

					// append the divBody into group preview
					groups[2].append(divBody);

					// set top position value of group preview
					groups[2].style.top = "10px";

					// assign new width, top position and left position value of group content and then assign a class that will change the background of the div-header
					groups[1].style.cssText = "width:92%; left:4%; top:0;";
					groups[1].firstElementChild.classList.add("heading-not-active");


					// assign new width, top position and left position value of group menu and then assign a class that will change the background of the div-header
					groups[0].style.cssText = "width:84%; left:8%; top:10px;";
					groups[0].firstElementChild.classList.add("heading-not-active");

					closeBtnFn(2);

					// invoke this function to set the height of the group menu
					setHeightGroupMenu();

					// invoke this function to set the height of the group content
					setHeightRemainingGroups(1, 0);

					// invoke this function to set the height of the group preview
					setHeightRemainingGroups(2, 1);

					if(catOrigin == "videos"){
						mediaPlayerFn(idControl);
					}

					// check if the item has a screenshot images, if no images then remove the div that has a class of preview screenshot or if there are images found invoke the scrollFunctionScreenshot
					if(screenShots == ""){
						divBodyInner.removeChild(divBodyInner.lastElementChild);
					}else{
						scrollFunctionScreenshot();
					}
					// remove double header
					removeDoubleHeading(groups[2]);
					// invoke function to add scroll effect on group preview div body inner
					scrollFunctionGroups(divBodyInner);
					
				} // end readyState
			};// end onreadystatechange

		}, false);
	});
}
// function for submenu button events
// send a ajax request to fetch all the contents in the database
function showContentList(){
	const subMenuLinks = document.querySelectorAll('.sub-link');
	
	subMenuLinks.forEach(function(subMenuLink){
		subMenuLink.addEventListener('click', function(e){
			e.preventDefault();

			// get all data attribute from submenu link
			let subCat = subMenuLink.dataset.subcat;
			let catId = subMenuLink.dataset.catid;
			let subCatId = subMenuLink.dataset.subcatid;
			let cat = subMenuLink.dataset.cat;
			let subMenuInner = subMenuLink.innerHTML;

			// create new div elements
			const divBody = document.createElement('div');
			const divBodyInner = document.createElement('div');
			const divHeader = document.createElement('div');

			// create html markup for the div-header
			let outputHeader = `	<h4>${subMenuLink.innerHTML}</h4>
									<a href="#" class="close-btn">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 8.586L2.929 1.515 1.515 2.929 8.586 10l-7.071 7.071 1.414 1.414L10 11.414l7.071 7.071 1.414-1.414L11.414 10l7.071-7.071-1.414-1.414L10 8.586z"/></svg>
									</a>`;

			// append the outputHeader into divHeader and assign a class
			divHeader.innerHTML = outputHeader;
			divHeader.classList.add("div-header", "heading-style");

			// append div header t group content
			groups[1].append(divHeader);


			xhr.open("POST", "connection/query.php", true);
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			// send the data-subCat, data-catId, and data-subCatId values in the query
			xhr.send("subcat="+subCat+"&catid="+catId+"&subcatid="+subCatId);

			xhr.onreadystatechange = function(){

				if(this.readyState == 4 && this.status == 200){
					// convert result to object
					let data = JSON.parse(this.responseText);

					let outputBody = "";
					let thumbnail = "";

					for(i in data){
						switch(data[i].fileExtension){
							case "mp3":
								// set the thumbnail as image if the fileExtension is equal to "mp3"
								thumbnail = `<img src="${contentDir}content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="${data[i].title}" />`;
								break;
							case "apk" || "xapk":
								// set the thumbnail as image if the fileExtension is equal to "apk or xapk"
								thumbnail = `<img src="${contentDir}content/${data[i].filename+thumbExt}" alt="${data[i].title}" />`;
								break;
							case "mp4":
								// set the thumbnail as video if the fileExtension is equal to "mp4"
								thumbnail = `	<video preload="metadata">
													<source src="${contentDir}content/${data[i].filename}.${data[i].fileExtension}" type='video/mp4; odecs="avcl.42E01E, mp4.40.2"'>
												</video>`;
								break;
							default:
								break;
						}

						// create html markup for the div-body-inner and assign the all the data
						outputBody += `	<div class="item" data-itemId="${data[i].contentId}" data-catOrigin="${cat}" data-subCatOrigin="${subMenuInner}">
											${thumbnail}
											<span class="item-title">${data[i].title}</span>
										</div>`;
					}

					// append the outputBody into divBodyInner and assign a class
					divBodyInner.innerHTML = outputBody;
					divBodyInner.classList.add("div-body-inner");

					// append the divBodyInner into divBody and assign a class
					divBody.append(divBodyInner);
					divBody.classList.add("div-body", "pad-content");

					// append the divBody into group content
					groups[1].append(divBody);

					// set top position value of group content
					groups[1].style.top = "10px";
					// assign new width, top position and left position value of group menu and then assign a class that will change the background of the div-header
					groups[0].style.cssText = "width:92%; left:4%; top:0;";
					groups[0].firstElementChild.classList.add("heading-not-active");

					closeBtnFn(1);

					// invoke this function to set the height of the group menu
					setHeightGroupMenu();
					// invoke this function to set the height of the group content
					setHeightRemainingGroups(1, 0);
					// send a ajax request to fetch details of the selected item
					showPreview();

					// remove double header
					removeDoubleHeading(groups[1]);

					// invoke function to add scroll effect on group content div body inner
					scrollFunctionGroups(divBodyInner);
				} // end readyState 4

			} // end xhr callback 

		}, false);
	});
};
// function for item events in tips category
// ssend a ajax request to fetch all the details of the item in the a json file
function showPreviewTips(){
	const items =  document.querySelectorAll('.item');

	items.forEach(function(item){
		item.addEventListener('click', function(e){
			e.preventDefault();

			// get the item data attribute
			let itemIdName = item.dataset.itemidname;
			let catOrigin = item.dataset.catorigin;
			let subCatOrigin = item.dataset.subcatorigin;

			// create new div elements
			const divBody = document.createElement('div');
			const divBodyInner = document.createElement('div');
			const divHeader = document.createElement('div');

			// create html markup for the div-header
			let outputHeader = `	<h4>Tip Preview</h4>
									<a href="#" class="close-btn">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 8.586L2.929 1.515 1.515 2.929 8.586 10l-7.071 7.071 1.414 1.414L10 11.414l7.071 7.071 1.414-1.414L11.414 10l7.071-7.071-1.414-1.414L10 8.586z"/></svg>
									</a>`;

			// append the outputHeader into divHeader and assign a class
			divHeader.innerHTML = outputHeader;
			divHeader.classList.add("div-header", "heading-style");

			// append div header t group preview
			groups[2].append(divHeader);

			xhr.open("GET", `${catOrigin}/${catOrigin}.json`, true);
			xhr.send();

			xhr.onreadystatechange = function(){
				if(this.readyState == 4 && this.status == 200){
					let data = JSON.parse(this.responseText);

					// get the item that equal to the itemIdName Value
					let item = data[subCatOrigin].filter(function(itm){
						if(itm.image == itemIdName) return itm;
					});

					// convert the first letter in the string to uppercase
					const capSubCat = `${subCatOrigin.charAt(0).toUpperCase()}${subCatOrigin.slice(1)}`;

					let outputBody = `	<div class="preview-thumb-tips">
											<img src="${catOrigin}/${subCatOrigin}/${item[0].image}.jpg" alt="${item[0].name}" />
										</div>
										<div class="preview-name-tips">
											<h3>${item[0].name}</h3>
											<span>${capSubCat}</span>
										</div>
										<div class="preview-description-tips">
											<p>${item[0].description}</p>
										</div>`;
					
					// append the outputBody into divBodyInner and assign a class
					divBodyInner.innerHTML = outputBody;
					divBodyInner.classList.add("div-body-inner");

					// append the divBodyInner into divBody and assign a class
					divBody.append(divBodyInner);
					divBody.classList.add("div-body", "pad-content");

					// append the divBody into group preview
					groups[2].append(divBody);

					// set top position value of group preview
					groups[2].style.top = "10px";

					// assign new width, top position and left position value of group content and then assign a class that will change the background of the div-header
					groups[1].style.cssText = "width:92%; left:4%; top:0;";
					groups[1].firstElementChild.classList.add("heading-not-active");


					// assign new width, top position and left position value of group menu and then assign a class that will change the background of the div-header
					groups[0].style.cssText = "width:84%; left:8%; top:10px;";
					groups[0].firstElementChild.classList.add("heading-not-active");

					closeBtnFn(2);

					// invoke this function to set the height of the group menu
					setHeightGroupMenu();

					// invoke this function to set the height of the group content
					setHeightRemainingGroups(1, 0);

					// invoke this function to set the height of the group preview
					setHeightRemainingGroups(2, 1);

					// remove double header
					removeDoubleHeading(groups[2]);
					// invoke function to add scroll effect on group preview div body inner
					scrollFunctionGroups(divBodyInner);
				};
			};
		}, false);
	});
};
// function for submenu button events in tips category
// send a ajax request to fetch all the contents in the a json file
function showContentListTips(){
	const subMenuLinks = document.querySelectorAll('.sub-link');

	subMenuLinks.forEach(function(subMenuLink){
		subMenuLink.addEventListener('click', function(e){
			e.preventDefault();
			// get all data attribute from submenu link
			let subCat = subMenuLink.dataset.subcat;
			let cat = subMenuLink.dataset.cat;
			let subMenuInner = subMenuLink.innerHTML;

			// create new div elements
			const divBody = document.createElement('div');
			const divBodyInner = document.createElement('div');
			const divHeader = document.createElement('div');

			// create html markup for the div-header
			let outputHeader = `	<h4>${subMenuLink.innerHTML}</h4>
									<a href="#" class="close-btn">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 8.586L2.929 1.515 1.515 2.929 8.586 10l-7.071 7.071 1.414 1.414L10 11.414l7.071 7.071 1.414-1.414L11.414 10l7.071-7.071-1.414-1.414L10 8.586z"/></svg>
									</a>`;

			// append the outputHeader into divHeader and assign a class
			divHeader.innerHTML = outputHeader;
			divHeader.classList.add("div-header", "heading-style");

			// append div header t group content
			groups[1].append(divHeader);

			xhr.open("GET", `${cat}/${cat}.json`, true);
			xhr.send();

			xhr.onreadystatechange = function(){
				if(this.readyState == 4 && this.status == 200){
					let data = JSON.parse(this.responseText);

					let outputBody = "";
					
					for(i in data[subCat]){
						outputBody += `	<div class="item" data-itemIdName="${data[subCat][i].image}" data-catOrigin="${cat}" data-subCatOrigin="${subCat}">
											<img src="${cat}/${subCat}/${data[subCat][i].image}.jpg" alt="${data[subCat][i].name}" />
											<span class="item-title">${data[subCat][i].name}</span>
										</div>`
					}

					// append the outputBody into divBodyInner and assign a class
					divBodyInner.innerHTML = outputBody;
					divBodyInner.classList.add("div-body-inner");

					// append the divBodyInner into divBody and assign a class
					divBody.append(divBodyInner);
					divBody.classList.add("div-body", "pad-content");

					// append the divBody into group content
					groups[1].append(divBody);

					// set top position value of group content
					groups[1].style.top = "10px";
					// assign new width, top position and left position value of group menu and then assign a class that will change the background of the div-header
					groups[0].style.cssText = "width:92%; left:4%; top:0;";
					groups[0].firstElementChild.classList.add("heading-not-active");

					closeBtnFn(1);

					// invoke this function to set the height of the group menu
					setHeightGroupMenu();
					// invoke this function to set the height of the group content
					setHeightRemainingGroups(1, 0);

					// invoke this function to get all the details of the selected item
					showPreviewTips();

					// remove double header
					removeDoubleHeading(groups[1]);

					// invoke function to add scroll effect on group content div body inner
					scrollFunctionGroups(divBodyInner);

				};
			};
		}, false);
	});
}

// function for mainlink and window events
// send a ajax request to fetch the sub category of the current main link in the database
function showSubMenu(mainLinkData){
	// if mainlinkdata is equal to tips
	if(mainLinkData == "tips"){
		const ul = document.createElement('ul');

		ul.innerHTML = `<li>
							<a href="#" class="sub-link green-button" data-subCat="food" data-cat="${mainLinkData}">Food</a>
						</li>
						<li>
							<a href="#" class="sub-link green-button" data-subCat="sport" data-cat="${mainLinkData}">Sport</a>
						</li>`;

		subMenu.append(ul);

		subMenu.style.opacity = "1";

		showContentListTips();

	}else{
		xhr.open("POST", "connection/query.php", true);
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		// send the data-category value in the query
		xhr.send("cat="+mainLinkData);

		xhr.onreadystatechange = function(){
			if(this.readyState == 4 && this.status == 200){
				// convert result to object
				let data = JSON.parse(this.responseText);
				const ul = document.createElement('ul');
				let outputBody = "";

				for(i in data){
					// remove the all the white spaces
					let dataSubCat = data[i].subCategory.replace(" ", "");
					// remove the hyphens
					dataSubCat = dataSubCat.replace("-", "");
					// replace hyphen to white space
					let subCategoryNoHyphen = data[i].subCategory.replace("-", " ");

					outputBody += `	<li>
										<a href="#" class="sub-link green-button" data-subCat="${dataSubCat}" data-cat="${mainLinkData}" data-catId="${data[i].catId}" data-subCatId="${data[i].subId}">${subCategoryNoHyphen}</a>
									</li>`;
				};

				ul.innerHTML = outputBody;
				//insert all data in the section that have an id of subMenu
				subMenu.append(ul);
				subMenu.style.opacity = "1";

				// send a ajax request to fetch all the contents in the database
				showContentList();
			};
		};
	}
};

//function for mainlink foreach
function mainLinkEvt(mainLink){
	mainLink.addEventListener('click', function(e){
		e.preventDefault();
		// get the data-category value
		let catData = mainLink.dataset.category;
		/// remove all children of sub menu
		removeChildren(subMenu);
		// show all the current main link submenu items
		// pass the current mainnlink data-category value as parameter
		showSubMenu(catData);

		// remove the active class if 1 of the main links have a class of active
		for(let i = 0; i < mainLinks.length; i++){
			if(mainLinks[i].classList.contains('active'))
				mainLinks[i].classList.remove('active');
		}

		// add active class to the current mainlink
		mainLink.classList.add('active');
	}, false)
};

mainLinks.forEach(mainLinkEvt);

window.addEventListener('load', function(){
	// invoke function to set height on the group menu
	setHeightGroupMenu();
	// pass the first mainlink data-category value as parameter
	// invoke function on load
	showSubMenu(mainLinks[0].dataset.category);
});

window.addEventListener('resize', function(){
	// invoke function to set height on the group menu
	setHeightGroupMenu();
});