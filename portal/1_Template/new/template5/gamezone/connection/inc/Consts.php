<?php

$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";


// base directory
$base_dir = __DIR__;
// server protocol
$protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';
// domain name
$domain = $_SERVER['SERVER_NAME'];
// base url
//$base_url = preg_replace("!^${doc_root}!", '', $base_dir);
$base_url = "/tmpqcnt/";
// server port
$port = $_SERVER['SERVER_PORT'];
$disp_port = ($protocol == 'http' && $port == 80 || $protocol == 'https' && $port == 443) ? '' : ":$port";
// put em all together to get the complete base URL
$url = "${protocol}://${domain}${disp_port}${base_url}";
//echo $url; // = http://example.com/path/directory

//server url :: http://localhost//C:\AppServ\www\gamezone\new\prop
?>