<?php include('includes/globals/head.php'); ?>
    <title>Document | Search</title>
</head>
<body class="bg-mainBg font-sans relative">
<?php
        include('includes/connect.php');
        include('includes/func.php');

        $searchVal = filter_var($_GET['search'], FILTER_SANITIZE_SPECIAL_CHARS);

        $search = new Search($mysqli, $searchVal);
        $utils = new Utils();
?>
<!-- main -->
<div>
    <!-- header -->
    <div>
        <!-- header -->
        <?php include('includes/globals/headerNav.php'); ?>
        <!-- end header -->

        <!-- search & sub menu -->
        <div>
            <!-- search container  -->
            <?php include('includes/globals/searchForm.php'); ?>
            <!-- end search container -->
        </div>
        <!-- end search & sub menu -->
    </div>
    <!-- end header -->

    <!-- search conrtainer -->
        <div class="container mx-auto px-5 mb-10">
            <?php 
                $results = $search->getResult(); 
                
                if(count($results) === 0):
            ?>
                    <h2 class="text-textColor text-center text-base lg:text-lg font-bold mb-6"><?php echo count($results); ?> results for "<?php echo $searchVal; ?>"</h2>
                    <p class="text-center text-sm lg:text-base text-textColor">Your search did not match any contents that are currently available to download.</p>
            <?php else: ?>
                <h2 class="text-textColor text-center text-base lg:text-lg font-bold mb-6"><?php echo count($results); ?> results for "<?php echo $searchVal; ?>"</h2>
                <div class="grid grid-cols-3 sm:grid-cols-4 lg:grid-cols-10 gap-4 md:gap-8 lg:gap-10">
                    <?php
                        foreach($results as $content):
                            $template = $utils->_switch($content['category'], $search->dir, ["category" => $content['category'], "subCategory" => $content['subCategory']], $content);
                    ?>
                            <div>
                                <a href="content.php?cat=<?php echo $content['category']; ?>&subcat=<?php echo $content['subCategory']; ?>&id=<?php echo $content['contentId']; ?>">
                                    <?php echo $template['thumbnail']; ?>
                                    <p><?php echo $content['title']; ?></p>
                                </a>
                            </div>
                    <?php
                        endforeach;
                    ?>
                </div>
            <?php endif; ?>
        </div>
    <!-- end search conrtainer -->

</div>
<!-- end main -->
    
</body>
</html>