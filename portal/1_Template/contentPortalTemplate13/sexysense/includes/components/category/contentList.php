<div class="container mx-auto px-5 mb-10">
    <?php
        $subId = isset($_GET['subId']) ? filter_var($_GET['subId'], FILTER_SANITIZE_SPECIAL_CHARS) : $subcategory->initialSubCatId;

        $contents = $content->getContents($subId);
    ?>
        <h2 class="text-center lg:text-left text-textColor text-xl lg:text-2xl font-bold mb-6 lg:mb-10 border-b-2 border-textColor py-2"><?php echo $contents[0]['subCategory']; ?></h2>

        <div class="grid grid-cols-3 sm:grid-cols-4 lg:grid-cols-10 gap-4 md:gap-8 lg:gap-10">
    <?php

        foreach($contents as $item):

            $template = $utils->_switch($item['category'], $content->dir, ["category" => $item['category'], "subCategory" => $item['subCategory']], $item);
    ?>
            <div>
                <a href="content.php?cat=<?php echo $item['category']; ?>&subcat=<?php echo $item['subCategory']; ?>&id=<?php echo $item['contentId']; ?>">
                    <?php echo $template['thumbnail']; ?>
                    <div class="mt-3.5 overflow-x-hidden whitespace-nowrap relative">
                        <span class="absolute top-0 right-0 w-2 h-full bg-gradient-to-l from-mainBg to-mainBgOpac"></span>
                        <p class="text-textColor text-sm"><?php echo $item['title']; ?></p>
                    </div>
                </a>
            </div>
    <?php
        endforeach;
    ?>
        </div>
</div>