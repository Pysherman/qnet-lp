<div class="container mx-auto px-5 mb-10 mt-3 lg:col-start-2 lg:col-end-3">
    <form action="search.php">
        <div class="grid grid-cols-customFormGrid gap-4">
            <input class="p-2 rounded-md shadow-customInner" required type="text" name="search" value="<?php echo isset($_GET['search']) ? $searchVal : ""; ?>">
            <button class="py-2 px-6 rounded-md <?php echo (isset($_GET['cat']) || isset($_GET['search'])) ? "bg-primaryColor text-white" : "bg-gradient-to-b from-cdPrimary to-cdSecondary text-textColor" ?> text-base font-bold shadow-customOuter">Search</button>
        </div>
    </form>
</div>