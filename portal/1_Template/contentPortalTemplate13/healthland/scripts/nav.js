$(() => {
    let isShow = false

    $('#navBtn').on('click', function(){
        if(isShow === false){
            $(this).css({ transform: 'rotate(180deg)' }).removeClass('text-primaryColor').addClass('text-white')
            $('#sideNav').removeClass('hidden').animate({ right: "0px" }, 200, "swing")
            $('#sideNav').children().find('a').removeClass('text-primaryColor').addClass('text-white')
            $('#sideNav').children().find('span').removeClass('text-primaryColor').addClass('text-white')
        }else{
            $('#sideNav').animate({ right: "-100%" }, 200, "swing", function(){
                $(this).addClass('hidden')
                $('#sideNav').children().find('a')
                $('#sideNav').children().find('span')
            })
            $(this).css({ transform: 'rotate(0deg)' })
        }

        isShow = !isShow
    })

    $(window).on('resize', function(){
        if($(this).width() > 1024)
            $('#sideNav').animate({ right: "0px" }, 200, "swing")
            $('#sideNav').children().find('a').addClass('text-white')
            $('#sideNav').children().find('span').addClass('text-white')
    })
})