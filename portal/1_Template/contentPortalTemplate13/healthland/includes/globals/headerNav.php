<div class="container mx-auto grid grid-cols-2 items-center p-5">
    <h1 class="text-2xl <?php echo (isset($_GET['cat']) || isset($_GET['search'])) ? "text-primaryColor" : "text-white" ?> font-bold">LOGO</h1>
    <nav class="justify-self-end">
        <!-- mobile menu -->
        <span id="navBtn" class="block text-4xl <?php echo (isset($_GET['cat']) || isset($_GET['search'])) ? "text-primaryColor" : "text-white" ?> relative z-20 cursor-pointer lg:hidden transition origin-center">
            <i class="fas fa-chevron-circle-left"></i>
        </span>
        <!-- menu list -->
        <ul id="sideNav" class="hidden lg:block absolute z-10 lg:relative top-0 -right-full lg:right-0 bg-textColor lg:bg-transparent w-4/5 lg:w-full h-screen lg:h-full pt-28 pl-10 lg:p-2">
            <li class="relative lg:inline-block lg:mr-4">
                <?php
                    if(!isset($_GET['cat']) && !isset($_GET['search'])):
                ?>
                        <span class="absolute top-0.5 -left-5 <?php echo (isset($_GET['cat']) || isset($_GET['search'])) ? "text-primaryColor" : "text-white" ?> text-xl lg:-top-5 lg:left-1/3 lg:transform lg:rotate-90"><i class="fas fa-caret-right"></i></span>
                <?php
                    endif;
                ?>
                <a href="index.php" class="<?php echo (isset($_GET['cat']) || isset($_GET['search'])) ? "text-primaryColor" : "text-white" ?> font-bold text-2xl lg:text-base mb-6 lg:mb-0 inline-block">Daily</a>
            </li>
            <li class="relative lg:inline-block lg:mr-4">
                <?php
                    if(isset($_GET['cat'])):
                        if($_GET['cat'] === "VIDEOS"):
                ?>  
                            <span class="absolute top-0.5 -left-5 <?php echo (isset($_GET['cat']) || isset($_GET['search'])) ? "text-primaryColor" : "text-white" ?> text-xl lg:-top-5 lg:left-1/3 lg:transform lg:rotate-90"><i class="fas fa-caret-right"></i></span>
                <?php
                        endif;
                    endif;
                ?>
                <a href="category.php?cat=VIDEOS" class="<?php echo (isset($_GET['cat']) || isset($_GET['search'])) ? "text-primaryColor" : "text-white" ?> font-bold text-2xl lg:text-base mb-6 lg:mb-0 inline-block">Videos</a>
            </li>
            <li class="relative lg:inline-block <?php echo (isset($_GET['search'])) ? "lg:mr-4" : "lg:mr-0"; ?>">
                <?php
                    if(isset($_GET['cat'])):
                        if($_GET['cat'] === "Apps"):
                ?>  
                            <span class="absolute top-0.5 -left-5 <?php echo (isset($_GET['cat']) || isset($_GET['search'])) ? "text-primaryColor" : "text-white" ?> text-xl lg:-top-5 lg:left-1/3 lg:transform lg:rotate-90"><i class="fas fa-caret-right"></i></span>
                <?php
                        endif;
                    endif;
                ?>
                <a href="category.php?cat=Apps" class="<?php echo (isset($_GET['cat']) || isset($_GET['search'])) ? "text-primaryColor" : "text-white" ?> font-bold text-2xl lg:text-base mb-6 lg:mb-0 inline-block">Apps</a>
            </li>
            <?php
                if(isset($_GET['search'])):
            ?>
                <li class="relative lg:inline-block">
                    <span class="absolute top-0.5 -left-5 <?php echo (isset($_GET['search'])) ? "text-primaryColor" : "text-white" ?> text-xl lg:-top-5 lg:left-1/2 lg:transform lg:rotate-90"><i class="fas fa-caret-right"></i></span>
                    <a href="#" class="<?php echo (isset($_GET['search'])) ? "text-primaryColor" : "text-white" ?> font-bold text-2xl lg:text-base mb-6 lg:mb-0 inline-block">Search Result</a>
                </li>
            <?php
                endif;
            ?>
        </ul>
    </nav>
</div>