<div class="px-5 mt-3 lg:row-start-1 lg:row-end-2">
    <?php $subCats = $subcategory->getSubcategories(); ?>
    <ul>
        <?php
            $activeSubCat = 0;

            if(isset($_GET['subId'])){
                $activeSubCat = (int)$_GET['subId'];
            }else{
                $activeSubCat = $subCats[0]['subId'];
            }

            foreach($subCats as $subcat):
        ?>
                <li class="mr-2 mb-2 last:mr-0 inline-block"><a class="block px-5 py-2 <?php echo ($subcat['subId'] === $activeSubCat) ? "bg-subActive shadow-customInner" : "bg-primaryColor shadow-customOuter" ?> rounded-md text-white text-base font-bold" href="category.php?cat=<?php echo $subcat['category']; ?>&subId=<?php echo $subcat['subId']; ?>"><?php echo $subcat['subcategory']; ?></a></li>
        <?php endforeach; ?>
    </ul>
</div>