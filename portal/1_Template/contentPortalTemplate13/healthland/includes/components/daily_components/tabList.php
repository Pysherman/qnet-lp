<div class="container mx-auto p-5">
    <?php
        include('tabs.php'); 

        $suggestedContents = new Tab("more", $daily->selectedSubId, $daily->selectedContentId, "healthlandSuggestedContents", 86400, $mysqli, "", "Suggested Contents");
        $suggestedContents->_execute();

        // $topGamesWeek = new Tab("games", 6, 0, "gamesOfWeek", 86400 * 7, $mysqli, "AND(sub_category_id=16 or sub_category_id=15 or sub_category_id=21 or sub_category_id=76 or sub_category_id=22 or sub_category_id=20 or sub_category_id=19)", "Top Games this Week");
        // $topGamesWeek->_execute();

        $topVideosWeek = new Tab("videos", 1, 0, "healthlandVideosOfWeek", 86400 * 7, $mysqli, "AND(sub_category_id=40 or sub_category_id=39 or sub_category_id=41)", "Top Videos this Week");
        $topVideosWeek->_execute();

        // $topTonesWeek = new Tab("tones", 4, 0, "tonesOfWeek", 86400 * 7, $mysqli, "AND(sub_category_id=89 or sub_category_id=90 or sub_category_id=92)", "Top Tones this Week");
        // $topTonesWeek->_execute();

        $topAppssWeek = new Tab("apps", 2, 0, "healthlandAppsOfWeek", 86400 * 7, $mysqli, "AND(sub_category_id=27 or sub_category_id=28 or sub_category_id=29)", "Top Apps this Week");
        $topAppssWeek->_execute();
    ?>
</div>