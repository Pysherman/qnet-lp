<?php
    class Tab{
        public $querySet;
        public $argsId;
        public $cookieName;
        public $cookieExpiry;
        public $selectedContentId;
        public $sqli;
        public $heading;

        function __construct($trigger, $argsId, $selectedContentId, $cookieName, $cookieExpiry, $mysqli, $subCatLimit, $heading){
            $this->querySet = $trigger === "more" ? "SELECT id, title, icon_file_name, content_file_name, category_id, sub_category_id FROM cms.portal_content WHERE sub_category_id=? AND id!=? ORDER BY RAND() LIMIT 10" : "SELECT id, title, icon_file_name, content_file_name, category_id, sub_category_id FROM cms.portal_content WHERE category_id=? AND id!=? $subCatLimit ORDER BY RAND() LIMIT 10";
            $this->argsId = $argsId;
            $this->cookieName = $cookieName;
            $this->cookieExpiry = $cookieExpiry;
            $this->selectedContentId = $selectedContentId;
            $this->sqli = $mysqli;
            $this->heading = $heading;
        }

        function checkCookie(){
            $daily = new Daily();

            if(isset($_COOKIE[$this->cookieName])){
                $sanitizeCookie = filter_var($_COOKIE[$this->cookieName], FILTER_SANITIZE_SPECIAL_CHARS);

                $toArrayToIntValues = array_map("intval", explode(",", $sanitizeCookie));

                return $daily->getContents_getCookies($this->sqli, $toArrayToIntValues);
            }else{
                $datas = $daily->getContents_setCookies($this->sqli, $this->argsId, $this->selectedContentId, $this->querySet);

                $idArr = [];

                foreach($datas as $data){
                    array_push($idArr, $data['contentId']);
                }

                setcookie($this->cookieName, implode(",", $idArr), time() + $this->cookieExpiry, "/");

                return $datas;
            }
        }
        
        function _execute(){
            $daily = new Daily();
            $utils = new Utils();

            $datas = $this->checkCookie();

?>
            <!-- tab container -->
            <div class="tabs rounded-xl bg-white shadow-customOuter mb-10 lg:mb-16 p-5">
                <div>
                    <h3 class="text-base lg:text-lg text-textColor font-bold pb-5"><?php echo $this->heading; ?></h3>
                </div>
                <div class="tabsContent grid grid-cols-3 sm:grid-cols-4 lg:grid-cols-10 gap-4 mb-5 lg:mb-0">
                    <?php 
                        foreach($datas as $data): 
                            $catNameSubName = $daily->getCatgeoryAndSubCategoryName($this->sqli, $data['catId'], $data['subId']);

                            $template = $utils->_switch($catNameSubName['category'], $daily->dir, $catNameSubName, $data);
                    ?>
                            <div><a href="content.php?cat=<?php echo $catNameSubName['category']; ?>&subcat=<?php echo $catNameSubName['subCategory']; ?>&id=<?php echo $data['contentId']; ?>"><?php echo $template['thumbnail'];?></a>
                            <div class="mt-3.5 overflow-x-hidden whitespace-nowrap relative">
                                <span class="absolute top-0 right-0 w-2 h-full bg-gradient-to-l from-white to-whiteOpac"></span>
                                <p class="text-textColor text-sm"><?php echo $data['title'];?></p>
                            </div>
                            </div>
                    <?php endforeach; ?>
                </div>
                <div class="flex flex-row-reverse lg:hidden">
                    <button class="bg-textColor text-white rounded-md px-4 py-1.5 text-sm font-bold">View More</button>
                </div>
            </div>
            <!-- end tab container -->
<?php
        }
    }
?>