<?php include('includes/globals/head.php'); ?>
    <?php $title = ['VIDEOS' => 'Videos', 'Apps' => 'Apps']; ?>
    <title>Powerland | <?php echo $title[$_GET['cat']]; ?></title>
</head>
<body class="bg-mainBg font-sans relative">
<?php
        include('includes/connect.php');
        include('includes/func.php');

        $sanitizeCat = filter_var($_GET['cat'], FILTER_SANITIZE_SPECIAL_CHARS);

        $subcategory = new Subcategory($sanitizeCat, $mysqli);
        $content = new Content($mysqli);
        $utils = new Utils();
?>
<!-- main -->
<div>
    <!-- header -->
    <div class="mb-6 lg:mb-12">
        <!-- header -->
        <?php include('includes/globals/headerNav.php'); ?>
        <!-- end header -->

        <!-- search & sub menu -->
        <div class="container mx-auto mb-20 lg:grid lg:grid-cols-2 lg:gap-10">
            <!-- search container  -->
            <?php include('includes/globals/searchForm.php'); ?>
            <!-- end search container -->

            <!-- sub menu container -->
            <?php include('includes/globals/subNav.php'); ?>
            <!-- end sub menu container -->
        </div>
        <!-- end search & sub menu -->
    </div>
    <!-- end header -->

    <!-- category content of day container -->
    <?php include('includes/components/category/categoryContentOfDay.php'); ?>
    <!-- end category content of day container -->

    <!-- content list container -->
    <?php include('includes/components/category/contentList.php'); ?>
    <!-- end content list container -->

</div>
<!-- end main -->
    
</body>
</html>