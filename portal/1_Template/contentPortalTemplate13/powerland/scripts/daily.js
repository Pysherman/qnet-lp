$(() => {
    if($(window).width() > 1024){
        $('.tabs').children('.tabsContent').each(function(){
            $(this).children().each(function(i){
                $(this).css({ display:"block" })
            })
        })

    }else if($(window).width() > 640){
        $('.tabs').children('.tabsContent').each(function(){
            $(this).children().each(function(i){
                if(i > 3){
                    $(this).css({ display:"none" })
                }
            })

            $(this).next().children('button').each(function(){
                $(this).on('click', function(){
                    if($(this).text() === "View More"){
                        $(this).parent().prev().children().css({ display:"block" })
                        $(this).text("Show Less")
                    }else{
                        $(this).parent().prev().children().each(function(i){
                            if(i > 3){
                                $(this).css({ display:"none" })
                            }
                        })

                        $(this).text("View More")
                    }
                })
            })
        })
    }else{
        $('.tabs').children('.tabsContent').each(function(){
            $(this).children().each(function(i){
                if(i > 2){
                    $(this).css({ display:"none" })
                }
            })

            $(this).next().children('button').each(function(){
                $(this).on('click', function(){
                    if($(this).text() === "View More"){
                        $(this).parent().prev().children().css({ display:"block" })
                        $(this).text("Show Less")
                    }else{
                        $(this).parent().prev().children().each(function(i){
                            if(i > 2){
                                $(this).css({ display:"none" })
                            }
                        })

                        $(this).text("View More")
                    }
                })
            })
        })
    }

    $(window).on('resize', function(){
        if($(window).width() > 1024){
            $('.tabs').children('.tabsContent').each(function(){
                $(this).children().each(function(i){
                    $(this).css({ display:"block" })
                })
            })

        }else if($(this).width() > 640){
            $('.tabs').children('.tabsContent').each(function(){
                $(this).children().each(function(i){
                    if(i === 3){
                        $(this).css({ display:"block" })
                    }

                    if(i > 3){
                        $(this).css({ display:"none" })
                    }
                })

                $(this).next().children('button').each(function(){
                    $(this).on('click', function(){
                        if($(this).text() === "View More"){
                            $(this).parent().prev().children().css({ display:"block" })
                            $(this).text("Show Less")
                        }else{
                            $(this).parent().prev().children().each(function(i){
                                if(i > 3){
                                    $(this).css({ display:"none" })
                                }
                            })

                            $(this).text("View More")
                        }
                    })
                })
            })
        }else{
            $('.tabs').children('.tabsContent').each(function(){
                $(this).children().each(function(i){
                    if(i > 2){
                        $(this).css({ display:"none" })
                    }
                })

                $(this).next().children('button').each(function(){
                    $(this).on('click', function(){
                        if($(this).text() === "View More"){
                            $(this).parent().prev().children().css({ display:"block" })
                            $(this).text("Show Less")
                        }else{
                            $(this).parent().prev().children().each(function(i){
                                if(i > 2){
                                    $(this).css({ display:"none" })
                                }
                            })

                            $(this).text("View More")
                        }
                    })
                })
            })
        }
    })
})