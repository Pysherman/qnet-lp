<!-- content of day -->
<div class="text-center">
    <h1 class="text-2xl text-white font-bold lg:text-4xl">CONTENT OF THE DAY</h1>
<?php
    $daily = new Daily();
    $utils = new Utils();

    if(isset($_COOKIE['powerlandContentOfDay'])):
        $sanitizedContentId = filter_var($_COOKIE['powerlandContentOfDay'], FILTER_SANITIZE_SPECIAL_CHARS);

        $contentOfDay = $daily->getSelectedContent($mysqli, $sanitizedContentId);

        $catNameSubName = $daily->getCatgeoryAndSubCategoryName($mysqli, $contentOfDay['catId'], $contentOfDay['subId']);

        $template = $utils->_switch($catNameSubName['category'], $daily->dir, $catNameSubName, $contentOfDay);

?>
        <div class="mt-10 lg:mt-20 mb-14 lg:mb-24 lg:grid lg:grid-cols-2 lg:gap-12 lg:items-center">
            <div class="mb-3.5 lg:mb-0 lg:justify-self-end">
                <a class="inline-block w-36 lg:w-56" href="content.php?cat=<?php echo $catNameSubName['category']; ?>&subcat=<?php echo $catNameSubName['subCategory']; ?>&id=<?php echo $contentOfDay['contentId']; ?>">
                    <?php echo $template['thumbnail']; ?>
                </a>
            </div>
            <div class="lg:justify-self-start">
                <p class="text-white text-sm lg:text-base mb-3 lg:text-left"><?php echo $template['displayCatName']; ?> | <?php echo $catNameSubName['subCategory']; ?></p>
                <p class="text-white font-bold text-xl lg:text-2xl lg:text-left"><?php echo $contentOfDay['title']; ?></p>
            </div>
        </div>
        <div>
            <a class="py-4 px-16 rounded-md bg-gradient-to-b from-cdPrimary to-cdSecondary text-textColor text-xl font-bold shadow-customOuter" href="<?php echo $daily->dir; ?>/<?php echo strtolower($catNameSubName['category']); ?>/<?php echo strtolower($catNameSubName['subCategory']); ?>/<?php echo str_replace(" ", "+", strtolower($contentOfDay['title'])); ?>/<?php echo str_replace(" ", "+", $contentOfDay['filename']); ?>">DOWNLOAD</a>
        </div>
<?php
    else:
        $cookieOfTheDay = $daily->getContentId_SubCat($mysqli);
        setcookie("powerlandContentOfDay", $cookieOfTheDay['contentId'], time() + 86400, "/");

        $contentOfDay = $daily->getSelectedContent($mysqli, $cookieOfTheDay['contentId']);

        $catNameSubName = $daily->getCatgeoryAndSubCategoryName($mysqli, $contentOfDay['catId'], $contentOfDay['subId']);

        $template = $utils->_switch($catNameSubName['category'], $daily->dir, $catNameSubName, $contentOfDay);
?>
        <div class="mt-10 lg:mt-20 mb-14 lg:mb-24 lg:grid lg:grid-cols-2 lg:gap-12 lg:items-center">
            <div class="mb-3.5 lg:mb-0 lg:justify-self-end">
                <a class="inline-block w-36 lg:w-56" href="content.php?cat=<?php echo $catNameSubName['category']; ?>&subcat=<?php echo $catNameSubName['subCategory']; ?>&id=<?php echo $contentOfDay['contentId']; ?>">
                    <?php echo $template['thumbnail']; ?>
                </a>
            </div>
            <div class="lg:justify-self-start">
                <p class="text-white text-sm lg:text-base mb-3 lg:text-left"><?php echo $template['displayCatName']; ?> | <?php echo $catNameSubName['subCategory']; ?></p>
                <p class="text-white font-bold text-xl lg:text-2xl lg:text-left"<?php echo $contentOfDay['title']; ?></p>
            </div>
            <div>
                <a class="py-4 px-16 rounded-md bg-gradient-to-b from-cdPrimary to-cdSecondary text-textColor text-xl font-bold shadow-customOuter" href="<?php echo $daily->dir; ?>/<?php echo strtolower($catNameSubName['category']); ?>/<?php echo strtolower($catNameSubName['subCategory']); ?>/<?php echo str_replace(" ", "+", strtolower($contentOfDay['title'])); ?>/<?php echo str_replace(" ", "+", $contentOfDay['filename']); ?>">DOWNLOAD</a>
            </div>
        </div>
<?php
    endif;
?>
</div>