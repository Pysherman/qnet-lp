<?php
    class Utils{
        function _switch($cat, $dir, $catNameSubName, $contentOfDay){
            $displayCatName = "";
            $thumbnail = "";
    
            switch($cat){
                case "Games-apk":
                    $displayCatName = "Games-apk";
                    $thumbnail = "<img class='rounded-full' src=".$dir."/".strtolower($catNameSubName["category"])."/".strtolower($catNameSubName['subCategory'])."/".str_replace(" ", "+", strtolower($contentOfDay['title']))."/".str_replace(" ", "+", $contentOfDay['icon'])." alt=".$contentOfDay['title']." />";
                    break;
                default:
                    $displayCatName = $catNameSubName['category'];
                    $thumbnail = "<img class='rounded-full' src=".$dir."/".strtolower($catNameSubName["category"])."/".strtolower($catNameSubName['subCategory'])."/".str_replace(" ", "+", strtolower($contentOfDay['title']))."/".$contentOfDay['icon']." alt=".$contentOfDay['title']." />";
                    break;
            }
    
            return ["displayCatName" => $displayCatName, "thumbnail" => $thumbnail];
        }

        function _switch_preview($cat, $dir, $catNameSubName, $details){
            $preview = "";

            switch($cat){
                case "Games-apk":
                case "Apps":
                    $images = [];

                    foreach($details['screenshots'] as $screen){
                        array_push($images, "<img class='inline-block mr-2 mb-4 last:mr-0' src=".$dir."/".strtolower($catNameSubName["category"])."/".strtolower($catNameSubName['subCategory'])."/".str_replace(" ", "+", strtolower($details['title']))."/screenshots/".$screen.">");
                    }

                    $preview = $images;
                    // $preview = $images;
                    break;
            }

            return $preview;
        }
    }

    class Daily{
        // ids of subcategories 
        public $subCats = [19, 20, 22, 76, 21, 15, 16, 77, 78, 79, 80];

        public $dir = "https://s3-ap-southeast-1.amazonaws.com/qcnt-portal/portal";

        public $selectedSubId;
        public $selectedContentId;

        function getSelectedContent($mysqli, $id){

            $query = "SELECT title, description, icon_file_name, content_file_name, category_id, sub_category_id FROM cms.portal_content WHERE id = ?";

            $stmt = $mysqli->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->num_rows();

            $stmt->bind_result($title, $description, $iconName, $fileName, $catId, $subId);

            $data = [];

            while($stmt->fetch()){
                $data = [
                    "contentId" => $id,
                    "title" => $title,
                    "description" => $description,
                    "icon" => $iconName,
                    "filename" => $fileName,
                    "catId" => $catId,
                    "subId" => $subId
                ];

                $this->selectedSubId = $subId;
                $this->selectedContentId = $id;
            }

            $stmt->close();
            return $data;
        }

        function getCatgeoryAndSubCategoryName($mysqli, $catId, $subId){
            $query = "SELECT a.category, b.sub_category FROM cms.categories a, cms.sub_categories b WHERE a.id = ? AND b.id = ?";

            $stmt = $mysqli->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("ii", $catId, $subId);
            $stmt->execute();
            $stmt->store_result();
            $stmt->num_rows();

            $stmt->bind_result($category, $subCategory);

            $data = [];

            while($stmt->fetch()){
                $data = [
                    "category" => $category,
                    "subCategory" => $subCategory
                ];
            }

            $stmt->close();
            return $data;
        }

        function getContents_setCookies($mysqli, $subId, $contentId, $querySet){

            $stmt = $mysqli->stmt_init();
            $stmt->prepare($querySet);
            $stmt->bind_param("ii", $subId, $contentId);
            $stmt->execute();
            $stmt->store_result();

            $data = [];

            if($stmt->num_rows() > 0){
                $stmt->bind_result($id, $title, $iconName, $fileName, $catId, $subId);
                while($stmt->fetch() > 0){
                    array_push($data, [
                        "contentId" => $id,
                        "title" => $title,
                        "icon" => $iconName,
                        "filename" => $fileName,
                        "catId" => $catId,
                        "subId" => $subId
                    ]);
                }
            }

            $stmt->close();
            return $data;
        }

        function getContentId_SubCat($mysqli){
            $data = [];


            foreach($this->subCats as $sub){
                $query = "SELECT id, sub_category_id FROM cms.portal_content WHERE sub_category_id=? ORDER BY RAND() LIMIT 10";

                $stmt = $mysqli->stmt_init();
                $stmt->prepare($query);
                $stmt->bind_param("i", $sub);
                $stmt->execute();
                $stmt->store_result();

                if($stmt->num_rows() > 0){
                    $stmt->bind_result($id, $subId);
                    while($stmt->fetch()){
                        array_push($data, ["contentId" => $id, "subId" => $subId]);
                    }

                }
            }

            $stmt->close();

            // shuffle position of each data in the array
            shuffle($data);
            // get index of selected data || get random number from 0 to the length of the data array
            $dataIndex = rand(0, count($data) - 1);
            return $data[$dataIndex];
        }

        function getContents_getCookies($mysqli, $ids){
            $data = [];

            foreach($ids as $id){
                $query = "SELECT title, icon_file_name, content_file_name, category_id, sub_category_id FROM cms.portal_content WHERE id = ?";

                $stmt = $mysqli->stmt_init();
                $stmt->prepare($query);
                $stmt->bind_param("i", $id);
                $stmt->execute();
                $stmt->store_result();
                $stmt->num_rows();

                $stmt->bind_result($title, $iconName, $fileName, $catId, $subId);

                while($stmt->fetch()){
                    array_push($data, [
                        "contentId" => $id,
                        "title" => $title,
                        "icon" => $iconName,
                        "filename" => $fileName,
                        "catId" => $catId,
                        "subId" => $subId
                    ]);
                }
            }

            $stmt->close();
            return $data;
        }
    }

    class Subcategory{
        public $cat;
        public $sqli;
        public $initialSubCatId;

        function __construct($cat, $mysqli){
            $this->cat = $cat;
            $this->sqli = $mysqli;
        }

        function getSubcategories(){
            $catCondition = "";
            $data = [];

            switch($this->cat){
                case "games-apk":
                    $catCondition = "AND (b.sub_category='Action' or b.sub_category='Adventure' or b.sub_category='Arcade' or b.sub_category='Puzzle' or b.sub_category='Simulation' or b.sub_category='Strategy' or b.sub_category='Board')";
                    break;
                case "html5":
                    $catCondition = "AND (b.sub_category='Action' or b.sub_category='Adventure' or b.sub_category='Racing' or b.sub_category='Strategy')";
                    break;
                default:
                    $catCondition = "AND (b.sub_category='Action' or b.sub_category='Adventure' or b.sub_category='Arcade' or b.sub_category='Puzzle' or b.sub_category='Simulation' or b.sub_category='Strategy' or b.sub_category='Board')";
                    break;
            }

            $catPercent = "{$this->cat}%";

            $stmt = $this->sqli->stmt_init();
            $stmt->prepare("SELECT a.id, a.category, b.id AS sc_id, b.sub_category FROM cms.categories a, cms.sub_categories b WHERE a.id = b.category_id $catCondition AND category like ?");
            $stmt->bind_param("s", $catPercent);
            $stmt->execute();
            $stmt->store_result();
            $stmt->num_rows();

            $stmt->bind_result($catId, $category, $subId, $subcategory);

            while($stmt->fetch()){
                array_push($data, [
                    "catId" => $catId,
                    "category" => $category,
                    "subId" => $subId,
                    "subcategory" => $subcategory
                ]);
            }

            $stmt->close();
            $this->initialSubCatId = $data[0]['subId'];
            return $data;
        }
    }

    class Category{
        public $sqli;
        public $cat;
        public $subCats = [];
        public $dir = "https://s3-ap-southeast-1.amazonaws.com/qcnt-portal/portal";

        function __construct($cat, $mysqli){
            $this->sqli = $mysqli;
            $this->cat = $cat;

            switch($cat){
                case "games-apk":
                    $this->subCats = [16, 15, 21, 76, 22, 20, 19];
                    break;
                case "html5":
                    $this->subCats = [77, 78, 79, 80];
                    break;
                default:
                    $this->subCats = [16, 15, 21, 76, 22, 20, 19];
                    break;
            }
        }

        function getCatgeoryAndSubCategoryName($catId, $subId){
            $query = "SELECT a.category, b.sub_category FROM cms.categories a, cms.sub_categories b WHERE a.id = ? AND b.id = ?";

            $stmt = $this->sqli->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("ii", $catId, $subId);
            $stmt->execute();
            $stmt->store_result();
            $stmt->num_rows();

            $stmt->bind_result($category, $subCategory);

            $data = [];

            while($stmt->fetch()){
                $data = [
                    "category" => $category,
                    "subCategory" => $subCategory
                ];
            }

            $stmt->close();
            return $data;
        }

        function getContentId_SubCat(){
            $data = [];


            foreach($this->subCats as $sub){
                $query = "SELECT id, sub_category_id FROM cms.portal_content WHERE sub_category_id=? ORDER BY RAND() LIMIT 10";

                $stmt = $this->sqli->stmt_init();
                $stmt->prepare($query);
                $stmt->bind_param("i", $sub);
                $stmt->execute();
                $stmt->store_result();

                if($stmt->num_rows() > 0){
                    $stmt->bind_result($id, $subId);
                    while($stmt->fetch()){
                        array_push($data, ["contentId" => $id, "subId" => $subId]);
                    }

                }
            }

            $stmt->close();

            // shuffle position of each data in the array
            shuffle($data);
            // get index of selected data || get random number from 0 to the length of the data array
            $dataIndex = rand(0, count($data) - 1);
            return $data[$dataIndex];
        }

        function getSelectedContent($id){

            $query = "SELECT title, description, icon_file_name, content_file_name, category_id, sub_category_id FROM cms.portal_content WHERE id = ?";

            $stmt = $this->sqli->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->num_rows();

            $stmt->bind_result($title, $description, $iconName, $fileName, $catId, $subId);

            $data = [];

            while($stmt->fetch()){
                $data = [
                    "contentId" => $id,
                    "title" => $title,
                    "description" => $description,
                    "icon" => $iconName,
                    "filename" => $fileName,
                    "catId" => $catId,
                    "subId" => $subId
                ];

                $this->selectedSubId = $subId;
                $this->selectedContentId = $id;
            }

            $stmt->close();
            return $data;
        }

    }

    class Content{
        public $sqli;
        public $dir = "https://s3-ap-southeast-1.amazonaws.com/qcnt-portal/portal";

        function __construct($mysqli){
            $this->sqli = $mysqli;
        }

        function getCatgeoryAndSubCategoryName($catId, $subId){
            $stmt = $this->sqli->stmt_init();
            $stmt->prepare("SELECT a.category, b.sub_category FROM cms.categories a, cms.sub_categories b WHERE a.id = ? AND b.id = ?");
            $stmt->bind_param("ii", $catId, $subId);
            $stmt->execute();
            $stmt->store_result();
            $stmt->num_rows();

            $stmt->bind_result($category, $subCategory);

            $data = [];

            while($stmt->fetch()){
                $data = [
                    "category" => $category,
                    "subCategory" => $subCategory
                ];
            }

            $stmt->close();
            return $data;
        }

        function getContents($subId){
            $stmt =  $this->sqli->stmt_init();
            $stmt->prepare("SELECT id, title, icon_file_name, content_file_name, category_id, sub_category_id FROM cms.portal_content WHERE id!=1 AND sub_category_id = ? ORDER BY id DESC LIMIT 20");
            $stmt->bind_param("i", $subId);
            $stmt->execute();
            $stmt->store_result();
            $stmt->num_rows();

            $data = [];

            $stmt->bind_result($id, $title, $icon, $filename, $catId, $subId);

            while($stmt->fetch()){
                $catNameSubName = $this->getCatgeoryAndSubCategoryName($catId, $subId);

                array_push($data, [
                    "contentId" => $id,
                    "title" => $title,
                    "icon" => $icon,
                    "filename" => $filename,
                    "category" => $catNameSubName['category'],
                    "subCategory" => $catNameSubName['subCategory']
                ]);
            }

            $stmt->close();
            return $data;
        }

        function getScreenshots($id){
            $stmt = $this->sqli->stmt_init();
            $stmt->prepare("SELECT screenshot_file_name FROM cms.portal_content_screenshots WHERE portal_content_id = ?");
            $stmt->bind_param("i", $id);
            $stmt->execute();

            $imgCont = [];

            $result = $stmt->get_result();

            while($row = $result->fetch_assoc()){
                array_push($imgCont, $row['screenshot_file_name']);
            }

            $stmt->close();

            return $imgCont;
        }

        function getContentDetails($id){
            $stmt = $this->sqli->stmt_init();
            $stmt->prepare("SELECT title, description, icon_file_name, content_file_name, category_id, sub_category_id FROM cms.portal_content WHERE id = ?");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->num_rows();

            $stmt->bind_result($title, $description, $icon, $filename, $catId, $subId);

            $data = [];

            while($stmt->fetch()){
                $dataScreen = [];

                $ext = pathinfo($filename, PATHINFO_EXTENSION);

                $dataAssoc = [
                    "title" => $title,
                    "description" => $description,
                    "icon" => $icon,
                    "filename" => $filename,
                    "catId" => $catId,
                    "subId" => $subId
                ];


                if($ext === "apk" || $ext === "xapk"){
                    $dataScreen = ['screenshots' => $this->getScreenshots($id)];
                }

                if(!empty($dataScreen)){
                    $merged = array_merge($dataAssoc, $dataScreen);
                    array_push($data, $merged);
                }else{
                    array_push($data, $dataAssoc);
                }
            }

            $stmt->close();

            return $data;
        }
    }

    class Search{
        public $sqli;
        public $searchVal;
        public $subCats = [19, 20, 22, 76, 21, 15, 16, 77, 78, 79, 80];
        public $dir = "https://s3-ap-southeast-1.amazonaws.com/qcnt-portal/portal";

        function __construct($mysqli, $searchVal){
            $this->sqli = $mysqli;
            $this->searchVal = $searchVal;
        }

        function getCatgeoryAndSubCategoryName($catId, $subId){
            $stmt = $this->sqli->stmt_init();
            $stmt->prepare("SELECT a.category, b.sub_category FROM cms.categories a, cms.sub_categories b WHERE a.id = ? AND b.id = ?");
            $stmt->bind_param("ii", $catId, $subId);
            $stmt->execute();
            $stmt->store_result();
            $stmt->num_rows();

            $stmt->bind_result($category, $subCategory);

            $data = [];

            while($stmt->fetch()){
                $data = [
                    "category" => $category,
                    "subCategory" => $subCategory
                ];
            }

            $stmt->close();
            return $data;
        }

        function getResult(){

            $data = [];

            foreach($this->subCats as $subId){
                $value = "%{$this->searchVal}%";
                $stmt = $this->sqli->stmt_init();
                $stmt->prepare("SELECT id, title, icon_file_name, content_file_name, category_id, sub_category_id FROM cms.portal_content WHERE sub_category_id = ? AND title LIKE ?");
                $stmt->bind_param("is", $subId, $value);
                $stmt->execute();
                $stmt->store_result();

                
                if($stmt->num_rows() > 0){
                    $stmt->bind_result($contId, $title, $icon, $filename, $catId, $subCatId);

                    while($stmt->fetch()){

                        $catNameSubName = $this->getCatgeoryAndSubCategoryName($catId, $subCatId);

                        $dataAssoc = [
                            "contentId" => $contId,
                            "title" => $title,
                            "icon" => $icon,
                            "filename" => $filename,
                            "catId" => $catId,
                            "subId" => $subCatId,
                            "category" => $catNameSubName['category'],
                            "subCategory" => $catNameSubName['subCategory']
                        ];

                        array_push($data, $dataAssoc);
                    }
                }
            }

            // $stmt->close();

            return $data;
        }
    }
?>