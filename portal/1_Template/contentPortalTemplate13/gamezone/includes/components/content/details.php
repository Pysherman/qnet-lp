<div class="container mx-auto px-5 mb-10">
    <?php 

        $id = filter_var($_GET['id'], FILTER_SANITIZE_SPECIAL_CHARS);

        $details = $content->getContentDetails($id);

        $catNameSubName = $content->getCatgeoryAndSubCategoryName($details[0]['catId'], $details[0]['subId']);

        $templateThumb = $utils->_switch($catNameSubName['category'], $content->dir, $catNameSubName, $details[0]);

        $cat = ['Games-apk' => 'Android Game', 'Html5' => 'Html5 Game'];
    ?>
    <!-- thumbnail container -->
    <div class="bg-white rounded-md shadow-customOuter p-5 md:w-5/6 lg:w-7/12 mb-10">
        <div class="text-center md:text-left md:grid md:grid-cols-customTabGrid md:gap-4 md:items-center">
            <div class="mb-6 w-36 lg:w-52 mx-auto">
                <?php echo $templateThumb['thumbnail']; ?>
            </div>
            <div class="mb-10 md:mb-0">
                <p class="text-textColor font-bold text-xl mb-2"><?php echo $details[0]['title']; ?></p>
                <p class="text-textColor text-sm"><?php echo $cat[$catNameSubName['category']]; ?> | <?php echo $catNameSubName['subCategory'] ?></p>
            </div>
        </div>
        <div class="text-center md:text-left">
            <?php 
                if($catNameSubName['category'] === 'Html5'):
            ?>
                    <a class="inline-block px-10 py-3 bg-primaryColor rounded-md text-xl font-bold text-white shadow-customOuter" href="<?php echo $details[0]['filename'] ?>">PLAY</a>
            <?php else: ?>
                    <a class="inline-block px-10 py-3 bg-primaryColor rounded-md text-xl font-bold text-white shadow-customOuter" href="<?php echo $content->dir; ?>/<?php echo strtolower($catNameSubName['category']); ?>/<?php echo strtolower($catNameSubName['subCategory']); ?>/<?php echo str_replace(" ", "+", strtolower($details[0]['title'])); ?>/<?php echo str_replace(" ", "+", $details[0]['filename']); ?>">DOWNLOAD</a>
            <?php endif; ?>
            
        </div>
    </div>
    <!-- end thumbnail container -->

    <!-- description -->
    <div class="mb-10">
        <h3 class="text-center md:text-left text-textColor text-base mb-4">Description</h3>
        <p class="text-justify leading-6 text-sm text-textColor"><?php echo $details[0]['description']; ?></p>
    </div>
    <!-- end description -->
    
    <!-- preview container -->
    <div>
        <h3 class="text-center md:text-left text-textColor text-base mb-4"><?php echo isset($details[0]['screenshots']) ? "Screenshots" : "Preview" ?></h3>

        <div>
            <?php 
                if(isset($details[0]['screenshots'])):
                    $screens = $utils->_switch_preview($catNameSubName['category'], $content->dir, $catNameSubName, $details[0]);
            ?>
                <div class="<?php echo (count($screens) > 1) ? "overflow-x-scroll whitespace-nowrap scrollbar" : ""; ?>">
                    <?php 
                        foreach($screens as $screen){
                            echo $screen;
                        }
                    ?>
                </div>
            <?php
                else:
                    echo $utils->_switch_preview($catNameSubName['category'], $content->dir, $catNameSubName, $details[0]);
                endif;
            ?>
        </div>
    </div>
    <!-- end preview container -->
</div>