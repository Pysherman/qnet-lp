<div class="container mx-auto px-5 mb-20">
    <div class="bg-white rounded-md shadow-customOuter p-5 md:w-5/6 lg:w-7/12">
        <?php 
            $removeS =  rtrim($title[$_GET['cat']], "s"); 
            $title = ['games-apk' => 'Android', 'html5' => 'Html5'];
        ?>
        <h3 class="text-base lg:text-xl text-textColor font-bold pb-5"><?php echo $title[$_GET['cat']]; ?> Game of the Day</h3>
        <?php
            $category = new Category($sanitizeCat, $mysqli);

            $toLower = strtolower($removeS);
            $cookieName = "gamezone{$toLower}OfDay";

            if(isset($_COOKIE[$cookieName])):
                $sanitizedCookieId = filter_var($_COOKIE[$cookieName], FILTER_SANITIZE_SPECIAL_CHARS);

                $catContentOfDay = $category->getSelectedContent($sanitizedCookieId);

                $catNameSubName = $category->getCatgeoryAndSubCategoryName($catContentOfDay['catId'], $catContentOfDay['subId']);

                $template = $utils->_switch($catNameSubName['category'], $category->dir, $catNameSubName, $catContentOfDay);
        ?>
                    <div class="grid grid-cols-customTabGrid gap-4">
                        <div>
                            <a class="block w-36 lg:w-52" href="content.php?cat=<?php echo $catNameSubName['category']; ?>&subcat=<?php echo $catNameSubName['subCategory']; ?>&id=<?php echo $catContentOfDay['contentId']; ?>">
                                <?php echo $template['thumbnail']; ?>
                            </a>
                        </div>
                        <div class="self-end">
                            <p class="text-textColor text-sm lg:text-base font-bold"><?php echo $catContentOfDay['title']; ?></p>
                            <p class="text-textColor text-xs lg:text-sm mb-4 lg:mb-6"><?php echo $catNameSubName['subCategory']; ?></p>

                            <?php if($_GET['cat'] === 'html5'): ?>
                                    <a class="inline-block px-10 py-3 bg-primaryColor rounded-md text-xl font-bold text-white shadow-customOuter" href="<?php echo $catContentOfDay[0]['filename'] ?>">PLAY</a>
                            <?php else: ?>
                                    <a class="inline-block bg-primaryColor rounded-md text-white font-bold text-sm lg:text-base px-6 py-2.5 shadow-customOuter" href="<?php echo $category->dir; ?>/<?php echo strtolower($catNameSubName['category']); ?>/<?php echo strtolower($catNameSubName['subCategory']); ?>/<?php echo str_replace(" ", "+", strtolower($catContentOfDay['title'])); ?>/<?php echo str_replace(" ", "+", $catContentOfDay['filename']); ?>">DOWNLOAD</a>
                            <?php endif; ?>
                        </div>
                    </div>
        <?php
            else:
                $cookieCat = $category->getContentId_SubCat();
                setCookie($cookieName, $cookieCat['contentId'], time() + 86400, "/");

                $catContentOfDay = $category->getSelectedContent($cookieCat['contentId']);

                $catNameSubName = $category->getCatgeoryAndSubCategoryName($catContentOfDay['catId'], $catContentOfDay['subId']);

                $template = $utils->_switch($catNameSubName['category'], $category->dir, $catNameSubName, $catContentOfDay);
        ?>
                    <div class="grid grid-cols-customTabGrid gap-4">
                        <div>
                            <a class="block w-36 lg:w-52" href="content.php?cat=<?php echo $catNameSubName['category']; ?>&subcat=<?php echo $catNameSubName['subCategory']; ?>&id=<?php echo $catContentOfDay['contentId']; ?>">
                                <?php echo $template['thumbnail']; ?>
                            </a>
                        </div>
                        <div class="self-end">
                            <p class="text-textColor text-sm lg:text-base font-bold"><?php echo $catContentOfDay['title']; ?></p>
                            <p class="text-textColor text-xs lg:text-sm mb-4 lg:mb-6"><?php echo $catNameSubName['subCategory']; ?></p>
                            <a class="inline-block bg-primaryColor rounded-md text-white font-bold text-sm lg:text-base px-6 py-2.5 shadow-customOuter" href="<?php echo $category->dir; ?>/<?php echo strtolower($catNameSubName['category']); ?>/<?php echo strtolower($catNameSubName['subCategory']); ?>/<?php echo str_replace(" ", "+", strtolower($catContentOfDay['title'])); ?>/<?php echo str_replace(" ", "+", $catContentOfDay['filename']); ?>">DOWNLOAD</a>
                        </div>
                    </div>
        <?php
            endif;
        ?>
    </div>
</div>