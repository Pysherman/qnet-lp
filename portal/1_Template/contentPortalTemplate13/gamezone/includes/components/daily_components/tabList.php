<div class="container mx-auto p-5">
    <?php
        include('tabs.php'); 

        $suggestedContents = new Tab("more", $daily->selectedSubId, $daily->selectedContentId, "gamezoneSuggestedContents", 86400, $mysqli, "", "Suggested Contents");
        $suggestedContents->_execute();

        $topGamesWeek = new Tab("games", 6, 0, "androidGamesOfWeek", 86400 * 7, $mysqli, "AND(sub_category_id=16 or sub_category_id=15 or sub_category_id=21 or sub_category_id=76 or sub_category_id=22 or sub_category_id=20 or sub_category_id=19)", "Top Android Games this Week");
        $topGamesWeek->_execute();

        $topVideosWeek = new Tab("videos", 9, 0, "html5GamesOfWeek", 86400 * 7, $mysqli, "AND(sub_category_id=77 or sub_category_id=78 or sub_category_id=79 or sub_category_id=80)", "Top Html5 Games this Week");
        $topVideosWeek->_execute();

        // $topTonesWeek = new Tab("tones", 4, 0, "tonesOfWeek", 86400 * 7, $mysqli, "AND(sub_category_id=89 or sub_category_id=90 or sub_category_id=92)", "Top Tones this Week");
        // $topTonesWeek->_execute();

        // $topAppssWeek = new Tab("apps", 2, 0, "appsOfWeek", 86400 * 7, $mysqli, "AND(sub_category_id=28 or sub_category_id=18 or sub_category_id=29 or sub_category_id=17)", "Top Apps this Week");
        // $topAppssWeek->_execute();
    ?>
</div>