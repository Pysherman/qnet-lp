<?php include('includes/globals/head.php'); ?>
    <title>Gamezone | Daily</title>
</head>
<body class="bg-mainBg font-sans relative">
<?php
        include('includes/connect.php');
        include('includes/func.php');
?>
<!-- main -->
<div>
    <!-- header & LP -->
    <div class="bg-gradient-to-b from-secondaryColor to-primaryColor mb-6 lg:mb-12">
        <!-- header -->
        <?php include('includes/globals/headerNav.php'); ?>
        <!-- end header -->

        <!-- search -->
        <div class="container mx-auto mb-20 lg:grid lg:grid-cols-2 lg:gap-10">
            <!-- search container  -->
            <?php include('includes/globals/searchForm.php'); ?>
            <!-- end search container -->
        </div>
        <!-- end search -->

        <!-- lp  -->
        <div class="pb-20 lg:pt-20 lg:pb-52">
            <?php include('includes/components/daily_components/contentOfDay.php'); ?>
        </div>
        <!-- end lp -->
    </div>
    <!-- end header & LP -->
    
    <!-- tabular list -->
    <div>
        <?php
            include('includes/components/daily_components/tabLIst.php');
        ?>
    </div>
    <!-- end tabular list -->
</div>
<!-- end main -->
    
</body>
</html>