const getSubMenuLinks = document.querySelectorAll(".sub-menu a");
const getGroups = document.querySelectorAll(".group");
const getItem = document.querySelectorAll(".item");
const getXbtn = document.querySelectorAll(".x-button");

function MainFunc(eTrigger, getGroups, getItem){
	this.eTrigger = eTrigger,
	this.getGroups = getGroups,
	this.getItem = getItem,
	this.subMenuFn = submenu => {
		submenu.addEventListener('click', e => {
			let createSpan = document.createElement('span');
			let currentSubCat = e.currentTarget.innerHTML;
			
			this.getItem.forEach(item => {
				if(currentSubCat !== item.dataset.subcat){
					item.style.cssText = "display:none;";
				}else if(currentSubCat == item.dataset.subcat){
					item.style.cssText = "display:block;";
				}
			});
			
			createSpan.innerHTML = e.currentTarget.innerHTML;
			this.getGroups[1].firstElementChild.prepend(createSpan);
			this.getGroups[0].style.cssText = "margin:0px 15px;";
			this.getGroups[1].style.cssText = "display:block; position:absolute; top:999px; left:0px;";
			
			this.timeOut(this.getGroups[1], 10);
		});
	},
	this.itemFn = item => {
		item.addEventListener('click', e => {
			if(e.currentTarget.classList.contains("Html5")){
				return false;
			}
			
			let getThumb = e.currentTarget.firstElementChild.children[0].firstElementChild.getAttribute('src');
			let getTitle = e.currentTarget.firstElementChild.children[1].innerHTML;
			let getDescrip = e.currentTarget.firstElementChild.children[3].innerHTML;
			let getDl = e.currentTarget.firstElementChild.children[2].getAttribute('href');
			
			let createSpan = document.createElement('span');
			let createImg = document.createElement('img');
			let createDl = document.createElement('a');
			let createP = document.createElement('p');
			
			createImg.setAttribute('src', getThumb);
			createDl.setAttribute('href', getDl);
			createDl.innerHTML = "DOWNLOAD";
			createP.innerHTML = getDescrip;
			createSpan.innerHTML = getTitle;
			
			this.getGroups[2].firstElementChild.prepend(createSpan);
			this.getGroups[2].lastElementChild.append(createImg);
			this.getGroups[2].lastElementChild.append(createDl);
			this.getGroups[2].lastElementChild.append(createP);
			
			this.getGroups[0].style.cssText = "margin:0px 30px; opacity:.8;";
			this.getGroups[1].style.cssText = "display:block; margin:0px 15px; position:absolute; top:10px; left:0px;";
			this.getGroups[2].style.cssText = "display:block; position:absolute; top:999px; left:0px;";
			
			this.timeOut(this.getGroups[2], 20);
		});
	},
	this.timeOut = (timeElem, posValue) => {
		setTimeout(() => {
			timeElem.style.cssText = "display:block; position:absolute; top:"+posValue+"px; left:0px;";
		}, 400)
	},
	this.forEachFn = () => {
		switch(eTrigger){
			case getSubMenuLinks:
				this.eTrigger.forEach(this.subMenuFn);
				break;
			case getItem:
				this.eTrigger.forEach(this.itemFn);
				break;
			default:
				return false;
				break;
		}
		
	}
}

function CloseFunc(eTrigger, className, getGroups){
	this.eTrigger = eTrigger,
	this.className = className,
	this.getGroups = getGroups,
	this.closeFn = () => {
		this.eTrigger.addEventListener('click', e => {
			let getHeaderName = document.querySelector("."+this.className);
			if(getHeaderName.classList.contains("sub-menu-title")){
				this.getGroups[1].style.cssText = "display:block; position:absolute; top:999px; left:0px;";
				this.getGroups[0].style.cssText = "margin:0;";
				
				this.closeTimeOut(this.getGroups[1], getHeaderName);
			}else if(getHeaderName.classList.contains("prev-title")){
				this.getGroups[1].style.cssText = "display:block; position:absolute; top:10px; left:0px; margin:0px;";
				this.getGroups[0].style.cssText = "margin:0px 15px;";
				this.getGroups[2].style.cssText = "display:block; position:absolute; top:999px; left:0px;";
				
				this.closeTimeOut(this.getGroups[2], getHeaderName);
			}
				
		});
	},
	this.closeTimeOut = (group, headerName) => {
		setTimeout(() => {
			group.style.cssText = "display:none;";
			headerName.removeChild(headerName.firstElementChild);
			
			if(headerName.classList.contains("prev-title")){
				while(headerName.nextElementSibling.lastElementChild){
					headerName.nextElementSibling.removeChild(headerName.nextElementSibling.lastElementChild)
				}
			}
		}, 400);
	};
};

let subMenuFunction = new MainFunc(getSubMenuLinks, getGroups, getItem);
let itemFunction = new MainFunc(getItem, getGroups, getItem);

let closeSubMenuFunction = new CloseFunc(getXbtn[0], "sub-menu-title", getGroups);
let closeItemFunction = new CloseFunc(getXbtn[1], "prev-title", getGroups);

subMenuFunction.forEachFn();
itemFunction.forEachFn();

closeSubMenuFunction.closeFn();
closeItemFunction.closeFn();