const getSubMenuLinks = document.querySelectorAll(".sub-menu a");
const getGroups = document.querySelectorAll(".group");
const getItem = document.querySelectorAll(".item");
const getXbtn = document.querySelectorAll(".x-button");

let subMenuLinksFn = subMenu => {
    subMenu.addEventListener('click', e => {
        let createSpan = document.createElement('span');
        let currentSubCat = e.currentTarget.innerHTML;
		let sib = e.currentTarget.parentElement.parentElement.nextElementSibling;
		
        getItem.forEach(item => {
            if(currentSubCat !== item.dataset.subcat){
                item.style.cssText = "display:none;";
            }else if(currentSubCat == item.dataset.subcat){
                item.style.cssText = "display:block;";
            }
        });

        createSpan.innerHTML = e.currentTarget.innerHTML;
		sib.firstElementChild.prepend(createSpan);
	
		let x = 0;
		
		for(let i = getGroups.length - 1; i < ; i--){
			x += 15;
			getGroups[i].style.margin = "0px "+x+"px";
			
			if(i == 0){
				break;
			}
			
			console.log(getGroups[i]);
		}
		
		sib.style.cssText = "display:block; position:absolute; top:999px; left:0px;";
		
		setTimeout(() => {
			sib.style.cssText = "display:block; position:absolute; top:10px; left:0px;";
		}, 400);
		
    });
};

let itemFn = item => {
    item.addEventListener('click', e => {
		if(e.currentTarget.classList.contains("Html5")){
			return false;
		}
		
		let sib = e.currentTarget.parentElement.parentElement.nextElementSibling;
		
        let getThumb = e.currentTarget.firstElementChild.children[0].firstElementChild.getAttribute('src');
        let getTitle = e.currentTarget.firstElementChild.children[1].innerHTML;
        let getDescrip = e.currentTarget.firstElementChild.children[3].innerHTML;
        let getDl = e.currentTarget.firstElementChild.children[2].getAttribute('href');

        let createSpan = document.createElement('span');
        let createImg = document.createElement('img');
		let createDl = document.createElement('a');
		let createP = document.createElement('p');

        createImg.setAttribute('src', getThumb);
		createDl.setAttribute('href', getDl);
		createDl.innerHTML = "DOWNLOAD";
		createP.innerHTML = getDescrip;
		createSpan.innerHTML = getTitle;

        sib.firstElementChild.prepend(createSpan);
        sib.lastElementChild.append(createImg);
        sib.lastElementChild.append(createDl);
        sib.lastElementChild.append(createP);
		
		let x = 0;
		let y = 10;
		
		for(let i = getGroups.length - 1; i < getGroups.length; i--){
			x += 15;
			getGroups[i].style.margin = "0px "+x+"px";
			getGroups[i].style.display = "block";
			getGroups[i].style.top = y+"px";
			
			if(i == 0){
				break;
			}
			console.log(x);
		}
		

        //getGroups[0].style.cssText = "margin:0px 30px; opacity:.8;";
        //getGroups[1].style.cssText = "display:block; margin:0px 15px; position:absolute; top:10px; left:0px;";
		sib.style.cssText = "display:block; position:absolute; top:999px; left:0px;";
        setTimeout(() => {
			sib.style.cssText = "display:block; position:absolute; top:20px; left:0px;";
		}, 400);
		
    });
};

getItem.forEach(itemFn);
getSubMenuLinks.forEach(subMenuLinksFn);