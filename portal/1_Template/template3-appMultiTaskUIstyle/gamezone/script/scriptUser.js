let storedUsers = JSON.parse(localStorage.getItem('user'));
const userVar = phpVar;
const getLogoutBtn = document.querySelector('.logout-btn');
const getUserImg = document.querySelector('.user-img');
const getOverClose = document.querySelector('.over-close');

let changeStatus = statusCode => {
	for(let i = 0; i < storedUsers.length; i++){
		//if(storedUsers[i][0].includes(userVar)){
		if(userVar == storedUsers[i][1]){
			let updatedUser = [storedUsers[i][0], storedUsers[i][1], storedUsers[i][2], statusCode]
			let indexUser = storedUsers.indexOf(storedUsers[i]);			
			storedUsers.splice(indexUser, 1);
			
			storedUsers = storedUsers || [];
			storedUsers.splice(indexUser, 0, updatedUser);
			localStorage.setItem('user', JSON.stringify(storedUsers));
			console.log('updated');
			
		}
	}
};

let fetchUserData = () => {
	const found = storedUsers.find(elem => {
		if(elem[3] == 1) return elem;
	});
	
	let getInfoDetails = document.querySelector('.info-details');
	getInfoDetails.firstElementChild.firstElementChild.innerHTML = found[0];
	getInfoDetails.firstElementChild.nextElementSibling.firstElementChild.innerHTML = found[1];
};

let checkStatus = () => {
	const found = storedUsers.find(elem => {
		if(elem[3] == 1) return elem;
	});
	if(found != undefined){
		if(userVar == "empty"){
			window.location.href = "index.php?varx=games-apk&user="+found[1];
		}
	}else{
		console.log('not')
		window.location.href = "login.php";
	}
}

let toggleOverview = eSelector => {
	let getOverview = document.querySelector('.overview');

    switch(eSelector){
        case "user-img":
            getOverview.classList.toggle('shw-elem');
            setTimeout(() => {
                getOverview.style.cssText = "top:0px;";
            }, 400);
            break;
        case "over-close":
            getOverview.style.cssText = "top:999px;";
            setTimeout(() => {
                getOverview.classList.toggle('shw-elem');
            }, 400);
            break;
        default:
            break;
    }   
};

window.addEventListener('load', () => {
	changeStatus(1);
	checkStatus();
	fetchUserData();
});

getLogoutBtn.addEventListener('click', () => {
	changeStatus(0);
});

getUserImg.addEventListener('click', e => {
    e.preventDefault();
	toggleOverview("user-img");
});

getOverClose.addEventListener('click', e => {
    e.preventDefault();
    toggleOverview("over-close");
});