const getErrorDiv = document.querySelector('.error-div');
const getSignUpBtn = document.querySelector('.sign-up-btn');
const getSignUpLink = document.querySelector('.sign-up-link');
const getFormHeader = document.querySelector('.form-header');
const getLoginBtn = document.querySelector('.login-btn');

let preventErrorStack = () => {
    while(getErrorDiv.firstElementChild){
        getErrorDiv.removeChild(getErrorDiv.firstElementChild);
    }
}

let loginUser = () => {
	let errorArr = [];
	let mnumValue = document.querySelector('.m-num-login');
	let pwordValue = document.querySelector('.p-word-login');
	let storedUsers = JSON.parse(localStorage.getItem('user'));
	
	if((mnumValue.value === "" || mnumValue.value === null) || (pwordValue.value === "" || pwordValue.value === null)){
        errorArr.push("Please provide your mobile number and password");
        pwordValue.style.borderBottom = "1px solid rgb(255, 72, 72)";
        mnumValue.style.borderBottom = "1px solid rgb(255, 72, 72)";
	}else if((mnumValue.value != "" || mnumValue.value != null) && (pwordValue.value != "" || pwordValue.value != null)){

        let rtn = (rtnValue, idxNum) => {
            for(let i = 0; i < storedUsers.length; i++){
                if(storedUsers[i][idxNum].includes(rtnValue)){
                    return storedUsers[i][idxNum];
                }
            }
            return undefined;
        }

        const pwordRtn = rtn(pwordValue.value, 2);
        const mnumRtn =  rtn(mnumValue.value, 1);

        if((mnumRtn != mnumValue.value && pwordRtn != pwordValue.value) || mnumRtn != mnumValue.value || pwordRtn != pwordValue.value){
            errorArr.push("Invalid Username or Password");
            pwordValue.style.borderBottom = "1px solid rgb(255, 72, 72)";
            mnumValue.style.borderBottom = "1px solid rgb(255, 72, 72)";
        }

        if(mnumRtn == mnumValue.value && pwordRtn == pwordValue.value){
            let currentUser = mnumRtn;
            window.location.href = "index.php?varx=games-apk&user="+currentUser;
            pwordValue.style.borderBottom = "1px solid #0C65A1";
            mnumValue.style.borderBottom = "1px solid #0C65A1";
            mnumValue.value = "";
            pwordValue.value = "";
        }
    }
    checkErrors2(errorArr);
};

let saveUser = () => {
    let errorArr = [];
    let storedUsers = JSON.parse(localStorage.getItem('user'));
    let unameValue = document.querySelector('.u-name');
    let mnumValue = document.querySelector('.m-num-signup');
    let pwordValue = document.querySelector('.p-word-signup');
    let rpwordValue = document.querySelector('.rp-word');

    storedUsers.forEach(user => {
        if(user.includes(mnumValue.value)){
            errorArr.push("Number already registered");
            errorArr.push("number");
        }
    });
	
	if(mnumValue.value === null || mnumValue.value === ""){
		errorArr.push("Please provide your mobile number");
		errorArr.push("number");
	}
	
	if(unameValue.value === null || unameValue.value === ""){
		errorArr.push("Please provide a name");
		errorArr.push("name");
    }
    
	
	if(pwordValue.value === null || pwordValue.value === ""){
		errorArr.push("Please provide a password");
		errorArr.push("password");
	}else if(pwordValue.value.length > 8 || pwordValue.value.length < 8){
		errorArr.push("Password must be 8 characters only");
		errorArr.push("password");
	}else if(pwordValue.value != ""){
		if(rpwordValue.value === "" || rpwordValue.value === null){
			errorArr.push("Please verify your password");
			errorArr.push("confirm");
		}else if(rpwordValue.value != pwordValue.value){
			errorArr.push("Password didn\'t match");
			errorArr.push("confirm");
		}
	}
    
    highlightErrors(errorArr, unameValue, mnumValue, rpwordValue, pwordValue);
    checkErrors(errorArr, unameValue.value, mnumValue.value, rpwordValue.value, 0, unameValue, mnumValue, pwordValue, rpwordValue);
};

let highlightErrors = (errorArr, unameValue, mnumValue, rpwordValue, pwordValue) => {
    if(errorArr.includes("name")){
        unameValue.style.borderBottom = "1px solid rgb(255, 72, 72)";
    }else{
        unameValue.style.borderBottom = "1px solid #0C65A1";
    }
    if(errorArr.includes("number")){
        mnumValue.style.borderBottom = "1px solid rgb(255, 72, 72)";
    }else{
        mnumValue.style.borderBottom = "1px solid #0C65A1";
    }
    if(errorArr.includes("password")){
        pwordValue.style.borderBottom = "1px solid rgb(255, 72, 72)";
    }else{
        pwordValue.style.borderBottom = "1px solid #0C65A1";
    }
    if(errorArr.includes("confirm")){
        rpwordValue.style.borderBottom = "1px solid rgb(255, 72, 72)";
    }else{
        rpwordValue.style.borderBottom = "1px solid #0C65A1";
    }
};

let checkErrors = (errorArr, unameValue, mnumValue, rpwordValue, userStatus, unameInput, mnumInput, pwordInput, rpwordInput) => {
	if(errorArr.length > 0){
		preventErrorStack();
		
		for(let i = 0; i < errorArr.length; i+=2){
			let createPTag = document.createElement('p');
			let createText = document.createTextNode(errorArr[i]);
			createPTag.appendChild(createText);
			getErrorDiv.appendChild(createPTag);
		}
	}else if(errorArr.length === 0 || errorArr.length === null){
		let getUserDataFromLocal = JSON.parse(localStorage.getItem('user'));
		let user =[unameValue, mnumValue, rpwordValue, userStatus];
		getUserDataFromLocal = getUserDataFromLocal || [];
        getUserDataFromLocal.push(user);
        localStorage.setItem('user', JSON.stringify(getUserDataFromLocal));
        getErrorDiv.innerHTML = '<h2 class="success-text">Succesfully Registered</h2>';

        unameInput.value = "";
        mnumInput.value = "";
        pwordInput.value = "";
        rpwordInput.value = "";

        setTimeout(() => {
            location.reload();
        }, 1000);
    }
}

let checkErrors2 = errorArr => {
	if(errorArr.length > 0){
        preventErrorStack();
        
		let createPTag = document.createElement('p');
		let createText = document.createTextNode(errorArr[0])
		createPTag.appendChild(createText);
		getErrorDiv.appendChild(createPTag);
	}
};

getSignUpLink.addEventListener('click', e => {
	const getHeaderH2 = document.querySelector('.form-header h2');
	e.currentTarget.parentElement.parentElement.style.display = "none";
	e.currentTarget.parentElement.parentElement.nextElementSibling.style.display = "block";
    getHeaderH2.innerHTML = "Sign Up";
    preventErrorStack();
});

getLoginBtn.addEventListener('click', e => {
	e.preventDefault();
	loginUser();
});

getSignUpBtn.addEventListener('click', e => {
	e.preventDefault();
	saveUser();
});

document.querySelector('.m-num-signup').addEventListener('input', () => {
    if(isNaN(document.querySelector('.m-num-signup').value)){
        document.querySelector('.m-num-signup').value = "";
    }
});

document.querySelector('.m-num-login').addEventListener('input', () => {
    if(isNaN(document.querySelector('.m-num-login').value)){
        document.querySelector('.m-num-login').value = "";
    }
});