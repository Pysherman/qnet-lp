const getSubMenuLinks = document.querySelectorAll(".sub-menu a");
const getGroups = document.querySelectorAll(".group");
const getItem = document.querySelectorAll(".item");
const getXbtn = document.querySelectorAll(".close-btn");
const getMainNavLink = document.querySelectorAll(".main-menu ul li a")

let closeFn = (closeBtn, headerName) => {
	closeBtn.addEventListener('click', () => {
		let getHeaderName = document.querySelector("."+headerName);
		switch(headerName){
			case "sub-menu-title":
				getGroups[1].style.cssText = "display:block; position:absolute; top:999px; left:0px;";
				getGroups[0].style.cssText = "margin:0;";
				getGroups[0].firstElementChild.style.cssText = "background:linear-gradient(90deg, #051140, #0C65A1);";
				
				setTimeout(() => {
					getGroups[1].style.cssText = "display:none;";
					getHeaderName.removeChild(getHeaderName.firstElementChild);
				}, 400);
				
				break;
				
			case "prev-title":
				getGroups[1].style.cssText = "display:block; position:absolute; top:10px; left:0px; margin:0px;";
				getGroups[1].firstElementChild.style.cssText = "background:linear-gradient(90deg, #051140, #0C65A1);";
				getGroups[0].style.cssText = "margin:0px 15px;";
				getGroups[2].style.cssText = "display:block; position:absolute; top:999px; left:0px;";
				
				setTimeout(() => {
					getGroups[2].style.cssText = "display:none;";
					getHeaderName.removeChild(getHeaderName.firstElementChild);
				
					while(getHeaderName.nextElementSibling.lastElementChild){
						getHeaderName.nextElementSibling.removeChild(getHeaderName.nextElementSibling.lastElementChild)
					}
				}, 400);
				break;
				
			default:
				break;
		}
	});
}

let subMenuLinksFn = subMenu => {
    subMenu.addEventListener('click', e => {
        let createSpan = document.createElement('span');
        let currentSubCat = e.currentTarget.innerHTML;

        getItem.forEach(item => {
            if(currentSubCat !== item.dataset.subcat){
                item.style.cssText = "display:none;";
            }else if(currentSubCat == item.dataset.subcat){
                item.style.cssText = "display:block;";
            }
        });

        createSpan.innerHTML = e.currentTarget.innerHTML;
        getGroups[1].firstElementChild.prepend(createSpan);
		getGroups[0].style.cssText = "margin:0px 15px;";
		getGroups[0].firstElementChild.style.cssText = "background:#051140;";
        getGroups[1].style.cssText = "display:block; position:absolute; top:999px; left:0px;";
		
		setTimeout(() => {
			getGroups[1].style.cssText = "display:block; position:absolute; top:10px; left:0px;";
		}, 400);
		
    });
};

let itemFn = item => {
    item.addEventListener('click', e => {
		if(e.currentTarget.classList.contains("Html5")){
			return false;
		}
		
        let getThumb = e.currentTarget.firstElementChild.children[0].firstElementChild.getAttribute('src');
        let getTitle = e.currentTarget.firstElementChild.children[1].innerHTML;
        let getDescrip = e.currentTarget.firstElementChild.children[3].innerHTML;
        let getDl = e.currentTarget.firstElementChild.children[2].getAttribute('href');

        let createSpan = document.createElement('span');
        let createImg = document.createElement('img');
		let createDl = document.createElement('a');
		let createP = document.createElement('p');

        createImg.setAttribute('src', getThumb);
		createDl.setAttribute('href', getDl);
		createDl.innerHTML = "DOWNLOAD";
		createP.innerHTML = getDescrip;
		createSpan.innerHTML = getTitle;

        getGroups[2].firstElementChild.prepend(createSpan);
        getGroups[2].lastElementChild.append(createImg);
        getGroups[2].lastElementChild.append(createDl);
        getGroups[2].lastElementChild.append(createP);

		getGroups[0].style.cssText = "margin:0px 30px;";
		getGroups[0].firstElementChild.style.cssText = "background:#051140;";
		getGroups[1].style.cssText = "display:block; margin:0px 15px; position:absolute; top:10px; left:0px;";
		getGroups[1].firstElementChild.style.cssText = "background:#051140;";
		getGroups[2].style.cssText = "display:block; position:absolute; top:999px; left:0px;";
        setTimeout(() => {
			getGroups[2].style.cssText = "display:block; position:absolute; top:20px; left:0px;";
		}, 400);
		
    });
};

let navFunc = () => {
    let currentUrl = document.location.href;
    getMainNavLink[0].classList.add('active');
    getMainNavLink.forEach( navLink => {
        if(currentUrl.includes(navLink.getAttribute('href'))){
            getMainNavLink[0].classList.remove('active');
            let selHref = document.querySelectorAll('[href = "'+navLink.getAttribute('href')+'"]');
            selHref.forEach(href => {
                href.parentElement.classList.add('active');
            })
        }
    });
}

navFunc();

getItem.forEach(itemFn);
getSubMenuLinks.forEach(subMenuLinksFn);


closeFn(getXbtn[1], "prev-title");
closeFn(getXbtn[0], "sub-menu-title");