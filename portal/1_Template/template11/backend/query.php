<?php
    require('connect.php');

    if(isset($_POST['id'])){
        $id = filter_var($_POST['id'], FILTER_SANITIZE_SPECIAL_CHARS);

        $stmt = $mysqli->stmt_init();
        $stmt->prepare("SELECT id, title, description, icon_file_name, content_file_name FROM cms.portal_content WHERE id = ?");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->num_rows();

        $stmt->bind_result($id, $title, $description, $iconName, $contentName);

        $data = [];

        while($stmt->fetch()){
            $dataScreen = [];

            $dataAssoc = [
                "title" => $title,
                "description" => $description,
                "iconName" => pathinfo($iconName, PATHINFO_FILENAME),
                "contentName" => $contentName
            ];

            $stmtScreen = $mysqli->stmt_init();
            $stmtScreen->prepare("SELECT screenshot_file_name FROM cms.portal_content_screenshots WHERE portal_content_id = ?");
            $stmtScreen->bind_param("i", $id);
            $stmtScreen->execute();

            $imgCont = [];

            $result = $stmtScreen->get_result();

            while($row = $result->fetch_assoc()){
                array_push($imgCont, $row['screenshot_file_name']);
            }

            $dataScreen = ["screenshots" => $imgCont];

            if(!empty($dataScreen)){
                $merged = array_merge($dataAssoc, $dataScreen);
                array_push($data, $merged);
                $stmtScreen->close();
            }else{
                array_push($data, $dataAssoc);
            }
        }

        echo json_encode($data);
        $stmt->close();
    }
?>