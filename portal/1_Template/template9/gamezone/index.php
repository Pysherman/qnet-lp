<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:wght@400;500;600&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="stylesheet/style.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/ScrollToPlugin.min.js" integrity="sha512-nTHzMQK7lwWt8nL4KF6DhwLHluv6dVq/hNnj2PBN0xMl2KaMm1PM02csx57mmToPAodHmPsipoERRNn4pG7f+Q==" crossorigin="anonymous" defer></script>
	<script src="script/script.js" defer></script>
	<title>Gamezone</title>
</head>
<body>
	<main class="mainContainer">
		<header class="headerContainer" id="headerContainer">
			<h1 class="logo">Gamezone</h1>
			<div class="searchContainer">
				<input type="text" name="searchInput" class="searchInput" id="searchInput" placeholder="Search...">
				<button type="button" class="searchBtn" id="searchBtn">Search</button>
			</div>
		</header>
		<nav class="navContainer" id="navContainer">
			<section class="navBurgWrapper" id="navBurgWrapper">
				<span></span>
				<span></span>
				<span></span>
			</section>
			<section class="navWrapper" id="navWrapper">
				<div class="catContainer" id="catContainer">
					
					<div class="catBtnContainer">
						<button class="catBtn active" type="button" data-cat="Games-apk">Android</button>
					</div>
					<div class="catBtnContainer">
						<button class="catBtn" type="button" data-cat="Html5">HTML5</button>
					</div>
					
				</div>
			</section>
		</nav>
		<section class="contentsContainer" id="contentsContainer">
			<div class="contentsWrapper" id="contentsWrapper"></div>
		</section>
	</main>
</body>
</html>