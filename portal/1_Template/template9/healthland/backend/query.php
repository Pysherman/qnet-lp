<?php 
	include('inc.php'); 

	class FetchData {
		public $cat;
		public $catCond;

		function __construct(){
			if(isset($_POST['catInitial'])){
				$this->cat = $_POST['catInitial'];
			}

			if($this->cat === "Videos"){
				$this->catCond = "and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
			}else if($this->cat === "Apps"){
				$this->catCond = "and (b.sub_category='Fitness' or b.sub_category='LifeStyle' or b.sub_category='Relaxation')";
			}elseif($this->cat === "Tips"){
				$this->catCond = "and (b.sub_category='Foods' or b.sub_category='Sports')";
			}else{
				return;
			}
		}

		function fetchSubcategories($conn){
			$cat = "{$this->cat}%";
			$catCond = $this->catCond;


			$stmt = $conn->prepare("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $catCond and category like ?");
			$stmt->bind_param("s", $cat);
			$stmt->execute();
			$stmt->store_result();
			$stmt->num_rows();

			$stmt->bind_result($catId, $category, $subCatId, $subCategory);

			$data = [];

			while($stmt->fetch()){
				$dataAssoc = [
					"catId" => $catId,
					"subCatId" => $subCatId,
					"subCategory" => ucwords(str_replace("-", " ", $subCategory)),
					"category" => $category
				];

				array_push($data, $dataAssoc);
			}

			$stmt->close();

			return $data;
		}

		function fetchContents($conn, $subCatId, $catId){
			$stmt = $conn->prepare("SELECT id, title, file_name, original_file_name, mime, description FROM cms.contents WHERE id!=1 and category_id=? and sub_category_id=? order by id desc limit 30");
			$stmt->bind_param("ii", $catId, $subCatId);
			$stmt->execute();
			$stmt->store_result();
			$stmt->num_rows();

			$stmt->bind_result($id, $title, $filename, $originalFilename, $mime, $description);

			$data = [];

			while($stmt->fetch()){
				$dataAssoc = [
					"contentId" => $id,
					"title" => $title,
					"filename" => $filename,
					"ext" => pathinfo($filename, PATHINFO_EXTENSION),
					"thumbname" => pathinfo($filename, PATHINFO_FILENAME),
					"originalFilename" => $originalFilename,
					"description" => $description
				];

				array_push($data, $dataAssoc);
			}

			$stmt->close();

			return $data;

		}

		function fetchContentsViaSearch($conn, $searchValue){
			$data = [];
			$subIds = [];
			$query = "sub_category='Facts and Tips' or sub_category='Balance Diet' or sub_category='Fitness' or sub_category='LifeStyle' or sub_category='Relaxation' or sub_category='Foods' or sub_category='Sports'";

			$stmtSubCats = $conn->prepare("SELECT * FROM cms.sub_categories WHERE $query");

			$stmtSubCats->execute();

			$result = $stmtSubCats->get_result();

			while($row = $result->fetch_assoc()){
				array_push($subIds, $row['id']);
			}

			$stmtSubCats->close();

			if(count($subIds) > 0){
				foreach ($subIds as $id) {
					$value = "%{$searchValue}%";
					$stmt = $conn->prepare("SELECT id, title, file_name, original_file_name, mime FROM cms.contents WHERE sub_category_id = ? and title LIKE ?");
					$stmt->bind_param("is", $id, $value);
					$stmt->execute();
					$stmt->store_result();
					$stmt->num_rows();

					$stmt->bind_result($id, $title, $filename, $originalFilename, $mime);

					while($stmt->fetch()){
						$dataAssoc = [
							"contentId" => $id,
							"title" => $title,
							"filename" => $filename,
							"ext" => pathinfo($filename, PATHINFO_EXTENSION),
							"thumbname" => pathinfo($filename, PATHINFO_FILENAME),
							"originalFilename" => $originalFilename
						];

						array_push($data, $dataAssoc);
					}

					$stmt->close();	
				}
			}

			return $data;
		}
		
		function fetchContentData($conn, $contentId){
			$stmt = $conn->prepare("SELECT title, description, file_name, original_file_name FROM cms.contents WHERE id = ?");
			$stmt->bind_param('i', $contentId);
			$stmt->execute();
			$stmt->store_result();
			$stmt->num_rows();

			$stmt->bind_result($title, $description, $filename, $originalFilename);

			$data = [];

			while($stmt->fetch()){
				$dataScreen = [];

				$dataAssoc = [
					"title" => $title,
					"description" => $description,
					"filename" => $filename,
					"ext" => pathinfo($filename, PATHINFO_EXTENSION),
					"thumbname" => pathinfo($filename, PATHINFO_FILENAME),
					"originalFilename" => $originalFilename
				];

				if($dataAssoc['ext'] === "apk" || $dataAssoc['ext'] === "xapk"){
					$stmtScreens = $conn->prepare("SELECT file_name FROM cms.preview WHERE content_id = ?");
					$stmtScreens->bind_param('i', $contentId);
					$stmtScreens->execute();

					$imgCont = [];

					$result = $stmtScreens->get_result();

					while($row = $result->fetch_assoc()){
						array_push($imgCont, $row['file_name']);
					}

					$dataScreen = ['screenshots' => $imgCont];
				}

				if(!empty($dataScreen)){
					$merged = array_merge($dataAssoc, $dataScreen);
					array_push($data, $merged);
					$stmtScreens->close();
				}else{
					array_push($data, $dataAssoc);
				}

			}

			$stmt->close();

			return $data;

		}

	}


	$fetchData = new FetchData();

	if(isset($_POST['catInitial'])){
		$subCategories = $fetchData->fetchSubcategories($conn);
		$contents = $fetchData->fetchContents($conn, $subCategories[0]['subCatId'], $subCategories[0]['catId']);
		echo json_encode(["subCategories" => $subCategories, "contents" => $contents]);
	}else if(isset($_POST['subCatId'])){
		$contents = $fetchData->fetchContents($conn, $_POST['subCatId'], $_POST['catId']);
		echo json_encode($contents);
	}else if(isset($_POST['search'])){
		$contents = $fetchData->fetchContentsViaSearch($conn, $_POST['search']);
		echo json_encode($contents);
	}else if(isset($_POST['contentId'])){
		$contentData = $fetchData->fetchContentData($conn, $_POST['contentId']);
		echo json_encode($contentData);
	}

	$conn->close();
?>
