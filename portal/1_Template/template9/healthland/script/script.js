$(function(){
	let navToggle = false

	class Navigation{
		constructor(){
			this.modalTl = gsap.timeline({defaults:{duration: .3, ease: Power4.easeOut}});
		}

		closeMenuModal(){

			if($('#navBurgWrapper').length > 0){
				this.modalTl.to('#navBurgWrapper', {height:"54px"})
			            .to($('#navCloseBtn'), {scale: 0, 
			            	onComplete:() => {
			            		this.modalTl.to($('#navBurgWrapper span:not(.closeBtn)'), {scale:1,
			            			onComplete:() => {
			            				$('#navCloseBtn').remove()
			            				$('#navWrapper').css({display: "none"})
			       						$('#navBurgWrapper').css({boxShadow: "0px 0px 20px rgba(0, 0, 0, 0.35)"})
			            			}
			            		}).to($('#navBurgWrapper span:first'), {top:"12px"}).to($('#navBurgWrapper span:nth-child(3)'), {top:"36px"}, "<")

			            		$('.bgOpacity').remove();
			            	}
			            })
			            .to('#navWrapper', {scale:0})
			}
			            
		}


		openMenuModal(){
			this.modalTl.to($('#navBurgWrapper span:first'), {top:"24px"})
			       .to($('#navBurgWrapper span:last'), {top:"24px"}, "<")
			       .to($('#navBurgWrapper span'), {scale:0,
			       		onComplete:() => {
			       			$('#navBurgWrapper').append(`<span class="closeBtn" id="navCloseBtn">Close</span>`)
			       			this.modalTl.from('#navCloseBtn', {scale:0}, "<")
			       			$('#navWrapper').css({display: "block"})
			       			$('#navBurgWrapper').css({boxShadow: "none"})

			       			$('.mainContainer').prepend(`<div class="bgOpacity"></div>`)

			       			$('.bgOpacity').on('click', () => {
								this.closeMenuModal();
								this.navToggle = false;
							})
			       		}
			   	    }).to('#navWrapper', {scale:1}).to('#navBurgWrapper', {height:"auto"})
		}

		events(){
			$('#navBurgWrapper').on('click', (e) => {
				if(!navToggle){
					this.openMenuModal()
					navToggle = true;
				}else{
					this.closeMenuModal()
					navToggle = false;
				}
			})
		}
	}

	class Content extends Navigation{
		constructor(){
			super()
			this.previewTl = gsap.timeline({defaults:{duration: .3, ease: Power4.easeOut}});
		}

		showPreview(contentId){
			this.previewTl.to('#previewWrapper', {left: "-40px", opacity: 0,
				onComplete:() => {
					const dataForm3 = new FormData;
					dataForm3.append('contentId', contentId);



					gsap.to(window, {duration:.5, scrollTo:{y: 0}, ease: Power4.easeOut})

					// if($('#previewWrapper').length > 0){
					// 	$('#previewWrapper').remove()
					// } 

					$.ajax({
						type:'POST',
						url:'backend/query.php',
						data: dataForm3,
						dataType: 'json',
						contentType:false,
						processData:false,
						beforeSend:() => $('#previewWrapper').remove(),
						success:(data, textStatus, xhr) => {
							if(xhr.status === 200){

								const { contentId, title, filename, ext, originalFilename, thumbname, description } = data[0];
								let catBG = null;

								switch($('#subCatContainer').attr('data-category')){
									case "Tips":
										catBG = "tipsBgPreview.jpg";
										break;
									case "Apps":
										catBG = "appsBgPreview.jpg";
										break;
									case "VIDEOS":
										catBG = "videosBgPreview.jpg";
										break;
								}

								$('#contentsContainer').prepend(`<div class="previewWrapper" id="previewWrapper" style="--catBG: url(../images/${catBG});"></div>`)
								
								let thumbnail = null;
								let prevDetails = "";

								switch(ext){
									case "apk":
									case "xapk":
										thumbnail = `<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.png" />`;
										prevDetails = `
											<div class="previewDetails">
												<div class="previewThumb">
													${thumbnail}
												</div>
												<p>${title}</p>
												<a href="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.${ext}" class="previewDlLink">DOWNLOAD</a>
											</div>
										`;
										break;
									case "mp4":
										thumbnail = `<video preload="metadata">
														<source src="https://s3-ap-southeast-1.amazonaws.com/qcnt/${filename}#t=10" type='video/mp4' />
													</video>`;
										prevDetails = `
											<div class="previewDetails">
												<div class="previewThumb">
													${thumbnail}
												</div>
												<p>${title}</p>
												<a href="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.${ext}" class="previewDlLink">DOWNLOAD</a>
											</div>
										`;
										break;
									case "jpg":
										thumbnail = `<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/${filename}" />`;
										prevDetails = `
											<div class="previewTipsDetails">
												<div class="previewTipsThumb">
													${thumbnail}
												</div>
												<p class="previewTipsTitle">${title}</p>
											</div>
										`;
										break;
									default:
										thumbnail = `<img src="https://via.placeholder.com/70/FFFFFF/000000/?text=No Thumbnail" />`;
										prevDetails = `
											<div class="previewDetails">
												<div class="previewThumb">
													${thumbnail}
												</div>
												<p>${title}</p>
												<a href="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.${ext}" class="previewDlLink">DOWNLOAD</a>
											</div>
										`;
										break;
								}

								$('#previewWrapper').append(`
									<span class="closeBtn" id="previewClose">Close</span>
									${prevDetails}
								`)


								this.downloadLinkEvent('.previewDlLink');

								if(ext === "apk" || ext === "xapk"){
									$('#previewWrapper').append(`
										<div class="previewTipsDescrip">
											<span>Description</span>
											<div class="descContainer">
												<p>${description}</p>
											</div>
										</div>
									`);

								}else if(ext === "mp4"){
									$('#previewWrapper').append(`
										<div class="previewMedia">
											<span>Video Preview</span>
											<div class="mediaContainer">
												<video preload="metadata" controls controlsList="nodownload">
													<source src="https://s3-ap-southeast-1.amazonaws.com/qcnt/${filename}" type='video/mp4'/>
												</video>
											</div>
										</div>
									`)
								}else if(ext === "jpg"){
									$('#previewWrapper').append(`
										<div class="previewTipsDescrip">
											<span>Description</span>
											<div class="descContainer">
												<p>${description}</p>
											</div>
										</div>
									`);
								}

								$('#previewClose').on('click', (e) => {
									this.previewTl.to('#previewWrapper', {left: "-40px", opacity: 0,
										onComplete:function(){
											$('#previewWrapper').remove()
											
											if(window.matchMedia('(min-width:570px)').matches){
												if($('#subCatContainer').css('display') === "block"){
													$('#contentsWrapper').css({top: `${$('#subCatContainer').outerHeight() + 25}px`})
												}else{
													$('#contentsWrapper').css({top:0})
												}
											}else{
												$('#contentsWrapper').css({top:0})
											}
										}
									})
								})

								this.previewTl.to('#contentsWrapper', {top: `${$('#previewWrapper').outerHeight() + 30}px`,
									onComplete: () => {
										if(window.matchMedia('(min-width:570px)').matches){
											if($('#subCatContainer').css('display') === "block"){
												$('#previewWrapper').css({top: `${$('#subCatContainer').outerHeight() + 25}px`})
												$('#contentsWrapper').css({top: `${$('#subCatContainer').outerHeight() + 50}px`})
											}else{
												$('#contentsWrapper').css({top: `${$('#subCatContainer').outerHeight() + 15}px`})
											}
										}else{
											$('#contentsWrapper').css({top:"30px"})
										}
										$('#previewWrapper').css({display:'block'})
										$('.previewTipsDetails').css({minHeight: `${$('.previewTipsTitle').height() + 170 + 20}px`})
										this.previewTl.to('#previewWrapper', {left:0, opacity:1});
									}

								})
							}
						},
						error:(err) => console.log(err)
					})
				}
			})
		}

		fetchPreviewContent(){
			$('.contentThumb').on('mouseenter', function(){
				gsap.to(this, {scale:1.2, duration:.3, ease:Power4.easeOut})
			}).on('mouseleave', function(){
				gsap.to(this, {scale:1, duration:.3, ease:Power4.easeOut})
			}).on('mousedown', function(){
				gsap.to(this, {scale:1, duration:.3, ease:Power4.easeOut})
			}).on('mouseup', function(){
				gsap.to(this, {scale:1.2, duration:.3, ease:Power4.easeOut})
			}).on('touchstart', function(){
				gsap.to(this, {scale:1, duration:.3, ease:Power4.easeOut})
			}).on('touchend', function(){
				gsap.to(this, {scale:1.2, duration:.3, ease:Power4.easeOut})
			}).on('click', (e) => {

				if($('#previewWrapper').length > 0){
					this.previewTl.to('#previewWrapper', {left: "-40px", opacity: 0,
						onComplete:function(){
							$('#previewWrapper').remove()
						}
					})
				}

				this.showPreview($(e.currentTarget).find('input').val());
			})
		}

		fetchContents(subCatId, catId){
			const dataForm1 = new FormData;
			dataForm1.append('subCatId', subCatId)
			dataForm1.append('catId', catId)

			$.ajax({
				type:'POST',
				url:'backend/query.php',
				data: dataForm1,
				dataType: 'json',
				contentType:false,
				processData:false,
				success:(data, textStatus, xhr) => {
					if(xhr.status === 200){
						this.tlContent.to('.contentThumb', {top:"20px", opacity: 0,
							onComplete:async () => {
								let promise = new Promise((resolve) => {
									if($('#previewWrapper').length > 0){
										gsap.to('#previewWrapper', {left: "-40px", opacity: 0, duration:.3, ease:Power4.easeOut,
											onComplete:function(){
												$('#previewWrapper').remove()
												$('#contentsWrapper').css({top: 0})
											}
										})
									} 
									$('#contentsWrapper').empty()
									resolve(true)
								})

								let result = await promise

								if(result){
									$.each(data, (i, conts) => {
										const { contentId, title, filename, ext, originalFilename, thumbname } = conts;

										let thumbnail = null;
										
										switch(ext){
											case "apk":
											case "xapk":
												thumbnail = `<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.png" />`;
												break;
											case "mp4":
												thumbnail = `<video preload="metadata">
																<source src="https://s3-ap-southeast-1.amazonaws.com/qcnt/${filename}#t=10" type='video/mp4' />
															</video>`;
												break;
											case "jpg":
												thumbnail = `<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/${filename}" />`;
												break;
											default:
												thumbnail = `<img src="https://via.placeholder.com/70/FFFFFF/000000/?text=No Thumbnail" />`;
												break;
										}

										$('#contentsWrapper').append(`
											<div class="contentWrapper">
												<div class="contentThumb">
												<input type="hidden" value="${contentId}" />
													${thumbnail}
												</div>
												<div class="contentDetails">
													<a href="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.${ext}" class="dlLink">Download</a>
													<p class="contentName">${title}</p>
												</div>
											</div>
										`)
									})

									$('.contentDetails').css({paddingTop:`${$('.contentWrapper .contentThumb').height() - 5}px`})

						 			this.fetchPreviewContent()
									this.downloadLinkEvent();

						 			this.tlSubCat.to('#subCatContainer', {opacity: 1, top:"0px"})
						 			             .from('.contentThumb', {top:"20px", opacity: 0})

						 			this.subCatBtns = $('.subCatBtn')
						 			this.eventsContent();

								}
							}
						})
					}
				},
				error:(err) => console.log(err)
			})
		}
		
		eventsContent(){
			this.subCatBtns.on('click', (e) => {
				const catId = $(e.currentTarget).prev().prev().val();
				const subCatId = $(e.currentTarget).prev().val();

				$.each(this.subCatBtns, (i, subCatBtn) => {
					$(subCatBtn).removeClass('active');
				})

				$(e.currentTarget).addClass('active');

				if(window.matchMedia('(min-width:570px)').matches){
					this.fetchContents(subCatId, catId);
				}else{
					this.fetchContents(subCatId, catId);

					this.closeMenuModal()
					navToggle = false;
				}
			})
		}
	}

	class Subcategory extends Content{
		constructor(){
			super()
			this.catBtns = $('.catBtn')
			this.initialHeightNav = $('#navWrapper').outerHeight()
			// this.initialHeightNavMob = $('#navWrapper').outerHeight()
			this.subCatBtns = null;
			this.tlSubCat = gsap.timeline({defaults:{duration: .3, ease: Power4.easeOut}});
			this.tlContent = gsap.timeline({defaults:{duration: .3, ease: Power4.easeOut}});
			this.subCatToggle = false;
		}

		animateTopSubCat(catContainer, resolve){
			if(this.subCatToggle){
				this.tlSubCat.to('#navWrapper', {height: "auto"})
							 .to('.subCatContainer', {opacity: 0, top:"-25px", 
							 	onComplete:function(){
							 		$('#subCatContainer').remove();
							 		resolve(true)
							 	}
							 })
				
			}else{
				resolve(true)
			}
		}

		appendSubCategories(data, catContainer, resolve){
			$(catContainer).after(`<div class="subCatContainer" id="subCatContainer"></div>`)

			$.each(data, (i, subCats) => {
				const { catId, subCatId, subCategory, category } = subCats;

				$('.subCatContainer').append(`
					<div class="subCatBtnContainer">
						<input type="hidden" value="${catId}" />
						<input type="hidden" value="${subCatId}" />
						<button class="subCatBtn">${subCategory}</button>
					</div>
				`).attr('data-category', category)
			})

			resolve(true)
		}

		downloadLinkEvent(download){
			$(download).on('mousedown', function(){
				gsap.to(this, {scale:.9, duration:.3, ease:Power4.easeOut})
			}).on('mouseup', function(){
				gsap.to(this, {scale:1, duration:.3, ease:Power4.easeOut})
			}).on('touchstart', function(){
				gsap.to(this, {scale:.9, duration:.3, ease:Power4.easeOut})
			}).on('touchend', function(){
				gsap.to(this, {scale:1, duration:.3, ease:Power4.easeOut})
			})
		}

		async appendDatasExitSubCategories(data){
			let promise = new Promise((resolve) => {
	 			if($('#previewWrapper').length > 0){
					gsap.to('#previewWrapper', {left: "-40px", opacity: 0, duration:.3, ease:Power4.easeOut,
						onComplete:function(){
							$('#previewWrapper').remove()
						}
					})
				} 
	 			$('#subCatContainer').remove();
	 			$('#contentsWrapper').empty();
	 			resolve(true)
	 		})

	 		let result = await promise;

	 		if(result){
	 			$('.navWrapper').append(`<div class="subCatContainer" id="subCatContainer"></div>`)

				$.each(data.subCategories, (i, subCats) => {
					const { catId, subCatId, subCategory, category } = subCats;

					$('#subCatContainer').append(`
						<div class="subCatBtnContainer">
							<input type="hidden" value="${catId}" />
							<input type="hidden" value="${subCatId}" />
							<button class="subCatBtn">${subCategory}</button>
						</div>
					`).attr('data-category', category)
				})

				$('.subCatBtn:first').addClass('active')

				$.each(data.contents, (i, conts) => {
					const { contentId, title, filename, ext, originalFilename, thumbname } = conts;

					let thumbnail = "";

					switch(ext){
						case "apk":
						case "xapk":
							thumbnail = `<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.png" />`;
							break;
						case "mp4":
							thumbnail = `<video preload="metadata">
											<source src="https://s3-ap-southeast-1.amazonaws.com/qcnt/${filename}#t=10" type='video/mp4' />
										</video>`;
							break;
						case "jpg":
							thumbnail = `<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/${filename}" />`;
							break;
						default:
							thumbnail = `<img src="https://via.placeholder.com/70/FFFFFF/000000/?text=No Thumbnail" />`;
							break;
					}

					$('#contentsWrapper').append(`
						<div class="contentWrapper">
							<div class="contentThumb">
								<input type="hidden" value="${contentId}" />
								${thumbnail}
							</div>
							<div class="contentDetails">
								<a href="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.${ext}" class="dlLink">Download</a>
								<p class="contentName">${title}</p>
							</div>
						</div>
					`)
				})

				$('.contentDetails').css({paddingTop:`${$('.contentWrapper .contentThumb').height() - 5}px`})

				this.fetchPreviewContent()
				this.downloadLinkEvent('.dlLink');

	 			this.tlSubCat.from('.contentThumb', {top:"20px", opacity: 0})

	 			this.subCatBtns = $('.subCatBtn')
	 			this.eventsContent();


				if(this.subCatToggle){
					this.showSubCategories();
				}
	 		}
		}

		fetchSubCategoriesContents(cat, catContainer){
			const dataForm = new FormData;
			dataForm.append("catInitial", cat);

			$.ajax({
				type:'POST',
				url:'backend/query.php',
				data: dataForm,
				dataType: 'json',
				contentType:false,
				processData:false,
				success: (data, textStatus, xhr) => {

					if(xhr.status === 200){
						if(window.matchMedia('(min-width:570px)').matches){
							if(this.subCatToggle){
								gsap.to('#subCatContainer', {opacity:0, top:"-25px", duration:.3, ease:Power4.easeOut, 
									onComplete: () => {
										if($('#subCatContainer').length > 0){
											$('#subCatContainer').remove();
										}

										this.appendDatasExitSubCategories(data)
									}
								})
							}else{
								this.appendDatasExitSubCategories(data)
							}

						}else{

							if(!this.subCatToggle){
								$('#subCatContainer').css({top:0, opacity:1})
							}

							this.tlSubCat.to('#navWrapper', {height: "auto", 
										 	onComplete:async () => {
										 		let promise = new Promise((resolve) => {
										 			if($('#previewWrapper').length > 0){
														gsap.to('#previewWrapper', {left: "-40px", opacity: 0, duration:.3, ease:Power4.easeOut,
															onComplete:function(){
																$('#previewWrapper').remove()
																$('#contentsWrapper').css({top: 0})
															}
														})
													} 
										 			$('#subCatContainer').remove();
										 			$('#contentsWrapper').empty();
										 			resolve(true)
										 		})

										 		let result = await promise;

										 		if(result){

										 			$(catContainer).after(`<div class="subCatContainer" id="subCatContainer"></div>`)

									 				$.each(data.subCategories, (i, subCats) => {
														const { catId, subCatId, subCategory, category } = subCats;

														$('#subCatContainer').append(`
															<div class="subCatBtnContainer">
																<input type="hidden" value="${catId}" />
																<input type="hidden" value="${subCatId}" />
																<button class="subCatBtn">${subCategory}</button>
															</div>
														`).attr('data-category', category)
													})

													$('.subCatBtn:first').addClass('active')


													$.each(data.contents, (i, conts) => {
														const { contentId, title, filename, ext, originalFilename, thumbname } = conts;

														let thumbnail = "";

														switch(ext){
															case "apk":
															case "xapk":
																thumbnail = `<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.png" />`;
																break;
															case "mp4":
																thumbnail = `<video preload="metadata">
																				<source src="https://s3-ap-southeast-1.amazonaws.com/qcnt/${filename}#t=10" type='video/mp4' />
																			</video>`;
																break;
															case "jpg":
																thumbnail = `<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/${filename}" />`;
																break;
															default:
																thumbnail = `<img src="https://via.placeholder.com/70/FFFFFF/000000/?text=No Thumbnail" />`;
																break;
														}

														$('#contentsWrapper').append(`
															<div class="contentWrapper">
																<div class="contentThumb">
																	<input type="hidden" value="${contentId}" />
																	${thumbnail}
																</div>
																<div class="contentDetails">
																	<a href="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.${ext}" class="dlLink">Download</a>
																	<p class="contentName">${title}</p>
																</div>
															</div>
														`)
													})

													$('.contentDetails').css({paddingTop:`${$('.contentWrapper .contentThumb').height() - 5}px`})


													this.fetchPreviewContent()
													this.downloadLinkEvent();

										 			this.tlSubCat.to('#subCatContainer', {opacity: 1, top:"0px"})
										 			             .from('.contentThumb', {top:"20px", opacity: 0})

										 			this.subCatBtns = $('.subCatBtn')
										 			this.eventsContent();

										 		}
										 	}
										 })
						}							
					}
				},
				error: (err) => console.log(err)
			})

		}

		showSubCategories(){
			$('#subCatContainer').css({display:"block"});
			$('#catContainer').css({marginBottom: "15px"})

			const tl = gsap.timeline({defaults:{duration: .3, ease: Power4.easeOut}});

			if($('#previewWrapper').length > 0){
				tl.to('#navWrapper', {height: "auto"})
				  .to('#previewWrapper', {top: `${$('#subCatContainer').outerHeight() + 25}px`}, "<")
				  .to('#contentsWrapper', {top: `${$('#subCatContainer').outerHeight() + 50}px`}, "<")
				  .to('#subCatContainer', {opacity:1, top:"0px"})
			}else{
				tl.to('#navWrapper', {height: "auto"})
				  .to('#contentsWrapper', {top: `${$('#subCatContainer').outerHeight() + 25}px`}, "<")
				  .to('#subCatContainer', {opacity:1, top:"0px"})
			}
			// gsap.to('#navWrapper', {height: "auto", duration:.3, ease: Power4.easeOut})
			// gsap.to('#subCatContainer', {opacity:1, top:"0px" , duration:.1, ease: Power4.easeOut, onComplete: function(){
			// 		gsap.to('#contentsWrapper', {top: `${$('#navWrapper').outerHeight()}px`, duration:.2, ease: Power4.easeOut})			
			// 	}
			// });
		}

		hideSubCategories(){
			const tl = gsap.timeline({defaults:{duration: .3, ease: Power4.easeOut}});
			if($('#previewWrapper').length > 0){
				tl.to('#subCatContainer', {opacity:0, top:"-25px" , onComplete: function(){
						$('#catContainer').css({marginBottom: 0})
					}
				})
				.to('#navWrapper', {height: this.initialHeightNav, onComplete: function(){
						$('#subCatContainer').css({display:"none"});
					}
				}).to('#contentsWrapper', {top:`${$('#subCatContainer').outerHeight() + 15}px`},"<").to('#previewWrapper', {top:0},"<")
			}else{
				tl.to('#subCatContainer', {opacity:0, top:"-25px" , onComplete: function(){
					$('#catContainer').css({marginBottom: 0})
				}
				})
				.to('#navWrapper', {height: this.initialHeightNav, onComplete: function(){
						$('#subCatContainer').css({display:"none"});
					}
				}).to('#contentsWrapper', {top:0},"<")	
			}
		}

		events(){
			this.catBtns.on('click', (e) => {

				if(window.matchMedia('(min-width:570px)').matches){
					if($(e.currentTarget).hasClass('active')){
						if(!this.subCatToggle){
							this.showSubCategories();
							this.subCatToggle = true;
						}else{
							this.hideSubCategories()
							this.subCatToggle = false;
						}
					}else{
						$.each(this.catBtns, (i, catBtn) => {
							$(catBtn).removeClass('active');
						})

						$(e.currentTarget).addClass('active');

						this.fetchSubCategoriesContents(
							$(e.currentTarget).attr('data-cat'), 
							$(e.currentTarget).parent()
						);

						this.subCatToggle = true;
					}
				}else{
					$.each(this.catBtns, (i, catBtn) => {
						$(catBtn).removeClass('active');
					})

					$(e.currentTarget).addClass('active');

					this.subCatToggle = true;

					this.fetchSubCategoriesContents(
						$(e.currentTarget).attr('data-cat'), 
						$(e.currentTarget).parent()
					);
				}


				// if($(e.currentTarget).hasClass('active')){
				// 	if(!this.subCatToggle){
				// 		this.showSubCategories();
				// 		this.subCatToggle = true;
				// 	}else if(this.subCatToggle){
				// 		this.hideSubCategories()
				// 		this.subCatToggle = false;
				// 	}
				// }else{
				// 	$.each(this.catBtns, (i, catBtn) => {
				// 		$(catBtn).removeClass('active');
				// 	})

				// 	$(e.currentTarget).addClass('active');

				// 	this.fetchSubCategoriesContents($(e.currentTarget).attr('data-cat'), $(e.currentTarget).parent(), this.showSubCategories);
				// 	this.subCatToggle = true;
				// }
			})
		}
	}

	class Search extends Subcategory{
		fetchContentsViaSearch(inputVal){
			const dataForm2 = new FormData;
			dataForm2.append('search', inputVal)

			$.ajax({
				type:'POST',
				url:'backend/query.php',
				data: dataForm2,
				dataType: 'json',
				contentType:false,
				processData:false,
				success:(data, textStatus, xhr) => {
					if(xhr.status === 200){
						if(data.length > 0){
							this.tlContent.to('.contentThumb', {top:"20px", opacity: 0,
								onComplete:async () => {
									let promise = new Promise((resolve) => {
										if($('#previewWrapper').length > 0){
											gsap.to('#previewWrapper', {left: "-40px", opacity: 0, duration:.3, ease:Power4.easeOut,
												onComplete:function(){
													$('#previewWrapper').remove()
													$('#contentsWrapper').css({top: 0})
												}
											})
										} 
										$('#contentsWrapper').empty()
										resolve(true)
									})

									let result = await promise

									if(result){
										$.each(data, (i, conts) => {
											const { contentId, title, filename, ext, originalFilename, thumbname } = conts;

											let thumbnail = "";

											switch(ext){
												case "apk" || "xapk":
													thumbnail = `<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.png" />`;
													break;
												case "mp4":
													thumbnail = `<video preload="metadata">
																	<source src="https://s3-ap-southeast-1.amazonaws.com/qcnt/${filename}#t=10" type='video/mp4' />
																</video>`;
													break;
												case "mp3":
													thumbnail = `<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" />`;
													break;
												default:
													thumbnail = `<img src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.png" />`;
													break;
											}

											$('#contentsWrapper').append(`
												<div class="contentWrapper">
													<div class="contentThumb">
														<input type="hidden" value="${contentId}" />
														${thumbnail}
													</div>
													<div class="contentDetails">
														<a href="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/${thumbname}.${ext}" class="dlLink">Download</a>
														<p class="contentName">${title}</p>
													</div>
												</div>
											`)
										})

										$('.contentDetails').css({paddingTop:`${$('.contentWrapper .contentThumb').height() - 5}px`})

							 			this.fetchPreviewContent()
										this.downloadLinkEvent();

							 			this.tlSubCat.to('#subCatContainer', {opacity: 1, top:"0px"})
							 			             .from('.contentThumb', {top:"20px", opacity: 0})

							 			this.subCatBtns = $('.subCatBtn')
							 			this.eventsContent();

									}
								}
							})
						}else{
							if($('#previewWrapper').length > 0){
								gsap.to('#previewWrapper', {left: "-40px", opacity: 0, duration:.3, ease:Power4.easeOut,
									onComplete:function(){
										$('#previewWrapper').remove()
										$('#contentsWrapper').css({top: 0})
									}
								})
							} 
							$('#contentsWrapper').empty().append(`<h1>No result for "${inputVal}"</h1>`)
						}
					}
				},
				error:(err) => console.log(err)
			})
		}

		events(){
			$('#searchBtn').on('click', (e) => {
				if($('#searchInput').val() === ""){
					return;
				}else{
					this.fetchContentsViaSearch($('#searchInput').val())
				}
			})
		}
	}


	const subcategory = new Subcategory;
	const navigation = new Navigation;
	const search = new Search;

	// subcategory.fetchSubCategoriesContents(
	// 	$('.catBtn:first').attr('data-cat'),
	// 	$('.catBtnContainer:first')
	// );

	subcategory.events();

	navigation.events();

	search.events();

	// $('#navContainer').css({top: `${$('#headerContainer').outerHeight() - ($('#navContainer').outerHeight() / 2)}px`});
	$('#navBurgWrapper').css({top: `-${$('#navBurgWrapper').outerHeight() / 2}px`});

	if(window.matchMedia('(min-width:570px)').matches){
		$('#navContainer').css({top: `${$('#headerContainer').outerHeight() - ($('#navContainer').outerHeight() / 2)}px`});
		$('#navWrapper').css({top: "0px", display: "block",  transform: "scale(1)"});
		// subcategory.initialHeightNav = $('#navWrapper').outerHeight();

		subcategory.fetchSubCategoriesContents(
			$('.catBtn:first').attr('data-cat'),
			$('.catBtnContainer:first')
		);
	}else{
		$('#navContainer').css({top: `${$('#headerContainer').outerHeight() - ($('#navContainer').outerHeight() / 2)}px`});
		$('#navWrapper').css({top: `-${$('#navBurgWrapper').outerHeight() / 2}px`, display: "none",  transform: "scale(0)"});

		subcategory.fetchSubCategoriesContents(
			$('.catBtn:first').attr('data-cat'),
			$('.catBtnContainer:first')
		);
	}

	$(window).on('resize', () => {
		$('.previewTipsDetails').css({minHeight: `${$('.previewTipsTitle').height() + 170 + 20}px`})

		if(window.matchMedia('(min-width:570px)').matches){
			$('#navContainer').css({top: `${$('#headerContainer').outerHeight() - ($('#navContainer').outerHeight() / 2)}px`});
			$('#navWrapper').css({top: "0px", display: "block",  transform: "scale(1)"});
			subcategory.initialHeightNav = $('#navWrapper').outerHeight();
			subcategory.subCatToggle = false;

			$('#subCatContainer').css({top:"-25px", opacity:0, display:"none"})

			if($('#previewWrapper').length > 0){
				$('#previewWrapper').css({top: 0})
				$('#contentsWrapper').css({top:"30px"})
			}else{
				$('#contentsWrapper').css({top:0})
			}
			$('#catContainer').css({marginBottom: "0px"})

			if($('#catContainer > #subCatContainer').length > 0){

				// if($('#navWrapper').children('#subCatContainer').length > 0){
				// 	$('#navWrapper').children('#subCatContainer').remove();
				// }

				$('#catContainer > #subCatContainer').clone(true, true).appendTo('#navWrapper');

				$('#catContainer > #subCatContainer').remove();

				if(navToggle){

					navToggle = false;

					const modalTl = gsap.timeline({defaults:{duration: .3, ease: Power4.easeOut}});
					
					modalTl.to('#navBurgWrapper', {height:"54px"})
			            .to($('#navCloseBtn'), {scale: 0, 
			            	onComplete:() => {
			            		modalTl.to($('#navBurgWrapper span:not(.closeBtn)'), {scale:1,
			            			onComplete:() => {
			            				$('#navCloseBtn').remove()
			       						$('#navBurgWrapper').css({boxShadow: "0px 0px 20px rgba(0, 0, 0, 0.35)"})
			            			}
			            		}).to($('#navBurgWrapper span:first'), {top:"12px"}).to($('#navBurgWrapper span:nth-child(3)'), {top:"36px"}, "<")

			            		$('.bgOpacity').remove();
			            	}
			            })
					// $('.bgOpacity').remove()
					// $('#subCatContainer').css({opacity:0, top:"-25px", display: "none"})
					// $('#navBurgWrapper').css({height:"54px", boxShadow: "0px 0px 20px rgba(0, 0, 0, 0.35)"})
					// $('#navCloseBtn').css({transform:"scale(0)"})
					// $('#navBurgWrapper span:not(.closeBtn)').css({transform:"scale(1)"})
					// $('#navBurgWrapper span:first').css({top:"12px"})
					// $('#navBurgWrapper span:nth-child(3)').css({top:"36px"})
				}

				// navigation.closeMenuModal();
			}

		}else{
			$('#navContainer').css({top: `${$('#headerContainer').outerHeight() - ($('#navContainer').outerHeight() / 2)}px`});
			$('#navWrapper').css({top: `-${$('#navBurgWrapper').outerHeight() / 2}px`, display: "none",  transform: "scale(0)", height: "auto"});
			$('#subCatContainer').css({top:0, opacity:1, display: "block"})

			if($('#previewWrapper').length > 0){
				$('#previewWrapper').css({top: 0})
				$('#contentsWrapper').css({top:"30px"})
			}else{
				$('#contentsWrapper').css({top:0})
			}

			if(subcategory.subCatToggle){
				$('#catContainer').css({marginBottom: "0px"})
			}

			if($('#navWrapper').children('#subCatContainer').length > 0){

				$.each($('.catBtn'), (i, catBtn) => {

					if($(catBtn).hasClass('active')){
						$(catBtn).parent().after($('#navWrapper').children('#subCatContainer').clone(true, true))

						$('#navWrapper').children('#subCatContainer').remove()
					}
				})
			}
		}
	})
	// $('#navWrapper').css({height:`${$('#navWrapper').outerHeight()}px`})
})