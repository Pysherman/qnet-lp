<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css2?family=Rubik:wght@400;500;600&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="stylesheet/style.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/ScrollToPlugin.min.js" integrity="sha512-nTHzMQK7lwWt8nL4KF6DhwLHluv6dVq/hNnj2PBN0xMl2KaMm1PM02csx57mmToPAodHmPsipoERRNn4pG7f+Q==" crossorigin="anonymous" defer></script>
	<script src="script/script.js" defer></script>
	<title>Sexysense</title>
</head>
<body>
	<main class="mainContainer">
		<header class="headerContainer" id="headerContainer">
			<h1 class="logo">Sexysense</h1>
			<div class="searchContainer">
				<input type="text" name="searchInput" class="searchInput" id="searchInput" placeholder="Search...">
				<button type="button" class="searchBtn" id="searchBtn">Search</button>
			</div>
		</header>
		<nav class="navContainer" id="navContainer">
			<section class="navBurgWrapper" id="navBurgWrapper">
				<span></span>
				<span></span>
				<span></span>
			</section>
			<section class="navWrapper" id="navWrapper">
				<div class="catContainer" id="catContainer">
					<div class="catBtnContainer">
						<button class="catBtn active" type="button" data-cat="Videos">Videos</button>
					</div>
					<div class="catBtnContainer">
						<button class="catBtn" type="button" data-cat="Wallpapers">Wallpapers</button>
					</div>			
					
					<section class="catBtnContainer sublang2" id="subcategoryContainer">
						<span>Språk:</span>
						<a href="#googtrans(en|en)" class="lang-sk  lang-select  languageSk" data-lang="en">EN</a>
						<a href="#googtrans(en|no)" class="lang-sk lang-select  languageSk" data-lang="no">NO</a>
					</section>
					
				</div>
			</section>
		</nav>
		<section class="contentsContainer" id="contentsContainer">
			<div class="contentsWrapper" id="contentsWrapper"></div>
		</section>
	</main>
<!-- Translate Custom JS-->
<script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }
	function triggerHtmlEvent(element, eventName) {
	  var event;
	  if (document.createEvent) {
		event = document.createEvent('HTMLEvents');
		event.initEvent(eventName, true, true);
		element.dispatchEvent(event);
	  } else {
		event = document.createEventObject();
		event.eventType = eventName;
		element.fireEvent('on' + event.eventType, event);
	  }
	}
	jQuery('.lang-select').click(function() {
	  var theLang = jQuery(this).attr('data-lang');
	  jQuery('.goog-te-combo').val(theLang);
	  //alert(jQuery(this).attr('href'));
	  window.location = jQuery(this).attr('href');
	  location.reload();

	});
</script>
<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</body>
</html>