<section class="main-category" id="mainCategory">
	<ul>
		<li data-main-cat="games"><a href="#"><span>Games</span><i class="fas fa-gamepad"></i></a></li>
		<li data-main-cat="videos"><a href="#"><span>Videos</span><i class="fas fa-film"></i></a></li>
        <li data-main-cat="tones"><a href="#"><span>Tones</span><i class="fas fa-music"></i></a></li>
		<li data-main-cat="apps"><a href="#"><span>Apps</span><i class="fas fa-cube"></i></a></li>
	</ul>
</section>