<section class="content-list" id="contentList">
	<a href="#" class="back-btn"><i class="fas fa-chevron-left"></i>Back</a>
	<?php
        include('contentList/games.php');
        include('contentList/videos.php'); 
        include('contentList/tones.php');
        include('contentList/apps.php'); 
	?>
</section>