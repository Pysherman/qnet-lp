<?php
    $CatConditionContentApps = "and(b.sub_category='Antivirus' or b.sub_category='Productivity' or b.sub_category='Utility')";
    $catContentApps = "apps";
    $queryJoinContentApps = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatConditionContentApps  and category like '$catContentApps%'");

    if($queryJoinContentApps){
        while($DateRow = mysqli_fetch_assoc($queryJoinContentApps)){
            $resCatgID = $DateRow['id'];
            $resName = $DateRow['category'];
            $resSbCatgID = $DateRow['sc_id'];
            $newResSname = $DateRow['sub_category'];
            $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";

            $newResSname = str_replace(' ', '', $newResSname);

            echo '<div class="'.$newResSname.' content-div">';

            if(!empty($_SESSION['categref'])){
                $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
                $categref = $_SESSION['categref'];
                $sesCatg = $_SESSION['sesCatg'];
    
                $getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and $categref order by id desc limit 20");

                if($getContent){
                    while($items = mysqli_fetch_array($getContent)){
                        $contentID = $items['id'];
                        $itemTitle = $items['title'];
                        $description = $items['description'];
                        $file_name = $items['file_name'];
                        $original_file_name = $items['original_file_name'];
                        $mime = $items['mime'];
                        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                        $filename = $contenrdir.$file_name;
                        $subcat = $items['sub_category_id'];

                        $file = pathinfo($file_name, PATHINFO_FILENAME);

                        if($ext == "mp4"){
                            $thumbimg = $file.'.png';
                            $preview ='<video width="100%" height="100"  controls preload="metadata">
                                <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
                            </video>';
                        }else if($ext == "mp3"){
                            $preview = '<img class="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
                        }else{
                            $thumbimg = $file.'.png';
                            $preview ='<img class="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
                        }

                        echo '<div class="item" data-genre="'.$newResSname.'" data-cat-origin="'.$resName.'">
								<a href="#">
									<span>'.$itemTitle.'</span>
									'.$preview.'
									<div class="hdn-elem">
										<span>'.$filename.'</span>';

										if($ext == "apk" || $ext == "xapk"){
											$prevFile = $conn->query("SELECT file_name FROM cms.preview WHERE content_id='$contentID'");
											
											if(mysqli_affected_rows($conn)){
												while($PrevItems = mysqli_fetch_array($prevFile)){
													$srcFiles = $PrevItems['file_name'];
													$screenshot = $contenrdir.$srcFiles;
													echo '<img src='.$screenshot.'>';
												}
											}
                                        }
                        echo '			<p>'.$description.'</p>
                                    </div>
                                </a>
                            </div>';

                    }

                }
            }

            echo '</div>';
        }
    }
?>