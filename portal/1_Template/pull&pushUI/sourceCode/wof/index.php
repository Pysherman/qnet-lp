<!DOCTYPE html>
<?php require('connection/header.php');?>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style/main.css" />
    <link rel="stylesheet" type="text/css" href="icofont/fontawesome5/css/all.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <title>World of Fun</title>
</head>
<body>
    <header class="header">
        <img src="img/logo.png" alt="Powerland">
    </header>
    <main class="main-container">
        <?php
            ini_set('max_execution_time', 200);
            include('pages/mainCategory.php');
            include('pages/subCategory.php');
            include('pages/contentList.php');
        ?>
        <section class="preview-section" id="previewSection">
	        <a href="#" class="back-btn"><i class="fas fa-chevron-left"></i>Back</a>
	        <div class="preview-item"></div>
        </section>	
    </main>
    <script type="text/javascript" src="script/script.js"></script>
    <script type="text/javascript" src="script/historyNavi.js"></script>
    <script type="text/javascript" src="script/scriptPrev.js"></script>
</body>
</html>