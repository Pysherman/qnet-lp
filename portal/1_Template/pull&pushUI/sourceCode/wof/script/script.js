const mainCatUl = document.querySelector('.main-category ul');
const subCatUl = document.querySelectorAll('.sub-category ul');
const contentDiv = document.querySelectorAll('.content-list .content-div');
const previewItem = document.querySelector('.preview-item');

const mainCatLinks = document.querySelectorAll('.main-category li a');
const subCatLinks = document.querySelectorAll('.sub-category li a');
const contentLinks = document.querySelectorAll('.content-list .item a');

const mainCategory = document.getElementById('mainCategory');
const subCategory = document.getElementById('subCategory');
const contentList = document.getElementById('contentList');


const backBtn = document.querySelectorAll('.back-btn');
const previewSection = document.getElementById('previewSection');

//add display none to the back button of the preview section
backBtn[backBtn.length - 1].style.display = "none";

//assign the icons to all of the sub category link
let iconFunc = () => {
	subCatLinks.forEach(subCat => {
		switch(subCat.parentElement.dataset.subCat){
			case 'Action':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-fighter-jet");
			case 'Adventure':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-dungeon");
			case 'Arcade':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-rocket");
			case 'Puzzle':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-puzzle-piece");
			case 'Simulation':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-building");
			case 'Strategy':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-chess");
			case 'carracing':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-car");
			case 'Cartoons':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-robot");
			case 'football':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-futbol");
			case 'Funny':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-grin-squint-tears");
			case 'Horror':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-ghost");
			case 'Tones-Portal':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-music");
			case 'Productivity':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-clipboard-list");
			case 'Utility':
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-tools");
				break;
			default:
				subCat.lastElementChild.classList.replace("fa-gamepad", "fa-icons");
				break;
		}
	})
}

let customLoop = (categoryUl, posLeft, timeMs) => {

	setTimeout(() => {
		for(let i = 0; i < categoryUl.children.length; i++){
			categoryUl.children[i].firstElementChild.style.left = posLeft;
		}
	}, timeMs);

	// let i = 0;
	// let timeInterval = setInterval(() => {
	// 	if(categoryUl.children.length > i){
	// 		categoryUl.children[i].firstElementChild.style.left = posLeft;
	// 		i++;
	// 	}else{
	// 		clearInterval(timeInterval);
	// 		i = 0;
	// 	}
	// }, timeMs);
}

let forLoopStatus = parentElem => {
	for(let i = 0; i < parentElem.children.length; i++){
		if(parentElem.children[i].hasAttribute('data-status')){
			return parentElem.children[i];
		}
	}
}

let slideIn = (categoryUl, categoryId) => {
	categoryId.style.display = "block";
	categoryId.style.left = "0px";
	categoryUl.style.display = "block";

	customLoop(categoryUl, "0px", 800);
}

let addScaleIn = (oldLink, newLinks) => {
	oldLink.classList.add('scaleIn');
	for(let i = 0; i < newLinks.length; i++){
		newLinks[i].style.left = "-9999px";
	}
	oldLink.style.left = "0px";
}

let hideContainer = (oldLink, oldCategoryId, newCategoryId, newCategoryUl, dataAttrib) => {
	setTimeout(() => {
		oldCategoryId.style.left = "-9999px";
		oldLink.style.left = "-9999px";
		oldLink.classList.remove('scaleIn');
	}, 500);

	setTimeout(() => {
		oldCategoryId.style.display = "none";
		newCategoryId.style.display = "block";
		newCategoryUl.forEach(categoryUl => {
			categoryUl.style.display = "none";
			if(categoryUl.classList.contains(oldLink.parentElement.getAttribute(dataAttrib))){
				categoryUl.style.display = "block";
			}
		})
	}, 600);
};

let slideLinks = (oldLink, newCategoryUl, dataAttrib) => {
	newCategoryUl.forEach(categoryUl => {
		if(categoryUl.classList.contains(oldLink.parentElement.getAttribute(dataAttrib))){
			categoryUl.dataset.status = "selected";

			customLoop(categoryUl, "0px", 800);
		}
	})
};

// testing purpose
// let historyFn = (bBtn) => {

//     const btnParent = bBtn;
//     const btnParentId = btnParent.id;
//     const siblingOfBtnParent = btnParent.previousElementSibling;
//     const childWithStatus = forLoopStatus(btnParent);
//     const childWithStatusSibling = forLoopStatus(siblingOfBtnParent);
    
//     childWithStatus.style.left = "-9999px";
//     childWithStatus.removeAttribute('data-status');

//     customLoop(childWithStatus, "-9999px", 500);

//     setTimeout(() => {
//         btnParent.style.display = "none";
//     }, 600);

//     switch(btnParentId){
//         case 'subCategory':
//             slideIn(mainCatUl, mainCategory);
//             break;
//         case 'contentList':
//             slideIn(childWithStatusSibling, siblingOfBtnParent);
//             break;
//         case 'previewSection':
//             slideIn(childWithStatusSibling, siblingOfBtnParent);
//             backBtn[backBtn.length - 1].style.display = "none";
//             break;
//         default:
//             return false;
//     }

// }

let mainCatFn = catlink => {
	catlink.addEventListener('click', e => {
		e.preventDefault();
		addScaleIn(catlink, mainCatLinks);
		hideContainer(catlink, mainCategory, subCategory, subCatUl, "data-main-cat");
		slideLinks(catlink, subCatUl, "data-main-cat");

		//testing purpose
		// history.pushState(null, null, catlink.parentElement.dataset.mainCat);

		// window.addEventListener('popstate', (e) => {
		// 	history.pushState({'cat' : catlink.parentElement.dataset.mainCat}, null, null);

		// 	if(history.state.cat == catlink.parentElement.dataset.mainCat)
		// 		historyFn(backBtn[0].parentElement);
		// })
	})
}

let subCatFn = subcatlink => {
	subcatlink.addEventListener('click', e => {
		e.preventDefault();
		addScaleIn(subcatlink, subCatLinks);
		hideContainer(subcatlink, subCategory, contentList, contentDiv, "data-sub-cat");
		slideLinks(subcatlink, contentDiv, "data-sub-cat");

		//testing purpose
		// history.pushState(null, null, `${location.href}/${subcatlink.parentElement.dataset.subCat}`);

		// window.addEventListener('popstate', (e) => {
		// 	history.pushState({'cat' : subcatlink.parentElement.dataset.subCat}, null, null);

		// 	if(history.state.cat == subcatlink.parentElement.dataset.subCat)
		// 		historyFn(backBtn[1].parentElement);
		// })
	})
};

let contentBackFn = bBtn => {
	bBtn.addEventListener('click', e => {
		e.preventDefault();
		const btnParent = e.currentTarget.parentElement;
		const btnParentId = btnParent.id;
		const siblingOfBtnParent = btnParent.previousElementSibling;
		const childWithStatus = forLoopStatus(btnParent);
		const childWithStatusSibling = forLoopStatus(siblingOfBtnParent);
		
		childWithStatus.style.left = "-9999px";
		childWithStatus.removeAttribute('data-status');

		customLoop(childWithStatus, "-9999px", 500);

		setTimeout(() => {
			btnParent.style.display = "none";
		}, 600);

		switch(btnParentId){
			case 'subCategory':
				slideIn(mainCatUl, mainCategory);
				break;
			case 'contentList':
				slideIn(childWithStatusSibling, siblingOfBtnParent);
				break;
			case 'previewSection':
				slideIn(childWithStatusSibling, siblingOfBtnParent);
				backBtn[backBtn.length - 1].style.display = "none";
				break;
			default:
				return false;
		}
	})
}

let windowLoad = () => {
	slideIn(mainCatUl, mainCategory);
	iconFunc();
	//console.log("execute");
}

window.addEventListener('DOMContentLoaded', windowLoad);
//window.addEventListener('load', );

mainCatLinks.forEach(mainCatFn);
subCatLinks.forEach(subCatFn);
backBtn.forEach(contentBackFn);

























