// let previewFn = () => {

// }

let contDivFn =  contLink => {
    contLink.addEventListener('click', e => {

        //for gamezone html category
        if(contLink.parentElement.dataset.genre === "Embed-Games"){
            return true;
        }else{
            e.preventDefault();
        }

        backBtn[backBtn.length - 1].style.display = "block";

        addScaleIn(contLink, contentLinks);

        const currentTargetParent = contLink.parentElement;
        const currentGenre = currentTargetParent.dataset.genre;

        const currentName = contLink.firstElementChild.innerText;
        const currentImg = contLink.firstElementChild.nextElementSibling.getAttribute('src');
        
        const currentLastChild = contLink.firstElementChild.nextElementSibling.nextElementSibling
        const currentFilePath = currentLastChild.firstElementChild.innerText;
        const currentDescription = currentLastChild.lastElementChild.innerText;
        const currentScreenshots = currentLastChild.querySelectorAll('img');

        const screenShotsImg = Array.from(currentScreenshots);
        const images = screenShotsImg.map(image => image.outerHTML);

        previewSection.style.display = "block";

        previewItem.innerHTML = `<div class="preview-head">
                                    <img src="${currentImg}" alt="${currentName}">
                                    <div class="thumb-name">
                                        <div class="thumb-cat-name">
                                            <h1>${currentName}</h1>
                                            <span>Category: ${currentGenre}</span>
                                        </div>
                                        <a href="${currentFilePath}" onclick="console.log('click')">INSTALL</a>
                                    </div>
                                </div>
                                <div class="preview-body">
                                        <h4>Information</h4>
                                        <div class="preview-info">
                                            <p>${currentDescription}</p>
                                        </div>
                                        <h4>Preview</h4>
                                        <div class="preview-screenshot">
                                            ${images.join("")}
                                        </div>
                                </div>`;

        const previewHead = document.querySelector('.preview-head');
        //const previewBody = document.querySelector('.preview-body');
        const imgCollection = document.querySelectorAll('.preview-screenshot img');
        const previewScreen = document.querySelector('.preview-screenshot');

        // previewHead.style.position = "relative";
        // previewHead.style.top = `${contLink.offsetTop}px`;

        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        })

        setTimeout(() => {
            //contLink.style.display = "none";
            contentList.style.left = "-9999px";
            previewHead.style.top = "0px";
            //previewHead.style.transition = "top 500ms ease-in-out";
            //previewHead.classList.add('grid-preview');
            //previewBody.style.left = "0px";
            previewItem.style.left = "0px"

        }, 500);

        imgFn(imgCollection, previewScreen);

        previewItem.classList.add(contLink.parentElement.getAttribute('data-genre'));
        previewItem.dataset.status = "selected";
    })
}

let imgFn = (imgCollection, previewScreen) => {
    let isDown = false;
    let startX;
    let scrollLeft;

    imgCollection.forEach(img => {
        img.addEventListener('click', e => {
            if(e.target == e.currentTarget){

                const getBody = document.body;

                const createDiv = document.createElement('div');
                const createXBtn = document.createElement('span');
                const cloneImg = e.currentTarget.cloneNode(true);

                window.scroll({
                    top: 0,
                    left: 0,
                    behavior: 'smooth'
                })

                createXBtn.id = "closeImg";
                createXBtn.classList.add("fas", "fa-window-close");
                createDiv.classList.add("selected-img");

                createDiv.style.cssText = ` position: absolute;
                                            z-index:3;
                                            top:0px;
                                            left:0px;
                                            width:100%;
                                            height:100%;
                                            background:rgba(0,0,0,.8);
                                            text-align:center;`;

                //testing button only

                createDiv.append(createXBtn);
                createDiv.append(cloneImg);
                getBody.prepend(createDiv);

                const xBtn = document.getElementById('closeImg');

                xBtn.addEventListener('click', () => {
                    getBody.removeChild(getBody.firstElementChild);
                })
            }
        })
    })

    previewScreen.addEventListener('mousedown', e => {
        isDown = true;
        startX = e.pageX - previewScreen.offsetLeft;
        scrollLeft = previewScreen.scrollLeft;
    })

    previewScreen.addEventListener('mousemove', e => {
        if(!isDown) return;
        e.preventDefault();
        const x = e.pageX - previewScreen.offsetLeft;
        const walk = (x - startX) * 2;
        previewScreen.scrollLeft = scrollLeft - walk;
    })

    previewScreen.addEventListener('mouseup', e => {
        isDown = false;
    })

    previewScreen.addEventListener('mouseleave', e => {
        isDown = false;
    })
}

contentLinks.forEach(contDivFn);