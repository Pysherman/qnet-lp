<?php
$strstatus=0;
if(isset($_GET['status']))
{
$strstatus=$_GET['status'];
}else{$strstatus=0;}

if($strstatus <> 0){

    header("Location: http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/nl/gamezone/gm1/errorstatus.php");
    die();
}

session_start();
include('inc.php');
require('prop/Consts.php');
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"];

	if($cat=="games-apk")
	{

		$CatCondition = " and (b.sub_category='Action' or b.sub_category='Arcade' or b.sub_category='Strategy')";

	}
	if($cat=="html5")
	{
		$CatCondition = " and (b.sub_category='Embed-Games')";
	}

}
else
{
	$cat = 'games-apk';
		$CatCondition = " and (b.sub_category='Action' or b.sub_category='Arcade' or b.sub_category='Strategy')";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gamespot</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="style/main.css">
<link rel="stylesheet" type="text/css" href="icofont/devicons/css/devicons.min.css">
<link rel="stylesheet" type="text/css" href="icofont/fontawesome5/css/all.min.css">

</head>
<body>

<header class="header">
	<img src="assets/logo.png" alt="GAMEZONE">
</header>
<main>
<?php
	ini_set('max_execution_time', 200);
	include('pages/mainCategory.php');
	include('pages/subCategory.php');
	include('pages/contentList.php');
?>
<section class="preview-section" id="previewSection">
	<a href="#" class="back-btn"><i class="fas fa-chevron-left"></i>Back</a>
	<div class="preview-item"></div>
</section>	
</main>
<script type="text/javascript" src="script/script.js"></script>
<script type="text/javascript" src="script/scriptPrev.js"></script>
</body>
</html>
