<section class="main-category" id="mainCategory">
	<ul>
		<li data-main-cat="videos"><a href="#"><span>Videos</span><i class="fas fa-film"></i></a></li>
		<li data-main-cat="apps"><a href="#"><span>Apps</span><i class="fas fa-cube"></i></a></li>
		<li data-main-cat="tips"><a href="#"><span>Tips</span><i class="fas fa-comment"></i></a></li>
	</ul>
</section>