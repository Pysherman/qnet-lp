<?php
	//Videos
	$CatConditionVid = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
	$catVid = 'videos';
	$queryJoinVid = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatConditionVid and category like '$catVid%'");

	echo '<ul class="'.$catVid.'" data-main-category="'.$catVid.'">';
	
	if($queryJoinVid){
		while($DateRow = mysqli_fetch_assoc($queryJoinVid)){
			$resCatgID = $DateRow['id'];
			$resName = $DateRow['category'];
			$resSbCatgID = $DateRow['sc_id'];
			$resSName = $DateRow['sub_category'];
			$_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";

			if($resSName == "Balance Diet"){
				$resSName = 'Balance-Diet';
				$icon = "<i class=\"fas fa-utensils\"></i>";
			}
			if($resSName == "Facts and Tips"){
				$resSName = "Facts-and-Tips";
				$icon = "<i class=\"fas fa-comment\"></i>";
			}
			if($resSName == "Fitness"){
				$resSName = "Fitness-Watch";
				$icon = "<i class=\"fas fa-heartbeat\"></i>";
			}

			echo '<li data-sub-cat="'.$resSName.'"><a href="#"><span>'.$resSName.'</span>'.$icon.'</a></li>';
		}
	}	
	echo '</ul>';
?>