<?php
$strstatus=0;
if(isset($_GET['status']))
{
$strstatus=$_GET['status'];
}else{$strstatus=0;}

if($strstatus <> 0){

    header("Location: http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/nl/gamezone/gm1/errorstatus.php");
    die();
}
session_start();
include('inc.php');
require('prop/Consts.php');
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"];
	if($cat=="videos")
	{
		$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
	}
	if($cat=="apps")
	{
		$CatCondition = " and (b.sub_category='Fitness' or b.sub_category='LifeStyle' or b.sub_category='Relaxation')";
	}
	if($cat=="tips")
	{
		$CatCondition = " and (b.sub_category='foods' or b.sub_category='sports' )";
	}
}
else
{
	$cat = 'videos';
	$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bodyfit</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="style/main.css">
<link rel="stylesheet" type="text/css" href="icofont/devicons/css/devicons.min.css">
<link rel="stylesheet" type="text/css" href="icofont/fontawesome5/css/all.min.css">

</head>
<body>

<header class="header">
	<img src="assets/logo.png" alt="Healthland">
</header>
<main>
<?php
	include('pages/mainCategory.php');
	include('pages/subCategory.php');
	include('pages/contentList.php');
?>
<section class="preview-section" id="previewSection">
	<a href="#" class="back-btn"><i class="fas fa-chevron-left"></i>Back</a>
	<div class="preview-item"></div>
</section>	
</main>
<script type="text/javascript" src="script/script.js"></script>
<script type="text/javascript" src="script/scriptPrev.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
  crossorigin="anonymous"></script>
</body>
</html>


