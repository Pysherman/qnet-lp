<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portal Apple UI Style</title>
    <link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>
    <section class="form-wrap">
		<div class="form-header">
			<h2>Login</h2>
		</div>
		<div class="error-div"></div>
		<form action="" method="GET" class="sign-in">
			<input type="text" class="m-num-login" name="Mnum" placeholder="Mobile Number">
			<input type="password" class="p-word-login" name="Pword" placeholder="Password">
			<input type="submit" class="login-btn" value="Sign In">
			<p>Don't have an account? <span class="sign-up-link">SIGN UP</span> now!</p>
		</form>
		
		<form action="" method="GET" class="sign-up">
			<input type="text" class="u-name" name="Uname" placeholder="Fullname">
			<input type="text" class="m-num-signup" name="Mnum" placeholder="Mobile Number">
			<input type="password" class="p-word-signup" name="Pword" placeholder="Password">
			<input type="password" class="rp-word" name="RPword" placeholder="Confirm Password">
			<input type="submit" class="sign-up-btn" value="Register">
		</form>
    </section>
    <script type="text/javascript" src="script/scriptLogin.js"></script>
</body>
</html>