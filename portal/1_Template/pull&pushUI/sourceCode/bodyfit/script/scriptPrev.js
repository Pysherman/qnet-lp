// let previewFn = () => {

// }

let contDivFn = contLink => {
    contLink.addEventListener('click', e => {

        //for gamezone html category
        if (contLink.parentElement.dataset.genre === "apps") {
            return true;
        } else {
            e.preventDefault();
        }

        backBtn[backBtn.length - 1].style.display = "block";

        addScaleIn(contLink, contentLinks);

        const currentTargetParent = contLink.parentElement;
        const currentGenre = currentTargetParent.dataset.genre;

        const currentName = contLink.firstElementChild.innerText;
        //const currentImg = contLink.firstElementChild.nextElementSibling.getAttribute('src');
        const currentImg = contLink.firstElementChild.nextElementSibling.cloneNode(true);

        const currentLastChild = contLink.firstElementChild.nextElementSibling.nextElementSibling
        const currentFilePath = currentLastChild.firstElementChild.innerText;
        const currentDescription = currentLastChild.lastElementChild.innerText;
        const currentScreenshots = currentLastChild.querySelectorAll('img');
        let images;
        let videoControls;
        let audioControls;
        let currentID;
        let linkText;



        previewSection.style.display = "block";

        //<img src="${currentImg}" alt="${currentName}"></img>

        if (currentTargetParent.dataset.catOrigin == "Apps" || currentTargetParent.dataset.catOrigin == "videos") {
            linkText = "INSTALL";
        } else {
            linkText = "DOWNLOAD";
        }
        if (currentTargetParent.dataset.catOrigin == "Tips") {
            linkText = "VIEW";
        }



        if (currentTargetParent.dataset.catOrigin == "videos") {
            currentImg.id = "videos";
            currentID = currentImg.id;
            videoControls = `<div class="controls">
                                <button class="btn" id="play">
                                    <i class="fas fa-play"></i>
                                </button>
                                <button class="btn" id="stop">
                                    <i class="fas fa-stop"></i>
                                </button>
                                <input type="range" id="progress" class="progress" min="0" max="100" step="0.1" value="0" />
                                <span class="timestamp" id="timestamp">00:00</span>
                            </div>`;
        }

        if (currentTargetParent.dataset.catOrigin == "Tones") {
            currentID = "audio";
            audioControls = `<div class="controls">
                                <audio id="${currentID}">
                                    <source src="${currentFilePath}" type="audio/mpeg">
                                </audio>
                                <button class="btn" id="play">
                                    <i class="fas fa-play"></i>
                                </button>
                                <button class="btn" id="stop">
                                    <i class="fas fa-stop"></i>
                                </button>
                                <input type="range" id="progress" class="progress" min="0" max="100" step="0.1" value="0" />
                                <span class="timestamp" id="timestamp">00:00</span>
                            </div>`;
        }



        if (currentScreenshots.length > 0) {
            const screenShotsImg = Array.from(currentScreenshots);
            images = screenShotsImg.map(image => image.outerHTML);
        }

        previewItem.innerHTML = `<div class="preview-head">
                                    ${currentImg.outerHTML}
                                    ${videoControls == undefined ? "" : videoControls}
                                    ${audioControls == undefined ? "" : audioControls}
                                    <div class="thumb-name">
                                        <div class="thumb-cat-name">
                                            <h1>${currentName}</h1>
                                            <span>Category: ${currentGenre}</span>
                                        </div>
                                        <a href="${currentFilePath}">${linkText}</a>
                                    </div>
                                </div>
                                <div class="preview-body">
                                        <h4>Information</h4>
                                        <div class="preview-info">
                                            <p>${currentDescription}</p>
                                        </div>
                                        <h4>Preview</h4>
                                        <div class="preview-screenshot">
                                            ${images == undefined ? "" : images.join("")}
                                        </div>
                                </div>`;

        const previewHead = document.querySelector('.preview-head');
        const previewBody = document.querySelector('.preview-body');
        const imgCollection = document.querySelectorAll('.preview-screenshot img');
        const previewScreen = document.querySelector('.preview-screenshot');

        // previewHead.style.position = "relative";
        // previewHead.style.top = `${contLink.offsetTop}px`;

        // if(previewScreen.innerHTML == " "){
        //     console.log(empty);
        // }

        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        })

        setTimeout(() => {
            //contLink.style.display = "none";
            contentList.style.left = "-9999px";
            previewHead.style.top = "0px";
            //previewHead.style.transition = "top 500ms ease-in-out";
            //previewHead.classList.add('grid-preview');
            //previewBody.style.left = "0px";
            previewItem.style.left = "0px"

        }, 500);

        if (currentTargetParent.dataset.catOrigin == "videos" || currentTargetParent.dataset.catOrigin == "Tones") {
            videoFn(currentID);
        }

        if (previewScreen.children.length == 0) {
            previewBody.removeChild(previewScreen.previousElementSibling);
            previewBody.removeChild(previewBody.lastElementChild);
        } else {
            imgFn(imgCollection, previewScreen);
        }

        //previewItem.classList.add(contLink.parentElement.getAttribute('data-genre'));
        previewItem.dataset.status = "selected";
    })
}

let videoFn = currentID => {
    let videoTag = document.getElementById(currentID);
    const play = document.getElementById('play');
    const stop = document.getElementById('stop');
    const progress = document.getElementById('progress');
    const timestamp = document.getElementById('timestamp');

    let toggleVideoStatus = () => {

        if (videoTag.paused) {
            videoTag.play();
        } else {
            videoTag.pause();
        }
    }

    let updatePlayIcon = () => {
        if (videoTag.paused) {
            play.innerHTML = '<i class="fas fa-play"></i>';
        } else {
            play.innerHTML = '<i class="fas fa-pause"></i>';
        }
    }

    let updateProgress = () => {
        progress.value = (videoTag.currentTime / videoTag.duration) * 100;

        let mins = Math.floor(videoTag.currentTime / 60);
        if (mins < 10) {
            mins = '0' + String(mins);
        }

        let secs = Math.floor(videoTag.currentTime % 60);
        if (secs < 10) {
            secs = '0' + String(secs);
        }

        timestamp.innerHTML = `${mins}:${secs}`;
    }

    let setVideoProgress = () => {
        videoTag.currentTime = (parseInt(progress.value) * videoTag.duration) / 100;
    }

    let stopVideo = () => {
        videoTag.currentTime = 0;
        videoTag.pause();
    }

    videoTag.addEventListener('play', updatePlayIcon);
    videoTag.addEventListener('click', toggleVideoStatus);
    videoTag.addEventListener('pause', updatePlayIcon);
    videoTag.addEventListener('timeupdate', updateProgress);

    play.addEventListener('click', toggleVideoStatus);
    stop.addEventListener('click', stopVideo);

    progress.addEventListener('change', setVideoProgress);

    backBtn[backBtn.length - 1].addEventListener('click', stopVideo);
}

let imgFn = (imgCollection, previewScreen) => {
    let isDown = false;
    let startX;
    let scrollLeft;

    imgCollection.forEach(img => {
        img.addEventListener('click', e => {
            if (e.target == e.currentTarget) {

                const getBody = document.body;

                const createDiv = document.createElement('div');
                const createXBtn = document.createElement('span');
                const cloneImg = e.currentTarget.cloneNode(true);

                window.scroll({
                    top: 0,
                    left: 0,
                    behavior: 'smooth'
                })

                createXBtn.id = "closeImg";
                createXBtn.classList.add("fas", "fa-window-close");
                createDiv.classList.add("selected-img");

                createDiv.style.cssText = ` position: absolute;
                                            z-index:3;
                                            top:0px;
                                            left:0px;
                                            width:100%;
                                            height:100%;
                                            background:rgba(0,0,0,.8);
                                            text-align:center;`;

                //testing button only

                createDiv.append(createXBtn);
                createDiv.append(cloneImg);
                getBody.prepend(createDiv);

                const xBtn = document.getElementById('closeImg');

                xBtn.addEventListener('click', () => {
                    getBody.removeChild(getBody.firstElementChild);
                })
            }
        })
    })

    previewScreen.addEventListener('mousedown', e => {
        isDown = true;
        startX = e.pageX - previewScreen.offsetLeft;
        scrollLeft = previewScreen.scrollLeft;
    })

    previewScreen.addEventListener('mousemove', e => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - previewScreen.offsetLeft;
        const walk = (x - startX) * 2;
        previewScreen.scrollLeft = scrollLeft - walk;
    })

    previewScreen.addEventListener('mouseup', e => {
        isDown = false;
    })

    previewScreen.addEventListener('mouseleave', e => {
        isDown = false;
    })
}

contentLinks.forEach(contDivFn);
