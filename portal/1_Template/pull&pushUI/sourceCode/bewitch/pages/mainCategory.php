<section class="main-category" id="mainCategory">
	<ul>
		<li data-main-cat="videos"><a href="#"><span>Videos</span><i class="fas fa-film"></i></a></li>
		<li data-main-cat="wallpaper"><a href="#"><span>Wallpaper</span><i class="fas fa-image"></i></a></li>
	</ul>
</section>