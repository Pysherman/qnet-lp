<?php
$strstatus=0;
if(isset($_GET['status']))
{
$strstatus=$_GET['status'];
}else{$strstatus=0;}

if($strstatus <> 0){

    header("Location: http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/nl/gamezone/gm1/errorstatus.php");
    die();
}

session_start();
include('inc.php');
require('prop/Consts.php');
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"]; 
	$btn_play="";
	if($cat=="games-apk")
	{
		//$btnImg = "<span id=\"active\">ANDROID GAMES</span>";
		$btnImg = "<span>ANDROID GAMES</span>";
		$htmlImg = "<span>HTML5 GAMES</span>";
		$CatCondition = " and (b.sub_category='Action' or b.sub_category='Arcade' or b.sub_category='Strategy')";
		$btn_play="DOWNLOAD";
	}
	if($cat=="html5")
	{
		$btnImg = "<span>ANDROID GAMES</span>";
		//$htmlImg = "<span id=\"active\">HTML5 GAMES</span>";
		$htmlImg = "<span>HTML5 GAMES</span>";
		$CatCondition = " and (b.sub_category='Embed-Games')";
		$btn_play="PLAY GAME";
	}

}
else
{
	$cat = 'games-apk';
		//$btnImg = "<span id=\"active\">ANDROID GAMES</span>";
		$btnImg = "<span>ANDROID GAMES</span>";
		$htmlImg = "<span>HTML5 GAMES</span>";
		$CatCondition = " and (b.sub_category='Action' or b.sub_category='Arcade' or b.sub_category='Strategy')";
		$btn_play="SKINITE";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gamezone</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript" src="js/jquery.js" ></script>
<script type="text/javascript">
$(document).ready(function(){
	$('div.expand:first-child').children('a').children('img').attr('src', 'img/minus.png');
	$('div.expand:first-child').children('a').children('img').css('margin-top', '6px');
    $('.adCntnr div.acco2:eq(0)').find('div.expand:eq(0)').addClass('openAd').end()
    .find('div.collapse:gt(0)').hide().end()
    .find('div.expand').click(function() {
        $(this).toggleClass('openAd').siblings().removeClass('openAd').end()
        .next('div.collapse').slideToggle().siblings('div.collapse:visible').slideUp();
		$('div.expand').children('a').children('img').attr('src', 'img/plus.png');
		if($(this).hasClass('openAd')){
			$(this).children('a').children('img').attr('src', 'img/minus.png');
			$(this).children('a').children('img').css('margin-top', '6px');
		}else{
			$(this).children('a').children('img').attr('src', 'img/plus.png');
			$(this).children('a').children('img').css('margin-top', '0px');
		}
        return false;
    });
});
</script>
</head>

<body> 
<header class="header-main">
	<section class="header-desktop-border">
		<section class="header-desktop-wrapper">
			<img class="gamezone-logo" src="img/gameCentral.png">
			<div class="menu-wrapper">
				<div class="desktop-menu active-list">
					<ul>
						<li><a href="?varx=html5"><?php echo $htmlImg; ?></a></li>
						<li><a href="?varx=games-apk"><?php echo $btnImg; ?></a></li>
					</ul>
				</div>
				<div class="mobile-menu-burger" id="NavBgrBtn">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
		</section>
	</section>
	<!-- header mobile display none at default -->
	<section class="header-mobile-wrapper" id="dropDown">
		<div class="mobile-menu active-list">
			<ul>
				<li><a href="?varx=html5"><?php echo $htmlImg; ?></a></li>
				<li><a href="?varx=games-apk"><?php echo $btnImg; ?></a></li>
			</ul>
		</div>
	</section>
</header>

<div id="contentWrap" class="contentWrap" style="margin-top:auto;">
	<div class="adCntnr">
    	<div class="acco2">
        	<?php

            $queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");
            //echo "SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'";
            if($queryJoin)
            {
			
                while ($DateRow = mysqli_fetch_assoc($queryJoin))
                {
                    $resCatgID	= $DateRow['id'];
                    $resName= $DateRow['category'];
                    $resSbCatgID= $DateRow['sc_id'];
                    $resSName= $DateRow['sub_category'];
                    $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";
                    
                    if($resSName=="Application")
                    {
                        $resSName = "Antivirus";
                    }
					
				echo '<div class="tab-title expand">
						<a href="#" title="expand/collapse">
							<span class="bg-block"></span>
							<span class="tab-name">'.ucfirst($resSName).'</span>
							<img src="img/plus.png">
						</a>
					  </div>';
                   
				echo '<div class="collapse">
					<div class="accCntnt">
						<ul class="container flex">';

				
	if(!empty($_SESSION['categref']))
	{
		//$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
		$categref 	= $_SESSION['categref'];
		$sesCatg 	= $_SESSION['sesCatg'];
		$html5stat = ($sesCatg == 'html5') ? "and status = 1": "";
		//echo $html5stat;

		$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime FROM cms.contents WHERE id!=1 and $categref $html5stat order by id desc limit 20");

		if($getContent)
		{
			while($items = mysqli_fetch_array($getContent))
			{
				$contentID 		= $items['id'];
				$itemTitle 		= $items['title'];
				$description 	= $items['description'];
				$file_name 		= $items['file_name'];
				$original_file_name = $items['original_file_name'];
				$mime 			= $items['mime'];
		  		$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
				if($cat=="games-apk")
				{ $filename = $contenrdir.$file_name;

				$file = pathinfo($file_name, PATHINFO_FILENAME);
				
				}
				
				if($cat=="html5")
				{ $filename = $file_name;
				/*$contenrdir = $url;*/
				//debug("Content dir ::$contenrdir");
				$file =$original_file_name;
				}
				//debug("$file");
				
				if($ext=="mp4")
				{
					$thumbimg = $file.'.png';
					$preview ='<video width="100%" height="100"  controls preload="metadata">
							<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
						  </video>';
				}
				elseif($ext=="mp3")
				{
					$preview = '<img class="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
				}
				else
				{
					
					$thumbimg = $file.'.png';
					$preview ='<img class="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';

				}
				//debug($preview);

					if($cat=="html5")
					{
					 echo'<li class="item">						  
							'.$preview.'
							<div id="title">'.$itemTitle.'</div>
							<a href="'.$filename.'" class="btn">'.$btn_play.'</a>
						  </li>';
					}else{

					echo'<li class="item">						  
							<a href="preview.php?varx='.$sesCatg.'&contid='.$contentID.'">'.$preview.'</a>
							<div id="title"><a href="preview.php?varx='.$sesCatg.'&contid='.$contentID.'">'.$itemTitle.'</a></div>
						  </li>';
						}
			} // 2nd while loop
		}
	}	

                echo"</ul>                
					</div>
				</div>";

                } // End of 1st while loop

            } // End of if condtion

			?>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>
