<?php
ob_start();
session_start();
include('inc.php');
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"]; 
	
	if($cat=="games-apk")
	{

		//$btnImg = "<span id=\"active\">ANDROID GAMES</span>";
		$btnImg = "<span>ANDROID GAMES</span>";
		$htmlImg = "<span>HTML5 GAMES</span>";
	}
	if($cat=="html5")
	{
		$btnImg = "<span>ANDROID GAMES</span>";
		$htmlImg = "<span>HTML5 GAMES</span>";
		// $htmlImg = "<span id=\"active\">HTML5 GAMES</span>";
	}

}
else
{
	$cat = 'games-apk';
	// $btnImg = "<span id=\"active\">ANDROID GAMES</span>";
	$btnImg = "<span>ANDROID GAMES</span>";
	$htmlImg = "<span>HTML5 GAMES</span>";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>Preview</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/style.css">
<script src="js/jquery-1.12.4.min.js"></script>
</head>

<body> 
<header class="header-main">
	<section class="header-desktop-border">
		<section class="header-desktop-wrapper">
			<img class="gamezone-logo" src="img/gameCentral.png">
			<div class="menu-wrapper">
				<div class="desktop-menu active-list">
					<ul>
						<li><a href="./?varx=html5"><?php echo $htmlImg; ?></a></li>
						<li><a href="./?varx=games-apk"><?php echo $btnImg; ?></a></li>
					</ul>
				</div>
				<div class="mobile-menu-burger" id="NavBgrBtn">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
		</section>
	</section>
	<!-- header mobile display none at default -->
	<section class="header-mobile-wrapper" id="dropDown">
		<div class="mobile-menu active-list">
			<ul>
				<li><a href="./?varx=html5"><?php echo $htmlImg; ?></a></li>
				<li><a href="./?varx=games-apk"><?php echo $btnImg; ?></a></li>
			</ul>
		</div>
	</section>
</header>
<div id="contentWrap" class="contentWrap">

	<div class="accordion_container">
        <?php
			if(isset($_GET['varx']))
			{
				$prevSesID = $_GET['contid'];
				$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
				
				if($_GET['varx']=="")
				{
					header("location:index.php");
				}
				else
				{
					$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
					$data = array();
					if($getContent)
					{
						while($items = mysqli_fetch_array($getContent))
						{
							$ContentID 		= $items['id'];
							$itemTitle 		= $items['title'];
							$description 	= $items['description'];
							$contentRate	= $items['rate'];
							$file_name 		= $items['file_name'];
							$original_file_name = $items['original_file_name'];
							$mime 			= $items['mime'];
							$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
							$filename = $contenrdir.$file_name;
							
							$file = pathinfo($file_name, PATHINFO_FILENAME);
							
							if($ext=="mp4" || $ext=="mp3")
							{
								$thumbimg = $file.'.png';
								$preview ='<video width="100%" height="200"  controls preload="metadata">
										<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
									  </video>';
							}
							else
							{
								$thumbimg = $file.'.png';
								$preview ='<img src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
							}
						}
					}
				}
			}
			else
			{
				header("location:index.php");
			}
		?>
		<div class="close-container">
			<a href="./?varx=games-apk" class="close-btn"><img src="img/close_btn.png"></a>
		</div>
		<section class="thumb-dl-section">
			<div class="thumb-dl-container">
				<div class="thumb">
					<?php echo $preview ?>
				</div>
				<div class="name-dl">
					<h4><?php echo $itemTitle; ?></h4>
					<a class="dl-btn" href="<?php echo $filename ?>"><img src="img/dl-icon.png"><span class="dl-text">Download APK</span></a>
				</div>
			</div>
			<div class="description">
				<h5>Game Description</h5>
				<p><?php echo stripslashes($description); ?></p>
			</div>
			<?php 
			if($ext=="apk" || $ext=="xapk"){
				$prevFile = $conn->query("SELECT file_name FROM cms.preview WHERE content_id='$ContentID'");
				if(mysqli_affected_rows($conn)){
					echo'<div class="screenshot">
							<div class="img-container">';
					while($PrevItems = mysqli_fetch_array($prevFile)){
						$scrFiles	= $PrevItems['file_name'];
						$screenshot = $contenrdir.$scrFiles;
						echo '<img src="'.$screenshot.'" width="221" height="191" class="mySlides">';
					}
					echo '</div>';
					echo '<a href="#" class="prev" onclick="plusDivs(-1)"><img src="img/prev.png"></a>';
					echo '<a href="#" class="next" onclick="plusDivs(1)"><img src="img/next.png"></a>';
					echo '</div>';
					echo '<script>
							var slideIndex = 1;
							showDivs(slideIndex);
							
							function plusDivs(n){
								showDivs(slideIndex += n);
							}
							
							function showDivs(n) {
							  var i;
							  var x = document.getElementsByClassName("mySlides");
							  if (n > x.length) {slideIndex = 1}    
							  if (n < 1) {slideIndex = x.length}
							  for (i = 0; i < x.length; i++) {
								 x[i].style.display = "none";  
							  }
							  x[slideIndex-1].style.display = "block";  
							}
							
						</script>';
				}
			}
			?>
		</section>        
        </div>   
    </div>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>
