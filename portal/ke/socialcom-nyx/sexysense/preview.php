<?php
   ob_start();
   session_start();
   include('connection/inc.php');
   if(isset($_GET["varx"]))
   {
   	$cat = $_GET["varx"];
   	@$sub = $_GET["subcat"];
   
   }
   $_SESSION['sesCatg'] = $cat;
   ?>
<!DOCTYPE html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <title>Preview</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <script type="text/javascript" src="js/jquery.js"></script>
   <!--link href="./css/toggle.css?2" rel="stylesheet" type="text/css">
      <script src="./js/jquery-1.12.4.min.js"></script>
      <script src="./js/toggle.js?2" type="text/javascript"></script-->
</head>
<body>
   <header>
      <div class="nav-burger" id="NavBurger">
         <span></span>
         <span></span>
         <span></span>
      </div>
      <div class="logo">
         <a href="./"><img src="images/logo.png" alt="Sexy Sense"></a>
      </div>
      <div class="menu"  id="sideNav">
         <a href="./?varx=videos">Videos</a>
         <a href="./?varx=wallpaper">Wallpaper</a>              
      </div>
   </header>
   <div class="wrap">
      <div class="main_container-preview">
         <div class="main_container_content">
            <a  class="close_btn"  href="<?php echo './?varx='.$cat;'' ?>"><img src="images/close_btn.png" alt=""></a>
               <div class="panitankonaimongitlogron">
                  <?php
                     if(isset($_GET['varx']))
                     {
                     $prevSesID = $_GET['contid'];
                     $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
                     
                     if($_GET['varx']=="")
                     {
                     header("location:index.php");
                     }
                     else
                     {
                     $getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
                     $data = array();
                     if($getContent)
                     {
                     while($items = mysqli_fetch_array($getContent))
                     {
                     $ContentID 		= $items['id'];
                     $itemTitle 		= $items['title'];
                     $description 	= $items['description'];
                     $contentRate	= $items['rate'];
                     $file_name 		= $items['file_name'];
                     $original_file_name = $items['original_file_name'];
                     $mime 			= $items['mime'];
                     $ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
                     $filename = $contenrdir.$file_name;
                     
                     $file = pathinfo($file_name, PATHINFO_FILENAME);
                     
                     if($ext=="mp4" || $ext=="mp3")
                     {
                     $thumbimg = $file.'.png';
                     $preview ='<video width="70%"  controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
                     <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
                     </video>';
                     }
                     else
                     {
                     $thumbimg = $file.'.png';
                     $preview ='<img src="'.$filename.'" alt="'.$itemTitle.'">';
                     }
                     }
                     }
                     }
                     }
                     else
                     {
                     header("location:index.php");
                     }
                     ?>
                  <div class='preview-content'>
                      <div class="preview-content-item">
                           <?php echo $preview; ?>
                    </div>
                    <div id="title-preview">
                        <div><?php echo $itemTitle; ?></div>
                        <div ><a href="<?php echo $filename; ?>" id="btn-preview"> 
                        <img src="images/download_icon.png" alt=""> <span>Download</span><span><?php echo $ext; ?></span></a></div>
                    </div>
                    <?php echo stripslashes($description); ?>
                  </div>
            </div>
         </div>
      </div>
   </div>
   <script>
var navBurger = document.getElementById('NavBurger');
var sideMenu = document.getElementById('sideNav');
var toggle = false;
navBurger.addEventListener('click', slideSideMenu);
function slideSideMenu() {
//slides the menu in or out
if (toggle === true) {
//slide out
sideMenu.classList.remove('menu-show');
sideMenu.classList.add('menu');
toggle = false;
} else {
//slide in
sideMenu.classList.add('menu-show');
sideMenu.classList.remove('menu');
toggle = true;
}
navBurger.classList.toggle('tog-burger');
};

var url = window.location.href;
var page = url.includes("videos");
   if (page) {
   $('#sideNav a:first').addClass('link_active')
   }
   else{
      $('#sideNav a:last').addClass('link_active')
   }
   </script>
</body>
</html>