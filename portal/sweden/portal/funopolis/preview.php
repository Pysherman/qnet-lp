<?php
ob_start();
session_start();
include('inc.php');
if(isset($_GET["varx"])){

	$cat = $_GET["varx"]; 
	$sub = $_GET["subcat"];
	
	if($cat=="Games-apk"){
		$gamesImg = "games_active.png";
		$videoImg = "videos.png";
		$tonesImg = "tones.png";
		$appsImg  = "apps.png";
		$CatCondition = " and (b.sub_category='Arcade' or b.sub_category='Strategy' or b.sub_category='Action')";
	}
	
	if($cat=="VIDEOS"){
		$gamesImg = "games.png";
		$videoImg = "videos_active.png";
		$tonesImg = "tones.png";
		$appsImg  = "apps.png";
		$CatCondition = " and (b.sub_category='Funny' or b.sub_category='Cartoons' or b.sub_category='Car Racing')";
	}

	if($cat=="Tones"){
		$gamesImg = "games.png";
		$videoImg = "videos.png";
		$tonesImg = "tones_active.png";
		$appsImg  = "apps.png";
		$CatCondition = " and (b.sub_category='Tones-Portal')";
	}

	if($cat=="Apps"){
		$gamesImg = "games.png";
		$videoImg = "videos.png";
		$tonesImg = "tones.png";
		$appsImg  = "apps_active.png";
		$CatCondition = " and (b.sub_category='Productivity' or b.sub_category='Utility')";
	}

}else{
	$cat = 'Games-apk';
	$gamesImg = "games_active.png";
	$videoImg = "videos.png";
	$tonesImg = "tones.png";
	$appsImg  = "apps.png";
	$CatCondition = " and (b.sub_category='Adventure' or b.sub_category='Puzzle' or b.sub_category='Simulation')";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>PREVIEW</title>
<meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/jquery.js" ></script>

</head>

<body>
<section class="header-nav">
		<div class="nav-burger" id="NavBurger">
			<span></span>
			<span></span>
			<span></span>
		</div>
		<img src="assets/funzone_logo.png" alt="Fun Zone">
	</section>
	<section class="side-nav" id="sideNav">
		<div class="logo">
			<img src="assets/funzone_logo.png" alt="">
		</div>
		<ul class="nav-link">
			<li><a href="index.php?varx=Games-apk"><img src="assets/<?php echo $gamesImg; ?>" alt=""></a></li>
			<li><a href="index.php?varx=VIDEOS"><img src="assets/<?php echo $videoImg; ?>" alt=""></a></li>
			<li><a href="index.php?varx=Tones"><img src="assets/<?php echo $tonesImg; ?>" alt=""></a></li>
			<li><a href="index.php?varx=Apps"><img src="assets/<?php echo $appsImg; ?>" alt=""></a></li>
		</ul>
	</section>

<div id="contentWrap">

	<div class="preview-wrapper">
    
    	<div class="preview-head">
				<h4>PREVIEW</h4>   
				<a href="<?php echo './?varx='.$cat; ?>" class="preview-close">
					<span></span>
					<span></span>
				</a>
			</div>   
				
			<div>
        <?php
			if(isset($_GET['varx']))
			{
				$prevSesID = $_GET['contid'];
				$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
				
				if($_GET['varx']=="")
				{
					header("location:index.php");
				}
				else
				{
					$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate,category_id FROM cms.contents WHERE id=$prevSesID");
					$data = array();
					if($getContent)
					{
						while($items = mysqli_fetch_array($getContent))
						{
							$category_id 		= $items['category_id'];
							$ContentID 		= $items['id'];
							$itemTitle 		= $items['title'];
							$description 	= $items['description'];
							$file_name 		= $items['file_name'];
							$original_file_name = $items['original_file_name'];
							$mime 			= $items['mime'];
							$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
														
							if($category_id==9){
								$filename = $file_name;
							}else{
								$filename = $contenrdir.$file_name;
							}
							
							
							$file = pathinfo($file_name, PATHINFO_FILENAME);
							
							if($ext=="mp4" || $ext=="mp3")
							{
								$thumbimg = $file.'.png';
								$preview ='<video width="100%"  controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
										<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
									  </video>';
							}
							else
							{
								$thumbimg = $file.'.png';
								$preview ='<img src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
							}
						}
					}
				}
			}
			else
			{
				header("location:index.php");
			}
		?>
			<div class="prev-main">
				<div class="preview-holder">
					<div class="preview-container">
						<div class="prev-image">
							<?php echo $preview; ?>
						</div>
						<div class="prev-content">
							<h3><?php echo $itemTitle; ?></h3>
							<a href="<?php echo $filename; ?>" id="btn">DOWNLOAD</a>
							<p><?php echo stripslashes($description); ?></p>
						</div>
												
						<?php
			if($ext=="apk" || $ext=="xapk")
			{
				$prevFile = $conn->query("SELECT file_name FROM cms.preview WHERE content_id='$ContentID'");
				//if($prevFile)
				if(mysqli_affected_rows($conn))
				{
					echo'<p style="padding:8px; font-weight:bold; text-align:left">Screenshots:</p>
				<div class="w3-content w3-display-container" style="padding:5px;">
				  <div style="width:221px; margin:auto">';
					while($PrevItems = mysqli_fetch_array($prevFile))
					{
						$scrFiles	= $PrevItems['file_name'];
						$screenshot = $contenrdir.$scrFiles;
						
						echo"<img src=\"".$screenshot."\" style=\"width:100%; height:av\" class=\"mySlides\">";
					}
					echo"</div>
					  <button class=\"w3-button w3-black w3-display-left\" onclick=\"plusDivs(-1)\"><img src=\"assets/btn_left.png\" width=\"19\" height=\"14\" /></button>
					  
					  <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs(1)\"><img src=\"assets/btn_right.png\" width=\"19\" height=\"14\" /></button>
				  </div>
					<script>
					var slideIndex = 1;
					showDivs(slideIndex);
					
					function plusDivs(n) {
					  showDivs(slideIndex += n);
					}
					
					function showDivs(n) {
					  var i;
					  var x = document.getElementsByClassName(\"mySlides\");
					  if (n > x.length) {slideIndex = 1}    
					  if (n < 1) {slideIndex = x.length}
					  for (i = 0; i < x.length; i++) {
						 x[i].style.display = \"none\";  
					  }
					  x[slideIndex-1].style.display = \"block\";  
					}
					</script>
				</div>";
				}
			}
			?>
					</div>					
				</div>
      	    
            </div>
        </div>
    
    </div>
    
</div>
<script type="text/javascript" src="js/sideNav.js" ></script>
</body>
</html>