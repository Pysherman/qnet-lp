var navBurger = document.getElementById('NavBurger');
var sideMenu = document.getElementById('sideNav');
var toggle = false;
navBurger.addEventListener('click', slideSideMenu);

if(window.innerWidth >= 900){
	sideMenu.classList.remove('slide-out');
}

function slideSideMenu(){
//slides the menu in or out
	if(toggle === true){
		//slide out
		sideMenu.classList.remove('slide-in');
		sideMenu.classList.add('slide-out');
		toggle = false;
	}else{
		//slide in
		sideMenu.classList.add('slide-in');
		sideMenu.classList.remove('slide-out');
		toggle = true;
	}

	navBurger.classList.toggle('tog-burger');

};
