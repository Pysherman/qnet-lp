$(window).load(function () {
	$(document).ready(function () {

		var items27 = $(".portfolio-item27");
		var scrollContainer27 = $(".offer-pg-cont27");


		function fetchItem(container, items27, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items27[0]);
			}

			for (i = 0; i < items27.length; i++) {

				if (isNext && $(items27[i]).position().left > 0) {
					return $(items27[i]);
				} else if (!isNext && $(items27[i]).position().left >= 0) {
					return i == 0 ? $(items27[items27.length - 1]) : $(items27[i - 1]);
				}
			}

			return null;
		}

		function moveToItem27(event) {
			var isNext = event.data.direction == "next27";
			var item = isNext ? fetchItem(scrollContainer27, items27, true) : fetchItem(scrollContainer27, items27, false);

			if (item) {
				scrollContainer27.animate({
					"scrollLeft": item.position().left + scrollContainer27.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left27").click({
			direction: "prev27"
		}, moveToItem27);
		$(".arrow-right27").click({
			direction: "next27"
		}, moveToItem27);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items28 = $(".portfolio-item28");
		var scrollContainer28 = $(".offer-pg-cont28");


		function fetchItem(container, items28, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items28[0]);
			}

			for (i = 0; i < items28.length; i++) {

				if (isNext && $(items28[i]).position().left > 0) {
					return $(items28[i]);
				} else if (!isNext && $(items28[i]).position().left >= 0) {
					return i == 0 ? $(items28[items28.length - 1]) : $(items28[i - 1]);
				}
			}

			return null;
		}

		function moveToItem28(event) {
			var isNext = event.data.direction == "next28";
			var item = isNext ? fetchItem(scrollContainer28, items28, true) : fetchItem(scrollContainer28, items28, false);

			if (item) {
				scrollContainer28.animate({
					"scrollLeft": item.position().left + scrollContainer28.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left28").click({
			direction: "prev28"
		}, moveToItem28);
		$(".arrow-right28").click({
			direction: "next28"
		}, moveToItem28);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items29 = $(".portfolio-item29");
		var scrollContainer29 = $(".offer-pg-cont29");


		function fetchItem(container, items29, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items29[0]);
			}

			for (i = 0; i < items29.length; i++) {

				if (isNext && $(items29[i]).position().left > 0) {
					return $(items29[i]);
				} else if (!isNext && $(items29[i]).position().left >= 0) {
					return i == 0 ? $(items29[items29.length - 1]) : $(items29[i - 1]);
				}
			}

			return null;
		}

		function moveToItem29(event) {
			var isNext = event.data.direction == "next29";
			var item = isNext ? fetchItem(scrollContainer29, items29, true) : fetchItem(scrollContainer29, items29, false);

			if (item) {
				scrollContainer29.animate({
					"scrollLeft": item.position().left + scrollContainer29.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left29").click({
			direction: "prev29"
		}, moveToItem29);
		$(".arrow-right29").click({
			direction: "next29"
		}, moveToItem29);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items39 = $(".portfolio-item39");
		var scrollContainer39 = $(".offer-pg-cont39");


		function fetchItem(container, items39, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items39[0]);
			}

			for (i = 0; i < items39.length; i++) {

				if (isNext && $(items39[i]).position().left > 0) {
					return $(items39[i]);
				} else if (!isNext && $(items39[i]).position().left >= 0) {
					return i == 0 ? $(items39[items39.length - 1]) : $(items39[i - 1]);
				}
			}

			return null;
		}

		function moveToItem39(event) {
			var isNext = event.data.direction == "next39";
			var item = isNext ? fetchItem(scrollContainer39, items39, true) : fetchItem(scrollContainer39, items39, false);

			if (item) {
				scrollContainer39.animate({
					"scrollLeft": item.position().left + scrollContainer39.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left39").click({
			direction: "prev39"
		}, moveToItem39);
		$(".arrow-right39").click({
			direction: "next39"
		}, moveToItem39);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items40 = $(".portfolio-item40");
		var scrollContainer40 = $(".offer-pg-cont40");


		function fetchItem(container, items40, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items40[0]);
			}

			for (i = 0; i < items40.length; i++) {

				if (isNext && $(items40[i]).position().left > 0) {
					return $(items40[i]);
				} else if (!isNext && $(items40[i]).position().left >= 0) {
					return i == 0 ? $(items40[items40.length - 1]) : $(items40[i - 1]);
				}
			}

			return null;
		}

		function moveToItem40(event) {
			var isNext = event.data.direction == "next40";
			var item = isNext ? fetchItem(scrollContainer40, items40, true) : fetchItem(scrollContainer40, items40, false);

			if (item) {
				scrollContainer40.animate({
					"scrollLeft": item.position().left + scrollContainer40.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left40").click({
			direction: "prev40"
		}, moveToItem40);
		$(".arrow-right40").click({
			direction: "next40"
		}, moveToItem40);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items41 = $(".portfolio-item41");
		var scrollContainer41 = $(".offer-pg-cont41");


		function fetchItem(container, items41, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items41[0]);
			}

			for (i = 0; i < items41.length; i++) {

				if (isNext && $(items41[i]).position().left > 0) {
					return $(items41[i]);
				} else if (!isNext && $(items41[i]).position().left >= 0) {
					return i == 0 ? $(items41[items41.length - 1]) : $(items41[i - 1]);
				}
			}

			return null;
		}

		function moveToItem41(event) {
			var isNext = event.data.direction == "next41";
			var item = isNext ? fetchItem(scrollContainer41, items41, true) : fetchItem(scrollContainer41, items41, false);

			if (item) {
				scrollContainer41.animate({
					"scrollLeft": item.position().left + scrollContainer41.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left41").click({
			direction: "prev41"
		}, moveToItem41);
		$(".arrow-right41").click({
			direction: "next41"
		}, moveToItem41);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items42 = $(".portfolio-item42");
		var scrollContainer42 = $(".offer-pg-cont42");


		function fetchItem(container, items42, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items42[0]);
			}

			for (i = 0; i < items42.length; i++) {

				if (isNext && $(items42[i]).position().left > 0) {
					return $(items42[i]);
				} else if (!isNext && $(items42[i]).position().left >= 0) {
					return i == 0 ? $(items42[items42.length - 1]) : $(items42[i - 1]);
				}
			}

			return null;
		}

		function moveToItem42(event) {
			var isNext = event.data.direction == "next42";
			var item = isNext ? fetchItem(scrollContainer42, items42, true) : fetchItem(scrollContainer42, items42, false);

			if (item) {
				scrollContainer42.animate({
					"scrollLeft": item.position().left + scrollContainer42.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left42").click({
			direction: "prev42"
		}, moveToItem42);
		$(".arrow-right42").click({
			direction: "next42"
		}, moveToItem42);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items43 = $(".portfolio-item43");
		var scrollContainer43 = $(".offer-pg-cont43");


		function fetchItem(container, items43, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items43[0]);
			}

			for (i = 0; i < items43.length; i++) {

				if (isNext && $(items43[i]).position().left > 0) {
					return $(items43[i]);
				} else if (!isNext && $(items43[i]).position().left >= 0) {
					return i == 0 ? $(items43[items43.length - 1]) : $(items43[i - 1]);
				}
			}

			return null;
		}

		function moveToItem43(event) {
			var isNext = event.data.direction == "next43";
			var item = isNext ? fetchItem(scrollContainer43, items43, true) : fetchItem(scrollContainer43, items43, false);

			if (item) {
				scrollContainer43.animate({
					"scrollLeft": item.position().left + scrollContainer43.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left43").click({
			direction: "prev43"
		}, moveToItem43);
		$(".arrow-right43").click({
			direction: "next43"
		}, moveToItem43);


	});

});