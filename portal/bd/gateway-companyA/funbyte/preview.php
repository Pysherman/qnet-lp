<?php
session_start();
include('inc.php');
$strstatus=0;
if(isset($_GET['status']))
{
$strstatus=$_GET['status'];
}else{$strstatus=0;}

if($strstatus <> 0){

    header("Location: http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/nl/powerland/pm1/errorstatus.php");
    die();
}
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"]; 

	if($cat=="games")
	{
		$btnImg  = "games_active.png";
		$vdoImg  = "videos_default.png";
		$toneImg = "tones_default.png";
		$appImg  = "wallpaper_default.png";
	}
	if($cat=="videos")
	{
		$btnImg  = "games_default.png";
		$vdoImg  = "videos_active.png";
		$toneImg = "tones_default.png";
		$appImg  = "wallpaper_default.png";
	}
	if($cat=="tones")
	{
		$btnImg  = "games_default.png";
		$vdoImg  = "videos_default.png";
		$toneImg = "tones_active.png";
		$appImg  = "wallpaper_default.png";
	}
	if($cat=="wallpaper")
	{
		$btnImg  = "games_default.png";
		$vdoImg  = "videos_default.png";
		$toneImg = "tones_default.png";
		$appImg  = "wallpaper_active.png";
	}
}
else
{
	$cat 		 = 'games';
	$btnImg  = "games_active.png";
	$vdoImg  = "videos_default.png";
	$toneImg = "tones_default.png";
	$appImg  = "wallpaper_default.png";
}
$_SESSION['sesCatg'] = $cat;
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>1577 - GAMING ROOM</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/portal.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/styles.css" media="screen" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

        <style>
            * {
                box-sizing: border-box;
            }
            
            .screenshots {
                display: none;
            }
            
            .w3-hover-black:hover {
                color: #8dc63f!important;
                background-color: #e8e8e8!important
            }
            
            #footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                color: white;
                text-align: center;
                z-index: -1;
            }
            
            .column {
								float: left;
								padding-left: 0.1rem;
            }
            
            .column img,
            .item img {
                max-width: 100% !important;
                width: auto !important;
                height: auto !important;
            }
            /* Clearfix (clear floats) */
            
            .row::after {
                content: "";
                clear: both;
                display: table;
            }
            /* ============================== */
            
            .container {
                list-style: none;
                margin: 0;
                padding: 0;
            }
            
            .item {
                /*background: tomato;
  border:1px solid #CCC;*/
                padding: 5px;
                width: 200px;
                /*height: 150px;*/
                margin: 10px;
                font-size: 12px;
                font-weight: normal;
                /*line-height: 150px;*/
                color: #000;
                border-radius: 10px;
                /*text-align: center;*/
            }
            
            .flex {
                padding: 0;
                margin: 0;
                list-style: none;
                display: -webkit-box;
                display: -moz-box;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
                flex-wrap: wrap;
                -webkit-flex-flow: row wrap;
                justify-content: space-around;
            }
            
            #title {
                padding: 10px;
                text-align: center;
                color: #fff;
            }
            
            #title a {
                text-decoration: none;
                color: #0094ca;
            }
            
            #title a:hover {
                text-decoration: underline;
            }
            
            #block {
                width: 100%;
                height: 100%;
                position: absolute;
                visibility: hidden;
                display: none;
                /*background-color: rgba(22,22,22,0.5);*/
                top: 0;
                left: 0;
            }
            
            #block:target {
                visibility: visible;
                display: block;
            }
            
            .prevw img {
                max-width: 100% !important;
                width: 100% !important;
                height: auto !important;
            }
            
            @media (min-width: 320px) and (max-width: 479px) {
                .item {
                    /*border:1px solid #CCC;*/
                    padding: 5px;
                    width: 100px;
                    margin: 10px;
                    font-size: 12px;
                    font-weight: normal;
                    color: #000;
                    border-radius: 10px;
                }
            }
            
            video[poster] {
                object-fit: inherit;
						}
						.column a img{
							width:100%;
						}
						.row_menu{
							display: flex;
							justify-content: center;
							flex-direction: row;
							align-items: center;
							align-self: center;
						}
						.prevvv{
							
						}
        </style>
    </head>

    <body id="body">

        <div id="header">
            <div id="box1">
                <div style="text-align: center;"><img src="assets/funbyte_logo.png" id="ptslogo" border="0" /></div>
            </div>
        </div>

        <div id="iconimg">
            <div class="row">&nbsp;</div>
        </div>

        <div id="contentWrap" style="margin-top:auto;">
            <div class="adCntnr">
                <div class="acco2" style="background: #FFFFFF; color: #000">
                    <div style="padding:2px;"></div>
                    <div class="expand" style="color: #000">
                        <div style="float: right">
                            <a href="index.php"><img src="assets/clse_btn.png"></a>
                        </div>
                        Preview</div>
                    <div class="accCntnt" style="color:#000000">
                        <?php
			if(isset($_GET['varx']))
			{
				$prevSesID = $_GET['contid'];
				$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";

				if($_GET['varx']=="")
				{
					header("location:index.php");
				}
				else
				{
					$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name,cover_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
					$data = array();
					if($getContent)
					{
						while($items = mysqli_fetch_array($getContent))
						{
							$ContentID 		= $items['id'];
							$itemTitle 		= $items['title'];
							$description 	= $items['description'];
							$contentRate	= $items['rate'];
							$file_name 		= $items['file_name'];
							$cover_file_name 		= $items['cover_file_name'];
							$original_file_name = $items['original_file_name'];
							$mime 			= $items['mime'];
							$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
							$filename = $contenrdir.$file_name;
							$cover_filename = $contenrdir.$cover_file_name;

							$file = pathinfo($file_name, PATHINFO_FILENAME);
							$file_vdo_cover=pathinfo($cover_file_name, PATHINFO_FILENAME);

							if($ext=="mp4")
							{
								$thumbimg = $file_vdo_cover.'.png';
								$preview ='<video width="100%"  controls preload="metadata" poster="'.$contenrdir.'covers/'.$thumbimg.'">
										<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
									  </video>';
							}
							elseif($ext=="m4v")
							{
								$thumbimg = $file_vdo_cover.'.png';
								$preview ='<video width="100%"  controls preload="metadata" poster="'.$contenrdir.'covers/'.$thumbimg.'">
										<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
									  </video>';
							}
							elseif($ext=="mp3") {
								$preview='<img id="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
							}
							elseif($ext=="jpg") {
								$thumbimg=$file.'.jpg';
								$preview='<img id="img-thumbnail" src="' .$filename. '" alt="'.$itemTitle.'">';
							}
							else
							{
								$thumbimg = $file.'.png';
								$preview ='<img src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
							}
						}
					}
				}
			}
			else
			{
				header("location:index.php");
			}
		?>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color: #00000; font-size: 12px; font-weight: normal; font-family: Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman', 'serif'">
                                <tbody>
                                    <tr>
                                        <td width="3%" style="background: #b2b2b2; color:#8dc63f">
                                            <button class="w3-button w3-black" onclick="plusDivs(-1)" style="height:100%;"><img src="assets/arrow_L.png" width="25" height="51" alt="" /></button>
                                        </td>
                                        <td width="96%" class="prevw">
                                            <div>

                                                <?php
							if($ext=="apk" || $ext=="xapk" || $ext=="jpg" || $ext=="mp4"  || $ext=="m4v"   || $ext=="mp3")
							{
								$prevFile = $conn->query("SELECT file_name FROM cms.preview WHERE content_id='$ContentID'");
								//if($prevFile)
								if(mysqli_affected_rows($conn))
								{
									while($PrevItems = mysqli_fetch_array($prevFile))
									{
										$scrFiles	= $PrevItems['file_name'];
										$screenshot = $contenrdir.$scrFiles;

										//echo"<img src=\"".$screenshot."\" width=\"221\" height=\"191\" class=\"mySlides\">";
										echo"<img class=\"screenshots\" src=\"".$screenshot."\" style=\"width:100%\">";
									}
								}
								else {
									echo "<div class=\"__item-preview\">$preview</div>";
								}
							}
							?>
                                                    <!--<img class="screenshots" src="assets/sample2.png" style="width:100%">
							<img class="screenshots" src="assets/sample2-b.png" style="width:100%">
							<img class="screenshots" src="assets/sample2-c.png" style="width:100%">
							<img class="screenshots" src="assets/sample2-d.png" style="width:100%">	-->
                                            </div>
                                        </td>
                                        <td width="5%" style="background: #b2b2b2; color:#8dc63f">
                                            <button class="w3-button w3-black" onclick="plusDivs(1)" style="height:100%;"><img src="assets/arrow_R.png" width="25" height="51" alt="" /></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <p style="font-weight: bold; color: #ef4136">
                                                <?php echo $itemTitle; ?>
                                            </p>
                                            <?php echo stripslashes(strip_tags($description)); ?>
                                                <p>
                                                    <a href="<?php echo $filename; ?>"><img src="assets/btn.png" width="90" height="30" alt="" /></a>
                                                </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                    </div>
                    <div style="padding:2px;"></div>

                </div>
            </div>
        </div>

        <div style="margin-top:70px;"></div>

        <div id="block">&nbsp;</div>
        <div id="footer">
            <div id="iconimg">
                <div class="row  row_menu">
                    <div class="column"><a href="index.php?varx=games"><img src='assets/<?php echo $btnImg; ?>' border="0"></a></div>
                    <div class="column"><a href="index.php?varx=videos"><img src='assets/<?php echo $vdoImg; ?>' border="0"></a></div>
                    <div class="column"><a href="index.php?varx=tones"><img src='assets/<?php echo $toneImg; ?>' border="0"></a></div>
                    <div class="column"><a href="index.php?varx=wallpaper"><img src='assets/<?php echo $appImg; ?>' border="0"></a></div>
                </div>
            </div>
        </div>
        <script>
            var slideIndex = 1;
            showDivs(slideIndex);

            function plusDivs(n) {
                showDivs(slideIndex += n);
            }

            function showDivs(n) {
                var i;
                var x = document.getElementsByClassName("screenshots");
                if (n > x.length) {
                    slideIndex = 1
                }
                if (n < 1) {
                    slideIndex = x.length
                }
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                x[slideIndex - 1].style.display = "block";
            }
        </script>
    </body>

    </html>