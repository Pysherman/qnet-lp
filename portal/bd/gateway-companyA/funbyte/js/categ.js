$(window).load(function () {
	$(document).ready(function () {

		var items15 = $(".portfolio-item15");
		var scrollContainer15 = $(".offer-pg-cont15");


		function fetchItem(container, items15, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items15[0]);
			}

			for (i = 0; i < items15.length; i++) {

				if (isNext && $(items15[i]).position().left > 0) {
					return $(items15[i]);
				} else if (!isNext && $(items15[i]).position().left >= 0) {
					return i == 0 ? $(items15[items15.length - 1]) : $(items15[i - 1]);
				}
			}

			return null;
		}

		function moveToItem15(event) {
			var isNext = event.data.direction == "next15";
			var item = isNext ? fetchItem(scrollContainer15, items15, true) : fetchItem(scrollContainer15, items15, false);

			if (item) {
				scrollContainer15.animate({
					"scrollLeft": item.position().left + scrollContainer15.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left15").click({
			direction: "prev15"
		}, moveToItem15);
		$(".arrow-right15").click({
			direction: "next15"
		}, moveToItem15);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items16 = $(".portfolio-item16");
		var scrollContainer16 = $(".offer-pg-cont16");


		function fetchItem(container, items16, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items16[0]);
			}

			for (i = 0; i < items16.length; i++) {

				if (isNext && $(items16[i]).position().left > 0) {
					return $(items16[i]);
				} else if (!isNext && $(items16[i]).position().left >= 0) {
					return i == 0 ? $(items16[items16.length - 1]) : $(items16[i - 1]);
				}
			}

			return null;
		}

		function moveToItem16(event) {
			var isNext = event.data.direction == "next16";
			var item = isNext ? fetchItem(scrollContainer16, items16, true) : fetchItem(scrollContainer16, items16, false);

			if (item) {
				scrollContainer16.animate({
					"scrollLeft": item.position().left + scrollContainer16.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left16").click({
			direction: "prev16"
		}, moveToItem16);
		$(".arrow-right16").click({
			direction: "next16"
		}, moveToItem16);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items19 = $(".portfolio-item19");
		var scrollContainer19 = $(".offer-pg-cont19");


		function fetchItem(container, items19, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items19[0]);
			}

			for (i = 0; i < items19.length; i++) {

				if (isNext && $(items19[i]).position().left > 0) {
					return $(items19[i]);
				} else if (!isNext && $(items19[i]).position().left >= 0) {
					return i == 0 ? $(items19[items19.length - 1]) : $(items19[i - 1]);
				}
			}

			return null;
		}

		function moveToitem19(event) {
			var isNext = event.data.direction == "next19";
			var item = isNext ? fetchItem(scrollContainer19, items19, true) : fetchItem(scrollContainer19, items19, false);

			if (item) {
				scrollContainer19.animate({
					"scrollLeft": item.position().left + scrollContainer19.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left19").click({
			direction: "prev19"
		}, moveToitem19);
		$(".arrow-right19").click({
			direction: "next19"
		}, moveToitem19);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items20 = $(".portfolio-item20");
		var scrollContainer20 = $(".offer-pg-cont20");


		function fetchItem(container, items20, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items20[0]);
			}

			for (i = 0; i < items20.length; i++) {

				if (isNext && $(items20[i]).position().left > 0) {
					return $(items20[i]);
				} else if (!isNext && $(items20[i]).position().left >= 0) {
					return i == 0 ? $(items20[items20.length - 1]) : $(items20[i - 1]);
				}
			}

			return null;
		}

		function moveToitem20(event) {
			var isNext = event.data.direction == "next20";
			var item = isNext ? fetchItem(scrollContainer20, items20, true) : fetchItem(scrollContainer20, items20, false);

			if (item) {
				scrollContainer20.animate({
					"scrollLeft": item.position().left + scrollContainer20.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left20").click({
			direction: "prev20"
		}, moveToitem20);
		$(".arrow-right20").click({
			direction: "next20"
		}, moveToitem20);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items21 = $(".portfolio-item21");
		var scrollContainer21 = $(".offer-pg-cont21");


		function fetchItem(container, items21, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items21[0]);
			}

			for (i = 0; i < items21.length; i++) {

				if (isNext && $(items21[i]).position().left > 0) {
					return $(items21[i]);
				} else if (!isNext && $(items21[i]).position().left >= 0) {
					return i == 0 ? $(items21[items21.length - 1]) : $(items21[i - 1]);
				}
			}

			return null;
		}

		function moveToitem21(event) {
			var isNext = event.data.direction == "next21";
			var item = isNext ? fetchItem(scrollContainer21, items21, true) : fetchItem(scrollContainer21, items21, false);

			if (item) {
				scrollContainer21.animate({
					"scrollLeft": item.position().left + scrollContainer21.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left21").click({
			direction: "prev21"
		}, moveToitem21);
		$(".arrow-right21").click({
			direction: "next21"
		}, moveToitem21);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items22 = $(".portfolio-item22");
		var scrollContainer22 = $(".offer-pg-cont22");


		function fetchItem(container, items22, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items22[0]);
			}

			for (i = 0; i < items22.length; i++) {

				if (isNext && $(items22[i]).position().left > 0) {
					return $(items22[i]);
				} else if (!isNext && $(items22[i]).position().left >= 0) {
					return i == 0 ? $(items22[items22.length - 1]) : $(items22[i - 1]);
				}
			}

			return null;
		}

		function moveToitem22(event) {
			var isNext = event.data.direction == "next22";
			var item = isNext ? fetchItem(scrollContainer22, items22, true) : fetchItem(scrollContainer22, items22, false);

			if (item) {
				scrollContainer22.animate({
					"scrollLeft": item.position().left + scrollContainer22.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left22").click({
			direction: "prev22"
		}, moveToitem22);
		$(".arrow-right22").click({
			direction: "next22"
		}, moveToitem22);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items8 = $(".portfolio-item8");
		var scrollContainer8 = $(".offer-pg-cont8");


		function fetchItem(container, items8, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items8[0]);
			}

			for (i = 0; i < items8.length; i++) {

				if (isNext && $(items8[i]).position().left > 0) {
					return $(items8[i]);
				} else if (!isNext && $(items8[i]).position().left >= 0) {
					return i == 0 ? $(items8[items8.length - 1]) : $(items8[i - 1]);
				}
			}

			return null;
		}

		function moveToitem8(event) {
			var isNext = event.data.direction == "next8";
			var item = isNext ? fetchItem(scrollContainer8, items8, true) : fetchItem(scrollContainer8, items8, false);

			if (item) {
				scrollContainer8.animate({
					"scrollLeft": item.position().left + scrollContainer8.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left8").click({
			direction: "prev8"
		}, moveToitem8);
		$(".arrow-right8").click({
			direction: "next8"
		}, moveToitem8);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items23 = $(".portfolio-item23");
		var scrollContainer23 = $(".offer-pg-cont23");


		function fetchItem(container, items23, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items23[0]);
			}

			for (i = 0; i < items23.length; i++) {

				if (isNext && $(items23[i]).position().left > 0) {
					return $(items23[i]);
				} else if (!isNext && $(items23[i]).position().left >= 0) {
					return i == 0 ? $(items23[items23.length - 1]) : $(items23[i - 1]);
				}
			}

			return null;
		}

		function moveToitem23(event) {
			var isNext = event.data.direction == "next23";
			var item = isNext ? fetchItem(scrollContainer23, items23, true) : fetchItem(scrollContainer23, items23, false);

			if (item) {
				scrollContainer23.animate({
					"scrollLeft": item.position().left + scrollContainer23.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left23").click({
			direction: "prev23"
		}, moveToitem23);
		$(".arrow-right23").click({
			direction: "next23"
		}, moveToitem23);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items7 = $(".portfolio-item7");
		var scrollContainer7 = $(".offer-pg-cont7");


		function fetchItem(container, items7, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items7[0]);
			}

			for (i = 0; i < items7.length; i++) {

				if (isNext && $(items7[i]).position().left > 0) {
					return $(items7[i]);
				} else if (!isNext && $(items7[i]).position().left >= 0) {
					return i == 0 ? $(items7[items7.length - 1]) : $(items7[i - 1]);
				}
			}

			return null;
		}

		function moveToitem7(event) {
			var isNext = event.data.direction == "next7";
			var item = isNext ? fetchItem(scrollContainer7, items7, true) : fetchItem(scrollContainer7, items7, false);

			if (item) {
				scrollContainer7.animate({
					"scrollLeft": item.position().left + scrollContainer7.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left7").click({
			direction: "prev7"
		}, moveToitem7);
		$(".arrow-right7").click({
			direction: "next7"
		}, moveToitem7);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items3 = $(".portfolio-item3");
		var scrollContainer3 = $(".offer-pg-cont3");


		function fetchItem(container, items3, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items3[0]);
			}

			for (i = 0; i < items3.length; i++) {

				if (isNext && $(items3[i]).position().left > 0) {
					return $(items3[i]);
				} else if (!isNext && $(items3[i]).position().left >= 0) {
					return i == 0 ? $(items3[items3.length - 1]) : $(items3[i - 1]);
				}
			}

			return null;
		}

		function moveToitem3(event) {
			var isNext = event.data.direction == "next3";
			var item = isNext ? fetchItem(scrollContainer3, items3, true) : fetchItem(scrollContainer3, items3, false);

			if (item) {
				scrollContainer3.animate({
					"scrollLeft": item.position().left + scrollContainer3.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left3").click({
			direction: "prev3"
		}, moveToitem3);
		$(".arrow-right3").click({
			direction: "next3"
		}, moveToitem3);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items12 = $(".portfolio-item12");
		var scrollContainer12 = $(".offer-pg-cont12");


		function fetchItem(container, items12, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items12[0]);
			}

			for (i = 0; i < items12.length; i++) {

				if (isNext && $(items12[i]).position().left > 0) {
					return $(items12[i]);
				} else if (!isNext && $(items12[i]).position().left >= 0) {
					return i == 0 ? $(items12[items12.length - 1]) : $(items12[i - 1]);
				}
			}

			return null;
		}

		function moveToitem12(event) {
			var isNext = event.data.direction == "next12";
			var item = isNext ? fetchItem(scrollContainer12, items12, true) : fetchItem(scrollContainer12, items12, false);

			if (item) {
				scrollContainer12.animate({
					"scrollLeft": item.position().left + scrollContainer12.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left12").click({
			direction: "prev12"
		}, moveToitem12);
		$(".arrow-right12").click({
			direction: "next12"
		}, moveToitem12);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items24 = $(".portfolio-item24");
		var scrollContainer24 = $(".offer-pg-cont24");


		function fetchItem(container, items24, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items24[0]);
			}

			for (i = 0; i < items24.length; i++) {

				if (isNext && $(items24[i]).position().left > 0) {
					return $(items24[i]);
				} else if (!isNext && $(items24[i]).position().left >= 0) {
					return i == 0 ? $(items24[items24.length - 1]) : $(items24[i - 1]);
				}
			}

			return null;
		}

		function moveToitem24(event) {
			var isNext = event.data.direction == "next24";
			var item = isNext ? fetchItem(scrollContainer24, items24, true) : fetchItem(scrollContainer24, items24, false);

			if (item) {
				scrollContainer24.animate({
					"scrollLeft": item.position().left + scrollContainer24.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left24").click({
			direction: "prev24"
		}, moveToitem24);
		$(".arrow-right24").click({
			direction: "next24"
		}, moveToitem24);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items17 = $(".portfolio-item17");
		var scrollContainer17 = $(".offer-pg-cont17");


		function fetchItem(container, items17, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items17[0]);
			}

			for (i = 0; i < items17.length; i++) {

				if (isNext && $(items17[i]).position().left > 0) {
					return $(items17[i]);
				} else if (!isNext && $(items17[i]).position().left >= 0) {
					return i == 0 ? $(items17[items17.length - 1]) : $(items17[i - 1]);
				}
			}

			return null;
		}

		function moveToitem17(event) {
			var isNext = event.data.direction == "next17";
			var item = isNext ? fetchItem(scrollContainer17, items17, true) : fetchItem(scrollContainer17, items17, false);

			if (item) {
				scrollContainer17.animate({
					"scrollLeft": item.position().left + scrollContainer17.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left17").click({
			direction: "prev17"
		}, moveToitem17);
		$(".arrow-right17").click({
			direction: "next17"
		}, moveToitem17);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items18 = $(".portfolio-item18");
		var scrollContainer18 = $(".offer-pg-cont18");


		function fetchItem(container, items18, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items18[0]);
			}

			for (i = 0; i < items18.length; i++) {

				if (isNext && $(items18[i]).position().left > 0) {
					return $(items18[i]);
				} else if (!isNext && $(items18[i]).position().left >= 0) {
					return i == 0 ? $(items18[items18.length - 1]) : $(items18[i - 1]);
				}
			}

			return null;
		}

		function moveToitem18(event) {
			var isNext = event.data.direction == "next18";
			var item = isNext ? fetchItem(scrollContainer18, items18, true) : fetchItem(scrollContainer18, items18, false);

			if (item) {
				scrollContainer18.animate({
					"scrollLeft": item.position().left + scrollContainer18.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left18").click({
			direction: "prev18"
		}, moveToitem18);
		$(".arrow-right18").click({
			direction: "next18"
		}, moveToitem18);


	});

});



// ID 70

$(window).load(function () {
	$(document).ready(function () {

		var items70 = $(".portfolio-item70");
		var scrollContainer70 = $(".offer-pg-cont70");


		function fetchItem(container, items70, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items70[0]);
			}

			for (i = 0; i < items70.length; i++) {

				if (isNext && $(items70[i]).position().left > 0) {
					return $(items70[i]);
				} else if (!isNext && $(items70[i]).position().left >= 0) {
					return i == 0 ? $(items70[items70.length - 1]) : $(items70[i - 1]);
				}
			}

			return null;
		}

		function moveToitem70(event) {
			var isNext = event.data.direction == "next70";
			var item = isNext ? fetchItem(scrollContainer70, items70, true) : fetchItem(scrollContainer70, items70, false);

			if (item) {
				scrollContainer70.animate({
					"scrollLeft": item.position().left + scrollContainer70.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left70").click({
			direction: "prev70"
		}, moveToitem70);
		$(".arrow-right70").click({
			direction: "next70"
		}, moveToitem70);


	});

});


// ID 71

$(window).load(function () {
	$(document).ready(function () {

		var items71 = $(".portfolio-item71");
		var scrollContainer71 = $(".offer-pg-cont71");


		function fetchItem(container, items71, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items71[0]);
			}

			for (i = 0; i < items71.length; i++) {

				if (isNext && $(items71[i]).position().left > 0) {
					return $(items71[i]);
				} else if (!isNext && $(items71[i]).position().left >= 0) {
					return i == 0 ? $(items71[items71.length - 1]) : $(items71[i - 1]);
				}
			}

			return null;
		}

		function moveToitem71(event) {
			var isNext = event.data.direction == "next71";
			var item = isNext ? fetchItem(scrollContainer71, items71, true) : fetchItem(scrollContainer71, items71, false);

			if (item) {
				scrollContainer71.animate({
					"scrollLeft": item.position().left + scrollContainer71.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left71").click({
			direction: "prev71"
		}, moveToitem71);
		$(".arrow-right71").click({
			direction: "next71"
		}, moveToitem71);


	});

});


// ID 72

$(window).load(function () {
	$(document).ready(function () {

		var items72 = $(".portfolio-item72");
		var scrollContainer72 = $(".offer-pg-cont72");


		function fetchItem(container, items72, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items72[0]);
			}

			for (i = 0; i < items72.length; i++) {

				if (isNext && $(items72[i]).position().left > 0) {
					return $(items72[i]);
				} else if (!isNext && $(items72[i]).position().left >= 0) {
					return i == 0 ? $(items72[items72.length - 1]) : $(items72[i - 1]);
				}
			}

			return null;
		}

		function moveToitem72(event) {
			var isNext = event.data.direction == "next72";
			var item = isNext ? fetchItem(scrollContainer72, items72, true) : fetchItem(scrollContainer72, items72, false);

			if (item) {
				scrollContainer72.animate({
					"scrollLeft": item.position().left + scrollContainer72.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left72").click({
			direction: "prev72"
		}, moveToitem72);
		$(".arrow-right72").click({
			direction: "next72"
		}, moveToitem72);


	});

});