<?php session_start();
include('inc.php');
$strstatus=0;
if(isset($_GET['status'])) {
	$strstatus=$_GET['status'];
}

else {
	$strstatus=0;
}

if($strstatus <> 0) {
	header("Location: http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/nl/powerland/pm1/errorstatus.php");
	die();
}

if(isset($_GET["varx"])) {
	$cat=$_GET["varx"];
	if($cat=="games") {
		$btnImg="games_active.png";
		$vdoImg="videos_default.png";
		$toneImg="tones_default.png";
		$appImg="wallpaper_default.png";
	}
	if($cat=="videos") {
		$btnImg="games_default.png";
		$vdoImg="videos_active.png";
		$toneImg="tones_default.png";
		$appImg="wallpaper_default.png";
	}
	if($cat=="tones") {
		$btnImg="games_default.png";
		$vdoImg="videos_default.png";
		$toneImg="tones_active.png";
		$appImg="wallpaper_default.png";
	}
	if($cat=="wallpaper") {
		$btnImg="games_default.png";
		$vdoImg="videos_default.png";
		$toneImg="tones_default.png";
		$appImg="wallpaper_active.png";
	}
}

else {
	$cat='games';
	$btnImg="games_active.png";
	$vdoImg="videos_default.png";
	$toneImg="tones_default.png";
	$appImg="wallpaper_default.png";
}

$_SESSION['sesCatg']=$cat;
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>1577 - Funbyte</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/portal.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/styles.css" media="screen" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
        <style>
            * {
                box-sizing: border-box;
            }
            a{
							text-decoration:none;
							color:#000;
                            display:block;
                            outline:none;
						}
            #footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                color: white;
                text-align: center;
            }
            
            .column {
								float: left;
								padding-left: 0.1rem;
                /*width: 50%;
    padding: 5px;*/
            }
            
            .column img,
            .item img {
                max-width: 100% !important;
                width: auto !important;
                height: auto !important;
            }
            /* Clearfix (clear floats) */
            
            .row::after {
                content: "";
                clear: both;
                display: table;
            }
            /* ============================== */
            
            .container {
                list-style: none;
                margin: 0;
                padding: 0;
            }
            
            .item {
                /*background: tomato;
  border:1px solid #CCC;*/
                overflow: hidden;
                padding: 5px;
                width: 200px;
                /*height: 30px;*/
                margin: 5px;
                font-size: 12px;
                font-weight: normal;
                /*line-height: 150px;*/
                color: #000;
                border-radius: 10px;
                /*text-align: center;*/
            }
            
            .flex {
                padding: 0;
                margin: 0;
                list-style: none;
                display: -webkit-box;
                display: -moz-box;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
                flex-wrap: wrap;
                -webkit-flex-flow: row wrap;
                justify-content: space-around;
            }
            
            #title {
                padding: 10px;
                text-align: center;
                color: #fff;
            }
            
            #title a {
                text-decoration: none;
                color: #0094ca;
            }
            
            #title a:hover {
                text-decoration: underline;
            }
            
            #block {
                width: 100%;
                height: 100%;
                position: absolute;
                visibility: hidden;
                display: none;
                /*background-color: rgba(22,22,22,0.5);*/
                top: 0;
                left: 0;
            }
            
            #block:target {
                visibility: visible;
                display: block;
            }
            
            @media (min-width: 320px) and (max-width: 479px) {
                .item {
                    /*border:1px solid #CCC;*/
                    padding: 5px;
                    width: 100px;
                    font-size: 12px;
                    font-weight: normal;
                    color: #000;
                    border-radius: 10px;
                }
            }
            
            .offer-pg-cont15,
            .offer-pg-cont16,
            .offer-pg-cont19,
            .offer-pg-cont20,
            .offer-pg-cont21,
            .offer-pg-cont22,
            .offer-pg-cont8,
            .offer-pg-cont23,
            .offer-pg-cont7,
            .offer-pg-cont3,
            .offer-pg-cont12,
            .offer-pg-cont24,
            .offer-pg-cont17,
            .offer-pg-cont18,
            .offer-pg-cont70,
            .offer-pg-cont71,
            .offer-pg-cont72 {
                width: 100%;
                overflow-x: scroll;
                scrollbar-width: none; /* Firefox */
                -ms-overflow-style: none;  /* IE 10+ */
                margin: 0px;
            }
            .offer-pg-cont15::-webkit-scrollbar,
            .offer-pg-cont16::-webkit-scrollbar,
            .offer-pg-cont19::-webkit-scrollbar,
            .offer-pg-cont20::-webkit-scrollbar,
            .offer-pg-cont21::-webkit-scrollbar,
            .offer-pg-cont22::-webkit-scrollbar,
            .offer-pg-cont8::-webkit-scrollbar,
            .offer-pg-cont23::-webkit-scrollbar,
            .offer-pg-cont7::-webkit-scrollbar,
            .offer-pg-cont3::-webkit-scrollbar,
            .offer-pg-cont12::-webkit-scrollbar,
            .offer-pg-cont24::-webkit-scrollbar,
            .offer-pg-cont17::-webkit-scrollbar,
            .offer-pg-cont18::-webkit-scrollbar,
            .offer-pg-cont70::-webkit-scrollbar,
            .offer-pg-cont71::-webkit-scrollbar,
            .offer-pg-cont72::-webkit-scrollbar { /* WebKit */
                width: 0;
                height:0;
            }
            
            span.arrow-left15,
            span.arrow-right15,
            span.arrow-left16,
            span.arrow-right16,
            span.arrow-left19,
            span.arrow-right19,
            span.arrow-left20,
            span.arrow-right20,
            span.arrow-left21,
            span.arrow-right21,
            span.arrow-left22,
            span.arrow-right22,
            span.arrow-left8,
            span.arrow-right8,
            span.arrow-right23,
            span.arrow-right7,
            span.arrow-right3,
            span.arrow-right12,
            span.arrow-right24,
            span.arrow-right17,
            span.arrow-left70,
            span.arrow-right70,
            span.arrow-left71,
            span.arrow-right71,
            span.arrow-left72,
            span.arrow-right72,
            span.arrow-right18 {
                z-index: 2;
                cursor: pointer;
            }
            
            .offer-pg {
                width: max-content;
            }
            
            .offer-con .left-item h4 {
                color: #fff;
                font-weight: normal;
                margin: 0px;
            }
            
            .offer-con .right-item {
                float: right;
                padding: 10px;
            }
            
            .offer-con .right-item h5 {
                color: #cb9944;
                margin: 0px;
                font-size: 14px;
            }
            
            .offer-pg>.portfolio-item15,.portfolio-item12,.portfolio-item16,.portfolio-item17,.portfolio-item18,.portfolio-item19,.portfolio-item20,.portfolio-item21,.portfolio-item22,.portfolio-item23,.portfolio-item24,.portfolio-item3,.portfolio-item7,.portfolio-item70,.portfolio-item71,.portfolio-item72,.portfolio-item8
            {
                width: 150px;
                float: left;
                border: 1px solid #eee;
                display:block;
                position:relative;
            }
            
            video[poster] {
                object-fit: inherit;
            }
						.column a img{
							width:100%;
						}
						.row_menu{
							display: flex;
							justify-content: center;
							flex-direction: row;
							align-items: center;
							align-self: center;
						}
                        video {
                            position: relative;
                        }
                        .separator{
                            clear:both;
                        }


@media screen and (max-width:240px){
    .offer-pg>.portfolio-item15,.portfolio-item12,.portfolio-item16,.portfolio-item17,.portfolio-item18,.portfolio-item19,.portfolio-item20,.portfolio-item21,.portfolio-item22,.portfolio-item23,.portfolio-item24,.portfolio-item3,.portfolio-item7,.portfolio-item70,.portfolio-item71,.portfolio-item72,.portfolio-item8
    {width:148px}
}
@media screen and (min-width:241px) and (max-width:343px){
    .offer-pg>.portfolio-item15,.portfolio-item12,.portfolio-item16,.portfolio-item17,.portfolio-item18,.portfolio-item19,.portfolio-item20,.portfolio-item21,.portfolio-item22,.portfolio-item23,.portfolio-item24,.portfolio-item3,.portfolio-item7,.portfolio-item70,.portfolio-item71,.portfolio-item72,.portfolio-item8
    {width:104px}
}
@media screen and (min-width:344px) and (max-width:380px){
    .offer-pg>.portfolio-item15,.portfolio-item12,.portfolio-item16,.portfolio-item17,.portfolio-item18,.portfolio-item19,.portfolio-item20,.portfolio-item21,.portfolio-item22,.portfolio-item23,.portfolio-item24,.portfolio-item3,.portfolio-item7,.portfolio-item70,.portfolio-item71,.portfolio-item72,.portfolio-item8
    {width:110px}
}
@media screen and (min-width:381px) and (max-width:400px){
    .offer-pg>.portfolio-item15,.portfolio-item12,.portfolio-item16,.portfolio-item17,.portfolio-item18,.portfolio-item19,.portfolio-item20,.portfolio-item21,.portfolio-item22,.portfolio-item23,.portfolio-item24,.portfolio-item3,.portfolio-item7,.portfolio-item70,.portfolio-item71,.portfolio-item72,.portfolio-item8
    {width:115px}
}
@media screen and (min-width:401px) and (max-width:420px){
    .offer-pg>.portfolio-item15,.portfolio-item12,.portfolio-item16,.portfolio-item17,.portfolio-item18,.portfolio-item19,.portfolio-item20,.portfolio-item21,.portfolio-item22,.portfolio-item23,.portfolio-item24,.portfolio-item3,.portfolio-item7,.portfolio-item70,.portfolio-item71,.portfolio-item72,.portfolio-item8
    {width:124px}
}
@media screen and (min-width:421px) and (max-width:460px){
    .offer-pg>.portfolio-item15,.portfolio-item12,.portfolio-item16,.portfolio-item17,.portfolio-item18,.portfolio-item19,.portfolio-item20,.portfolio-item21,.portfolio-item22,.portfolio-item23,.portfolio-item24,.portfolio-item3,.portfolio-item7,.portfolio-item70,.portfolio-item71,.portfolio-item72,.portfolio-item8
    {width:135px}
}
        </style>
        <script src="js/categ.js" type="text/javascript"></script>
    </head>

    <body id="body">
        <div id="header">
            <div id="box1">
                <div style="text-align: center;"><img src="assets/funbyte_logo.png" id="ptslogo" border="0" /></div>
            </div>
        </div>
        <div id="iconimg">
            <div class="row">&nbsp;
            </div>
        </div>
        <div id="contentWrap" style="margin-top:auto;">
            <div class="adCntnr">

                <?php $queryJoin=$conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id and b.sub_category!='Utility_Apps' and b.sub_category!='Application' and b.sub_category!='Fitness' and b.sub_category!='Games' and b.sub_category!='Games-Portal' and b.sub_category!='LifeStyle' and b.sub_category!='Relaxation'and b.sub_category!='Blonde'
 and b.sub_category!='Asian' and b.sub_category!='Balance Diet' and b.sub_category!='car racing' and b.sub_category!='Cartoons' and b.sub_category!='football' and b.sub_category!='Cosplay' and b.sub_category!='European' and b.sub_category!='Facts and Tips' and b.sub_category!='Funny' and b.sub_category!='Funny-HD' and b.sub_category!='Horror' and b.sub_category!='VDO1' and b.sub_category!='Tones' and b.sub_category!='Tones-Portal' and b.sub_category!='Nature' and b.sub_category!='Origami' and b.sub_category!='PornStar' and category like '$cat%'");
 if($queryJoin) {
	while ($DateRow=mysqli_fetch_assoc($queryJoin)) {
		$resCatgID=$DateRow['id'];
		$resName=$DateRow['category'];
		$resSbCatgID=$DateRow['sc_id'];
		$resSName=$DateRow['sub_category'];
		$_SESSION['categref']="category_id='$resCatgID' and sub_category_id='$resSbCatgID'";
		if($resSName=="Application") {
			$resSName="Antivirus";
		}
		if($resSName=="BD_funny_videos") {
			$resSName="Bangladesh Funny Videos";
		}
		if($resSName=="BD_tones") {
			$resSName="Bangladesh Premium Tones";
		}
		if($resSName=="BD_wallpaper") {
			$resSName="Bangladesh Wallpapers";
		}
		echo'<div class="acco2" style="background: #FFFFFF; color: #000">
 <div style="padding:2px;"></div> <div class="expand" style="color: #000"> <div style="float: right"> <span class="arrow-left'.$resSbCatgID.'"><img src="assets/slider_prv.png"></span> <span class="arrow-right'.$resSbCatgID.'"><img src="assets/slider_nxt.png"></span> </div> '.ucfirst($resSName) .'</div>';
 if(isset($_GET["varx"])) {
			$cat=$_GET["varx"]='games';
		}
		else {
			$cat='games';
		}
		echo'<div class="accCntnt" style="color:#000000">
 <div class="row offer-pg-cont'.$resSbCatgID.'"> <div class="offer-pg">';	

 if(!empty($_SESSION['categref'])) {
			$contenrdir="https://s3-ap-southeast-1.amazonaws.com/qcnt/";
			$categref=$_SESSION['categref'];
			$sesCatg=$_SESSION['sesCatg'];
            $getContent=$conn->query("SELECT id, title, description, file_name, original_file_name,cover_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and $categref order by id desc LIMIT 30");

            $data=array();
			if(mysqli_num_rows($getContent)) {
                $x=0;
				while($items=mysqli_fetch_array($getContent)) {
                    $data[] = $items;                   
					$contentID=$items['id'];
					$itemTitle=$items['title'];
					$description=$items['description'];
					$file_name=$items['file_name'];
					$cover_file_name=$items['cover_file_name'];
					$original_file_name=$items['original_file_name'];
					$mime=$items['mime'];
					$ext=pathinfo($file_name, PATHINFO_EXTENSION);
					$filename=$contenrdir.$file_name;
					$cover_file_name=$contenrdir.$cover_file_name;
					$subcat=$items['sub_category_id'];
					$file=pathinfo($file_name, PATHINFO_FILENAME);
					$file_vdo_cover=pathinfo($cover_file_name, PATHINFO_FILENAME);

					if($ext=="mp4") {
						$thumbimg=$file_vdo_cover.'.png';
						$preview='<video style="z-index:-2;" width="100%" height="100"  controls preload="metadata" poster="'.$contenrdir.'covers/'.$thumbimg.'">
							<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
							</video>';
					}
					elseif($ext=="m4v") {
						$thumbimg=$file_vdo_cover.'.png';
						$preview='<video style="z-index:-2;" width="100%" height="100"  controls preload="metadata" poster="'.$contenrdir.'covers/'.$thumbimg.'">
							<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
							</video>';
					}
					elseif($ext=="mp3") {
						$preview='<img id="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
					}
					elseif($ext=="jpg") {
						$thumbimg=$file.'.jpg';
						$preview='<img id="img-thumbnail_wallpaper" src="' .$filename. '" alt="'.$itemTitle.'">';
					}
					elseif($ext=="apk" || $ext=="xapk" ) {
						$thumbimg=$file.'.png';
						$preview='<img id="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
					}
					else {
						$thumbimg = $file.'.png';
						$preview ='<img class="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
                    }
                    if ($x<=14){
                    echo'<div style="z-index:0;" class="col-md-3 portfolio-item'.$resSbCatgID.' item flex-item">
                    <a style="z-index:100;" href="preview.php?varx='.$sesCatg.'&contid='.$contentID.'&subcat='.$subcat.'">
                    '.$preview.'<p>'.substr($description, 0, 35).'..</p></a></div>';
                }else{
                    if($x==15){
                        echo "<div class=\"separator\"></div>";
                    }
                    echo'<div style="z-index:0;" class="col-md-3 portfolio-item'.$resSbCatgID.' item flex-item">
                    <a style="z-index:100;" href="preview.php?varx='.$sesCatg.'&contid='.$contentID.'&subcat='.$subcat.'">
                    '.$preview.'<p>'.substr($description, 0, 35).'..</p></a></div>';
                }
                $x+=1;
            }
        }
        }
		echo'</div>
 </div> </div> <div style="padding:2px;"></div> </div><br>';

	}
}

?> </div>
        </div>
        <div style="margin-top:70px;"></div>
        <div id="block">&nbsp;
        </div>
        <div id="footer">
            <div id="iconimg">
                <div class="row row_menu">
                    <div class="column"><a href="?varx=games"><img src='assets/<?php echo $btnImg; ?>' border="0"></a></div>
                    <div class="column"><a href="?varx=videos"><img src='assets/<?php echo $vdoImg; ?>' border="0"></a></div>
                    <div class="column"><a href="?varx=tones"><img src='assets/<?php echo $toneImg; ?>' border="0"></a></div>
                    <div class="column"><a href="?varx=wallpaper"><img src='assets/<?php echo $appImg; ?>' border="0"></a></div>
                </div>
            </div>
        </div>
        <script>
            if (window.parent && window.parent.parent) {
                window.parent.parent.postMessage(["results_1Frame", {
                    height: document.body.getBoundingClientRect().height,
                    slug: "c6kf2"
                }], "*")
            }

            window.name = "result"
        </script>
    </body>

    </html>