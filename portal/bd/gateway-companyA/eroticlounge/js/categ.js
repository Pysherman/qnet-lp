$(window).load(function () {
	$(document).ready(function () {

		var items30 = $(".portfolio-item30");
		var scrollContainer30 = $(".offer-pg-cont30");


		function fetchItem(container, items30, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items30[0]);
			}

			for (i = 0; i < items30.length; i++) {

				if (isNext && $(items30[i]).position().left > 0) {
					return $(items30[i]);
				} else if (!isNext && $(items30[i]).position().left >= 0) {
					return i == 0 ? $(items30[items30.length - 1]) : $(items30[i - 1]);
				}
			}

			return null;
		}

		function moveToItem30(event) {
			var isNext = event.data.direction == "next30";
			var item = isNext ? fetchItem(scrollContainer30, items30, true) : fetchItem(scrollContainer30, items30, false);

			if (item) {
				scrollContainer30.animate({
					"scrollLeft": item.position().left + scrollContainer30.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left30").click({
			direction: "prev30"
		}, moveToItem30);
		$(".arrow-right30").click({
			direction: "next30"
		}, moveToItem30);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items32 = $(".portfolio-item32");
		var scrollContainer32 = $(".offer-pg-cont32");


		function fetchItem(container, items32, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items32[0]);
			}

			for (i = 0; i < items32.length; i++) {

				if (isNext && $(items32[i]).position().left > 0) {
					return $(items32[i]);
				} else if (!isNext && $(items32[i]).position().left >= 0) {
					return i == 0 ? $(items32[items32.length - 1]) : $(items32[i - 1]);
				}
			}

			return null;
		}

		function moveToItem32(event) {
			var isNext = event.data.direction == "next32";
			var item = isNext ? fetchItem(scrollContainer32, items32, true) : fetchItem(scrollContainer32, items32, false);

			if (item) {
				scrollContainer32.animate({
					"scrollLeft": item.position().left + scrollContainer32.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left32").click({
			direction: "prev32"
		}, moveToItem32);
		$(".arrow-right32").click({
			direction: "next32"
		}, moveToItem32);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items34 = $(".portfolio-item34");
		var scrollContainer34 = $(".offer-pg-cont34");


		function fetchItem(container, items34, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items34[0]);
			}

			for (i = 0; i < items34.length; i++) {

				if (isNext && $(items34[i]).position().left > 0) {
					return $(items34[i]);
				} else if (!isNext && $(items34[i]).position().left >= 0) {
					return i == 0 ? $(items34[items34.length - 1]) : $(items34[i - 1]);
				}
			}

			return null;
		}

		function moveToItem34(event) {
			var isNext = event.data.direction == "next34";
			var item = isNext ? fetchItem(scrollContainer34, items34, true) : fetchItem(scrollContainer34, items34, false);

			if (item) {
				scrollContainer34.animate({
					"scrollLeft": item.position().left + scrollContainer34.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left34").click({
			direction: "prev34"
		}, moveToItem34);
		$(".arrow-right34").click({
			direction: "next34"
		}, moveToItem34);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items35 = $(".portfolio-item35");
		var scrollContainer35 = $(".offer-pg-cont35");


		function fetchItem(container, items35, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items35[0]);
			}

			for (i = 0; i < items35.length; i++) {

				if (isNext && $(items35[i]).position().left > 0) {
					return $(items35[i]);
				} else if (!isNext && $(items35[i]).position().left >= 0) {
					return i == 0 ? $(items35[items35.length - 1]) : $(items35[i - 1]);
				}
			}

			return null;
		}

		function moveToItem35(event) {
			var isNext = event.data.direction == "next35";
			var item = isNext ? fetchItem(scrollContainer35, items35, true) : fetchItem(scrollContainer35, items35, false);

			if (item) {
				scrollContainer35.animate({
					"scrollLeft": item.position().left + scrollContainer35.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left35").click({
			direction: "prev35"
		}, moveToItem35);
		$(".arrow-right35").click({
			direction: "next35"
		}, moveToItem35);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items36 = $(".portfolio-item36");
		var scrollContainer36 = $(".offer-pg-cont36");


		function fetchItem(container, items36, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items36[0]);
			}

			for (i = 0; i < items36.length; i++) {

				if (isNext && $(items36[i]).position().left > 0) {
					return $(items36[i]);
				} else if (!isNext && $(items36[i]).position().left >= 0) {
					return i == 0 ? $(items36[items36.length - 1]) : $(items36[i - 1]);
				}
			}

			return null;
		}

		function moveToItem36(event) {
			var isNext = event.data.direction == "next36";
			var item = isNext ? fetchItem(scrollContainer36, items36, true) : fetchItem(scrollContainer36, items36, false);

			if (item) {
				scrollContainer36.animate({
					"scrollLeft": item.position().left + scrollContainer36.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left36").click({
			direction: "prev36"
		}, moveToItem36);
		$(".arrow-right36").click({
			direction: "next36"
		}, moveToItem36);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items37 = $(".portfolio-item37");
		var scrollContainer37 = $(".offer-pg-cont37");


		function fetchItem(container, items37, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items37[0]);
			}

			for (i = 0; i < items37.length; i++) {

				if (isNext && $(items37[i]).position().left > 0) {
					return $(items37[i]);
				} else if (!isNext && $(items37[i]).position().left >= 0) {
					return i == 0 ? $(items37[items37.length - 1]) : $(items37[i - 1]);
				}
			}

			return null;
		}

		function moveToItem37(event) {
			var isNext = event.data.direction == "next37";
			var item = isNext ? fetchItem(scrollContainer37, items37, true) : fetchItem(scrollContainer37, items37, false);

			if (item) {
				scrollContainer37.animate({
					"scrollLeft": item.position().left + scrollContainer37.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left37").click({
			direction: "prev37"
		}, moveToItem37);
		$(".arrow-right37").click({
			direction: "next37"
		}, moveToItem37);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items38 = $(".portfolio-item38");
		var scrollContainer38 = $(".offer-pg-cont38");


		function fetchItem(container, items38, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items38[0]);
			}

			for (i = 0; i < items38.length; i++) {

				if (isNext && $(items38[i]).position().left > 0) {
					return $(items38[i]);
				} else if (!isNext && $(items38[i]).position().left >= 0) {
					return i == 0 ? $(items38[items38.length - 1]) : $(items38[i - 1]);
				}
			}

			return null;
		}

		function moveToItem38(event) {
			var isNext = event.data.direction == "next38";
			var item = isNext ? fetchItem(scrollContainer38, items38, true) : fetchItem(scrollContainer38, items38, false);

			if (item) {
				scrollContainer38.animate({
					"scrollLeft": item.position().left + scrollContainer38.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left38").click({
			direction: "prev38"
		}, moveToItem38);
		$(".arrow-right38").click({
			direction: "next38"
		}, moveToItem38);


	});

});