<?php include "view.php";  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Preview</title>
      <meta name="viewport" content="width=device-width,initial-scale=1.0">
      <link rel="stylesheet" type="text/css" href="css/portal.css">
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
      <!-- UIkit CSS -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.6.16/dist/css/uikit.min.css" />
      <!-- UIkit JS -->
      <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.16/dist/js/uikit.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.16/dist/js/uikit-icons.min.js"></script>
      <script src="./js/jquery-1.12.4.min.js"></script>
   </head>
   <body>
      <div class="tm-navbar-container uk-background-primary">
         <div class="uk-container uk-container-expand">
            <nav class="uk-background-primary uk-light" uk-navbar>
               <div class="uk-navbar-left">
                  <ul class="uk-navbar-nav">
                     <li class="uk-active"><a class="navtitle" href="#">Qnet Play</a></li>
                  </ul>
               </div>
               <div class="uk-navbar-right">
                  <ul class="uk-navbar-nav">
                     <li><a href="./?varx=games-apk">APK GAMES</a></li>
                     <li><a href="./?varx=html5">HTML5 GAMES</a></li>
                  </ul>
               </div>
            </nav>
         </div>
      </div>
      <div class="midheader uk-height-small uk-background-cover uk-flex uk-flex-center uk-flex-middle uk-light">
         <h1 class="uk-margin-remove-bottom"><?php echo $heads?></h1>
      </div>
      <div class="uk-section uk-section-default uk-padding-small uk-margin-top">
         <div  class="uk-container uk-container-expand">
            <hr class="uk-divider-icon">
            <div  class="uk-child-width-1-2@l uk-child-width-1-1@m uk-child-width-1-1@s uk-grid-small uk-margin-top uk-grid-match" uk-grid>
               <div class="uk-align-center">
                  <?php if(isset($_GET['varx'])) {
                     $prevSesID=$_GET['contid'];
                     $subcat=$_GET['category'];
                     $contenrdir="https://qcnt-portal.s3-ap-southeast-1.amazonaws.com/portal/";
                     if($_GET['varx']=="") {
                     	header("location:index.php");
                     }
                     else {
                     	$getContent=$conn->query("SELECT * FROM cms.portal_content WHERE id=$prevSesID");
                     	$data=array();
                     	if($getContent) {
                     		while($items=mysqli_fetch_array($getContent)) {
                     			$ContentID=$items['id'];
                     			$itemTitle=$items['title'];
                     			$sub_category_id=$items['sub_category_id'];
                     			$description=$items['description'];
                     			$file_name=$items['content_file_name'];
                     			$icon_file_name=$items['icon_file_name'];
                     			$mime=$items['content_file_mime'];
                     			$ext=pathinfo($file_name, PATHINFO_EXTENSION);
                     			$filename=$contenrdir.$file_name;
                     			$file=pathinfo($file_name, PATHINFO_FILENAME);
                     			if($ext=="mp4" || $ext=="mp3") {
                     				$thumbimg=$file.'.png';
                     				$preview='<video width="100%" height="200"  controls preload="metadata">
                     <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E,
                     				mp4a.40.2">
                     </video>';
                     
                     			}
                     			if($cat=="html5" || $ext=="mp3") {
                     				$thumbimg=$file.'.png';
                     				$html5games='<iframe gesture="media" allow="encrypted-media" allowfullscreen src="'.$file_name.'" title="'.$itemTitle.'">
                     </iframe>';
                     
                     			}
                     			else {
                     				$thumbimg=$file.'.png';
                     				$preview='<img src="'.$contenrdir.$cat.'/'.strtolower($subcat).'/'.str_replace(' ', '+', strtolower($itemTitle)).'/'.$icon_file_name.'" alt="'.$itemTitle.'">';
                     			}
                     		}
                     	}
                     }
                     }
                     
                     else {
                     header("location:index.php");
                     }
                     
                     ?><?php if($ext=="mp4" || $ext=="mp3" || $ext=="apk" || $ext=="xapk") {
                     echo'
                     <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-1@s uk-margin" uk-grid>
                     	<div class="uk-card-media-left uk-cover-container  uk-flex uk-flex-center uk-flex-middle ">'.$preview.'</div>
                     <div class="uk-card-body uk-padding">
                     <h3  class="uk-card-title">'.$itemTitle.'</h3>
                     <a href="'.$filename.'"  class="uk-button uk-button-primary">Download</a>
                     <p>'.$description.'</p>
                     </div>
                     	</div>
                     </div>
                     ';
                     }
                     else {
                     echo'
                     <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-1@s uk-margin" uk-grid>
                     <div class="uk-card-media-left uk-cover-container">'.$html5games.'</div>
                     <div class="uk-card-body uk-padding">
                     <h3  class="uk-card-title">'.$itemTitle.'</h3>
                     <p>'.$description.'</p>
                     </div>
                     	</div>
                     </div>
                     ';
                     }
                     ?>
               </div>
               <div  class="uk-child-width-1-2@m uk-margin-remove-top" uk-grid>
                  <div class="uk-align-center">
                        <?php if($ext=="apk" || $ext=="xapk") {
                           $prevFile=$conn->query("SELECT screenshot_file_name FROM cms.portal_content_screenshots WHERE portal_content_id='$ContentID'");
                           //if($prevFile)
                           if(mysqli_affected_rows($conn)) {
                           	echo'
                           <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slideshow>
                           <ul class="uk-slideshow-items">';
                           	while($PrevItems=mysqli_fetch_array($prevFile)) {
                           		$scrFiles=$PrevItems['screenshot_file_name'];
                           		$screenshot=$contenrdir.$cat."/".strtolower($subcat)."/".str_replace(' ', '+', strtolower($itemTitle))."/"."screenshots"."/".$scrFiles;
                           		echo"	<li  tabindex=\"-1\"> <img data-src=\"".$screenshot."\"  src=\"".$screenshot."\"  uk-cover uk-img=\"target: !.uk-slideshow-items\"> ";
                           	}
                           	echo"							 
                           </li> </ul>
                           <a class=\"uk-position-center-left uk-button uk-button-default uk-position-small \" href=\"#\" uk-slidenav-previous uk-slideshow-item=\"previous\"></a>
                           <a class=\"uk-position-center-right uk-button uk-button-default uk-position-small \"  href=\"#\" uk-slidenav-next uk-slideshow-item=\"next\"></a>
                           </div>";
                           }
                           }
                           ?> 
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>