<?php include "header.php"; ?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width,initial-scale=1.0">
      <title>Qnet | Download & Play</title>
      <link rel="stylesheet" type="text/css" href="css/portal.css" media="screen" />
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
      <!-- UIkit CSS -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.6.16/dist/css/uikit.min.css" />
      <!-- UIkit JS -->
      <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.16/dist/js/uikit.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/uikit@3.6.16/dist/js/uikit-icons.min.js"></script>
      <script type="text/javascript" src="js/jquery.js" ></script>
   </head>
   <body>
      <div class="tm-navbar-container uk-background-primary">
         <div class="uk-container uk-container-expand">
            <nav class="uk-background-primary uk-light" uk-navbar>
               <div class="uk-navbar-left">
                  <ul class="uk-navbar-nav">
                     <li class="uk-active"><a class="navtitle" href="#">Qnet Play</a></li>
                  </ul>
               </div>
               <div class="uk-navbar-right">
                  <ul class="uk-navbar-nav">
                     <li><a href="?varx=games-apk">APK GAMES</a></li>
                     <li><a href="?varx=html5">HTML5 GAMES</a></li>
                  </ul>
               </div>
            </nav>
         </div>
      </div>
      <div class="midheader uk-height-small uk-background-cover uk-flex uk-flex-center uk-flex-middle uk-light">
      <h1 class="uk-margin-remove-bottom"><?php echo $heads?></h1>
      </div>
      <div class="uk-section uk-section-default uk-padding-small uk-margin-top">
         <div class="uk-container uk-container-expand">
            <hr class="uk-divider-icon">
            <ul uk-accordion="active:0"> 
               <?php
                  $queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");
                  if($queryJoin)
                  {
                      while ($DateRow = mysqli_fetch_assoc($queryJoin))
                      {
                          $resCatgID	= $DateRow['id'];
                          $resName= $DateRow['category'];
                          $resSbCatgID= $DateRow['sc_id'];
                          $resSName= $DateRow['sub_category'];
                          $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";
                          
                     echo'
                  <li>
                  <a href="#" class="uk-accordion-title uk-text-light uk-text-uppercase uk-placeholder">'.ucfirst($resSName).'</a>';
                  echo"
                  <div class=\"uk-accordion-content uk-child-width-1-6@l uk-child-width-1-4@m uk-child-width-1-2@s uk-grid-small uk-margin-top uk-grid-match\" uk-grid>";
                  if(!empty($_SESSION['categref']))
                  {
                  $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt-portal/portal/";
                  $categref 	= $_SESSION['categref'];
                  $sesCatg 	= $_SESSION['sesCatg'];
                  
                  $getContent = $conn->query("SELECT * FROM cms.portal_content WHERE id!=1 and $categref  order by id desc limit 20");
                  
                  if($getContent)
                  {
                  while($items = mysqli_fetch_array($getContent))
                  {
                  $contentID 		= $items['id'];
                  $itemTitle 		= $items['title'];
                  $description 	= $items['description'];
                  $icon_file_name 		= $items['icon_file_name'];
                  $content_file_name = $items['content_file_name'];
                  $mime 			= $items['content_file_mime'];
                  $ext 			= pathinfo($icon_file_name, PATHINFO_EXTENSION);
                  
                  if($cat=="games-apk")
                  { $filename = $contenrdir.$cat."/".strtolower($resSName)."/".str_replace(' ', '+',strtolower($itemTitle))."/".str_replace(' ', '+',($content_file_name));
                  $file = pathinfo($icon_file_name, PATHINFO_FILENAME);
                  
                  }
                  
                  if($cat=="html5")
                  { $filename = $contenrdir.$cat."/".strtolower($resSName)."/".str_replace(' ', '+',strtolower($itemTitle))."/".str_replace(' ', '+',($content_file_name));
                  $file = pathinfo($icon_file_name, PATHINFO_FILENAME);
                  }
                  
                  if($ext=="mp4")
                  {
                  $thumbimg = $file.'.png';
                  $preview ='<video width="100%" height="100"  controls preload="metadata">
                  <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
                  </video>';
                  }
                  elseif($ext=="mp3")
                  {
                  $preview = '<img class="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt-portal/portal/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
                  }
                  else
                  {
                  
                  $thumbimg = $file.'.png';
                  $preview ='<img class="img-thumbnail" src="'.$contenrdir.$cat.'/'.strtolower($resSName).'/'.str_replace(' ', '+',strtolower($itemTitle)).'/'.$thumbimg.'" alt="'.$itemTitle.'">';
                  
                  }
                  
                  if($cat=="html5")
                  {
                  echo"
                  <div  class=\"uk-animation-toggle\" tabindex=\"0\">
                     <div class=\"uk-card uk-card-primary uk-card-hover uk-card-body  uk-animation-fade\">	  
                        <a href=\"preview.php?varx=$sesCatg&contid=$contentID&category=$resSName\">$preview</a>
                           <h3 class=\"uk-card-title\"><a  class=\"uk-link-reset\"  href=\"preview.php?varx=$sesCatg&contid=$contentID\">$itemTitle</a></h3>
                        <a href=\"preview.php?varx=$sesCatg&contid=$contentID\" class=\"uk-button uk-button-default\">$btn_play</a>
                     </div>
                  </div>";
                  }else{
                  
                  echo"
                  <div class=\"uk-animation-toggle\" tabindex=\"0\">
                  <div class=\"uk-card uk-card-primary uk-card-hover uk-card-body uk-animation-fade\">	 			  
                  <a href=\"preview.php?varx=$sesCatg&contid=$contentID&category=$resSName\">$preview</a>
                      <h3 class=\"uk-card-title\"><a class=\"uk-link-reset\"  href=\"preview.php?varx=$sesCatg&contid=$contentID\">$itemTitle</a></h3>
                  <a href=\"$filename\" class=\"uk-button uk-button-default\">$btn_play</a>
                  </div>
                  </div>";
                  }
                  } // 2nd while loop
                  }
                  }	
                  
                      echo"</div>
                      </li>";
                  
                      } // End of 1st while loop
                  
                  } // End of if condtion
                  
                  ?>
            </ul>
         </div>
      </div>
   </body>
</html>