<?php
ob_start();
session_start();
include('connection/inc.php');
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"];
	$sub = $_GET["subcat"];

	if($cat=="videos")
	{
		$vdoImg = "videos_icon.png";
		$tipImg = "tips_icon.png";
		$appImg = "apps_icon.png";
	}
	if($cat=="tip")
	{
		$vdoImg = "videos_icon.png";
		$tipImg = "tips_icon.png";
		$appImg = "apps_icon.png";
	}
	if($cat=="app")
	{
		$vdoImg = "videos_icon.png";
		$tipImg = "tips_icon.png";
		$appImg = "apps_icon.png";

	}

}
else
{
	$cat = 'videos';
	$sub = $_GET["subcat"];
	$vdoImg = "videos_icon.png";
	$tipImg = "tips_icon.png";
	$appImg = "apps_icon.png";
}
$_SESSION['sesCatg'] = $cat;
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
   
	<meta charset="UTF-8">
	<title>Fitcore</title>
</head>
<body>
	<header class="logoMenu">
		<section class="logo">
			<img src="img/logo.png" alt="Naughtybabe">
		</section>
		<section class="menuNav">
			<section class="navBurger">
				<span></span>
				<span></span>
				<span></span>
			</section>
			<section class="navList">
				<ul class="links">
					<li><a href="./?varx=videos"><img src="img/<?php echo $vdoImg;?>" alt="Videos"></a></li>
					<li><a href="./?varx=app"><img src="img/<?php echo $appImg;?>" alt="Apps"></a></li>
					<li><a href="./?varx=tip"><img src="img/<?php echo $tipImg;?>" alt="Tips"></a></li>
				</ul>
			</section>
		</section>
	</header>
	
	<main class="mainContainer">
		<div class="divContainer">
			<section class="prevTitle">
	    		<p>Preview</p>
	    		<a href="<?php echo './?varx='.$cat.'#'.$sub; ?>" class="preview-close">
	    			<span></span>
					<span></span>
				</a>
    		</section>
			<?php
            if(isset($_GET['varx'])){
                $prevSesID = $_GET['contid'];
                $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";

                if($_GET['varx']==""){
                    header("location:index.php");
                }else{
                    $getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
                    $data = array();
                    if($getContent){
                        while($items = mysqli_fetch_array($getContent)){
                            $ContentID 		= $items['id'];
							$itemTitle 		= $items['title'];
							$description 	= $items['description'];
							$contentRate	= $items['rate'];
							$file_name 		= $items['file_name'];
							$original_file_name = $items['original_file_name'];
							$mime 			= $items['mime'];
							$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
							$filename = $contenrdir.$file_name;	
                            $file = pathinfo($file_name, PATHINFO_FILENAME);
                            
                            if($ext=="mp4" || $ext=="mp3"){
                                $thumbimg = $file.'.png';
                                $preview ='<video width="100%"  controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
										<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
									  </video>';
                            }else{
                                $thumbimg = $file.'.png';
                                $preview ='<img src="' .$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
                            }
                        }
                    }
                }
                
            }else{
                header("location:index.php");
            }
        ?>
            <section class="dlSection">
            	<div class="dlImage">
                    <?php echo $preview; ?>
                </div>
                <div class="dlName">
                    <p><?php echo $itemTitle; ?></p>
                </div>
                <div class="dlDescrip">
                    <p><?php echo stripslashes($description); ?></p>
                </div>
                <div class="dlDownload">
                    <a href="<?php echo $filename; ?>">DOWNLOAD</a>
                </div>
            </section>
		<!-- <?php
			if($ext=="apk" || $ext=="xapk")
			{
				$prevFile = $conn->query("SELECT file_name FROM cms.preview WHERE content_id='$ContentID'");
				//if($prevFile)
				if(mysqli_affected_rows($conn))
				{
					echo'<tr>
					  <td colspan="2" style="padding:8px; font-weight:bold; text-align:left">Screenshots:</td>
					</tr>
					<tr>
				  <td colspan="2"><div style="margin-top:5px;">
				<div class="w3-content w3-display-container" style="background:#f0ca4d; padding:5px;">
				  <div style="width:221px; margin:auto">';
					while($PrevItems = mysqli_fetch_array($prevFile))
					{
						$scrFiles	= $PrevItems['file_name'];
						$screenshot = $contenrdir.$scrFiles;
						
						echo"<img src=\"".$screenshot."\" width=\"221\" height=\"191\" class=\"mySlides\">";
					}
					echo"</div>
					  <button class=\"w3-button w3-black w3-display-left\" onclick=\"plusDivs(-1)\"><img src=\"img/btn_left.png\" width=\"19\" height=\"14\" /></button>
					  
					  <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs(1)\"><img src=\"img/btn_right.png\" width=\"19\" height=\"14\" /></button>
				  </div>
					<script>
					var slideIndex = 1;
					showDivs(slideIndex);
					
					function plusDivs(n) {
					  showDivs(slideIndex += n);
					}
					
					function showDivs(n) {
					  var i;
					  var x = document.getElementsByClassName(\"mySlides\");
					  if (n > x.length) {slideIndex = 1}    
					  if (n < 1) {slideIndex = x.length}
					  for (i = 0; i < x.length; i++) {
						 x[i].style.display = \"none\";  
					  }
					  x[slideIndex-1].style.display = \"block\";  
					}
					</script>
				</div>
				</td>
				</tr>";
				}
			}
			?> -->
			
			


		</div>
	</main>

    

<script src="js/script.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
  crossorigin="anonymous"></script>      
</body>
</html>