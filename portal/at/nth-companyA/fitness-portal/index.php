<?php require ('connection/header.php');?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css" />

   
	<meta charset="UTF-8">
	<title>Fitcore</title>
</head>
<body onload="init()">
	<header class="logoMenu">
		<section class="logo">
			<img src="img/logo.png" alt="Fitcore">
		</section>
		<section class="menuNav">
			<section class="navBurger">
				<span></span>
				<span></span>
				<span></span>
			</section>
			<section class="navList">
				<ul class="links">
					<li><a href="?varx=videos"><img src="img/<?php echo $vdoImg;?>" alt="Videos"></a></li>
					<li><a href="?varx=app"><img src="img/<?php echo $appImg;?>" alt="Apps"></a></li>
					<li><a href="?varx=tip"><img src="img/<?php echo $tipImg;?>" alt="Tips"></a></li>
				</ul>
			</section>
		</section>
	</header>

	<main class="mainContainer">
		<div class="divContainer">
			<section class="tabLinks">
				<ul class="tabNav" id="tabs">
			<?php
				if($cat!="tip"){

					$queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");
					if($queryJoin){
		            	while ($DateRow = mysqli_fetch_assoc($queryJoin)){

							

		                    $resCatgID	= $DateRow['id'];
		                    $resName= $DateRow['category'];
		                    $resSbCatgID= $DateRow['sc_id'];
		                    $resSName= $DateRow['sub_category'];
		                    $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";

		                    if($resSName=="Application"){
		                    	$resSName = "Antivirus";
		                    }

		                    echo'<li><a href="#'.ucfirst($resSName).'" style="display: block;">'.ucfirst($resSName).'</a></li>';

		           		}
		           	}

		          }else{
		            	echo'<li><a href="#food" style="display: block;">Food</a></li>';
		            	echo'<li><a href="#sport" style="display: block;">Sport</a></li>';
		            	
		            }


		            echo '		</ul>
		            		</section>';

		            if($cat == "tip"){
		            	echo '<section class="tabContent" id="food"></section>
		        			<section class="tabContent" id="sport"></section>';
		            }
		            	
		            if($cat!="tip"){

						$queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");
						
						
						if($queryJoin){

		            	while ($DateRow = mysqli_fetch_assoc($queryJoin)){

		                    $resCatgID	= $DateRow['id'];
		                    $resName= $DateRow['category'];
		                    $resSbCatgID= $DateRow['sc_id'];
		                    $resSName= $DateRow['sub_category'];
		                    $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";

		                    echo '<section class="tabContent" id="'.ucfirst($resSName).'">';

	                        if(isset($_GET["varx"])){
	                        	$cat = $_GET["varx"]='videos'; 
	                        }else{
	                        	$cat = 'videos';
	                        }

	                        if(!empty($_SESSION['categref'])){
		                        $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
		                        $categref 	= $_SESSION['categref'];
		                        $sesCatg 	= $_SESSION['sesCatg'];

	                        	$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and $categref order by id asc limit 20");
	                        	$data = array();
	                        
	                        
	                        if($getContent){
	                        	while($items = mysqli_fetch_array($getContent)){

		                        $contentID 		= $items['id'];
		                        $itemTitle 		= $items['title'];
		                        $description 	= $items['description'];
		                        $file_name 		= $items['file_name'];
		                        $original_file_name = $items['original_file_name'];
		                        $mime 			= $items['mime'];
		                        $ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
		                        $filename = $contenrdir.$file_name;
		                        $subcat = $items['sub_category_id'];

	                        	$file = pathinfo($file_name, PATHINFO_FILENAME);

		                        if($ext=="mp4"){
			                        $thumbimg = $file.'.png';
			                        $preview ='<video style="z-index:-2;" width="100%" height="100"  controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
			                        <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
			                        </video>';
		                        }
		                        elseif($ext=="mp3"){
		                        	$preview = '<img id="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
		                        }
		                   

	                        	else{
		                        	$thumbimg = $file.'.png';
		                        	$preview ='<img id="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
	                       		}

	                        echo"<article>
	                        		<div class=\"thumbImage\">
									<a href=\"preview.php?varx=$sesCatg&contid=$contentID&subcat=$subcat\" class=\"thumbnail\">$preview</a>
	                                </div>
	                                <div class=\"thumbName\">
	                                    <a href=\"preview.php?varx=$sesCatg&contid=$contentID&subcat=$subcat\" class=\"title\">$itemTitle</a>
	                                    <a href=\"$filename\" class=\"btn\">DOWNLOAD</a>
	                                </div>
	                              </article>";

	                    	}
	                   	 	}
	                   	 	echo'</section>';
	                		}
		            	}
		        	
		     		}
		        }
		        
		       

			?>
		</div>
	</main>


	<script>
    /////concatenate url parameter for 'Tip'//////////////
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

	document.addEventListener("DOMContentLoaded", function(){
	    if(getUrlParameter('varx') == 'tip'){
	    	loadXMLDoc('food');
	    	loadXMLDoc('sport');
	    }
	});

	function loadXMLDoc(str) {

		  var xmlhttp = new XMLHttpRequest();
		  xmlhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		      getData(this, str);
		    }
		  };
		  xmlhttp.open("GET", str + "s.xml", true);
		  xmlhttp.send();
		}

	function getData(xml, str) {
		  var i;
		  var xmlDoc = xml.responseXML;
		  var data1 = "";
		  var data2 = "";
		  var img = "";
		  var ext = "";


		  var div = "";
		  var x = xmlDoc.getElementsByTagName("ITEM");
		  for (i = 0; i <x.length; i++) { 
		 
		    data1 = x[i].getElementsByTagName("TITLE")[0].childNodes[0].nodeValue;
		    data2 = x[i].getElementsByTagName("DESCRIPTION")[0].childNodes[0].nodeValue;
		    img = x[i].getElementsByTagName("IMG")[0].childNodes[0].nodeValue;
		    ext = x[i].getElementsByTagName("EXTENSION")[0].childNodes[0].nodeValue;

		    div += "<div class='article'><div class='thumbImage " +img + "'><img onclick='display(\"" + img +"\",\"on\")' class='thumbnail' src='sysprop/" + str +"/" + img + "."+ ext +"' /></div>" +
		    "<div class='thumbName'><a class='title'>" + data1 + 
		    "</a> <a href='#' class='btn' onclick='display(\"" + img +"\",\"on\")'>VIEW MORE</a> </div></div>" +
			"<div id='" + img + "' class='tipsCnt'>" +
    	 	"<div class='modalContent'><div class='tipHeader'><div class='tipName'><p>"+ data1+ "</p></div> <div class='modalClose' onclick='display(\"" + img +"\",\"off\")'><span></span><span></span></div></div>" +
    		"<div class='popImage'><img class='img' src='sysprop/" + str +"/" + img + "."+ ext +"' /></div>" +
    		"<div class='popContent'><p>" + data2 + "</p></div></div></div>";
		  }

		  document.getElementById(str).innerHTML= div;
		}

		var scrl = "";
		 function display(str, str2){
			 if(str2 == 'on'){
			 	scrl = $(window).scrollTop();	
			 }
			 

		 	var modal = document.getElementById(str);
		 	if(str2 == 'on'){
		 		modal.style.display = "block";
		 	}else{
		 		modal.style.display = "none";
		 	}

			 window.onclick = function(event) {
			    if (event.target == modal) {
			        modal.style.display = "none";
			        modal:focus;
			        $(window).scrollTop(scrl);
			    }
		    }
		   	//alert(scrl);
		    $(window).scrollTop(scrl);


		 }

	</script>

	<script src="js/script.js" type="text/javascript"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
  crossorigin="anonymous"></script>
</body>
</html>