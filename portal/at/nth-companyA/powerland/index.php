<?php
   session_start();
   include('connection/inc.php');
   include('connection/header.php');
    include('sysprop/Consts.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="de"  xml:lang="de" translate="yes"> 
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
      <title>Explorella | Download Store</title>
      <meta name="viewport" content="width=device-width,initial-scale=1.0">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/main.css">
      <script type="text/javascript" src="js/jquery.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
   </head>
   <body>
         <div class="header">
            <div class="logo">
            <a href="./"><img src="assets/logo.png" alt=""></a>
            </div>
            <div id='main-menu' class='main-menu'>
               <div class='container-menu'>
                  <nav class='navigation'>
                     <span class='hamburger-menu'>
                     <span class='burger-1'></span>
                     <span class='burger-2'></span>
                     <span class='burger-3'></span>
                     </span>
                     <ul class='core-menu'>
                        <li class='menus'> <a href="?varx=games"><img src='assets/<?php echo $btnImg; ?>' border="0"></a></li>
                        <li class='menus'> <a href="?varx=videos"><img src='assets/<?php echo $vdoImg; ?>' border="0"></a></li>
                        <li class='menus'> <a href="?varx=tones"><img src='assets/<?php echo $toneImg; ?>' border="0"></a></li>
                        <li class='menus'> <a href="?varx=apps"><img src='assets/<?php echo $appImg; ?>' border="0"></a> </li>
                     </ul>
                  </nav>
               </div>
            </div>
         </div>

   <main>
         <?php
   $queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM 
   cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");
   
   if($queryJoin)
   {
   while ($DateRow = mysqli_fetch_assoc($queryJoin))
   {
   $resCatgID	= $DateRow['id'];
   $resName= $DateRow['category'];
   $resSbCatgID= $DateRow['sc_id'];
   $resSName= $DateRow['sub_category'];
   $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";
   
   if($resSName=="Action")
   {
   $resSName = "Aktion";
   }
   if($resSName=="Adventure")
   {
   $resSName = "Abenteuer";
   }
   if($resSName=="Arcade")
   {
   $resSName = "Arkade";
   }
   if($resSName=="Strategy")
   {
   $resSName = "Strategie";
   }
   if($resSName=="car racing")
   {
   $resSName = "Autorennen";
   }
   if($resSName=="football")
   {
   $resSName = "Fußball";
   }
   if($resSName=="Cartoons")
   {
   $resSName = "Karikatur";
   }
   if($resSName=="Funny")
   {
   $resSName = "Lustig";
   }
   if($resSName=="Horror")
   {
   $resSName = "Grusel";
   }
   if($resSName=="Tones-Portal")
   {
   $resSName = "Töne-Portal";
   }
   if($resSName=="Productivity")
   {
   $resSName = "Produktivität";
   }
   if($resSName=="Utility")
   {
   $resSName = "Nützlichkeit";
   }


   if(ucfirst($resSName)=="Produktivität" || ucfirst($resSName)=="Aktion"  || ucfirst($resSName)=="Töne-Portal" 
   || ucfirst($resSName)=="Autorennen")
   
   {
   $strContent .= "<li id='".ucfirst($resSName)."1' class='active'><a data-toggle='tab' href='#".ucfirst($resSName)."' class='collapse' ><span>".ucfirst($resSName)."</span></a></li>";
   $strContent1 .= "<div id='".ucfirst($resSName)."' class='tab-pane  fade in active'>";
   
   }else{
   $strContent .= "<li id='".ucfirst($resSName)."1'><a data-toggle='tab' href='#".ucfirst($resSName)."' class='collapse' ><span>".ucfirst($resSName)."</span></a></li>";
   $strContent1 .= "<div id='".ucfirst($resSName)."' class='tab-pane fade in'>";
   }
   
   if(isset($_GET["varx"]))
   {
   $cat = $_GET["varx"]='games'; 
   }
   else
   {
   $cat = 'games';
   }
   
   if(!empty($_SESSION['categref']))
   {
   $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
   $categref   = $_SESSION['categref'];
   $sesCatg    = $_SESSION['sesCatg'];
   
   $getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and $categref   order by id desc limit 20");
   
   $data = array();
   if($getContent)
   {
   while($items = mysqli_fetch_array($getContent))
   {
   $contentID      = $items['id'];
   $itemTitle      = $items['title'];
   $description    = $items['description'];
   $file_name      = $items['file_name'];
   $original_file_name = $items['original_file_name'];
   $mime           = $items['mime'];
   $ext            = pathinfo($file_name, PATHINFO_EXTENSION);
   $filename = $contenrdir.$file_name;
   $subcat = $items['sub_category_id'];
   
   $file = pathinfo($file_name, PATHINFO_FILENAME);
   
   if($ext=="mp4")
   {
   $thumbimg = $file.'.png';
   $preview ='<video style="z-index:-2;" width="100%" height="100" controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
   <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
   </video>';
   }
   elseif($ext=="mp3")
   {
   $preview = '<img id="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
   }
   elseif($ext=="jpg")
   {
   $thumbimg = $file.'.jpg';
   $preview ='<img id="img-thumbnail" src="' .$filename. '" alt="'.$itemTitle.'">';
   }
   else
   {
   $thumbimg = $file.'.png';
   $preview ='<img id="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
   }
   
   $strContent1 .= " <div id=\"item\" style=\"z-index:0;\" class=\"content-items\">						  
   <a style='z-index:100;' href=\"preview.php?varx=$sesCatg&contid=$contentID&subcat=$subcat\"><div style='z-index:100;'>$preview</div></a>
   <div id=\"title\" class=\"item-holder\">
   <a class=\"title\" href=\"preview.php?varx=$sesCatg&contid=$contentID&subcat=$subcat\">$itemTitle</a>
   <div id=\"btnalign\"><a href=\"$filename\" class=\"btn\">HERUNTERLADEN</a></div>
   </div>
   
   </div>";
   } // End of 2nd while loop
   }
   }
   $strContent1 .="
   </div>";
   
   } // End of 1st while class='cssmenu'  " 
   
   echo "<div class='main-content'><ul class='nav nav-tabs'>".$strContent."</ul>";
   echo  "<div class='tab-content '>" . $strContent1 ."</div></div>";
   }
   ?>

         
      </main>
      <script src="js/script.js"></script>
      
   </body>
</html>