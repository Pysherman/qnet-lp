<?php
ob_start();
session_start();
include('inc.php');
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"]; 
	
	if($cat=="videos")
	{
		$btnImg = "wallpaper1.png";
		$vdoImg = "vdo2.png";
	}
	if($cat=="Wallpaper")
	{
		$btnImg = "wallpaper2.png";
		$vdoImg = "vdo1.png";
	}

}
else
{
	$cat = 'videos';
	$btnImg = "wallpaper1.png";
	$vdoImg = "vdo2.png";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Preview</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/portal.css">
<!-- <link rel="stylesheet" type="text/css" href="styles.css" media="screen" /> 
<script type="text/javascript" src="./js/jquery.js" ></script> -->
<link href="./css/toggle.css?2" rel="stylesheet" type="text/css">
<script src="./js/jquery-1.12.4.min.js"></script>
<script src="./js/toggle.js?2" type="text/javascript"></script>

<style>


.column {
    float: left;
    /*width: 33.33%;*/
    width: 25%;
    padding: 5px;
}
/*.item img,*/
.column img,  .prevw img { max-width: 100% !important; width: auto !important; height: auto !important; }

/* Clearfix (clear floats) */
.row::after {
    content: "";
    clear: both;
    display: table;
}

/* ============================== */
.container {
  list-style:none;
  margin: 0;
  padding: 0;
}


.flex {
  padding: 0;
  margin: 0;
  list-style: none;
  
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  
  -webkit-flex-flow: row wrap;
  justify-content: space-around;
}

#title
{
	padding:10px;
	text-align:center;
	color:#0094ca;
}

.btn
{
	width:100%;
}

.btn, .btn2{
	text-align:center;
border:1px solid #006b92; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px; padding: 10px 10px 10px 10px; text-decoration:none; display:inline-block;text-shadow: -1px -1px 0 rgba(0,0,0,0.3);font-weight:bold; color: #FFFFFF;
 background-color: #b95e0f; background-image: -webkit-gradient(linear, left top, left bottom, from(#0094CA), to(#0094CA));
 background-image: -webkit-linear-gradient(top, #b95e0f, #b95e0f);
 background-image: -moz-linear-gradient(top, #b95e0f, #b95e0f);
 background-image: -ms-linear-gradient(top, #b95e0f, #b95e0f);
 background-image: -o-linear-gradient(top, #b95e0f, #b95e0f);
 background-image: linear-gradient(to bottom, #b95e0f, #b95e0f);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#b95e0f, endColorstr=#b95e0f);
}

.btn:hover, .btn2:hover{
 border:1px solid #004964;
 background-color: #940705; background-image: -webkit-gradient(linear, left top, left bottom, from(#006f97), to(#006f97));
 background-image: -webkit-linear-gradient(top, #940705, #940705);
 background-image: -moz-linear-gradient(top, #940705, #940705);
 background-image: -ms-linear-gradient(top, #940705, #940705);
 background-image: -o-linear-gradient(top, #940705, #940705);
 background-image: linear-gradient(to bottom, #940705, #940705);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#940705, endColorstr=#940705);
}

#block{
    width: 100%;
    height: 100%;
    position: absolute;
    visibility:hidden;
    display:none;
    background-color: rgba(22,22,22,0.5);
	top:0;
	left:0;
}

#block:target {
    visibility: visible;
    display: block;
}

* {
  box-sizing: border-box;
}


</style>
</head>

<body id="body">

<div id="proj" style="background-color:rgba(0,0,0,0.4); width:100%; height:100%; position:absolute; display:none">&nbsp;</div>
<div id="header">
    <div id="box1">
    	<div style="float:right; padding:25px;"><span onclick="openNav()" style="cursor:pointer"><a href="#block"><img src="img/menu.png" id="img" border="0" /></a></span></div>      
        <div > <img src="img/pllogo.png" id="ptslogo" border="0" />  
</div>


<div id="mySidenav" class="sidenav">
    <div id="box1">
    	<!-- style="padding:5px 0 15px 15px;"-->
    	<div >
        <a href="#" class="closebtn" onclick="closeNav()" style="padding-top:10px; padding-right:10px;"><img src="img/btn_close.png" width="32" height="32" border="0" /></a></div>
        	<div > <img src="img/pllogo.png" id="ptslogo" border="0" /> </div>
    </div>
    <div id="menu_link">
        <a href="./?varx=videos">Videos</a>
    	<a href="./?varx=Wallpaper">WallPapers</a>
    </div>
</div>
<!-- style="padding-top:70px; width:80%; margin:auto; text-align:center;" -->
<div  id="previconimg">
    <div class="row">
    <div class="column"><a href="./?varx=videos"><img src='img/<?php echo $vdoImg; ?>' border="0"></a></div>
      <div class="column"><a href="./?varx=Wallpaper"><img src='img/<?php echo $btnImg; ?>' border="0"></a></div>
    </div>
</div>

<div id="prevcontentWrap">

	<div class="accordion_container" style="margin-top:5px;">
    
    	<div class="accordion_head">PREVIEW</div>     
        <div style="background-color:#f2f2f2; color:#000">
        <?php
			if(isset($_GET['varx']))
			{
				$prevSesID = $_GET['contid'];
				$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
				
				if($_GET['varx']=="")
				{
					header("location:index.php");
				}
				else
				{
					$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
					$data = array();
					if($getContent)
					{
						while($items = mysqli_fetch_array($getContent))
						{
							$ContentID 		= $items['id'];
							$itemTitle 		= $items['title'];
							$description 	= $items['description'];
							$contentRate	= $items['rate'];
							$file_name 		= $items['file_name'];
							$original_file_name = $items['original_file_name'];
							$mime 			= $items['mime'];
							$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
							$filename = $contenrdir.$file_name;
							
							$file = pathinfo($file_name, PATHINFO_FILENAME);
							
							if($ext=="mp4" || $ext=="mp3")
							{
								$thumbimg = $file.'.png';
								$preview ='<video width="100%" height="200"  controls preload="metadata">
										<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
									  </video>';
							}
							else
							{
								$thumbimg = $file.'.png';
								$preview ='<img src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
							}
						}
					}
				}
			}
			else
			{
				header("location:index.php");
			}
		?>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      	    <tr>
      	      <td width="43%" align="center" style="padding:5px;" class="prevw" ><?php echo $preview; ?></td>
      	      <td width="57%" align="left" valign="middle">
              <div id="title" style="text-align:left; font-weight:bold; font-size:13px;"><?php echo $itemTitle; ?></div>
              <img src="img/<?php echo $contentRate; ?>.png" border="0" />
              <div style="margin-top:10px; padding-left:8px;"><a href="<?php echo $filename; ?>" class="btn2">DOWNLOAD</a></div>
              </td>
   	        </tr>
      	    <tr>
      	      <td colspan="2" style="padding:8px; text-align:left"><?php echo stripslashes($description); ?></td>
   	        </tr>
      	    <?php
			if($ext=="apk" || $ext=="xapk")
			{
				$prevFile = $conn->query("SELECT file_name FROM cms.preview WHERE content_id='$ContentID'");
				//if($prevFile)
				if(mysqli_affected_rows($conn))
				{
					echo'<tr>
					  <td colspan="2" style="padding:8px; font-weight:bold; text-align:left">Screenshots:</td>
					</tr>
					<tr>
				  <td colspan="2"><div style="margin-top:5px;">
				<div class="w3-content w3-display-container" style="background:#f0ca4d; padding:5px;">
				  <div style="width:221px; margin:auto">';
					while($PrevItems = mysqli_fetch_array($prevFile))
					{
						$scrFiles	= $PrevItems['file_name'];
						$screenshot = $contenrdir.$scrFiles;
						
						echo"<img src=\"".$screenshot."\" width=\"221\" height=\"191\" class=\"mySlides\">";
					}
					echo"</div>
					  <button class=\"w3-button w3-black w3-display-left\" onclick=\"plusDivs(-1)\"><img src=\"img/btn_left.png\" width=\"19\" height=\"14\" /></button>
					  
					  <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs(1)\"><img src=\"img/btn_right.png\" width=\"19\" height=\"14\" /></button>
				  </div>
					<script>
					var slideIndex = 1;
					showDivs(slideIndex);
					
					function plusDivs(n) {
					  showDivs(slideIndex += n);
					}
					
					function showDivs(n) {
					  var i;
					  var x = document.getElementsByClassName(\"mySlides\");
					  if (n > x.length) {slideIndex = 1}    
					  if (n < 1) {slideIndex = x.length}
					  for (i = 0; i < x.length; i++) {
						 x[i].style.display = \"none\";  
					  }
					  x[slideIndex-1].style.display = \"block\";  
					}
					</script>
				</div>
				</td>
				</tr>";
				}
			}
			?>
            </table>
        </div>
    
    </div>
    
</div>
<div style="margin-top:70px;"></div>

<script>

function openNav() {
    document.getElementById("mySidenav").style.height = "auto";
	document.getElementById("contentWrap").style.marginRight = "80%";
	document.getElementById("contentWrap").style.margin = "auto";
	document.getElementById("box4").style.marginRight = "80%"; 
	document.getElementById("nvmg").style.marginRight = "80%";
	document.getElementById("proj").style.display = "block";

}
function closeNav() {
    document.getElementById("mySidenav").style.height = "0";	
   /* document.getElementById("contentWrap").style.margin = "margin-top:200px";*/
    document.getElementById("contentWrap").style.margin = "auto";
    document.getElementById("box4").style.margin = "0";
	document.getElementById("nvmg").style.margin = "auto";
	document.getElementById("proj").style.display = "none";
	/*document.getElementById("block").style.display = "none";*/
}


</script>
<div id="block">&nbsp;</div>
</body>
</html>