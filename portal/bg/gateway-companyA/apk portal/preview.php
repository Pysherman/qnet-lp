<?php
ob_start();
session_start();
include('includes/inc.php');
if(isset($_GET["varx"])){
	$cat = $_GET["varx"]; 
}else{
	$cat = 'games-apk';
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <title>DOWNLOAD</title>
</head>
<body>
    <div class="container">
        <header class="logo">
            <img src="assets/apk_header.png" alt="Logo">
        </header>
        <main class="dl-container">
        <?php
            if(isset($_GET['varx'])){
                $prevSesID = $_GET['contid'];
                $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";

                if($_GET['varx']==""){
                    header("location:index.php");
                }else{
                    $getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
                    $data = array();
                    if($getContent){
                        while($items = mysqli_fetch_array($getContent)){
                            $ContentID 		= $items['id'];
							$itemTitle 		= $items['title'];
							//$description 	= $items['description'];
							//$contentRate	= $items['rate'];
							$file_name 		= $items['file_name'];
							$original_file_name = $items['original_file_name'];
							$mime 			= $items['mime'];
							$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
							$filename = $contenrdir.$file_name;	
                            $file = pathinfo($file_name, PATHINFO_FILENAME);
                            
                            if($ext=="mp4"){
                                $thumbimg = $file.'.png';
                                $preview ='<video width="100%" height="100"  controls preload="metadata">
                                        <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
                                    </video>';
                            }else{
                                $thumbimg = $file.'.png';
                                $preview ='<img class="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
                            }
                        }
                    }
                }
                
            }else{
                header("location:index.php");
            }
        ?>
            <section>
                <div class="dl-name">
                    <h5><?php echo $itemTitle; ?></h5>
                </div>
                <div class="dl-image">
                    <?php echo $preview; ?>
                </div>
                <div class="dl-download">
                    <a href="<?php echo $filename; ?>">DOWNLOAD</a>
                </div>
            </section>
        </main>
    </div>

    <script type="text/javascript" src="script/jquery.js" ></script>
</body>
</html>