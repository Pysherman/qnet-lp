<?php
ob_start();
session_start();
include('inc.php');
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"]; 
	$sub = $_GET["subcat"];
	
	if($cat=="videos")
	{
		$vdoImg = "vdo2h.png";
		$tipImg = "tip1.png";
		$appImg = "app1.png";
	}
	if($cat=="tip")
	{
		$vdoImg = "vdo1h.png";
		$tipImg = "tip2.png";
		$appImg = "app1.png";
	}
	if($cat=="app")
	{
		$vdoImg = "vdo1h.png";
		$tipImg = "tip1.png";
		$appImg = "app2.png";
	}
}
else
{
	$cat = 'videos';
	$sub = $_GET["subcat"];
	$vdoImg = "vdo2h.png";
	$tipImg = "tip1.png";
	$appImg = "app1.png";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Menu</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/portal.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="css/toggle.css" rel="stylesheet" type="text/css">
<script src="./js/jquery-1.12.4.min.js"></script>
<script src="./js/toggle.js?2" type="text/javascript"></script>
<style>
* {
    box-sizing: border-box;
}


.column {
    float: left;
    /*width: 33.33%;*/
    width: 25%;
    padding: 5px;
}
@media screen and (max-width:480px){
.column img {
	width:80% !important;
}	
}

.column img, .item img, .prevw img { max-width: 100%; width: auto !important; height: auto !important; }

/* Clearfix (clear floats) */
.row::after {
    content: "";
    clear: both;
    display: table;
    text-align: center;
}

/* ============================== */
.container {
  list-style:none;
  margin: 0;
  padding: 0;
}


.flex {
  padding: 0;
  margin: 0;
  list-style: none;
  
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  
  -webkit-flex-flow: row wrap;
  justify-content: space-around;
}



#title
{
	padding:10px;
	text-align:center;
	color:#0094ca;
}



.btn
{
	width:100%;
}

.btn, .btn2{
	text-align:center;
border:1px solid #006b92; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px; padding: 10px 10px 10px 10px; text-decoration:none; display:inline-block;text-shadow: -1px -1px 0 rgba(0,0,0,0.3);font-weight:bold; color: #FFFFFF;
 background-color: #355f7e; background-image: -webkit-gradient(linear, left top, left bottom, from(#0094CA), to(#0094CA));
 background-image: -webkit-linear-gradient(top, #355f7e, #355f7e);
 background-image: -moz-linear-gradient(top, #355f7e, #355f7e);
 background-image: -ms-linear-gradient(top, #355f7e, #355f7e);
 background-image: -o-linear-gradient(top, #355f7e, #355f7e);
 background-image: linear-gradient(to bottom, #355f7e, #355f7e);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#355f7e, endColorstr=#355f7e);
}

.btn:hover, .btn2:hover{
 border:1px solid #004964;
 background-color: #347cb0; background-image: -webkit-gradient(linear, left top, left bottom, from(#006f97), to(#006f97));
 background-image: -webkit-linear-gradient(top, #347cb0, #347cb0);
 background-image: -moz-linear-gradient(top, #347cb0, #347cb0);
 background-image: -ms-linear-gradient(top, #347cb0, #347cb0);
 background-image: -o-linear-gradient(top, #347cb0, #347cb0);
 background-image: linear-gradient(to bottom, #347cb0, #347cb0);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#347cb0, endColorstr=#347cb0);
}

#block{
    width: 100%;
    height: 100%;
    position: absolute;
    visibility:hidden;
    display:none;
    background-color: rgba(22,22,22,0.5);
	top:0;
	left:0;
}

#block:target {
    visibility: visible;
    display: block;
}
.row-main{
    max-width: 100% !important;
    padding: 0 !important;
    display: flex !important;
    flex-direction: row !important;
	justify-content: space-around !important;
	
}
.column{
	align-self: center !important;
	vertical-align: middle;
	display: inline-block;
	clear: both;
}


</style>
</head>

<body id="body">
<div id="proj" style="background-color:rgba(0,0,0,0.4); width:100%; height:100%; position:absolute; display:none">&nbsp;</div>

<div id="header">
<div id="box1">  
        <div style="float:right; padding:25px;"><span onclick="openNav()" style="cursor:pointer"><a href="#block"><i style="color:#837F7A;" id="img" class='fa fa fa-align-justify fa-2x'></i>	</a></span></div>
 		<div > <img src="img/hllogo.png" id="ptslogo" border="0" />  	
        </div>
    </div>
</div>


<div id="mySidenav" class="sidenav">
    <div id="box1"> 
   <div><a href="#" class="closebtn" onclick="closeNav()" style="padding-top:10px; padding-right:18px;"><i style="margin-top: -0.15em; width:125px;font-size:33px;color:#837F7A;" class='fa fa-close'></i></a></div>
        <div><img src="img/hllogo.png" id="ptslogo" border="0" /></div>
    </div>
    <div id="menu_link">
        <a href="./?varx=videos"><span>Videos</span></a>        
        <a href="./?varx=app"><span>Apps</span></a>
        <a href="./?varx=tip"><span>Tips</span></a>
    </div>
</div>

<div class="iconimages">
    <div class="row-main">
      <div class="column"><a href="./?varx=videos"><img src='img/<?php echo $vdoImg; ?>' border="0"></a></div>
      <div class="column"><a href="./?varx=app"><img src='img/<?php echo $appImg; ?>' border="0"></a></div>
      <div class="column"><a href="./?varx=tip" id="tips"><img src='img/<?php echo $tipImg; ?>' border="0"></a></div>
    </div>
</div>

<div id="contentWrap" style="margin-top:auto;">

	<div class="accordion_container" style="margin-top:5px;">
    
    	<div class="accordion_head">PREVIEW     
    	<a href="<?php echo './?varx='.$cat.'#'.$sub; ?>"><span class='close cvid'>&times;</span></a></div>   
        <div style="background-color:#f2f2f2; color:#000; overflow:hidden; margin-bottom:3%;">
        <?php
			if(isset($_GET['varx']))
			{
				$prevSesID = $_GET['contid'];
				$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
				
				if($_GET['varx']=="")
				{
					header("location:index.php");
				}
				else
				{
					$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
					$data = array();
					if($getContent)
					{
						while($items = mysqli_fetch_array($getContent))
						{
							$ContentID 		= $items['id'];
							$itemTitle 		= $items['title'];
							$description 	= $items['description'];
							$contentRate	= $items['rate'];
							$file_name 		= $items['file_name'];
							$original_file_name = $items['original_file_name'];
							$mime 			= $items['mime'];
							$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
							$filename = $contenrdir.$file_name;
							
							$file = pathinfo($file_name, PATHINFO_FILENAME);
							
							if($ext=="mp4" || $ext=="mp3")
							{
								$thumbimg = $file.'.png';
								$preview ='<video width="100%"  controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
										<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
									  </video>';
							}
							else
							{
								$thumbimg = $file.'.png';
								$preview ='<img src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
							}
						}
					}
				}
			}
			else
			{
				header("location:index.php");
			}
		?>
			<div class="prev-main">
				<div class="prev-image">
					<?php echo $preview; ?>
				</div>
				<div class="prev-content">
					<h3><?php echo $itemTitle; ?></h3>
					<!--<img src="img/4.5.png" border="0" />-->
					<img src="img/<?php echo $contentRate; ?>.png" border="0" />
					<p><?php echo stripslashes($description); ?></p>
					<a href="<?php echo $filename; ?>" class="btn2">DOWNLOAD</a>
				</div>
      	    <?php
			if($ext=="apk" || $ext=="xapk")
			{
				$prevFile = $conn->query("SELECT file_name FROM cms.preview WHERE content_id='$ContentID'");
				//if($prevFile)
				if(mysqli_affected_rows($conn))
				{
					echo'<tr>
					  <td colspan="2" style="padding:8px; font-weight:bold; text-align:left">Screenshots:</td>
					</tr>
					<tr>
				  <td colspan="2"><div style="margin-top:5px;">
				<div class="w3-content w3-display-container" style="background:#f0ca4d; padding:5px;">
				  <div style="width:221px; margin:auto">';
					while($PrevItems = mysqli_fetch_array($prevFile))
					{
						$scrFiles	= $PrevItems['file_name'];
						$screenshot = $contenrdir.$scrFiles;
						
						echo"<img src=\"".$screenshot."\" width=\"221\" height=\"191\" class=\"mySlides\">";
					}
					echo"</div>
					  <button class=\"w3-button w3-black w3-display-left\" onclick=\"plusDivs(-1)\"><img src=\"img/btn_left.png\" width=\"19\" height=\"14\" /></button>
					  
					  <button class=\"w3-button w3-black w3-display-right\" onclick=\"plusDivs(1)\"><img src=\"img/btn_right.png\" width=\"19\" height=\"14\" /></button>
				  </div>
					<script>
					var slideIndex = 1;
					showDivs(slideIndex);
					
					function plusDivs(n) {
					  showDivs(slideIndex += n);
					}
					
					function showDivs(n) {
					  var i;
					  var x = document.getElementsByClassName(\"mySlides\");
					  if (n > x.length) {slideIndex = 1}    
					  if (n < 1) {slideIndex = x.length}
					  for (i = 0; i < x.length; i++) {
						 x[i].style.display = \"none\";  
					  }
					  x[slideIndex-1].style.display = \"block\";  
					}
					</script>
				</div>
				</td>
				</tr>";
				}
			}
			?>
            </div>
        </div>
    
    </div>
    
</div>

<script>
function openNav() {
    document.getElementById("mySidenav").style.height = "auto";
	
	document.getElementById("contentWrap").style.marginRight = "80%";
	document.getElementById("contentWrap").style.margin = "auto";
	document.getElementById("box4").style.marginRight = "80%"; 
	/*document.getElementById("nvmg").style.marginRight = "auto";*/
	document.getElementById("proj").style.display = "block";
	/*document.getElementById("block").style.display = "block";
	
	
	*/
}
function closeNav() {
    document.getElementById("mySidenav").style.height = "0";	
   /* document.getElementById("contentWrap").style.margin = "margin-top:200px";*/
    document.getElementById("contentWrap").style.margin = "auto";
    document.getElementById("box4").style.margin = "0";
	/*document.getElementById("nvmg").style.margin = "auto";*/
	document.getElementById("proj").style.display = "none";
	/*document.getElementById("block").style.display = "none";*/
}
</script>
<div id="block">&nbsp;</div>
</body>
</html>