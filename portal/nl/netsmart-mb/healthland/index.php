<?php
$strstatus=0;
if(isset($_GET['status']))
{
$strstatus=$_GET['status'];
}else{$strstatus=0;}

if($strstatus <> 0){

    header("Location: http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/nl/healthland/hl1/errorstatus.php");
    die();
}

session_start();
include('inc.php');
//require('sysprop/filedisp.php');
//require('sysprop/mobiledetect.php');


if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"]; 
	
	if($cat=="videos")
	{
		$vdoImg = "vdo2h.png";
		$tipImg = "tip1.png";
		$appImg = "app1.png";
		$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
	}
	if($cat=="tip")
	{
		$vdoImg = "vdo1h.png";
		$tipImg = "tip2.png";
		$appImg = "app1.png";
			$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
	}
	if($cat=="app")
	{
		$vdoImg = "vdo1h.png";
		$tipImg = "tip1.png";
		$appImg = "app2.png";
			$CatCondition = " and (b.sub_category='Fitness' or b.sub_category='LifeStyle' or b.sub_category='Relaxation')";
	}
}
else
{
	$cat = 'videos';
		$vdoImg = "vdo2h.png";
		$tipImg = "tip1.png";
		$appImg = "app1.png";
		$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
<title>Health Land</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/portal.css" media="screen"  />
<link rel="stylesheet" type="text/css" href="styles.css" media="screen"  />
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<script type="text/javascript" src="js/jquery.js" ></script>
<script  type="text/javascript" language="javascript">
	var hash = (window.location.hash).replace('#', '');
    var str2 = hash != '' ? str2 = hash.substr(0) : str2 = '';

    if(str2 == 40 || str2 == 27){ str2 = '';}
    else{ str2 = '#a' + str2; }
	
	$(document).ready(function(){
	     $('.adCntnr div.acco2:eq(0)').find('div.expand:eq(0)').addClass('openAd').end()
	     .find('div.collapse:gt(0)').hide().end()
	    .find('div.expand').click(function() {
	        $(this).toggleClass('openAd').siblings().removeClass('openAd').end()
	        .next('div.collapse').slideToggle().siblings('div.collapse:visible').slideUp();
	        return false;
	    });
	    
	    if(str2 != '' ){$(str2 + " a").click();}
	})


</script>
<style>
* {
    box-sizing: border-box;
}

.column {
    float: left;
    /*width: 33.33%;*/
    width: 25%;
    padding: 5px;
}


.column img, .item img { max-width: 100% !important; width: auto !important; height: auto !important; }

/* Clearfix (clear floats) */
.row::after {
    content: "";
    clear: both;
    display: table;
}

/* ============================== */
.container {
  list-style:none;
  margin: 0;
  padding: 0;
}


.flex{
  padding: 0;
  margin: 0;
  list-style: none;
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  -webkit-flex-flow: row wrap;
  justify-content: space-around;
  flex-wrap:wrap;
}

#title
{
	padding:10px;
	text-align:center;
	color:#0094ca;
	height:  80px;
	overflow-y: scroll;
}

#title::-webkit-scrollbar { 
    display: none; 
}

#title a
{
	text-decoration:none;
	color:#0094ca;
}



#title a:hover
{
	text-decoration:underline;
}

#block{
    width: 100%;
    height: 100%;
    position: absolute;
    visibility:hidden;
    display:none;
	top:0;
	left:0;
}

#block:target {
    visibility: visible;
    display: block;
}
.row-main{
    max-width: 100% !important;
    padding: 0 !important;
    display: flex !important;
    flex-direction: row !important;
	justify-content: space-around !important;
	
}
.column{
	align-self: center !important;
	vertical-align: middle;
	display: inline-block;
	clear: both;
}


</style>
</head>

<body id="body">
<div id="header">
    <div id="box1">  
        <div style="float:right; padding:25px;"><span onclick="openNav()" style="cursor:pointer"><a href="#block"><i style="color:#837F7A;" id="img" class='fa fa fa-align-justify fa-2x'></i>	</a></span></div>
 		<div > <img src="img/hllogo.png" id="ptslogo" border="0" />  	
        </div>
    </div>
</div>



<div id="mySidenav" class="sidenav">
    <div id="box1"> 
   <div><a href="#" class="closebtn" onclick="closeNav()" style="padding-top:10px; padding-right:18px;"><i style="margin-top: -0.15em; width:125px;font-size:33px;color:#837F7A;" class='fa fa-close'></i></a></div>
        <div><img src="img/hllogo.png" id="ptslogo" border="0" /></div>
    </div>
    <div id="menu_link">
        <a href="?varx=videos"><span>Videos</span></a>        
        <a href="?varx=app"><span>Apps</span></a>
        <a href="?varx=tip"><span>Tips</span></a>
    </div>
</div>


 
<!-- <div style="padding-top:90px; width:80%; margin:auto; text-align:center;">-->
<div class="iconimages">
    <div class="row-main">
      <div class="column"><a href="?varx=videos"><img src='img/<?php echo $vdoImg; ?>' border="0"></a></div>
      <div class="column"><a href="?varx=app"><img src='img/<?php echo $appImg; ?>' border="0"></a></div>
      <div class="column"><a href="?varx=tip" id="tips"><img src='img/<?php echo $tipImg; ?>' border="0"></a></div>
    </div>
</div>
<div id="contentWrap" style="margin-top:auto; ">
	<div class="adCntnr">
    	<div class="acco2">
        	<?php
  	
        	if($cat!="tip")
        	{
			
            $queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");

            if($queryJoin)
            {
                while ($DateRow = mysqli_fetch_assoc($queryJoin))
                {
                    $resCatgID	= $DateRow['id'];
                    $resName= $DateRow['category'];
                    $resSbCatgID= $DateRow['sc_id'];
                    $resSName= $DateRow['sub_category'];
                    $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";
                    
                    if($resSName=="Application")
                    {
                       $resSName = "Antivirus";
                    }
                    
                    echo'<div style="padding:2px;"></div>
					<div id="a'.$resSbCatgID.'" class="expand"><a title="expand/collapse" href="#" style="display: block;">'.ucfirst($resSName).'</a></div>';
                   echo"<div class=\"collapse\">
					<div class=\"accCntnt\" style=\"background-color:#f2f2f2; z-index: 0; position: relative; color:#000000\">
						<ul class=\"container flex\">";
	if(isset($_GET["varx"]))
	{
		$cat = $_GET["varx"]='videos'; 
	}
	else
	{
		$cat = 'videos';
	}
	
	if(!empty($_SESSION['categref']))
	{
		$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
		$categref 	= $_SESSION['categref'];
		$sesCatg 	= $_SESSION['sesCatg'];
		
		$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and $categref order by id desc limit 20");
		$data = array();
		if($getContent)
		{
			while($items = mysqli_fetch_array($getContent))
			{
				$contentID 		= $items['id'];
				$itemTitle 		= $items['title'];
				$description 	= $items['description'];
				$file_name 		= $items['file_name'];
				$original_file_name = $items['original_file_name'];
				$mime 			= $items['mime'];
		  		$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
				$filename = $contenrdir.$file_name;
                $subcat = $items['sub_category_id'];

				$file = pathinfo($file_name, PATHINFO_FILENAME);
				
				
				if($ext=="mp4")
				{
					$thumbimg = $file.'.png'; /*poster="img/play.png"*/
					$preview ='<div style="z-index:2;">
					<video style="z-index:-1;" width="100%" height="100%" controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
							<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
						  </video></div>';
				}
				elseif($ext=="mp3")
				{
					$preview = '<img id="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
				}
				else
				{
					$thumbimg = $file.'.png';

					$preview ='<img id="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';

				}

				echo"<li id=\"item\">		
							<div class=\"item-holder\">				  
							<a style='z-index:100;' href=\"preview.php?varx=$sesCatg&contid=$contentID&subcat=$subcat\"><div style='z-index:2;'>$preview</div>
							<div id=\"title\">$itemTitle</div></a>
							<a href=\"$filename\" id=\"btn\">DOWNLOAD</a>
							</div>
						  </li>";
			}
		}
	}	  
						  
						echo"</ul>                
					</div>
				</div>";
					
                }
            }
			}else{
					
					echo "<div style='padding:2px;' class></div> 
					<div class='expand openAd'>
		  						<a title='expand/collapse' href='#' style='display:block;'>Food</a></div>
					<div id='food' class='collapse'></div>


					<div style='padding:2px;' class></div> 
					<div class='expand openAd'>
		  						<a title='expand/collapse' href='#' style='display:block;'>Sport</a></div>
					<div id='sport' class='collapse'></div>";



					/*echo dirToArray($word_file);*/
					
                 /*   echo'<div style="padding:2px;"></div>
					<div class="expand"><a title="expand/collapse" href="#" style="display: block;">Food</a></div>';
                   echo"<div class=\"collapse\">
					<div class=\"accCntnt\" style=\"background-color:#f2f2f2; color:#000000\">"
						.parseWord($word_file)."</div></div>";
						*/

			}	

            ?>
        </div>
    </div>
</div>

<div style="margin-top:70px;"></div>
	<script>
	function openNav() {
	    document.getElementById("mySidenav").style.height = "auto";
		
		document.getElementById("contentWrap").style.marginRight = "80%";
		document.getElementById("contentWrap").style.margin = "auto";
		document.getElementById("box4").style.marginRight = "80%"; 
		/*document.getElementById("nvmg").style.marginRight = "auto";*/
		document.getElementById("proj").style.display = "block";
		/*document.getElementById("block").style.display = "block";
		
		
		*/
	}
	function closeNav() {
	    document.getElementById("mySidenav").style.height = "0";	
	   /* document.getElementById("contentWrap").style.margin = "margin-top:200px";*/
	    document.getElementById("contentWrap").style.margin = "auto";
	    document.getElementById("box4").style.margin = "0";
		/*document.getElementById("nvmg").style.margin = "auto";*/
		document.getElementById("proj").style.display = "none";
		/*document.getElementById("block").style.display = "none";*/
	}

    /////concatenate url parameter for 'Tip'//////////////
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

	document.addEventListener("DOMContentLoaded", function(){
	    if(getUrlParameter('varx') == 'tip'){
	    	loadXMLDoc('food');
	    	loadXMLDoc('sport');
	    }
	});

	function loadXMLDoc(str) {

		  var xmlhttp = new XMLHttpRequest();
		  xmlhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		      getData(this, str);
		    }
		  };
		  xmlhttp.open("GET", str + "s.xml", true);
		  xmlhttp.send();
		}

	function getData(xml, str) {
		  var i;
		  var xmlDoc = xml.responseXML;
		  var data1 = "";
		  var data2 = "";
		  var img = "";
		  var ext = "";
		  var rate = "";


		  var div = "";
		  var x = xmlDoc.getElementsByTagName("ITEM");
		  for (i = 0; i <x.length; i++) { 
		 
		    data1 = x[i].getElementsByTagName("TITLE")[0].childNodes[0].nodeValue;
		    data2 = x[i].getElementsByTagName("DESCRIPTION")[0].childNodes[0].nodeValue;
		    img = x[i].getElementsByTagName("IMG")[0].childNodes[0].nodeValue;
		    ext = x[i].getElementsByTagName("EXTENSION")[0].childNodes[0].nodeValue;
		    rate = x[i].getElementsByTagName("RATE")[0].childNodes[0].nodeValue;

		    div += "<div class='xml-container " +img + "'><img onclick='display(\"" + img +"\",\"on\")' class='img' src='sysprop/" + str +"/" + img + "."+ ext +"' />" +
		    "<h3 class='tile block'>" + data1 + 
		     "</h3> <a href='#' id='btn' onclick='display(\"" + img +"\",\"on\")'>VIEW MORE</a> </div>" +
    	 	"<div id='" + img + "' class='tips_cnt'>" +
    	 	"<div class='modal-content'> <span class='close' onclick='display(\"" + img +"\",\"off\")'>&times;</span>" +
    		"<div class='pop-image'><div class='wrapper-pop'><img class='img' src='sysprop/" + str +"/" + img + "."+ ext +"' /></div></div>" +
    		"<div class='pop-content'><div class='tile block'>" + data1 +
    		"</div> <br><img class='img-pop' src='img/"+ rate +".png' border='0' />" +
     		"<div class='description block'>" + data2 + "</div></div></div></div>";
		  }

		  document.getElementById(str).innerHTML="<div class='accCntnt' style='background-color:#f2f2f2;''><div class='xml-main'>" + div + "</div></div>";
		}

		var scrl = "";
		 function display(str, str2){
			 if(str2 == 'on'){
			 	scrl = $(window).scrollTop();	
			 }
			 

		 	var modal = document.getElementById(str);
		 	if(str2 == 'on'){
		 		modal.style.display = "block";
		 	}else{
		 		modal.style.display = "none";
		 	}

			 window.onclick = function(event) {
			    if (event.target == modal) {
			        modal.style.display = "none";
			        modal:focus;
			        $(window).scrollTop(scrl);
			    }
		    }
		   	//alert(scrl);
		    $(window).scrollTop(scrl);


		 }

	</script>
<div id="block">&nbsp;</div>
</body>
</html>
