<?php
ob_start();
session_start();
include('connection/inc.php');
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"];
	$sub = $_GET["subcat"];

	if($cat=="videos")
	{
		$vdoImg = "videos_png_active.png";
		$tipImg = "tips_icon.png";
		$appImg = "app_icon.png";
	}
	if($cat=="tip")
	{
		$vdoImg = "videos_png.png";
		$tipImg = "tips_icon_active.png";
		$appImg = "app_icon.png";
	}
	if($cat=="app")
	{
		$vdoImg = "videos_png.png";
		$tipImg = "tips_icon.png";
		$appImg = "app_icon_active.png";

	}

}
else
{
	$cat = 'videos';
	$sub = $_GET["subcat"];
	$vdoImg = "videos_png_active.png";
	$tipImg = "tips_icon.png";
	$appImg = "app_icon.png";
}
$_SESSION['sesCatg'] = $cat;
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
   
	<meta charset="UTF-8">
	<title>Fitcore</title>
</head>
<body>
	<header class="logoMenu">
		<img src="img/bodyfit_logo.png" alt="bodyfit">
	</header>
	
	<main class="mainContainer">
		<div class="prevContainer">
			<section class="prevClose">
	    		<a href="<?php echo './?varx='.$cat.'#'.$sub; ?>" class="preview-close">Close</a>
    		</section>
			<?php
            if(isset($_GET['varx'])){
                $prevSesID = $_GET['contid'];
                $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";

                if($_GET['varx']==""){
                    header("location:index.php");
                }else{
                    $getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
                    $data = array();
                    if($getContent){
                        while($items = mysqli_fetch_array($getContent)){
                            $ContentID 		= $items['id'];
							$itemTitle 		= $items['title'];
							$description 	= $items['description'];
							$contentRate	= $items['rate'];
							$file_name 		= $items['file_name'];
							$original_file_name = $items['original_file_name'];
							$mime 			= $items['mime'];
							$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
							$filename = $contenrdir.$file_name;	
                            $file = pathinfo($file_name, PATHINFO_FILENAME);
                            
                            if($ext=="mp4" || $ext=="mp3"){
                                $thumbimg = $file.'.png';
                                $preview ='<video width="100%"  controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
										<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
									  </video>';
                            }else{
                                $thumbimg = $file.'.png';
                                $preview ='<img src="' .$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
                            }
                        }
                    }
                }
                
            }else{
                header("location:index.php");
			}
			
			if($ext == "mp4"){
				echo"<section class=\"dlSection\">
						<div class=\"dlImage\">$preview</div>
						<div class=\"dlName\"><p>$itemTitle</p></div>
						<div class=\"dlWrapper\">
							<div class=\"dlDownload\">
								<img src=\"img/download icon preview.png\"><a href=\"$filename\">DOWNLOAD</a>
							</div>
						</div>
						<div class=\"dlDescrip\"><p>$description</p></div>
					</section>";
			}else if($ext == "apk"){
				echo"<section class=\"dlSection\">
						<div class=\"apkContainer\">
							<div class=\"dlImage\">$preview</div>
							<div class=\"apkName\">
								<div class=\"dlName\"><p>$itemTitle</p></div>
								<div class=\"dlDownload\">
									<img src=\"img/download icon preview.png\"><a href=\"$filename\">DOWNLOAD</a>
								</div>
							</div>
						</div>
						<div class=\"dlDescrip\"><p>$description</p></div>
					</section>";
			}
        ?>			
			


		</div>
	</main>

    

<script src="js/script.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
  crossorigin="anonymous"></script>      
</body>
</html>