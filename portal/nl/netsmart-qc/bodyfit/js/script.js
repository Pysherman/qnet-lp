/****************  Tabs  ****************/

var tablinks = new Array();
var contentdivs = new Array();

function init(){
	tabs();
}

function tabs(){
	var tablistitems = document.getElementById('tabs').childNodes;
	for(var i = 0; i < tablistitems.length; i++){
		if(tablistitems[i].nodeName == "LI"){
			var tablink = getFirstChildWithTagName( tablistitems[i], 'A');
			var id = getHash( tablink.getAttribute('href'));
			tablinks[id] = tablink;
			contentdivs[id] = document.getElementById(id);
		}
	}

	var i = 0;

	for(var id in tablinks){
		tablinks[id].onclick = showtab;
		tablinks[id].onfocus = function(){this.blur()};
		if(i == 0) tablinks[id].className = 'selected';
		i++;
	}

	var i = 0;

	for(var id in contentdivs){
		if(i != 0)contentdivs[id].className = 'tabContent hide';
		i++;
	}
}

function showtab(){
	var selectedid = getHash(this.getAttribute('href'));

	for(var id in contentdivs){
		if(id == selectedid){
			tablinks[id].className = 'selected';
			contentdivs[id].className = 'tabContent'
		}else{
			tablinks[id].className = '';
			contentdivs[id].className = 'tabContent hide';
		}
	}

	return false;
}

function getFirstChildWithTagName(element, tagName) {
	for (var i = 0; i < element.childNodes.length; i++) {
		if (element.childNodes[i].nodeName == tagName) return element.childNodes[i];
	}
}

function getHash(url) {
	var hashPos = url.lastIndexOf ('#');
	return url.substring(hashPos + 1);
}

/****************  End Tabs  ****************/



// var nav_li = document.querySelectorAll('.links li');
// var nav_burger = document.querySelector('.navBurger');
// var nav_list = document.querySelector('.navList');
// var toggle = true;

// nav_burger.addEventListener('click', DropIt);

// function DropIt(){
// 	if(toggle === true){
// 		nav_list.style.opacity = "1";
// 		nav_burger.style.borderBottomRightRadius = "0px";
// 		nav_burger.style.borderBottomLeftRadius = "0px"; 
// 		nav_li[0].style.transform = "translate(0px, 54px)";
// 		nav_li[1].style.transform = "translate(0px, 54px)";
// 		nav_li[2].style.transform = "translate(0px, 54px)";
// 		toggle = false;
// 	}else{
// 		nav_list.style.opacity = "0";
// 		nav_burger.style.borderBottomRightRadius = "50px";
// 		nav_burger.style.borderBottomLeftRadius = "50px"; 
// 		nav_li[0].style.transform = "translate(0px, 0px)";
// 		nav_li[1].style.transform = "translate(0px, -74px)";
// 		nav_li[2].style.transform = "translate(0px, -124px)";
// 		toggle = true;
// 	}

// 	nav_burger.classList.toggle('togBurger');
// }

