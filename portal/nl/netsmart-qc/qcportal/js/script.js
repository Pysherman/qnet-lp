var url = window.location.href;
var page = url.substr(url.lastIndexOf('/') + 1);

if (page) {
    $('#menu_holder li [href="' + page + '"]').addClass('link_active');
} else if (page) {
    $('#menu_holder li [href=!"' + page + '"]').addClass('link_active');
} else {
    $('#menu_holder li:first').addClass('link_active')
}

