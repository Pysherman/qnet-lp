//dropdown menu
var navButton = document.getElementById('NavButton');
var dropMenu = document.getElementById('NavdropMenu');
var toggle = true;

navButton.addEventListener('click', dropitBoi);

function dropitBoi(){
    if(toggle === true){
        dropMenu.classList.remove('drop-up');
        dropMenu.classList.add('drop-down');
        toggle = false;
    }else{
        dropMenu.classList.remove('drop-down');
        dropMenu.classList.add('drop-up');
        toggle = true;
    }

    navButton.classList.toggle('tog-button');
}
/*
//Sets the links to active state
var navLink = document.getElementById('nav-links');

navLink.addEventListener('click', setActive);

function setActive(e){
    
    var navActive = document.querySelector('.active');

    if(navActive !== null){
        navActive.classList.remove("active");
    }

    e.target.className = "active";
    dropMenu.classList.remove('drop-down');
    dropMenu.classList.add('drop-up');
    toggle = true;

    navButton.classList.toggle('tog-button');
}*/

var NavLinks = new Array();
var contentDivs = new Array();

function init(){
    content();
}

function content(){
    var NavLinksItems = document.getElementById('nav-links').childNodes;
    for(var i = 0; i < NavLinksItems.length; i++){
        if(NavLinksItems[i].nodeName == "LI"){
            var menuLink = getFirstChildWithTagName(NavLinksItems[i], 'A');
            var id = getHash(menuLink.getAttribute('href'));
            NavLinks[id] = menuLink;
            contentDivs[id] = document.getElementById(id);
        }
    }

    var i = 0;

    for(var id in NavLinks){
        NavLinks[id].onclick = showDiv;
        NavLinks[id].onfocus =function(){this.blur()};
        if(i == 0)NavLinks[id].className = 'active';
        i++;
    }

    var i = 0;

    for(var id in contentDivs){
        if(i != 0)contentDivs[id].className = 'con con-invi';
        i++;
    }
}

function showDiv(){
    var selectedId = getHash(this.getAttribute('href'));

    for(var id in contentDivs){
        if(id === selectedId){
            NavLinks[id].className = 'active';
            contentDivs[id].className = 'con';

        if(window.innerWidth < 900){
            dropMenu.classList.remove('drop-down');
            dropMenu.classList.add('drop-up');
            toggle = true;

            navButton.classList.toggle('tog-button');
        }
        }else{
            NavLinks[id].className = '';
            contentDivs[id].className = 'con con-invi';
        }
    }

    return false;
}

function getFirstChildWithTagName(element, tagName){
    for(var i=0;i<element.childNodes.length;i++){
        if(element.childNodes[i].nodeName == tagName) return element.childNodes[i];
    }
}

function getHash(url){
    var hashPos = url.lastIndexOf ('#');
    return url.substring(hashPos + 1);
}


