<?php
$strstatus=0;
	if(isset($_GET['status'])){
		$strstatus=$_GET['status'];
	}else{
		$strstatus=0;
	}

if($strstatus <> 0){

    header("Location: http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/nl/healthland/hl1/errorstatus.php");
    die();
}

session_start();
include('inc.php');

if(isset($_GET["varx"])){

	$cat = $_GET["varx"]; 
	
	if($cat=="videos"){
		$vdoImg = "videos_active.png";
		$tipImg = "tips.png";
		$appImg = "apps.png";
		$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
	}

	if($cat=="tip"){
		$vdoImg = "videos.png";
		$tipImg = "tips_active.png";
		$appImg = "apps.png";
		$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
	}

	if($cat=="app"){
		$vdoImg = "videos.png";
		$tipImg = "tips.png";
		$appImg = "apss_active.png";
		$CatCondition = " and (b.sub_category='Fitness' or b.sub_category='LifeStyle' or b.sub_category='Relaxation')";
	}

}else{
	$cat = 'videos';
	$vdoImg = "videos_active.png";
	$tipImg = "tips.png";
	$appImg = "apps.png";
	$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"></html>
<head>
    <meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/jquery.js" ></script>
	<script  type="text/javascript" language="javascript">
		var hash = (window.location.hash).replace('#', '');
    	var str2 = hash != '' ? str2 = hash.substr(0) : str2 = '';

    	if(str2 == 40 || str2 == 27){
			str2 = '';
		}else{ 
			str2 = '#a' + str2;
		}
	
	$(document).ready(function(){
	     $('.adCntnr div.acco2:eq(0)').find('div.expand:eq(0)').addClass('openAd').end()
	     .find('div.collapse:gt(0)').hide().end()
	    .find('div.expand').click(function() {
	        $(this).toggleClass('openAd').siblings().removeClass('openAd').end()
	        .next('div.collapse').slideToggle().siblings('div.collapse:visible').slideUp();
	        return false;
	    });
	    
	    if(str2 != '' ){
			$(str2 + " a").click();
		}
	})


</script>
    <title>FITWORLD</title>
</head>
<body>
	<section class="header-nav">
		<div class="nav-burger" id="NavBurger">
			<span></span>
			<span></span>
			<span></span>
		</div>
		<img src="img/fitworld_logo.png" alt="FITWORLD">
	</section>
	<section class="side-nav" id="sideNav">
		<div class="logo">
			<img src="img/fitworld_logo.png" alt="">
		</div>
		<ul class="nav-link">
			<li><a href="?varx=videos"><img src="img/<?php echo $vdoImg; ?>" alt=""></a></li>
			<li><a href="?varx=app"><img src="img/<?php echo $appImg; ?>" alt=""></a></li>
			<li><a href="?varx=tip" id="tips"><img src="img/<?php echo $tipImg; ?>" alt=""></a></li>
		</ul>
	</section>

<!--TEST-->

<div id="contentWrap">
	<div class="adCntnr">
    	<div class="acco2">
        	<?php
        	if($cat!="tip"){
            	$queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition and category like '$cat%'");

            	if($queryJoin){
                	while ($DateRow = mysqli_fetch_assoc($queryJoin)){
                    	$resCatgID	= $DateRow['id'];
                    	$resName= $DateRow['category'];
                    	$resSbCatgID= $DateRow['sc_id'];
                    	$resSName= $DateRow['sub_category'];
                    	$_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";
                    
                    	if($resSName=="Application"){
                       		$resSName = "Antivirus";
                    	}
						
						echo'<div id="a'.$resSbCatgID.'" class="expand">
								<a title="expand/collapse" href="#">'.ucfirst($resSName).'</a>
							</div>';

                   		echo"<div class=\"collapse\">
								<div class=\"accCntnt\">
									<ul class=\"container\">";
										
										if(isset($_GET["varx"])){
											$cat = $_GET["varx"]='videos'; 
										}else{
											$cat = 'videos';
										}
										
										if(!empty($_SESSION['categref'])){
											$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
											$categref 	= $_SESSION['categref'];
											$sesCatg 	= $_SESSION['sesCatg'];
											
											$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and $categref order by id desc limit 20");
											$data = array();
											if($getContent){

												while($items = mysqli_fetch_array($getContent)){

													$contentID 		= $items['id'];
													$itemTitle 		= $items['title'];
													$description 	= $items['description'];
													$file_name 		= $items['file_name'];
													$original_file_name = $items['original_file_name'];
													$mime 			= $items['mime'];
													$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
													$filename = $contenrdir.$file_name;
													$subcat = $items['sub_category_id'];

													$file = pathinfo($file_name, PATHINFO_FILENAME);
													
													
													if($ext=="mp4"){

														$thumbimg = $file.'.png'; 
														$preview ='<div>
																		<video width="100%" height="100%" controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
																			<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
																		</video>
																	</div>';

													}else if($ext=="mp3"){

														$preview = '<img id="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
													
													}else{
														
														$thumbimg = $file.'.png';

														$preview ='<img id="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
													}

													echo"<li id=\"item\">		
															<div class=\"item-holder\">	
																<div class=\"thumb\">		  
																	<a href=\"preview.php?varx=$sesCatg&contid=$contentID&subcat=$subcat\">
																		$preview
																	</a>
																</div>
																<div class=\"name-DL\">
																	<div id=\"title\">$itemTitle</div>
																	<a href=\"$filename\" id=\"btn\">DOWNLOAD</a>
																</div>
															</div>
														</li>";
												}
											}
										}	  
						  
									echo"</ul>                
								</div>
							</div>";
					
                }
            }
			}else{
					
					echo "	<div class='expand openAd'>
								  <a title='expand/collapse' href='#'>Food</a>
							</div>
							<div id='food' class='collapse'></div>

							<div class='expand openAd'>
								  <a title='expand/collapse' href='#'>Sport</a>
							</div>
							<div id='sport' class='collapse'></div>";
			}	

            ?>
        </div>
    </div>
</div>

	<script>
    /////concatenate url parameter for 'Tip'//////////////
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

	document.addEventListener("DOMContentLoaded", function(){
	    if(getUrlParameter('varx') == 'tip'){
	    	loadXMLDoc('food');
	    	loadXMLDoc('sport');
	    }
	});

	function loadXMLDoc(str) {

		  var xmlhttp = new XMLHttpRequest();
		  xmlhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		      getData(this, str);
		    }
		  };
		  xmlhttp.open("GET", str + "s.xml", true);
		  xmlhttp.send();
		}

	function getData(xml, str) {
		  var i;
		  var xmlDoc = xml.responseXML;
		  var data1 = "";
		  var data2 = "";
		  var img = "";
		  var ext = "";


		  var div = "";
		  var x = xmlDoc.getElementsByTagName("ITEM");
		  for (i = 0; i <x.length; i++) { 
		 
		    data1 = x[i].getElementsByTagName("TITLE")[0].childNodes[0].nodeValue;
		    data2 = x[i].getElementsByTagName("DESCRIPTION")[0].childNodes[0].nodeValue;
		    img = x[i].getElementsByTagName("IMG")[0].childNodes[0].nodeValue;
		    ext = x[i].getElementsByTagName("EXTENSION")[0].childNodes[0].nodeValue;

		    div += "<div class='xml-container " +img + "'><div class='tip-image'><img onclick='display(\"" + img +"\",\"on\")' class='img' src='sysprop/" + str +"/" + img + "."+ ext +"' /></div>" +
		    "<div class='tip-name'><h3 class='tile block'>" + data1 + 
		    "</h3> <a href='#' id='btn' onclick='display(\"" + img +"\",\"on\")'>VIEW MORE</a> </div></div>" +
			 
			"<div id='" + img + "' class='tips_cnt'>" +
    	 	"<div class='modal-content'> <div class='modal-close' onclick='display(\"" + img +"\",\"off\")'><span></span><span></span></div>" +
    		"<div class='pop-image'><div class='wrapper-pop'><img class='img' src='sysprop/" + str +"/" + img + "."+ ext +"' /></div></div>" +
    		"<div class='pop-content'><div class='tile block'>" + data1 +
    		"</div>" +
     		"<div class='description block'>" + data2 + "</div></div></div></div>";
		  }

		  document.getElementById(str).innerHTML="<div class='accCntnt'><div class='xml-main'>" + div + "</div></div>";
		}

		var scrl = "";
		 function display(str, str2){
			 if(str2 == 'on'){
			 	scrl = $(window).scrollTop();	
			 }
			 

		 	var modal = document.getElementById(str);
		 	if(str2 == 'on'){
		 		modal.style.display = "block";
		 	}else{
		 		modal.style.display = "none";
		 	}

			 window.onclick = function(event) {
			    if (event.target == modal) {
			        modal.style.display = "none";
			        modal:focus;
			        $(window).scrollTop(scrl);
			    }
		    }
		   	//alert(scrl);
		    $(window).scrollTop(scrl);


		 }

	</script>

	<script type="text/javascript" src="js/sideNav.js" ></script>
</body>