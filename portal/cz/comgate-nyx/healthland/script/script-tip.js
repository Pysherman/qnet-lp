   /////concatenate url parameter for 'Tip'//////////////
   var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

document.addEventListener("DOMContentLoaded", function(){
    if(getUrlParameter('varx') == 'tip'){
        loadXMLDoc('food');
        //loadXMLDoc('sport');
    }
});

function loadXMLDoc(str) {

      var xmlhttp = new XMLHttpRequest();
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          getData(this, str);
        }
      };
      xmlhttp.open("GET", str + "s.xml", true);
      xmlhttp.send();
    }

function getData(xml, str) {
      var i;
      var xmlDoc = xml.responseXML;
      var data1 = "";
      var data2 = "";
      var img = "";
      var ext = "";
      var subCat = "";


      var div = "";
      var x = xmlDoc.getElementsByTagName("ITEM");
      for (i = 0; i <x.length; i++) { 
     
        data1 = x[i].getElementsByTagName("TITLE")[0].childNodes[0].nodeValue;
        data2 = x[i].getElementsByTagName("DESCRIPTION")[0].childNodes[0].nodeValue;
        img = x[i].getElementsByTagName("IMG")[0].childNodes[0].nodeValue;
        ext = x[i].getElementsByTagName("EXTENSION")[0].childNodes[0].nodeValue;
        subCat = x[i].getElementsByTagName("SUBCATEGORY")[0].childNodes[0].nodeValue;

        // div += "<div class='article'><div class='thumbImage " +img + "'><img onclick='display(\"" + img +"\",\"on\")' class='thumbnail' src='sysprop/" + str +"/" + img + "."+ ext +"' /></div>" +
        // "<div class='thumbName'><a class='title'>" + data1 + 
        // "</a> <a href='#' class='btn' onclick='display(\"" + img +"\",\"on\")'>VIEW MORE</a> </div></div>";

        div += `<div class="item ${str}" data-itemid="${subCat}">
                    <div id="preview">
                        <a href="#"><img src="sysprop/${str}/${img}.${ext}" /></a>
                    </div>
                    <a href="#" class="tip-title">${data1}</a>
                    <p>${subCat}</p>
                    <p class="tip-discrip">${data2}</p>
                </div>`;
      }
      document.querySelector('.sect-content').innerHTML = div;

      const getAllContent = document.querySelectorAll('.sect-content .item');
      const getSubCategory = document.querySelectorAll('.sub-category .check-box');
      const getAllCategory = document.getElementById('All');
      const getSectContent = document.querySelector('.sect-content');
      const getPreview = document.querySelector('.sect-preview');
      const styleCSS = document.styleSheets;
      
      if(getPreview.firstElementChild == null){
          getPreview.style.display = "none";
      }else{
          getPreview.style.display = "grid";
          getPreview.style.transform ="scale(1)";
      }
      
      if(getSubCategory.length <= 2){
          getSubCategory[0].classList.add('none');
      }
      
      let getTimeDuration = () => {
          for(let i = 0; i < styleCSS.length; i++){
              if(styleCSS[i].href.includes("style.css")){
                  let styleCSSRule = styleCSS[i].cssRules;
                  for(let i = 0; i < styleCSSRule.length; i++){
                      if(styleCSSRule[i].selectorText.includes(".sect-content .item")){
                          return styleCSSRule[i].style.getPropertyValue('transition-duration');
                      }
                  }
              }
          }
      };
      
      let randomizeItems = () => {
          for(let i = 0; i < getAllContent.length; i++){
              let randomNum =  Math.floor(Math.random() * getAllContent.length);
              getSectContent.appendChild(getAllContent[randomNum]);
          } 
      };
      
      let removeContents = (getTimeDuration, idOfCurrentEvent) => {
          const removeMS = parseInt(getTimeDuration.replace("ms", ""));
          getAllContent.forEach(content => {
              //if(content.classList.contains(idOfCurrentEvent))
              if(content.getAttribute('data-itemid') == idOfCurrentEvent){
                  content.classList.add('fade-out');
                  content.classList.remove('fade-in');
                  setTimeout( () => {
                      content.classList.add('none');
                  }, removeMS);
              }else if(idOfCurrentEvent == "All"){
                  content.classList.add('fade-out');
                  setTimeout( () => {
                      content.classList.add('none');
                  }, removeMS);
              }
          });
      };
      
      let displayContents = content => {
          content.classList.remove('none');
          setTimeout(() => {
              content.classList.remove('fade-out');
          }, 200);
      };
      
      let customizedAllCheckbox = (booleanValue, colorHex) => {
          getSubCategory[0].style.color = colorHex;
          getSubCategory.forEach(subCat => {
              subCat.firstElementChild.checked = booleanValue;
              subCat.style.color = colorHex;
          });
      };
      
      let subCatFunc = subCategory => {
      
          subCategory.firstElementChild.checked = true;
      
          subCategory.addEventListener('click', e => {
              let idOfCurrentEvent = e.currentTarget.getAttribute('data-id');
              if(e.currentTarget.firstElementChild.checked == false){
                  if(idOfCurrentEvent == "All"){
                      removeContents(getTimeDuration(), idOfCurrentEvent);
                      customizedAllCheckbox(false, "#ccc");
                  }else{
                      removeContents(getTimeDuration(), idOfCurrentEvent);
                      getSubCategory[0].firstElementChild.checked = false;
                      e.currentTarget.style.color = "#ccc";
                      getSubCategory[0].style.color = "#ccc";
      
                  }
              }else{
                  if(idOfCurrentEvent == "All"){
                      getAllContent.forEach(content => {
                          displayContents(content);
                      });
                      customizedAllCheckbox(true, "#000");
                  }else{
                      getAllContent.forEach(content => {
                          //if(content.classList.contains(idOfCurrentEvent))
                          if(content.getAttribute('data-itemid') == idOfCurrentEvent){
                              displayContents(content);
                          }
                      });
                      e.currentTarget.style.color = "#000";
                      let arrayTrue = [];
                      let subCatlength = getSubCategory.length - 1;
                      getSubCategory.forEach(subCat => {
                          if(subCat.firstElementChild.checked == true){
                              arrayTrue.push("true");
                          }
                      });
                      if(arrayTrue.length == subCatlength){
                          getSubCategory[0].firstElementChild.checked = true;
                          getSubCategory[0].style.color = "#000";
                      }
                  }
              }
          });
      };

      let displayOnPreview = itemContent => {
        const getPreview = document.querySelector('.sect-preview');
        itemContent.addEventListener('click', e => {
            while(getPreview.firstElementChild){
                getPreview.removeChild(getPreview.firstElementChild);
            }
            let createTipImageDiv = document.createElement('div');
            let getCurrentImage = e.currentTarget.firstElementChild.firstElementChild.firstElementChild.cloneNode(true);
            createTipImageDiv.setAttribute('class', 'preview-image');
            createTipImageDiv.appendChild(getCurrentImage);
            getPreview.appendChild(createTipImageDiv);

            let createTipDetailsDiv = document.createElement('div');
            let createH3 = document.createElement('h3')
            let createP = document.createElement('p');
            let getTitle = e.currentTarget.querySelector('.tip-title').innerHTML;
            let getDiscrip = e.currentTarget.querySelector('.tip-discrip').innerHTML
            createH3.innerHTML = getTitle;
            createP.innerHTML = getDiscrip;
            createTipDetailsDiv.appendChild(createH3);
            createTipDetailsDiv.appendChild(createP);
            createTipDetailsDiv.setAttribute('class', 'preview-details');
            getPreview.appendChild(createTipDetailsDiv);

            getPreview.style.display = "grid";
            getPreview.style.transform ="scale(1)";
        });
    };

      getSubCategory.forEach(subCatFunc);
      window.addEventListener('load', randomizeItems);
        getAllContent.forEach(displayOnPreview);
    }
