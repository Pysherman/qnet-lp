<!DOCTYPE html>
<?php require('connection/header.php');?>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style/style.css" />
    <title>Healthland</title>
</head>
<body>
    <header class="header">
        <section class="header-desktop-wrapper">
            <img class="logo" src="img/logo.png" alt="Healthland Logo">
            <div class="menu-wrapper">
                <div class="desktop-menu active-list">
                    <ul>
                        <li><a href="?varx=videos">VIDEOS</a></li>
                        <li><a href="?varx=app">APPS</a></li>
                        <li><a href="?varx=tip">TIPS</a></li>
                    </ul>
                </div>
                <div class="mobile-menu-burger" id="NavBgrBtn">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </section>
        <section class="header-mobile-wrapper" id="dropDown">
            <div class="mobile-menu active-list">
                <ul>
                    <li><a href="?varx=videos">VIDEOS</a></li>
                    <li><a href="?varx=app">APPS</a></li>
                    <li><a href="?varx=tip">TIPS</a></li>
                </ul>
            </div> 
        </section>
    </header>
    <main class="main-container">
        <section class="sect-preview">
        <?php
        if(isset($_GET['varx'])){	
            if(isset($_GET['contid'])){
                $prevSesID = $_GET['contid'];
                $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
            

                $getContentPreview = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
                $data = array();

                if($getContentPreview){
                    while($items = mysqli_fetch_array($getContentPreview)){
                        $ContentID 		= $items['id'];
                        $itemTitle 		= $items['title'];
                        $description 	= $items['description'];
                        $contentRate	= $items['rate'];
                        $file_name 		= $items['file_name'];
                        $original_file_name = $items['original_file_name'];
                        $mime 			= $items['mime'];
                        $ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
                        $filename = $contenrdir.$file_name;
                        
                        $file = pathinfo($file_name, PATHINFO_FILENAME);

                        if($ext == "mp4" || $ext == "mp3"){
                            $thumbimg = $file.'.png';
                            $preview ='<video width="100%" height="200"  controls preload="metadata">
                                    <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
                                    </video>';
                        }else{
                            $thumbimg = $file.'.png';
                            $preview ='<img src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
                        }
                    }

                    echo '<div class="preview-image">'.$preview.'</div>
                          <div class="preview-details">
                                <h3>'.$itemTitle.'</h3>
                                <a href="'.$filename.'" class="dl-btn">DOWNLOAD</a>
                                <p>'.$description.'</p>
                          </div>';
                }
            }else{
                error_reporting(0);
            }
        }else{
            error_reporting(0);
        } 
        ?>
        </section>
        <section class="sect-tag">
            <ul class="sub-category">
                <li class="check-box" data-id="All"><input type="checkbox"><label for="All">All</label></li>
                <?php
                    if($cat != "tip"){
                        $queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");

                        if($queryJoin){
                            while($DateRow = mysqli_fetch_assoc($queryJoin)){
                                $resCatgID = $DateRow['id'];
                                $resName= $DateRow['category'];
                                $resSbCatgID= $DateRow['sc_id'];
                                $resSName= $DateRow['sub_category'];
                                $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";

                                if($resSName=="Application"){
                                    $resSName = "Antivirus";
                                }

                                echo '<li class="check-box" data-id="'.$resSName.'"><input type="checkbox"><label for="'.$resSName.'">'.$resSName.'</label></li>';
                            }
                        }
                    }else{
                        echo '<li class="check-box" data-id="Food"><input type="checkbox"><label for="Food">Food</label></li>';
                        echo '<li class="check-box" data-id="Sport"><input type="checkbox"><label for="Sport">Sport</label></li>';
                    }
                ?>
            </ul>
        </section>
        <section class="sect-content">
        <?php
            if($cat != "tip"){
                $queryJoinContent = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");

                if($queryJoinContent){
                    while ($DateRow = mysqli_fetch_assoc($queryJoinContent)){
                        $resCatgID	= $DateRow['id'];
                        $resName= $DateRow['category'];
                        $resSbCatgID= $DateRow['sc_id'];
                        $resSName= $DateRow['sub_category'];
                        $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";
                        
                        if(isset($_GET["varx"])){
                            $cat = $_GET["varx"]='videos'; 
                        }else{
                            $cat = 'videos';
                        }
                        
                        if(!empty($_SESSION['categref'])){
                            $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
                            $categref 	= $_SESSION['categref'];
                            $sesCatg 	= $_SESSION['sesCatg'];
                
                            $getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and $categref order by id desc limit 20");
        
                            if($getContent){
                                while($items = mysqli_fetch_array($getContent)){
                                    $contentID 		= $items['id'];
                                    $itemTitle 		= $items['title'];
                                    $description 	= $items['description'];
                                    $file_name 		= $items['file_name'];
                                    $original_file_name = $items['original_file_name'];
                                    $mime 			= $items['mime'];
                                    $ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
                                    $filename = $contenrdir.$file_name;
                                    $subcat = $items['sub_category_id'];

                                    $file = pathinfo($file_name, PATHINFO_FILENAME);
                                    
                                    if($ext == "mp4"){
                                        $thumbimg = $file.'.png';
                                        $preview ='<video width="100%" height="100"  controls preload="metadata">
                                            <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
                                        </video>';
                                    }else if($ext == "mp3"){
                                        $preview = '<img class="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
                                    }else{
                                        $thumbimg = $file.'.png';
                                        $preview ='<img class="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
                                    }
                                    
                                    
                                        echo'<div class="item '.$resName.'" data-itemid="'.$resSName. '">
                                                <div id="preview">
                                                    <a href="?varx='.$sesCatg.'&contid='.$contentID.'">'.$preview.'</a>
                                                </div>
                                                <a href="?varx='.$sesCatg.'&contid='.$contentID.'">'.$itemTitle.'</a>
                                                <p>'.$resSName.'</p>
                                            </div>';
                                    
                                }
                            }
                        }
                    }
                }
            }else
        ?>
        </section>
    </main>
    <script type="text/javascript" src="script/script-tip.js"></script>
    <script type="text/javascript" src="script/script-main.js"></script>
    <script type="text/javascript" src="script/script-nav.js"></script>
</body>
</html>