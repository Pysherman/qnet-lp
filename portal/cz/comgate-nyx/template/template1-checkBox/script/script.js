const getAllContent = document.querySelectorAll('.sect-content .item');
const getSubCategory = document.querySelectorAll('.sub-category .check-box');
const getAllCategory = document.getElementById('All');
const getSectContent = document.querySelector('.sect-content');
const styleCSS = document.styleSheets;

if (getSubCategory.length <= 2) {
    getSubCategory[0].classList.add('none');
}

let getTimeDuration = () => {
    for (let i = 0; i < styleCSS.length; i++) {
        if (styleCSS[i].href.includes("style.css")) {
            let styleCSSRule = styleCSS[i].cssRules;
            for (let i = 0; i < styleCSSRule.length; i++) {
                if (styleCSSRule[i].selectorText.includes(".sect-content .item")) {
                    return styleCSSRule[i].style.getPropertyValue('transition-duration');
                }
            }
        }
    }
};

let randomizeItems = () => {
    for (let i = 0; i < getAllContent.length; i++) {
        let randomNum = Math.floor(Math.random() * getAllContent.length);
        getSectContent.appendChild(getAllContent[randomNum]);
    }
};

let removeContents = (getTimeDuration, idOfCurrentEvent) => {
    const removeMS = parseInt(getTimeDuration.replace("ms", ""));
    getAllContent.forEach(content => {
        //if(content.classList.contains(idOfCurrentEvent))
        if (content.getAttribute('data-itemid') == idOfCurrentEvent) {
            content.classList.add('fade-out');
            content.classList.remove('fade-in');
            setTimeout(() => {
                content.classList.add('none');
            }, removeMS);
        } else if (idOfCurrentEvent == "All") {
            content.classList.add('fade-out');
            setTimeout(() => {
                content.classList.add('none');
            }, removeMS);
        }
    });
};

let displayContents = content => {
    content.classList.remove('none');
    setTimeout(() => {
        content.classList.remove('fade-out');
    }, 300);
};

let customizedAllCheckbox = (booleanValue, colorHex) => {
    getSubCategory[0].style.color = colorHex;
    getSubCategory.forEach(subCat => {
        subCat.firstElementChild.checked = booleanValue;
        subCat.style.color = colorHex;
    });
};

let subCatFunc = subCategory => {

    subCategory.firstElementChild.checked = true;

    subCategory.addEventListener('click', e => {
        let idOfCurrentEvent = e.currentTarget.getAttribute('data-id');
        if (e.currentTarget.firstElementChild.checked == false) {
            if (idOfCurrentEvent == "All") {
                removeContents(getTimeDuration(), idOfCurrentEvent);
                customizedAllCheckbox(false, "#ccc");
            } else {
                removeContents(getTimeDuration(), idOfCurrentEvent);
                getSubCategory[0].firstElementChild.checked = false;
                e.currentTarget.style.color = "#ccc";
                getSubCategory[0].style.color = "#ccc";
            }
        } else {
            if (idOfCurrentEvent == "All") {
                getAllContent.forEach(content => {
                    displayContents(content);
                });
                customizedAllCheckbox(true, "#000");
            } else {
                getAllContent.forEach(content => {
                    //if(content.classList.contains(idOfCurrentEvent))
                    if (content.getAttribute('data-itemid') == idOfCurrentEvent) {
                        displayContents(content);
                    }
                });
                e.currentTarget.style.color = "#000";
                let arrayTrue = [];
                let subCatlength = getSubCategory.length - 1;
                getSubCategory.forEach(subCat => {
                    if (subCat.firstElementChild.checked == true) {
                        arrayTrue.push("true");
                    }
                });
                if (arrayTrue.length == subCatlength) {
                    getSubCategory[0].firstElementChild.checked = true;
                    getSubCategory[0].style.color = "#000";
                }
            }
        }
    });
};
getSubCategory.forEach(subCatFunc);
window.addEventListener('load', randomizeItems);