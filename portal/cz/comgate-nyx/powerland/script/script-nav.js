const getMenuBgrBtn = document.getElementById('NavBgrBtn');
const getDropDown = document.getElementById('dropDown');
const getLinks = document.querySelectorAll('.active-list ul li a');
const getCloseBtn = document.querySelector('.close-btn');
const getMainContainer = document.querySelector('.main-container');
let queryHrefGames = document.querySelectorAll('[href = "?varx=games"]');
//const getApkHref = document.querySelector('[href="?varx=games-apk"]') || document.querySelector('[href="./?varx=games-apk"]');
let toggleSwitcher = 1;

//getApkHref.classList.add('active');

queryHrefGames.forEach(hrefGames => {
	hrefGames.parentElement.classList.add('active');
});

let addActive = () => {
	let current = document.location.href;
	
	getLinks.forEach(getLink => {
		let getHref = getLink.getAttribute('href');
		if(current.includes(getHref)){
			let queryHref = document.querySelectorAll('[href = "'+ getHref +'"]');
			
			queryHrefGames.forEach(hrefGames => {
				hrefGames.parentElement.classList.remove('active');
			});

			queryHref.forEach(href => {
				/* if(href.getAttribute('href') != getApkHref.getAttribute('href'))
					getApkHref.classList.remove('active'); */
				href.parentElement.classList.add('active');
			});

		}
		
		
		/* else if(!current.includes(getHref)){
			let removeChars = getHref.slice(2);
			if(current.includes(removeChars)){
				if(getHref.includes(removeChars)){
					let queryHref = document.querySelectorAll('[href = "'+ getHref +'"]');
					getCloseBtn.classList.remove('active');
					queryHref.forEach(href => {
						if(href.getAttribute('href') != getApkHref.getAttribute('href'))
							getApkHref.classList.remove('active');
						href.classList.add('active');
					});
				}
			}
		} */
	});	
};

let windowSize = () => {
	if(window.innerWidth >= 758){
		getDropDown.classList.remove('slide-left');
		getDropDown.classList.add('slide-right');
		getMainContainer.style.cssText = "right:0px;"
		setTimeout(() => {
            getDropDown.style.cssText = "display:none;";
        }, 800)
		toggleSwitcher = 1;
		getMenuBgrBtn.classList.remove('toggle-burger');
	}
};

windowSize();

let showDropDown = () => {
	if(toggleSwitcher === 1){
		getDropDown.style.cssText = "display:block;";
		getDropDown.classList.add('slide-left');
		getDropDown.classList.remove('slide-right');
		toggleSwitcher = 0;
		getMainContainer.style.cssText = "right:200px;"
	}else if(toggleSwitcher === 0){
		getDropDown.classList.remove('slide-left');
		getDropDown.classList.add('slide-right');
		toggleSwitcher = 1;
        getMainContainer.style.cssText = "right:0px;"
        setTimeout(() => {
            getDropDown.style.cssText = "display:none;";
        }, 800)
	}
	getMenuBgrBtn.classList.toggle('toggle-burger');
};

window.addEventListener('resize', windowSize)
window.addEventListener('load', addActive);
getMenuBgrBtn.addEventListener('click', showDropDown);