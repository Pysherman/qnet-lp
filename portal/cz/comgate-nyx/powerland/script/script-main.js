const getAllContent = document.querySelectorAll('.sect-content .item');
const getSubCategory = document.querySelectorAll('.sub-category .check-box');
const getAllCategory = document.getElementById('All');
const getSectContent = document.querySelector('.sect-content');
const getPreview = document.querySelector('.sect-preview');
const styleCSS = document.styleSheets;

if(getPreview.firstElementChild == null){
    getPreview.style.display = "none";
}else{
    getPreview.style.display = "grid";
    getPreview.style.transform ="scale(1)";
}

if(getSubCategory.length <= 2){
    getSubCategory[0].classList.add('none');
}

let getTimeDuration = () => {
    for(let i = 0; i < styleCSS.length; i++){
        if(styleCSS[i].href.includes("style.css")){
            let styleCSSRule = styleCSS[i].cssRules;
            for(let i = 0; i < styleCSSRule.length; i++){
                if(styleCSSRule[i].selectorText.includes(".sect-content .item")){
                    return styleCSSRule[i].style.getPropertyValue('transition-duration');
                }
            }
        }
    }
};

let randomizeItems = () => {
    for(let i = 0; i < getAllContent.length; i++){
        let randomNum =  Math.floor(Math.random() * getAllContent.length);
        getSectContent.appendChild(getAllContent[randomNum]);
    } 
};

let removeContents = (getTimeDuration, idOfCurrentEvent) => {
    const removeMS = parseInt(getTimeDuration.replace("ms", ""));
    getAllContent.forEach(content => {
        //if(content.classList.contains(idOfCurrentEvent))
        if(content.getAttribute('data-itemid') == idOfCurrentEvent){
            content.classList.add('fade-out');
            content.classList.remove('fade-in');
            setTimeout( () => {
                content.classList.add('none');
            }, removeMS);
        }else if(idOfCurrentEvent == "All"){
            content.classList.add('fade-out');
            setTimeout( () => {
                content.classList.add('none');
            }, removeMS);
        }
    });
};

let displayContents = content => {
    content.classList.remove('none');
    setTimeout(() => {
        content.classList.remove('fade-out');
    }, 200);
};

let customizedAllCheckbox = (booleanValue, colorHex) => {
    getSubCategory[0].style.color = colorHex;
    getSubCategory.forEach(subCat => {
        subCat.firstElementChild.checked = booleanValue;
        subCat.lastElementChild.style.color = colorHex;
    });
};

let subCatFunc = subCategory => {

    subCategory.firstElementChild.checked = true;

    subCategory.addEventListener('click', e => {
        let idOfCurrentEvent = e.currentTarget.getAttribute('data-id');
        if(e.currentTarget.firstElementChild.checked == false){
            if(idOfCurrentEvent == "All"){
                removeContents(getTimeDuration(), idOfCurrentEvent);
                customizedAllCheckbox(false, "#ccc");
            }else{
                removeContents(getTimeDuration(), idOfCurrentEvent);
                getSubCategory[0].firstElementChild.checked = false;
                e.currentTarget.lastElementChild.style.color = "#ccc";
                getSubCategory[0].lastElementChild.style.color = "#ccc";

            }
        }else{
            if(idOfCurrentEvent == "All"){
                getAllContent.forEach(content => {
                    displayContents(content);
                });
                customizedAllCheckbox(true, "#FFDD00");
            }else{
                getAllContent.forEach(content => {
                    //if(content.classList.contains(idOfCurrentEvent))
                    if(content.getAttribute('data-itemid') == idOfCurrentEvent){
                        displayContents(content);
                    }
                });
                e.currentTarget.lastElementChild.style.color = "#FFDD00";
                let arrayTrue = [];
                let subCatlength = getSubCategory.length - 1;
                getSubCategory.forEach(subCat => {
                    if(subCat.firstElementChild.checked == true){
                        arrayTrue.push("true");
                    }
                });
                if(arrayTrue.length == subCatlength){
                    getSubCategory[0].firstElementChild.checked = true;
                    getSubCategory[0].lastElementChild.style.color = "#FFDD00";
                }
            }
        }
    });
};
getSubCategory.forEach(subCatFunc);
window.addEventListener('load', randomizeItems);