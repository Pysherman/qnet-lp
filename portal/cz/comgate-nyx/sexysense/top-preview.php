<?php
   if(isset($_GET['varx']))
   {
   $prevSesID = @$_GET['contid'];
   $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
   
   if($_GET['varx']=="")
   {
   header("location:index.php");
   }
   else
   {
   $getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
   $data = array();
   if($getContent)
   {
   while($items = mysqli_fetch_array($getContent))
   {
   $ContentID 		= $items['id'];
   $itemTitle 		= $items['title'];
   $description 	= $items['description'];
   $contentRate	= $items['rate'];
   $file_name 		= $items['file_name'];
   $original_file_name = $items['original_file_name'];
   $mime 			= $items['mime'];
   $ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
   $filename = $contenrdir.$file_name;
   
   $file = pathinfo($file_name, PATHINFO_FILENAME);
   
   if($ext=="mp4" || $ext=="mp3")
   {
   $thumbimg = $file.'.png';
   $preview ='<div class="preview-content-item-vids">
      <video width="40%"  controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
      <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
      </video>
      <div id="title-preview-vids">
      <div>'.$itemTitle.'</div>
      <div ><a  href="'.$filename.'" id="btn-preview"> 
      <span>Download</span></a></div>
      </div>
      <?php echo stripslashes($description); ?>
   </div>';
   }
   else
   {
   $thumbimg = $file.'.png';
   $preview ='
   <div class="preview-content-item">
   <img src="'.$filename.'" alt="'.$itemTitle.'">
      <div id="title-preview">
      <div>'.$itemTitle.'</div>
      <div ><a href="'.$filename.'" id="btn-preview"> 
      <span>Download</span></a></div>
      </div>
      <?php echo stripslashes($description); ?>
   </div>
   ';
   }
   }
   }
   }
   }
?>