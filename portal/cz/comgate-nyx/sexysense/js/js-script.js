//  Script for Burger Icon
var navBurger = document.getElementById('NavBurger');
var toggle = false;
navBurger.addEventListener('click', slideSideMenu);

function slideSideMenu() {
  navBurger.classList.toggle('tog-burger');
};



// Script for Side bar menu (Mobile)  
$(function () {
  'use strict'

  $('[data-toggle="offcanvas"]').on('click', function () {
    $('.offcanvas-collapse').toggleClass('open')
  })
})



//  Script for Active Link - Preview.php
var url = window.location.href;
var page = url.includes("videos");
if (page) {
  $('#sideNav a:first').addClass('link_active')
}
var page = url.includes("wallpaper");
if (page) {
  $('#sideNav a:last').addClass('link_active')
}
else {
  $('#sideNav a:first').addClass('link_active')
}



// Script for CheckBox navigation menu
$(function () {
  $('.hidden').hide();
  $('#Asian, #Cosplay, #PornStar, #Blonde, #European').fadeIn("slow");
  $('.trigger').change(function () {
    var hiddenId = $(this).attr("data-trigger");
    if ($(this).is(':checked')) {
      $("#" + hiddenId).fadeIn("slow");
    } else {
      $("#" + hiddenId).fadeOut("slow");
    }
  });
});
//  Check All
$("#chckall").click(function () {
  var hiddenId = $(this).attr("data-trigger");
  ($('input:checkbox').not(this).prop('checked', this.checked))
  if ($(this).is(':checked')) {
    $("#Asian, #Cosplay, #PornStar, #Blonde, #European").fadeIn("slow");
  } else {
    $("#Asian, #Cosplay, #PornStar, #Blonde, #European").fadeOut("slow");
  }
});


jQuery(function() {
	jQuery('[data-check-pattern]').checkAll();
});

;(function($) {
	'use strict';
	
	$.fn.checkAll = function(options) {
		return this.each(function() {
			var CheckAll = $(this);
			var selector = CheckAll.attr('data-check-pattern');
			var onChangeHandler = function(e) {
				var $currentCheckbox = $(e.currentTarget);
				var $subCheckboxes;
				
				if ($currentCheckbox.is(CheckAll)) {
					$subCheckboxes = $(selector);
					$subCheckboxes.prop('checked', CheckAll.prop('checked'));
				} else if ($currentCheckbox.is(selector)) {
					$subCheckboxes = $(selector);
					CheckAll.prop('checked', $subCheckboxes.filter(':checked').length === $subCheckboxes.length);
				}
			};
			
			$(document).on('change', 'input[type="checkbox"]', onChangeHandler);
		});
	};
}(jQuery));
