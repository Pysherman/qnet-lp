<?php
   ob_start();
   session_start();
   include('connection/inc.php');
   if(isset($_GET["varx"]))
   {
   	$cat = $_GET["varx"];
   	@$sub = $_GET["subcat"];
   
   }
   $_SESSION['sesCatg'] = $cat;
   ?>
<!DOCTYPE html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <title>Preview</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <script type="text/javascript" src="js/jquery.js"></script>
   <!--link href="./css/toggle.css?2" rel="stylesheet" type="text/css">
      <script src="./js/jquery-1.12.4.min.js"></script>
      <script src="./js/toggle.js?2" type="text/javascript"></script-->
</head>
<body>
   <header>
      <div class="nav-burger" id="NavBurger"  data-toggle="offcanvas">
         <span></span>
         <span></span>
         <span></span>
      </div>
      <div class="logo">
         <a href="./"><img src="images/logo.png" alt="Sexy Sense"></a>
      </div>
      <div class="offcanvas-collapse"  id="sideNav">
         <a href="./?varx=videos">Videos</a>
         <a href="./?varx=wallpaper">Wallpaper</a>              
      </div>
   </header>
   <div class="wrap">
      <div class="main_container-preview">
         <div class="main_container_content">
               <div class="panitankonaimongitlogron">
                  <?php
                     if(isset($_GET['varx']))
                     {
                     $prevSesID = $_GET['contid'];
                     $contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
                     
                     if($_GET['varx']=="")
                     {
                     header("location:index.php");
                     }
                     else
                     {
                     $getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
                     $data = array();
                     if($getContent)
                     {
                     while($items = mysqli_fetch_array($getContent))
                     {
                     $ContentID 		= $items['id'];
                     $itemTitle 		= $items['title'];
                     $description 	= $items['description'];
                     $contentRate	= $items['rate'];
                     $file_name 		= $items['file_name'];
                     $original_file_name = $items['original_file_name'];
                     $mime 			= $items['mime'];
                     $ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
                     $filename = $contenrdir.$file_name;
                     
                     $file = pathinfo($file_name, PATHINFO_FILENAME);
                     
                     if($ext=="mp4" || $ext=="mp3")
                     {
                     $thumbimg = $file.'.png';
                     $preview ='<div class="preview-content-item-vids">
                        <video width="40%"  controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
                        <source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
                        </video>
                        <div id="title-preview-vids">
                        <div>'.$itemTitle.'</div>
                        <div ><a  href="'.$filename.'" id="btn-preview"> 
                        <span>Download</span></a></div>
                        </div>
                        <?php echo stripslashes($description); ?>
                     </div>';
                     }
                     else
                     {
                     $thumbimg = $file.'.png';
                     $preview ='
                     <div class="preview-content-item">
                     <img src="'.$filename.'" alt="'.$itemTitle.'">
                        <div id="title-preview">
                        <div>'.$itemTitle.'</div>
                        <div ><a href="'.$filename.'" id="btn-preview"> 
                        <span>Download</span></a></div>
                        </div>
                        <?php echo stripslashes($description); ?>
                     </div>
                     ';
                     }
                     }
                     }
                     }
                     }
                     else
                     {
                     header("location:index.php");
                     }
                     ?>
                  <div class='preview-content'>
                           <?php echo $preview; ?>
                  </div>
            </div>
         </div>
      </div>
   </div>
   <script>

   </script>
   <script src="js/js-script.js"></script>
</body>
</html>