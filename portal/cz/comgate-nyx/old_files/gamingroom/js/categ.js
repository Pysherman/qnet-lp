$(window).load(function () {
	$(document).ready(function () {

		var items16 = $(".portfolio-item16");
		var scrollContainer16 = $(".offer-pg-cont16");


		function fetchItem(container, items16, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items16[0]);
			}

			for (i = 0; i < items16.length; i++) {

				if (isNext && $(items16[i]).position().left > 0) {
					return $(items16[i]);
				} else if (!isNext && $(items16[i]).position().left >= 0) {
					return i == 0 ? $(items16[items16.length - 1]) : $(items16[i - 1]);
				}
			}

			return null;
		}

		function moveToItem16(event) {
			var isNext = event.data.direction == "next16";
			var item = isNext ? fetchItem(scrollContainer16, items16, true) : fetchItem(scrollContainer16, items16, false);

			if (item) {
				scrollContainer16.animate({
					"scrollLeft": item.position().left + scrollContainer16.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left16").click({
			direction: "prev16"
		}, moveToItem16);
		$(".arrow-right16").click({
			direction: "next16"
		}, moveToItem16);


	});

});

$(window).load(function () {
	$(document).ready(function () {

		var items19 = $(".portfolio-item19");
		var scrollContainer19 = $(".offer-pg-cont19");


		function fetchItem(container, items19, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items19[0]);
			}

			for (i = 0; i < items19.length; i++) {

				if (isNext && $(items19[i]).position().left > 0) {
					return $(items19[i]);
				} else if (!isNext && $(items19[i]).position().left >= 0) {
					return i == 0 ? $(items19[items19.length - 1]) : $(items19[i - 1]);
				}
			}

			return null;
		}

		function moveToitem19(event) {
			var isNext = event.data.direction == "next19";
			var item = isNext ? fetchItem(scrollContainer19, items19, true) : fetchItem(scrollContainer19, items19, false);

			if (item) {
				scrollContainer19.animate({
					"scrollLeft": item.position().left + scrollContainer19.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left19").click({
			direction: "prev19"
		}, moveToitem19);
		$(".arrow-right19").click({
			direction: "next19"
		}, moveToitem19);


	});

});


$(window).load(function () {
	$(document).ready(function () {

		var items21 = $(".portfolio-item21");
		var scrollContainer21 = $(".offer-pg-cont21");


		function fetchItem(container, items21, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items21[0]);
			}

			for (i = 0; i < items21.length; i++) {

				if (isNext && $(items21[i]).position().left > 0) {
					return $(items21[i]);
				} else if (!isNext && $(items21[i]).position().left >= 0) {
					return i == 0 ? $(items21[items21.length - 1]) : $(items21[i - 1]);
				}
			}

			return null;
		}

		function moveToitem21(event) {
			var isNext = event.data.direction == "next21";
			var item = isNext ? fetchItem(scrollContainer21, items21, true) : fetchItem(scrollContainer21, items21, false);

			if (item) {
				scrollContainer21.animate({
					"scrollLeft": item.position().left + scrollContainer21.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left21").click({
			direction: "prev21"
		}, moveToitem21);
		$(".arrow-right21").click({
			direction: "next21"
		}, moveToitem21);


	});

});



$(window).load(function () {
	$(document).ready(function () {

		var items42 = $(".portfolio-item42");
		var scrollContainer42 = $(".offer-pg-cont42");


		function fetchItem(container, items42, isNext) {
			var i,
				scrollLeft = container.scrollLeft();

			if (isNext === undefined) {
				isNext = true;
			}

			if (isNext && container[0].scrollWidth - container.scrollLeft() <= container.outerWidth()) {
				return $(items42[0]);
			}

			for (i = 0; i < items42.length; i++) {

				if (isNext && $(items42[i]).position().left > 0) {
					return $(items42[i]);
				} else if (!isNext && $(items42[i]).position().left >= 0) {
					return i == 0 ? $(items42[items42.length - 1]) : $(items42[i - 1]);
				}
			}

			return null;
		}

		function moveToitem42(event) {
			var isNext = event.data.direction == "next42";
			var item = isNext ? fetchItem(scrollContainer42, items42, true) : fetchItem(scrollContainer42, items42, false);

			if (item) {
				scrollContainer42.animate({
					"scrollLeft": item.position().left + scrollContainer42.scrollLeft()
				}, 400);
			}
		}

		$(".arrow-left42").click({
			direction: "prev42"
		}, moveToitem42);
		$(".arrow-right42").click({
			direction: "next42"
		}, moveToitem42);


	});

});
