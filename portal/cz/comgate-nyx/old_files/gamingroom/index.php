<?php
$strstatus=0;
if(isset($_GET['status']))
{
$strstatus=$_GET['status'];
}else{$strstatus=0;}

if($strstatus <> 0){

    header("Location: http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/nl/gamezone/gm1/errorstatus.php");
    die();
}

session_start();
include('inc.php');
require('prop/Consts.php');
if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"]; 
	$btn_play="";
	if($cat=="games-apk")
	{
		$btnImg = "apk_active.png";
		$htmlImg = "html5_default.png";
		$CatCondition = " and (b.sub_category='Action' or b.sub_category='Arcade' or b.sub_category='Strategy')";
	}
	if($cat=="html5")
	{
		$btnImg = "apk_default.png";
		$htmlImg = "html5_active.png";
		$CatCondition = " and (b.sub_category='Embed-Games')";
	}

}
else
{
	$cat = 'games-apk';
		$btnImg = "apk_active.png";
		$htmlImg = "html5_default.png";
		$CatCondition = " and (b.sub_category='Action' or b.sub_category='Arcade' or b.sub_category='Strategy')";
		$btn_play="DOWNLOAD";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>1577 - GAMING ROOM</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/portal.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/styles.css" media="screen"  />
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<style>
* {
    box-sizing: border-box;
}
	
	
#footer
{
    position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   color: white;
   text-align: center;
}

.column {
    float: left;
    width: 50%;
    /*padding: 5px;*/
}

.column img, .item img { max-width: 100% !important; width: auto !important; height: auto !important; }

/* Clearfix (clear floats) */
.row::after {
    content: "";
    clear: both;
    display: table;
}

/* ============================== */
.container {
  list-style:none;
  margin: 0;
  padding: 0;
}
.item {
  /*background: tomato;
  border:1px solid #CCC;*/
  padding: 5px;
  width: 200px;
  /*height: 150px;*/
  margin: 10px;
  font-size:12px;
  font-weight:normal;
  /*line-height: 150px;*/
  color: #000;
  border-radius:10px;
  /*text-align: center;*/
}

.flex {
  padding: 0;
  margin: 0;
  list-style: none;
  
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  flex-wrap:wrap;
  -webkit-flex-flow: row wrap;
  justify-content: space-around;
}

#title
{
	padding:10px;
	text-align:center;
	color:#fff;
}

#title a
{
	text-decoration:none;
	color:#0094ca;
}

#title a:hover
{
	text-decoration:underline;
}

#block{
    width: 100%;
    height: 100%;
    position: absolute;
    visibility:hidden;
    display:none;
    /*background-color: rgba(22,22,22,0.5);*/
	top:0;
	left:0;
}

#block:target {
    visibility: visible;
    display: block;
}

@media (min-width: 320px) and (max-width: 479px)
{
	.item {
  /*border:1px solid #CCC;*/
  padding: 5px;
  width: 100px;
  margin: 10px;
  font-size:12px;
  font-weight:normal;
  color: #000;
  border-radius:10px;
}
}

	
.offer-pg-cont16,.offer-pg-cont19,.offer-pg-cont21,.offer-pg-cont42{
    width: 100%;
    overflow-x: hidden;
    margin: 0px;
}
  span.arrow-left16,span.arrow-right16,
	span.arrow-left19,span.arrow-right19,
	span.arrow-left21,span.arrow-right21,
	span.arrow-left42,span.arrow-right42{
    z-index: 2;
    cursor: pointer;
}
.offer-pg{
    width: 9500px;
}

.offer-con .left-item h4 {
    color: #fff;
    font-weight: normal;
    margin: 0px;
}
.offer-con .right-item{
    float: right;
    padding: 10px;
}
.offer-con .right-item h5{
    color: #cb9944;
    margin: 0px;
    font-size: 14px;
}
.offer-pg > .portfolio-item16,.portfolio-item19,.portfolio-item21,.portfolio-item42{
    width: 100px;
    margin-left:10px;
    float:left;
}	
	
</style>
 <script src="js/categ.js" type="text/javascript"></script>
</head>

<body id="body"> 

<div id="header">
    <div id="box1">  
        <div style="text-align: center;"><img src="assets/gamingroom_logo.png" id="ptslogo" border="0" /></div>
    </div>
</div>

<div id="iconimg">
    <div class="row">&nbsp;</div>
</div>

<div id="contentWrap" style="margin-top:auto;">

	<div class="adCntnr">
       	<?php

            $queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");
            if($queryJoin)
            {
			
                while ($DateRow = mysqli_fetch_assoc($queryJoin))
                {
                    $resCatgID	= $DateRow['id'];
                    $resName= $DateRow['category'];
                    $resSbCatgID= $DateRow['sc_id'];
                    $resSName= $DateRow['sub_category'];
                    $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";
                    
                    if($resSName=="Application")
                    {
                        $resSName = "Antivirus";
                    }
									
									echo'<div class="acco2" style="color: #fff">
									<div style="padding:2px;"></div>
							<div class="expand" style="color: #fff">
							<div style="float: right">
								<span class="arrow-left'.$resSbCatgID.'"><img src="assets/slider_prv.png"></span>
								<span class="arrow-right'.$resSbCatgID.'"><img src="assets/slider_nxt.png"></span>

							</div> 
							'.ucfirst($resSName) .'</div>';
									
									echo'<div class="accCntnt" style="color:#fff">
									<div class="row offer-pg-cont'.$resSbCatgID.'">									
									<div class="offer-pg">';	


						if(!empty($_SESSION['categref']))
						{
							$categref 	= $_SESSION['categref'];
							$sesCatg 	= $_SESSION['sesCatg'];
							$html5stat = ($sesCatg == 'html5') ? "and status = 1": "";

							$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime FROM cms.contents WHERE id!=1 and $categref $html5stat order by id desc limit 20");

							if($getContent)
							{
								while($items = mysqli_fetch_array($getContent))
								{
									$contentID 		= $items['id'];
									$itemTitle 		= $items['title'];
									$description 	= $items['description'];
									$file_name 		= $items['file_name'];
									$original_file_name = $items['original_file_name'];
									$mime 			= $items['mime'];
										$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
									if($cat=="games-apk")
									{ $filename = $contenrdir.$file_name;

									$file = pathinfo($file_name, PATHINFO_FILENAME);

									}

									if($cat=="html5")
									{ $filename = $file_name;
									$file =$original_file_name;
									}

									if($ext=="mp4")
									{
										$thumbimg = $file.'.png';
										$preview ='<video width="100%" height="100"  controls preload="metadata">
												<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
												</video>';
									}
									elseif($ext=="mp3")
									{
										$preview = '<img class="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
									}
									else
									{

										$thumbimg = $file.'.png';
										$preview ='<img class="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';

									}
				
										echo'<div class="col-md-3 portfolio-item'.$resSbCatgID.' item flex-item">
											<a href="preview.php?varx='.$sesCatg.'&contid='.$contentID.'">'.$preview.'</a>
											<p style="color: #fff">'.substr($description, 0, 40).'..</p>
										</div>';
				
						} 
					}
				}	

                echo'</div>
									</div>
									</div>
								<div style="padding:2px;"></div>
								</div><br>';

                } 

            } 

			?>
       	
				
    </div>
	
</div>

<div style="margin-top:70px;"></div>

<div id="block">&nbsp;</div>

<div id="footer">	
	<div id="iconimg">
    <div class="row">
      <div class="column"><a href="?varx=games-apk"><img src='assets/<?php echo $btnImg; ?>' border="0"></a></div>
      <div class="column"><a href="?varx=html5"><img src='assets/<?php echo $htmlImg; ?>' border="0"></a></div>
    </div>
</div>
</div>

<script>
    if (window.parent && window.parent.parent){
      window.parent.parent.postMessage(["resultsFrame", {
        height: document.body.getBoundingClientRect().height,
        slug: "c6kf2"
      }], "*")
    }

    window.name = "result"
  </script>
</body>
</html>
