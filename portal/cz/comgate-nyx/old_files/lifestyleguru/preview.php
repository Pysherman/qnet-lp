<?php
$strstatus=0;
if(isset($_GET['status']))
{
$strstatus=$_GET['status'];
}else{$strstatus=0;}

if($strstatus <> 0){

    header("Location: http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/nl/healthland/hl1/errorstatus.php");
    die();
}

session_start();
include('inc.php');
//require('sysprop/filedisp.php');
//require('sysprop/mobiledetect.php');


if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"]; 
	
	if($cat=="videos")
	{
		$vdoImg = "videos_active.png";
		$tipImg = "tips_normal.png";
		$appImg = "apps_normal.png";
		$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
	}
	if($cat=="tip")
	{
		$vdoImg = "videos_normal.png";
		$tipImg = "tips_active.png";
		$appImg = "apps_normal.png";
			$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Food' or b.sub_category='Sport')";
	}
	if($cat=="app")
	{
		$vdoImg = "videos_normal.png";
		$tipImg = "tips_normal.png";
		$appImg = "apps_active.png";
			$CatCondition = " and (b.sub_category='Fitness' or b.sub_category='LifeStyle' or b.sub_category='Relaxation')";
	}
}
else
{
	$cat = 'videos';
		$vdoImg = "videos_active.png";
		$tipImg = "tips_normal.png";
		$appImg = "apps_normal.png";
		$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>1576 PORTAL</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/portal.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/styles.css" media="screen"  />
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 

<style>
* {
    box-sizing: border-box;
}

#footer
{
    position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   color: white;
   text-align: center;
	padding: 10px;
}
	
	#footer img
	{
		width: 110px;
		height: 35px;
	}
.column {
    float: left;
    width: 30%;
}

.column img, .item img { max-width: 100% !important; width: auto !important; height: auto !important; }

/* Clearfix (clear floats) */
.row::after {
    content: "";
    clear: both;
    display: table;
}

/* ============================== */
.container {
  list-style:none;
  margin: 0;
  padding: 0;
}
.item {
  /*background: tomato;
  border:1px solid #CCC;*/
  padding: 5px;
  width: 200px;
  /*height: 150px;*/
  margin: 10px;
  font-size:12px;
  font-weight:normal;
  /*line-height: 150px;*/
  color: #000;
  border-radius:10px;
  /*text-align: center;*/
}

.flex {
  padding: 0;
  margin: 0;
  list-style: none;
  
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  flex-wrap:wrap;
  -webkit-flex-flow: row wrap;
  justify-content: space-around;
}

#title
{
	padding:10px;
	text-align:center;
	color:#fff;
}

#title a
{
	text-decoration:none;
	color:#0094ca;
}

#title a:hover
{
	text-decoration:underline;
}

#block{
    width: 100%;
    height: 100%;
    position: absolute;
    visibility:hidden;
    display:none;
    /*background-color: rgba(22,22,22,0.5);*/
	top:0;
	left:0;
}

#block:target {
    visibility: visible;
    display: block;
}
.prevw img { max-width: 100% !important; width: auto !important; height: auto !important; border-radius:4px; }
@media (min-width: 320px) and (max-width: 479px)
{
	.item {
  /*border:1px solid #CCC;*/
  padding: 5px;
  width: 100px;
  margin: 10px;
  font-size:12px;
  font-weight:normal;
  color: #000;
  border-radius:10px;
}
}

</style>
</head>

<body id="body"> 

<div id="header">
    <div id="box1">  
        <div style="text-align: center;"><img src="assets/lifestyleguru_logo.png" width="193" height="91" id="ptslogo" border="0" /></div>
    </div>
</div>

<div id="iconimg2">
    <div class="row">&nbsp;</div>
</div>

<div id="contentWrap" style="margin-top:auto;">
	<div class="adCntnr">
    	<div class="acco2">
        	<div style="padding:2px;"></div>
				<div class="expand" style="color:#000000">
				<div style="float: right"><a href="index.php"><img src="assets/clse_btn.png"></a></div> 
				Preview</div>
				<div class="accCntnt" style="color:#000000">
						<?php
					if(isset($_GET['varx']))
					{
						$prevSesID = $_GET['contid'];
						$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";

						if($_GET['varx']=="")
						{
							header("location:index.php");
						}
						else
						{
							$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
							$data = array();
							if($getContent)
							{
								while($items = mysqli_fetch_array($getContent))
								{
									$ContentID 		= $items['id'];
									$itemTitle 		= $items['title'];
									$description 	= $items['description'];
									$contentRate	= $items['rate'];
									$file_name 		= $items['file_name'];
									$original_file_name = $items['original_file_name'];
									$mime 			= $items['mime'];
									$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
									$filename = $contenrdir.$file_name;

									$file = pathinfo($file_name, PATHINFO_FILENAME);

									if($ext=="mp4" || $ext=="mp3")
									{
										$thumbimg = $file.'.png';
										$preview ='<video width="100%" height="200"  controls preload="metadata">
												<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
												</video>';
									}
									else
									{
										$thumbimg = $file.'.png';
										$preview ='<img src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
									}
								}
							}
						}
					}
					else
					{
						header("location:index.php");
					}
				?>	
					 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 12px; font-weight: normal; font-family: Cambria, 'Hoefler Text', 'Liberation Serif', Times, 'Times New Roman', 'serif'">
					 <tbody>
						<tr>
						 <td class="prevw"><?php echo $preview; ?></td>
						</tr>
						<tr>
						 <td>
						 <p style="font-weight: bold; color: #0067a5"><?php echo $itemTitle; ?></p>
							 <?php echo stripslashes($description); ?> 							
							<p><a href="<?php echo $filename; ?>"><img src="assets/btn.png" width="90" height="30" alt=""/></a></p>
						 </td>
						</tr>
					 </tbody>
					</table>

						              
						                                          
					</div>
				<div style="padding:2px;"></div>
				
				
				</div>
    </div>
</div>

<div style="margin-bottom:70px;"></div>

<div id="block">&nbsp;</div>
<div id="footer">	
	<div id="iconimg">
    <div class="row">     
      <div class="column"><a href="index.php?varx=videos"><img src='assets/<?php echo $vdoImg; ?>' border="0"></a></div>
      <div class="column"><a href="index.php?varx=app"><img src='assets/<?php echo $appImg; ?>' border="0"></a></div>
      <div class="column"><a href="index.php?varx=tip"><img src='assets/<?php echo $tipImg; ?>' border="0"></a></div>
    </div>
</div>
</div>
</body>
</html>
