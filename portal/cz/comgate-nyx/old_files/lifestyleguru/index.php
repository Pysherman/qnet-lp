<?php
$strstatus=0;
if(isset($_GET['status']))
{
$strstatus=$_GET['status'];
}else{$strstatus=0;}

if($strstatus <> 0){

    header("Location: http://ec2-52-77-123-181.ap-southeast-1.compute.amazonaws.com/nl/healthland/hl1/errorstatus.php");
    die();
}

session_start();
include('inc.php');
//require('sysprop/filedisp.php');
//require('sysprop/mobiledetect.php');


if(isset($_GET["varx"]))
{
	$cat = $_GET["varx"]; 
	
	if($cat=="videos")
	{
		$vdoImg = "videos_active.png";
		$tipImg = "tips_normal.png";
		$appImg = "apps_normal.png";
		$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
	}
	if($cat=="tip")
	{
		$vdoImg = "videos_normal.png";
		$tipImg = "tips_active.png";
		$appImg = "apps_normal.png";
			$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Food' or b.sub_category='Sport')";
	}
	if($cat=="app")
	{
		$vdoImg = "videos_normal.png";
		$tipImg = "tips_normal.png";
		$appImg = "apps_active.png";
			$CatCondition = " and (b.sub_category='Fitness' or b.sub_category='LifeStyle' or b.sub_category='Relaxation')";
	}
}
else
{
	$cat = 'videos';
		$vdoImg = "videos_active.png";
		$tipImg = "tips_normal.png";
		$appImg = "apps_normal.png";
		$CatCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
}
$_SESSION['sesCatg'] = $cat;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>1576 Lifestyle Guru</title>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/portal.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/styles.css" media="screen"  />
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript">
        function showPopUp(el) {
            var cvr = document.getElementById("cover")
            var dlg = document.getElementById(el)
            cvr.style.display = "block"
            dlg.style.display = "block"
            if (document.body.style.overflow = "hidden") {
                cvr.style.width = "100%"
                cvr.style.height = "100%"
            }
        }
        function closePopUp(el) {
            var cvr = document.getElementById("cover")
            var dlg = document.getElementById(el)
            cvr.style.display = "none"
            dlg.style.display = "none"
            document.body.style.overflowY = "scroll"
        }
    </script>
<style>
* {
    box-sizing: border-box;
}
	
	
#footer
{
    position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   color: white;
   text-align: center;
	padding: 10px;
}
	
	#footer img
	{
		width: 110px;
		height: 35px;
	}
.column {
    float: left;
    width: 30%;
}

.column img, .item img { max-width: 100% !important; width: auto !important; height: auto !important; }

/* Clearfix (clear floats) */
.row::after {
    content: "";
    clear: both;
    display: table;
}

/* ============================== */
.container {
  list-style:none;
  margin: 0;
  padding: 0;
}
.item {
  /*background: tomato;
  border:1px solid #CCC;*/
  padding: 5px;
  width: 200px;
  /*height: 150px;*/
  margin: 10px;
  font-size:12px;
  font-weight:normal;
  /*line-height: 150px;*/
  color: #000;
  border-radius:10px;
  /*text-align: center;*/
}

.flex {
  padding: 0;
  margin: 0;
  list-style: none;
  
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  flex-wrap:wrap;
  -webkit-flex-flow: row wrap;
  justify-content: space-around;
}

#title
{
	padding:10px;
	text-align:center;
	color:#fff;
}

#title a
{
	text-decoration:none;
	color:#0094ca;
}

#title a:hover
{
	text-decoration:underline;
}

#block{
    width: 100%;
    height: 100%;
    position: absolute;
    visibility:hidden;
    display:none;
    /*background-color: rgba(22,22,22,0.5);*/
	top:0;
	left:0;
}

#block:target {
    visibility: visible;
    display: block;
}

@media (min-width: 320px) and (max-width: 479px)
{
	.item {
  /*border:1px solid #CCC;*/
  padding: 5px;
  width: 100px;
  margin: 10px;
  font-size:12px;
  font-weight:normal;
  color: #000;
  border-radius:10px;
}
}

	
.offer-pg-cont27,.offer-pg-cont28,.offer-pg-cont29,
.offer-pg-cont39,.offer-pg-cont40,.offer-pg-cont41,
.offer-pg-cont42,.offer-pg-cont43{
    width: 100%;
    overflow-x: hidden;
    margin: 0px;
}
span.arrow-left27,span.arrow-right27,
span.arrow-left28,span.arrow-right28,
span.arrow-left29,span.arrow-right29,
span.arrow-left39,span.arrow-right39,
span.arrow-left40,span.arrow-right40,
span.arrow-left40,span.arrow-right41,
span.arrow-left42,span.arrow-right42,
span.arrow-left43,span.arrow-right43{
    z-index: 2;
    cursor: pointer;
}
.offer-pg{
    width: 9500px;
}

.offer-con .left-item h4 {
    color: #fff;
    font-weight: normal;
    margin: 0px;
}
.offer-con .right-item{
    float: right;
    padding: 10px;
}
.offer-con .right-item h5{
    color: #cb9944;
    margin: 0px;
    font-size: 14px;
}
.offer-pg > .portfolio-item9,.portfolio-item27,.portfolio-item28,.portfolio-item29,.portfolio-item39,
.portfolio-item40,.portfolio-item41,.portfolio-item42,.portfolio-item43{
    width: 100px;
    margin-left:10px;
    float:left;
}	
#cover {
	display:        none;
  position:       absolute;
  left:           0px;
	top:            0px;
	width:          100%;
	height:         100%;
	/*background:     blue;
	filter:         alpha(Opacity = 50);
	opacity:        0.5;
	-moz-opacity:   0.5;
	-khtml-opacity: 0.5*/
}

#dialog,#dialog2,#dialog3,#dialog4,#dialog5,#dialog6,#dialog7,#dialog8,#dialog9,#dialog10,#dialog11,
#dialog12,#dialog13,#dialog14,#dialog15,#dialog16,#dialog17,#dialog18,#dialog19,#dialog20,#dialog21,
#dialog22,#dialog23,#dialog24,#dialog25,#dialog26,#dialog27,#dialog28,#dialog29,#dialog30,#dialog31,
#dialog32,#dialog33,#dialog34,#dialog35,#dialog36,#dialog37,#dialog38,#dialog39,#dialog40,#dialog41,
#dialog42,#dialog43,#dialog44,#dialog45,#dialog46,#dialog47,#dialog48,#dialog49,#dialog50,#dialog51,
#dialog52,#dialog53,#dialog54,#dialog55,#dialog56,#dialog57,#dialog58,#dialog59{
	display:    none;
	left:       100px;
	top:        100px;
	width:      70%;
	height:     100%hv;
	position:   absolute;
	z-index:    100;
	padding:    2px;
}
	
</style>
 <script src="js/categ.js" type="text/javascript"></script>
</head>

<body id="body"> 

<div id="header">
    <div id="box1">  
        <div style="text-align: center;"><img src="assets/lifestyleguru_logo.png" width="193" height="91" id="ptslogo" border="0" /></div>
    </div>
</div>

<div id="iconimg2">
    <div class="row">&nbsp;</div>
</div>

<div id="cover"></div>
<div id="contentWrap" style="margin-top:auto;">
	<div class="adCntnr">
   	<?php
		
		if($cat!="tip")
		{
			$queryJoin = $conn->query("SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $CatCondition  and category like '$cat%'");

            if($queryJoin)
            {
                while ($DateRow = mysqli_fetch_assoc($queryJoin))
                {
                    $resCatgID	= $DateRow['id'];
                    $resName= $DateRow['category'];
                    $resSbCatgID= $DateRow['sc_id'];
                    $resSName= $DateRow['sub_category'];
                    $_SESSION['categref'] = "category_id='$resCatgID' and sub_category_id='$resSbCatgID'";
                    
                    if($resSName=="Application")
                    {
                       $resSName = "Antivirus";
                    }
                    
                    /*echo'<div style="padding:2px;"></div>
					<div id="a'.$resSbCatgID.'" class="expand"><a title="expand/collapse" href="#" style="display: block;">'.ucfirst($resSName).'</a></div>';
                   echo"<div class=\"collapse\">
					<div class=\"accCntnt\" style=\"background-color:#f2f2f2; z-index: 0; position: relative; color:#000000\">
						<ul class=\"container flex\">";*/
									
									echo'<div class="acco2" style="color: #000">
									<div style="padding:2px;"></div>
							<div class="expand" style="color: #000">
							<div style="float: right">
								<span class="arrow-left'.$resSbCatgID.'"><img src="assets/slider_prv.png"></span>
								<span class="arrow-right'.$resSbCatgID.'"><img src="assets/slider_nxt.png"></span>

							</div> 
							'.ucfirst($resSName) .'</div>';
									

							if(isset($_GET["varx"]))
							{
								$cat = $_GET["varx"]='videos'; 
							}
							else
							{
								$cat = 'videos';
							}
									
							echo'<div class="accCntnt" style="color:#fff">
									<div class="row offer-pg-cont'.$resSbCatgID.'">									
									<div class="offer-pg">';	
									

						if(!empty($_SESSION['categref']))
						{
							$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
							$categref 	= $_SESSION['categref'];
							$sesCatg 	= $_SESSION['sesCatg'];

							$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and $categref order by id desc limit 20");
							$data = array();
							if($getContent)
							{
								while($items = mysqli_fetch_array($getContent))
								{
									$contentID 		= $items['id'];
									$itemTitle 		= $items['title'];
									$description 	= $items['description'];
									$file_name 		= $items['file_name'];
									$original_file_name = $items['original_file_name'];
									$mime 			= $items['mime'];
										$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
									$filename = $contenrdir.$file_name;
													$subcat = $items['sub_category_id'];

									$file = pathinfo($file_name, PATHINFO_FILENAME);


									if($ext=="mp4")
									{
										$thumbimg = $file.'.png'; /*poster="img/play.png"*/
										$preview ='<div style="z-index:2;">
										<video style="z-index:-1;" width="100%" height="100%" controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
												<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
												</video></div>';
									}
									elseif($ext=="mp3")
									{
										$preview = '<img id="img-thumbnail" src="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/672f065d-0ee5-41f4-85b3-eb7efdb0ddb9.png" alt="">';
									}
									else
									{
										$thumbimg = $file.'.png';

										$preview ='<img id="img-thumbnail" src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';

									}
									
									echo'<div class="col-md-3 portfolio-item'.$resSbCatgID.' item flex-item">
											<a href="preview.php?varx='.$sesCatg.'&contid='.$contentID.'&subcat='.$subcat.'">'.$preview.'</a>
											<p>'.substr($description, 0, 40).'..</p>
										</div>';
									
								}
							}
						}	  
						  
						echo'</div>
									</div>
									</div>
								<div style="padding:2px;"></div>
								</div><br>';
					
                }
            }
		}else{
					
			echo"<div class=\"acco2\" style=\"color: #000\">
									<div style=\"padding:2px;\"></div>
							<div class=\"expand\" style=\"color: #000\">
							<div style=\"float: right\">
								<span class=\"arrow-left42\"><img src=\"assets/slider_prv.png\"></span>
								<span class=\"arrow-right42\"><img src=\"assets/slider_nxt.png\"></span>
							</div> 
							FOOD</div>
				<div class=\"accCntnt\" style=\"color:#000000\">
					<div class=\"row offer-pg-cont42\">
					<div class=\"offer-pg\">";
					
					include("food-php.php");	
						
					echo"</div>
					</div>
			</div>           
					</div>
				<div style=\"padding:2px;\"></div>
				
				
				<div class=\"acco2\" style=\"color: #000\">
									<div style=\"padding:2px;\"></div>
							<div class=\"expand\" style=\"color: #000\">
							<div style=\"float: right\">
								<span class=\"arrow-left43\"><img src=\"assets/slider_prv.png\"></span>
								<span class=\"arrow-right43\"><img src=\"assets/slider_nxt.png\"></span>
							</div> 
							SPORTS</div>
				<div class=\"accCntnt\" style=\"color:#000000\">
					<div class=\"row offer-pg-cont43\">
					<div class=\"offer-pg\">";
					
					include("sports-php.php");	
						
					echo"</div>
			</div>           
					</div>
				<div style=\"padding:2px;\"></div>";
			
			
			/*echo "<div style='padding:2px;' class></div> 
					<div class='expand openAd'>
		  						<a title='expand/collapse' href='#' style='display:block;'>Food</a></div>
					<div id='food' class='collapse'></div>";


					echo "<div style='padding:2px;' class></div> 
					<div class='expand openAd'>
		  						<a title='expand/collapse' href='#' style='display:block;'>Sport</a></div>
					<div id='sport' class='collapse'></div>";*/
			
					/*echo '<div class="acco2" style="color: #000">
									<div style="padding:2px;"></div>
							<div class="expand" style="color: #000">
							<div style="float: right">
								<span class="arrow-left42"><img src="assets/slider_prv.png"></span>
								<span class="arrow-right42"><img src="assets/slider_nxt.png"></span>

							</div> 
							FOOD</div>
				<div class="accCntnt" style="color:#000000">
					<div class="row offer-pg-cont42">
					<div class="offer-pg">
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Thai fried prawn &amp; pineapple rice</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Asparagus &amp; new potato frittata</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Simple Fish Stew</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Greek courgetti salad</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Asian pulled chicken salad</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Spiced black bean &amp; chicken soup with kale</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">One-pan egg &amp; veg brunch</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Mushroom brunch</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Chicken and vegetable curry</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Smoked mackerel with orange, watercress and potato salad</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Crunchy detox salad</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Mumbai potato wraps with minted yogurt relish</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Overnight oats</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Fruit and nut breakfast bowl</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Summer porridge</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Salmon sushi salad</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Walnut and almond muesli with grated apple</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Warming chocolate and banana porridge</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Quinoa porridge</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Baked banana porridge</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Poached eggs with smashed avocado and tomatoes</a></div>
							</div>
					</div>
			</div>           
					</div>
				<div style="padding:2px;"></div>';
			
			echo '<div class="acco2" style="color: #000">
									<div style="padding:2px;"></div>
							<div class="expand" style="color: #000">
							<div style="float: right">
								<span class="arrow-left43"><img src="assets/slider_prv.png"></span>
								<span class="arrow-right43"><img src="assets/slider_nxt.png"></span>
							</div> 
							SPORTS</div>
				<div class="accCntnt" style="color:#000000">
					<div class="row offer-pg-cont43">
					<div class="offer-pg">
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Stick to It for 2 Weeks</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Bring Up Your Rear</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Never Skip the Most Important Meal</a></div>
							</div>
							<div class="col-md-3 portfolio-item42 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Mind Your Muscle</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Be Less Specific</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Step It Up</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Hang Tight</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Not So Fast</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Bust Out Your Bikini</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Pep Up Your Run</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Never Do the Same Workout</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Freshen Your Breath and Your Muscles</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Go 2-for-1</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Be True to Form</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">One Day at a Time</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Find a Fit Friend</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Dig Deeper</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Learn the Ropes</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Heed Your Hunger</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Take It Outside</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Ace Your Serving Sizes</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Multi-Goal</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Sweat to a Beat of Your Own</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Heart Your Trainer</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Seek Out Text Support</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Put on More Weight</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Be Here. Now.</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Work It from Every Angle</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Muscle Over Mind</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Roll With It</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Be Picky About Dinner Dates</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Consider Retail Therapy</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Take 5 to Visualize</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Melt Fat on the Mat</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Put Your Money Where Your Muscle Is</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Finish Strong</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Reduce with Produce</a></div>
							</div>
							<div class="col-md-3 portfolio-item43 item flex-item">
									<a href="preview.php"><img src="assets/sample1.png"></a>
									<div id="title"><a href="preview.php">Compress Forward</a></div>
							</div>
					</div>
			</div>           
					</div>
				<div style="padding:2px;"></div>';*/
		}
		?>   	
    </div>
</div>

<div style="margin-top:70px;"></div>



<div id="block">&nbsp;</div>
<div id="footer">	
	<div id="iconimg">
    <div class="row">     
      <div class="column"><a href="?varx=videos"><img src='assets/<?php echo $vdoImg; ?>' border="0"></a></div>
      <div class="column"><a href="?varx=app"><img src='assets/<?php echo $appImg; ?>' border="0"></a></div>
      <div class="column"><a href="?varx=tip"><img src='assets/<?php echo $tipImg; ?>' border="0"></a></div>
    </div>
</div>
</div>
<script>
    if (window.parent && window.parent.parent){
      window.parent.parent.postMessage(["resultsFrame", {
        height: document.body.getBoundingClientRect().height,
        slug: "c6kf2"
      }], "*")
    }

    window.name = "result"
  </script>
  
  
  <?php
include("food-php-c.php");
include("sports-php-c.php");
?>

</body>
</html>
