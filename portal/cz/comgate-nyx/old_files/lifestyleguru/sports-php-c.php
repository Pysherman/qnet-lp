<div id="dialog22">     
    <div>
			<div class='modal-content'> <span class='close' onclick="closePopUp('dialog22');">&times;</span>
			<div class='pop-image'><div class='wrapper-pop'>
			<img src="sysprop/sport/1Stick%20.jpg">
			</div></div>
			<div class='pop-content'><div class='tile block'>Stick to It for 2 Weeks</div>
			<div class='description block'>If motivation is your hang-up, change your exercise routine every 14 days. A University of Florida study discovered that people who modified their workouts twice a month were more likely than to stick to their plans compared to those who changed their regimens whenever they wanted to. Boredom didn’t appear to be a factor; it seems people simply enjoyed the variety more.</div></div></div>
		</div>
</div>


<div id="dialog23">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog23');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/2Bring.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Bring Up Your Rear</div>
				<div class='description block'>For a strong backside that will turn heads wherever you go, Marta Montenegro, a Miami-based exercise physiologist and strength and conditioning coach, recommends completing 100 kettlebell swings nonstop with a moderate weight at the end of a legs workout. [Tweet this tip!] If you can’t access a kettlebell, do deadlifts and hip-thrusters instead. “Women tend to overemphasize the quadriceps even when they think they are working the butt. With these two exercises, you'll have no problem engaging the glutes and posterior muscles of the legs,” Montenegro says.</div>
			</div>
		</div>
	</div>
</div>


<div id="dialog24">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog24');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/3Neverskipyourmeal.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Never Skip the Most Important Meal</div>
				<div class='description block'>For once we're not talking about breakfast but rather the recovery meal after your workout. “So many women skip post-exercise nutrition because they don’t want to 'undo the calories they just burned,'” says Amanda Carlson-Phillips, vice president of nutrition and research for Athletes’ Performance and Core Performance. “But getting a combination of 10 to 15 grams of protein and 20 to 30 grams of carbohydrates within 30 minutes of your workout will help to refuel your body, promote muscle recovery, amp up your energy, and build a leaner physique.”</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog25">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog25');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/4MindYourMuscle.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Mind Your Muscle</div>
				<div class='description block'>It's easy to get lost in a killer playlist or Friends rerun on the TV attached to the elliptical, but mindless exercise makes all your hard work forgettable—and you can forget about seeing results too. “There is a huge difference between going through the motions of an exercise and truly thinking, feeling, and engaging the key muscles,” says Kira Stokes, master instructor at the New York City location of indoor cycling studio Revolve. “Be conscious of and enjoy the sensation of your muscles contracting and the feelings of growing stronger and more powerful with each rep.”</div>
			</div>
		</div>
	</div>
</div>


<div id="dialog26">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog26');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/5BeLessSpecific.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Be Less Specific</div>
				<div class='description block'>Just like trying to find a guy who meets certain exact standards, trying to reach an exact weight is a lofty—and often unattainable—goal. Having a range, such as losing five to 10 pounds, may lead to a more successful outcome than if you aim to lose precisely 8 pounds in four weeks, according to a study published in the Journal of Consumer Research. Flexible goals seem more feasible, which in turn boosts your sense of accomplishment, encouraging you to stay driven, the study authors say.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog27">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog27');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/6StepItUp.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Step It Up</div>
				<div class='description block'>Instinct may tell you to slow down when running in wintery conditions, but the secret to not slipping is actually to speed up and shorten your stride. Aim to have each foot strike the ground 90 times per minute, says Terry Chiplin, owner of Active at Altitude, a Colorado-based facility for endurance athletes. This high cadence helps ensure that each foot lands beneath the center of your weight rather than ahead of it, which can throw off your balance on slick terrain. </div>
			</div>
		</div>
	</div>
</div>

<div id="dialog28">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog28');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/7HangTight.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Hang Tight</div>
				<div class='description block'>Not being able to do a pull-up doesn’t mean you shouldn’t step up to the bar. Simply hanging on for as long as possible can improve your upper-body strength, Montenegro says. Concentrate on keeping your body as still as possible, and you’ll naturally recruit your abs, hips, and lower back in addition to your arms, she explains, or slowly move your legs in circles or up and down to further engage your abs.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog29">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog29');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/8NotSoFast.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Not So Fast</div>
				<div class='description block'>Before you start a juice cleanse diet, know that drastically restricting your caloric intake to drop pounds may backfire: In a 2010 study, women placed on a 1,200-calorie diet for three weeks had elevated levels of cortisol, our primary stress hormone. [Tweet this fact!] Chronic stress has been associated with an increased risk of weight gain as well as coronary heart disease, hypertension, diabetes, cancer, and impaired immune functioning. </div>
			</div>
		</div>
	</div>
</div>

<div id="dialog30">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog30');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/9BustOutYourBikini.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Bust Out Your Bikini</div>
				<div class='description block'>Packing your two-piece away for winter means you won't think about how you'll look in it until about April. Avoid any potential “how did my butt get this big?!” panics come spring by keeping your swimsuit handy and putting it on every so often to make sure you like what you see, says Tanya Becker, co-founder of the Physique 57 barre program. You can also toss it on when you're tempted to overindulge, she adds. “There’s no better way to keep yourself from having that after-dinner cookie or slice of cake."</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog31">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog31');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/10PepUpYourRun.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Pep Up Your Run</div>
				<div class='description block'>Sweet chili peppers may not be a winter food, but continue eating them in your burritos, stir-fries, and soups, and you may burn more fat during your outdoor cold-weather runs. These not-hot veggies contain chemicals called capsinoids, which are similar to the capsaicin found in hot peppers. Combine capsinoids with 63-degree or cooler temps, and you increase the amount and activity of brown fat cells—those that burn energy—and give your metabolism an extra boost, according to a study published in the Journal of Clinical Investigation.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog32">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog32');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/11NeverDothe.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Never Do the Same Workout</div>
				<div class='description block'>“The reason most people don't see changes isn't because they don't work hard—it's because they don't make their workouts harder,” says Adam Bornstein, founder of Born Fitness. His suggestion: Create a challenge every time you exercise. “Use a little more weight, rest five to 10 seconds less between sets, add a few more reps, or do another set. Incorporating these small variations into your routine is a recipe for change,” he says.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog33">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog33');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/12FreshenYourBreathandYourMuscles.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Freshen Your Breath and Your Muscles</div>
				<div class='description block'>Consider including peppermint in your pre-workout snack or drink. In a small study published in theJournal of the International Society of Sports Nutrition, men drank 2 cups water with 0.05 milliliters (basically, a drop) peppermint oil mixed in and then ran on a treadmill to test their stamina and power. The mint appeared to help relax muscles, boost oxygen to muscles and the brain, and elevate pain threshold, leading to improved overall performance.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog34">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog34');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/13Go2-for-1.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Go 2-for-1</div>
				<div class='description block'>When you do high-intensity interval training (and if you’re not, you should be!), follow a 2:1 work-to-rest ratio, such as sprinting one minute followed by 30 seconds of recovery. [Tweet this secret!] According to several studies, the most recent out of Bowling Green State University, this formula maximizes your workout results. The BGSU researchers also say to trust your body: Participants in the study set their pace for both running and recovery according to how they felt, and by doing so women worked at a higher percentage of their maximum heart rate and maximum oxygen consumption than the men did.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog35">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog35');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/14BeTruetoForm.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Be True to Form</div>
				<div class='description block'>It doesn't matter how many pushups you can do in a minute if you're not doing a single one correctly. “There is no point in performing any exercise without proper form,” says Stokes, who recommends thinking in terms of progression: Perfect your technique, then later add weight and/or speed. This is especially important if your workout calls for performing “as many reps as possible” during a set amount of time. Choose quality over quantity, and you can stay injury-free.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog36">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog36');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/15OneDayataTime.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>One Day at a Time</div>
				<div class='description block'>Long-term goals are imperative, but they can make you feel overwhelmed or discouraged at times. Instead of thinking about how many dress sizes smaller you want to be in four months, focus on small everyday victories, suggests Michael Snader,BodyAware specialist and nutritionist at The BodyHoliday, a health and wellness resort in St. Lucia. “For example, today you are going to eat breakfast, fit in a workout, and drink more water,” he says. Stay focused on the present, and your future will be successful.div>
			</div>
		</div>
	</div>
</div>

<div id="dialog37">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog37');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/16FindaFitriend.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Find a Fit Friend</div>
				<div class='description block'>A workout partner not only keeps you accountable, she also may help you clock more time at the gym and torch more fat. A British survey of 1,000 women found that those who exercise with others tend to train six minutes longer and burn an extra 41 calories per session compared to solo fitness fanatics. [Tweet this fact!] Women with Bikram buddies and CrossFit comrades said they push themselves harder and are more motivated than when they hit the gym alone.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog38">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog38');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/17DigDeeper.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Dig Deeper</div>
				<div class='description block'>It takes a lot of discipline to turn down a cupcake or roll out of your warm bed for a cold morning run. To make staying on track easier, it's important to make a real connection with your motivation, says Tara Gidus, R.D., co-host of Emotional Mojo. So think less about fitting into your skinny jeans or spring break bikini and more about emotional ties to the people you love. “Your relationships will grow stronger when you are physically healthy and taking care of yourself,” she says.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog39">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog39');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/18LearntheRopes.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Learn the Ropes</div>
				<div class='description block'>The best training tool you're not using: a jump rope. “It may seem a little juvenile until you think of all the hot-bodied boxing pros who jump rope every single day,” says Landon LaRue, a CrossFit level-one trainer at Reebok CrossFit LAB in L.A. Not only is it inexpensive, portable, and easy to use almost anywhere, you’ll burn about 200 calories in 20 minutes and boost your cardiovascular health while toning, he adds.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog40">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog40');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/19HeedYourHunger.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Heed Your Hunger</div>
				<div class='description block'>Give your body a little more credit: It tells you when you’re hungry—you may not be listening, though. Before chowing down because there’s only one slice of pie left or because the last guest arrived at the brunch, stop and check in with your stomach. “If you’re not hungry, make yourself a small plate and sip on some tea or coffee while everyone else digs in,” recommends Elle Penner, M.P.H., R.D., a MyFitnessPal expert. When your belly starts to finally grumble, food will be there.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog41">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog41');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/20TakeItOutside.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Take It Outside</div>
				<div class='description block'>A study by the National Institutes of Health that found people could burn up to 7 percent more calories in the cold. So if you're torching 268 calories during a half-hour indoor run at a 12-minute-mile pace, you may hoof off closer to 300 calories if you head outdoors.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog42">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog42');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/21AceYourServing.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Ace Your Serving Sizes</div>
				<div class='description block'>When dishing out dinner, put away the measuring cups and grab a standard size plate. “If your food fits with no individual item touching another, you can be pretty confident that your portion sizes are appropriate,” says Snader.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog43">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog43');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/22Multi-Goal.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Multi-Goal</div>
				<div class='description block'>Popular belief says if you really want to make a big change, focus on one new healthy habit at a time. But Stanford University School of Medicine researchers say working on your diet and fitness simultaneously may put the odds of reaching both goals more in your favor. They followed four groups of people: The first zoned in on their diets before adding exercise months later, the second did the opposite, the third focused on both at once, and the last made no changes. Those who doubled up were most likely to work out 150 minutes a week and get up to nine servings of fruits and veggies daily while keeping their calories from saturated fat at 10 percent or less of their total intake.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog44">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog44');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/23SweattoaBeatofYourOwn.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Sweat to a Beat of Your Own</div>
				<div class='description block'>Rocking out to your fave playlist helps you power through a grueling workout, and now research shows singing, humming, or whistling may be just as beneficial. [Tweet this tip!] A German and Belgian study found that making music—and not just listening to it—could impact exercise performance. People who worked out on machines designed to create music based on their efforts exerted more energy (and didn't even know it) compared to others who used traditional equipment. Sweating to your own tune may help make physical activities less exhausting, researchers say.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog45">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog45');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/24HeartYourTrainer.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Heart Your Trainer</div>
				<div class='description block'>Find an instructor who motivates you to get out the door or turn on a video—they will be your best advocate,” Becker says. If you look forward to seeing your favorite Pilates teacher, you'll be more likely to hit the studio regularly. Same goes if you love a trainer's energy in their DVD or online videos.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog46">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog46');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/25SeekOutTextSupport.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Seek Out Text Support</div>
				<div class='description block'>If you thought texting changed your love life, imagine what it could do for your waistline. When people received motivational text messages promoting exercise and healthy behaviors twice a week (i.e., “Keep in the fridge a Ziploc with washed and precut vegetables 4 quick snack. Add 1 string cheese 4 proteins”), they lost an average of about 3 percent of their body weight in 12 weeks. Participants in the Virginia Commonwealth University study also showed an improvement in eating behaviors, exercise, and nutrition self-efficacy, and reported that the texts helped them adopt these new habits. Find health-minded friends and message each other reminders, or program your phone to send yourself healthy eating tips.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog47">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog47');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/26PutonMoeWeight.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Put on More Weight</div>
				<div class='description block'>You know strength training is the best way to trim down, tone up, and get into “I love my body” shape. But always reaching for the 10-pound dumbbells isn’t going to help you. “Add two or three compound barbell lifts (such as a squat, deadlift, or press) to your weekly training schedule and run a linear progression, increasing the weight used on each lift by two to five pounds a week,” says Noah Abbott, a coach at CrossFit South Brooklyn. Perform three to five sets of three to five reps, and you’ll boost strength, not bulk. “The short, intense training will not place your muscles under long periods of muscle fiber stimulation, which corresponds with muscle growth,” Abbott explains.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog48">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog48');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/27BeHereNow..jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Be Here. Now.</div>
				<div class='description block'>CONTENT HERE</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog49">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog49');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/28WorkItromEveryAngle.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Work It from Every Angle</div>
				<div class='description block'>Before you convince yourself that you’re too busy to mediate, consider this: “Adding mediation to your daily fitness routine can be a crucial part of body transformation,” says Mark Fisher, founder of Mark Fisher Fitness in NYC. Find five to 10 minutes once or twice a day to focus on your breath, he suggests. “Taking the time to do this can help your body and brain de-stress and recover better from all your hard work at the gym and the office.”</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog50">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog50');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/29MuscleOverMind.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Muscle Over Mind</div>
				<div class='description block'>If you usually head to the gym after work, take heed: Mental exhaustion can make you feel physically exhausted, even when you have plenty of energy, reports a Medicine &amp; Science in Sports &amp; Exercise study. When people played a brain-draining computer game before exercising, they reported a subsequent workout as being harder, yet their muscles showed the same activity as they did doing the same workout after an easy mental game. So if you think you can’t eke out those last 10 minutes on the rowing machine, remember: You can! [Tweet this motivation!]</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog51">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog51');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/30RollWitIt.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Roll With It</div>
				<div class='description block'>Also known as “myofascial release,” foam rolling is an easy way to benefit your entire body. “While stretching addresses the length of muscle fiber, rolling improves the quality of the tissue,” says Rob Sulaver, CEO and founder of Bandana Training. This leads to tension- and pain-free muscles, which function better so you perform better. Be sure to roll for five minutes before your workout. </div>
			</div>
		</div>
	</div>
</div>

<div id="dialog52">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog52');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/31AboutDinnerDates.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Be Picky About Dinner Dates</div>
				<div class='description block'>You know it's easier to fall off the healthy-eating wagon when the person across the table from you is going whole-hog on mozzarella sticks, but science still felt the need to study this. And evidence presented at the 2013 Agricultural and Applied Economic Associations annual meeting backs you up: In the study, people made similar dining choices as their companions did, possibly because we simply want to fit in. Not all eating partners make a bad influence, though. The report further speculates that if you're eating with a health-conscious person, you may be more likely to order something more nutritious as well.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog53">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog53');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/32ConsiderTherapy.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Consider Retail Therapy</div>
				<div class='description block'>“Whenever I buy a new pair of athletic shoes, an enthusiasm comes over me. I’m eager to wear them,” says celebrity trainer and New Balance fitness ambassador Holly Perkins. Follow her lead and occasionally buy new kicks or gym clothes if it helps revitalize your passion for the gym.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog54">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog54');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/33Take5toVisualize.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Take 5 to Visualize</div>
				<div class='description block'>Picture your perfect self with your flat abs, firmer butt, and slim thighs every day. Seeing really is believing: “You become consciously and acutely aware of everything that can help you achieve the visualized outcome that you desire when you impress an idea into the subconscious part of you,” says celebrity yoga coach Gwen Lawrence. “It eventually becomes ‘fixed,’ and you automatically move toward that which you desire.” </div>
			</div>
		</div>
	</div>
</div>

<div id="dialog55">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog55');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/34MeltFatontheMat.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Melt Fat on the Mat</div>
				<div class='description block'>Vinyasa and power may not be the only forms of yoga that will get you closer to that long, lean, limber look. Research presented at the 73rd Scientific Sessions of the American Diabetes Association found that restorative yoga—which focuses more on relaxing and stress-reducing movements rather than a challenging flow or balancing poses—burns more subcutaneous fat (the kind directly under your skin) than stretching does. By the end of the yearlong study, yogis who practiced at least once a month lost an average of about three pounds, nearly double the amount lost by those who only stretched. So if you don’t feel up for a more athletic yoga class, ease your way into a practice with a gentle one.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog56">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog56');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/35PutYourMoney.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Put Your Money Where Your Muscle Is</div>
				<div class='description block'>Although creating financial incentives to lose weight isn't a new idea, now we know cashing in to stay motivated works long-term. In the longest study yet on this topic, Mayo Clinic researchers weighed 100 people monthly for one year, offering half the group $20 per pound lost plus a $20 penalty for every pound gained. Those in the monetary group dropped an average of nine pounds by the end of the year, while non-paid participants shed about two pounds. If you’re ready to gamble away weight, consider sites such as Healthywage, FatBet, or stick.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog57">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog57');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/36FinishStrong.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Finish Strong</div>
				<div class='description block'>When you're in the homestretch of your workout, kick it up a notch. “At the end of every longer or easy run or bike ride, pick up your pace for the last minute as you would do when you're finishing a race,” Chiplin says. “This adds minimal stress physiologically while creating a mindset of ending each run fast and strong.”</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog58">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog58');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/37ReducewithProduce.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Reduce with Produce</div>
				<div class='description block'>Adopting a plant-based diet could help tip the scales in your favor. A five-year study of 71,751 adults published in the Journal of the Academy of Nutrition and Dietetics found that vegetarians tend to be slimmer than meat-eaters even though both groups eat about the same number of calories daily. Researchers say it may be because carnivores consume more fatty acids and fewer weight-loss promoting nutrients, like fiber, than herbivores do. Go green to find out if it works for you.</div>
			</div>
		</div>
	</div>
</div>

<div id="dialog59">
	<div>
		<div class='modal-content'> <span class='close' onclick="closePopUp('dialog59');">&times;</span>
			<div class='pop-image'>
				<div class='wrapper-pop'>
					<img src="sysprop/sport/38CompressForward.jpg">
				</div>
			</div>
			<div class='pop-content'>
				<div class='tile block'>Compress Forward</div>
				<div class='description block'>When layering for an outdoor activity this winter, consider a compression fabric for your base layers. “These fabrics are fantastic at wicking moisture from the body, which allows you to sweat and breath while keeping you warm,” says Chiplin, who notes they can also reduce fatigue and muscle soreness so you’re ready to head out again tomorrow. Consider throwing them in the dryer for a minute before dressing to further chase away the morning chill. </div>
			</div>
		</div>
	</div>
</div>
