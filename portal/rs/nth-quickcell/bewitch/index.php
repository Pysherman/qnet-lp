<!DOCTYPE html>
<?php require('connection/inc.php');?>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style/main.css" />
    <link rel="stylesheet" type="text/css" href="icofont/fontawesome5/css/all.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <title>Bewitch</title>
</head>
<body>
    <header class="header">
        <img src="img/logo.png" alt="Powerland">
    </header>
    <section class="mainCategory">
        <ul>
            <li data-main-cat="videos"><a href="#">Videos</a></li>
            <li data-main-cat="wallpaper"><a href="#">Wallpaper</a></li>
            <div class="catActive"></div>
        </ul>
    </section>
    <section class="subCategory">
        <?php
            $catConditionGames = "and (b.sub_category='Asian' or b.sub_category='Blonde' or b.sub_category='Cosplay' or b.sub_category='European')";
            $catGames = "videos";
            $querySubCat = "SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $catConditionGames and category like ?";

            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt, $querySubCat);
            $bindCatGames = "{$catGames}%";
            $bindParam = mysqli_stmt_bind_param($stmt, "s", $bindCatGames);
            $ids = [];

            if($bindParam):
                mysqli_stmt_execute($stmt);
        ?>
                <ul data-main-category="<?php echo $catGames; ?>">
        <?php
                $result = mysqli_stmt_get_result($stmt);
                while($data = mysqli_fetch_assoc($result)):
                    $catId = $data['id'];
                    $category = $data['category'];
                    $subCatId = $data['sc_id'];
                    $subCategory = $data['sub_category'];

                    $idsData = ["catId" => $catId, "subCatId" => $subCatId];
                    array_push($ids, $idsData);

                    $newSubCategory = str_replace(" ", "", $subCategory);
        ?>
                    <li data-sub-cat="<?php echo $newSubCategory; ?>">
                        <a href="#"><?php echo ucwords($subCategory); ?></a>
                        <input type="hidden" name="catId" value="<?php echo $catId; ?>" />
                        <input type="hidden" name="subCatId" value="<?php echo $subCatId; ?>" />
                    </li>
        <?php   endwhile; ?>

                </ul>
                
        <?php
                // mysqli_stmt_close($stmt);
                // mysqli_close($conn);
            endif;
        ?>
    </section>
    <main class="mainContainer">
        <section class="contentWrapper">
            <?php
                if(!empty($ids)):

                    $getContent = "SELECT id, title, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 and category_id=? and sub_category_id=? order by id desc limit 30";

                    $dir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
                    $imgExt = ".mp4";

                    mysqli_stmt_prepare($stmt, $getContent);
                    mysqli_stmt_bind_param($stmt, "ii", $ids[0]['catId'], $ids[0]['subCatId']);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_store_result($stmt);

                    $result = mysqli_stmt_num_rows($stmt);
                    if($result > 0):
                        mysqli_stmt_bind_result($stmt, $id, $title, $filename, $originalFilename, $mime, $subCategoryId);
                        
                        while(mysqli_stmt_fetch($stmt)):       
            ?>
                            <div class="content">
                                <input type="hidden" value="<?php echo $id; ?>" />
                                <div class="contentThumb">
                                    <video preload="metadata" id="videoFile">
                                        <source src="<?php echo $dir;?>content/<?php echo pathinfo($filename, PATHINFO_FILENAME).$imgExt.'#t=21';?>" type="video/mp4">
                                    </video>
                                </div>
                                <div class="contentName">
                                    <p><?php echo $title; ?></p>
                                </div>
                                <div class="contentCta">
                                    <a href="<?php echo $dir.$filename; ?>">Download</a>
                                </div>
                            </div>
            <?php
                        endwhile;
                    endif;
                endif;
                mysqli_stmt_close($stmt);
                mysqli_close($conn);
            ?>
        </section>
    </main>
    <!-- <main class="main-container">
        <section class="preview-section" id="previewSection">
	        <a href="#" class="back-btn"><i class="fas fa-chevron-left"></i>Back</a>
	        <div class="preview-item"></div>
        </section>	
    </main>
    <script type="text/javascript" src="script/script.js"></script>
    <script type="text/javascript" src="script/historyNavi.js"></script>
    <script type="text/javascript" src="script/scriptPrev.js"></script> -->
    <script type="text/javascript" src="script/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.1/gsap.min.js"></script>
    <script type="text/javascript" src="script/main.js"></script>
</body>
</html>