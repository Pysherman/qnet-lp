<?php
    require('../connection/inc.php');
    
    if(isset($_POST['cat'])){
        $cat = $_POST['cat'];
        if($cat == "videos"){
            $catCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
        }else if($cat == "apps"){
            $catCondition  = " and (b.sub_category='Fitness' or b.sub_category='LifeStyle' or b.sub_category='Relaxation')";
        }else if($cat == "tips"){
            $catCondition  = " and (b.sub_category='foods' or b.sub_category='sports' )";
        }else{
            $cat = "videos";
            $catCondition = " and (b.sub_category='Facts and Tips' or b.sub_category='Balance Diet' or b.sub_category='Fitness')";
        }

        $querySubMenu = "SELECT a.id, a.category, b.id as sc_id, b.sub_category FROM cms.categories a,cms.sub_categories b WHERE a.id = b.category_id $catCondition and category like ?";
        $bindCat = "{$_POST['cat']}%";
        $stmt = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt, $querySubMenu);
        $bindParam = mysqli_stmt_bind_param($stmt, "s", $bindCat);

        if($bindParam){
            mysqli_stmt_execute($stmt);
            $subCat = [];
            $result = mysqli_stmt_get_result($stmt);
            while($data = mysqli_fetch_assoc($result)){
                $catId = $data['id'];
                $category = $data['category'];
                $subCatId = $data['sc_id'];
                $subCategory = $data['sub_category'];

                $newSubCategory = str_replace(" ", "", $subCategory);

                $dataAssoc = ["catId" => $catId, "category" => $category, "subId" => $subCatId, "subCategory" => ucwords($subCategory), "subCategoryAttr" => $newSubCategory];

                array_push($subCat, $dataAssoc);
            }
            echo json_encode($subCat);
            mysqli_stmt_close($stmt);
        }

        mysqli_close($conn);

    }
?>