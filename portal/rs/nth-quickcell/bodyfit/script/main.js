$(document).ready(() => {
    // drop down sub category
    gsap.from(".subCategory", { duration: .5, y: -200, ease: "power4.inOut" });
    // first category active
    gsap.from(".catActive", { duration: .5, y: 200, ease: "power4.inOut" });
    // first sub category text color active
    $('.subCategory ul li a:first').css({ color: "rgba(248, 245, 130, 1)" });

    // show sub category of current category
    let showSubCat = (dataMainCat) => {
        $.ajax({
            type: 'POST',
            url: 'php/subCategories.php',
            data: `cat=${dataMainCat}`,
            dataType: 'json',
            beforeSend: () => {
                // hide sub category of previous category
                gsap.to(".subCategory", { duration: .5, y: -200, ease: "power4.inOut" });
            },
            success: (data, textStatus, xhr) => {
                if (xhr.status == 200) {
                    // replace sub category of previous category
                    $('.subCategory').html(`<ul data-main-category="${dataMainCat}"></ul>`);

                    $.each(data, (key, value) => {
                        const { subCategory, subId, catId, subCategoryAttr } = value;

                        $('.subCategory ul').append(`
                            <li data-sub-cat="${subCategoryAttr}">
                                <a href="#">${subCategory}</a>
                                <input type="hidden" name="catId" value="${catId}" />
                                <input type="hidden" name="subCatId" value="${subId}" />
                            </li>
                        `)
                    })
                    // drop down sub category of current category
                    gsap.to(".subCategory", { duration: .5, y: 0, ease: "power4.inOut", delay: .3 });
                    // get value of hidden inputs
                    const catId = $('.subCategory ul li:first a').siblings("input[name=catId]").val();
                    const subCatId = $('.subCategory ul li:first a').siblings("input[name=subCatId]").val();
                    // show content of current sub category
                    showContent(catId, subCatId, dataMainCat);
                    // first sub category text color active
                    $('.subCategory ul li a:first').css({ color: "rgba(248, 245, 130, 1)" });
                    // add click event to all sub categories
                    subCategoryLinkEvent();
                }
            },
            error: (errorThrown) => {
                console.log(errorThrown);
            }
        })
    }

    let showContent = (catId, subCatId, dataMainCat) => {
        $.ajax({
            type: 'POST',
            url: 'php/contents.php',
            data: JSON.stringify({
                "catId": catId,
                "subCatId": subCatId
            }),
            contentType: 'application/json',
            dataType: 'json',
            beforeSend: () => {
                // hide and remove contents of previous sub category
                gsap.to('.content', {
                    duration: .5,
                    opacity: 0,
                    scale: .2,
                    stagger: {
                        amount: .2,
                        from: "random",
                        grid: "auto",
                        ease: "power4.inOut"
                    }
                });
                // hide and remove selected content
                if ($('.preview') != null) {
                    gsap.to('.preview', {
                        duration: .5,
                        opacity: 0,
                        scale: .2,
                        ease: "power4.inOut"
                    });
                }
            },
            success: (data, textStatus, xhr) => {
                if (xhr.status == 200) {
                    const contentDir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
                    const thumbExt = ".png";
                    // replace all contents of previous sub category
                    $('.mainContainer').html(`<section class="contentWrapper"></section>`).attr("data-category", dataMainCat);

                    // assign each content
                    $.each(data, (key, value) => {
                        const { contentId, title, filename, fileExtension, originalFilename } = value;

                        let thumbnail = "";

                        switch (dataMainCat) {
                            case "games":
                            case "apps":
                                thumbnail = `<img src="${contentDir}content/${filename + thumbExt}" />`;
                                viewapps = `Download`;
                                break;

                            case "tips":
                                thumbnail = `<img src="${contentDir}content/${filename + thumbExt}" />`;
                                viewapps = `View More`;
                                break;

                            case "videos":
                                viewapps = `Play`;
                                thumbnail = `
                                    <video preload="metadata">
                                        <source src="${contentDir}content/${filename}.${fileExtension}#t=21" type="video/mp4">
                                    </video>
                                `;
                                break;

                            default:
                                viewapps = `Download`;
                                thumbnail = `<img src="${contentDir}content/${filename + thumbExt}" />`;
                                break;
                        }

                        $('.contentWrapper').append(`
                            <div class="content">
                                <input type="hidden" value="${contentId}" />
                                <div class="contentThumb">
                                    ${thumbnail}
                                </div>
                                <div class="contentName">
                                    <p>${title}</p>
                                </div>
                                <div class="contentCta">
                                    <a href="${contentDir}content/${filename}.${fileExtension}">${viewapps}</a>
                                </div>
                            </div>
                        `);
                    })
                    // show content of current sub category
                    gsap.from('.content', {
                        duration: .5,
                        opacity: 0,
                        scale: .2,
                        delay: .5,
                        stagger: {
                            amount: .2,
                            from: "random",
                            grid: "auto",
                            ease: "power4.inOut"
                        }
                    });
                    // add click event to each contents
                    contentEvent(dataMainCat);
                }
            },
            error: (errorThrown) => {
                console.log(errorThrown);
            }
        })
    }

    let showPreview = (contentId, dataMainCat) => {
        $.ajax({
            type: 'POST',
            url: 'php/preview.php',
            data: `contentId=${contentId}`,
            dataType: 'json',
            beforeSend: () => {
                // hide and remove contents
                gsap.to('.content', {
                    duration: .5,
                    opacity: 0,
                    scale: .2,
                    stagger: {
                        amount: .2,
                        from: "random",
                        grid: "auto",
                        ease: "power4.inOut"
                    }
                });
            },
            success: (data, textStatus, xhr) => {
                if (xhr.status == 200) {
                    const contentDir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
                    const thumbExt = ".png";
                    let thumbnail;
                    let controls;

                    const { contentId, categoryId, description, fileExtension, filename, subCategoryId, title, screenshots } = data[0];

                    switch (dataMainCat) {
                        case "games":
                        case "apps":
                            thumbnail = `<img src="${contentDir}content/${filename + thumbExt}" />`;
                            viewapp = `Download`;
                            break;

                        case "tips":
                            thumbnail = `<img src="${contentDir}content/${filename + thumbExt}" />`;
                            viewapp = `View`;
                            break;

                            break;
                        case "videos":
                            viewapp = `Play`;
                            thumbnail = `
                                    <video preload="metadata" id="videoFile">
                                        <source src="${contentDir}content/${filename}.${fileExtension}" type="video/mp4">
                                    </video>
                                `;

                            controls = `
                                    <div class="controls">
                                        <button class="btn" id="play">
                                            <i class="fas fa-play"></i>
                                        </button>
                                        <button class="btn" id="stop">
                                            <i class="fas fa-stop"></i>
                                        </button>
                                        <input type="range" id="progress" class="progress" min="0" max="100" step="0.1" value="0" />
                                        <span class="timestamp" id="timestamp">00:00</span>
                                    </div>
                                `;

                            break;
                        default:
                            viewapp = `Download`;
                            thumbnail = `<img src="${contentDir}content/${filename + thumbExt}" />`;
                            break;
                    }
                    // change class name contentWrapper to previewWrapper
                    $('.mainContainer').html(`<section class="previewWrapper"></section>`);

                    $('.previewWrapper').append(`
                        <div class="preview">
                            <div class="prevThumb">
                                ${thumbnail}
                                ${  // show controls if category is tones/videos
                        (dataMainCat == "videos" || dataMainCat == "tones") ? controls : ""}
                            </div>
                            <div class="prevNameCta">
                                <div class="prevName">
                                    <h3>${title}</h3>
                                </div>
                                <div class="prevCta">
                                    <a href="${contentDir}content/${filename}.${fileExtension}">${viewapp}</a>
                                </div>
                            </div>
                            <div class="prevDesc">
                                <p>${description}</p>
                            </div>
                        </div>
                    `);
                    // screenshots
                    if (dataMainCat == "none" || dataMainCat == "none" || dataMainCat == "none") {
                        // add container
                        $('.preview').append(`<div class="prevScreenshots"></div>`);
                        // assign the screenshots to img tag
                        $.each(screenshots, (key, value) => {
                            $('.prevScreenshots').append(`
                                <img src="${contentDir + value}" />
                            `);
                        })

                        let isDown = false;
                        let startX;
                        let scrollLeft;

                        $('.prevScreenshots')
                            .on('mousedown', function (e) {
                                isDown = true;
                                startX = e.pageX - $(this).offset().left;
                                scrollLeft = $(this).scrollLeft();
                            })
                            .on('mousemove', function (e) {
                                e.preventDefault();
                                if (!isDown) return;
                                let x = e.pageX - $(this).offset().left;
                                let walk = (x - startX) * 2;
                                $(this).scrollLeft(scrollLeft - walk);
                            })
                            .on('mouseup', (e) => isDown = false)
                            .on('mouseleave', (e) => isDown = false);
                    }
                    // video /  audio media controls
                    let mediaControls = (mediaFile) => {
                        let file = document.getElementById(mediaFile);

                        let toggleVideoStatus = () => {
                            if (file.paused) {
                                file.play();
                            } else {
                                file.pause();
                            }
                        }

                        let updatePlayIcon = () => {
                            if (file.paused) {
                                $('#play').html(`<i class="fas fa-play"></i>`);
                            } else {
                                $('#play').html(`<i class="fas fa-pause"></i>`);
                            }
                        }

                        let updateProgress = () => {
                            $('#progress').val((file.currentTime / file.duration) * 100);
                            let mins = Math.floor(file.currentTime / 60);
                            if (mins < 10) mins = '0' + String(mins);
                            let secs = Math.floor(file.currentTime % 60);
                            if (secs < 10) secs = '0' + String(secs);

                            $('#timestamp').html(`${mins}:${secs}`);
                        }

                        let setVideoProgress = () => file.currentTime = (parseInt($('#progress').val()) * file.duration) / 100;

                        let stopVideo = () => {
                            file.currentTime = 0;
                            file.pause();
                        }

                        $(file)
                            .on('play', updatePlayIcon)
                            .on('click', toggleVideoStatus)
                            .on('pause', updatePlayIcon)
                            .on('timeupdate', updateProgress);

                        $('#play').on('click', toggleVideoStatus);
                        $('#stop').on('click', stopVideo);

                        $('#progress').on('change', setVideoProgress);

                        if (mediaFile == "audioFile") {
                            $('#audioThumb')
                                .on('play', updatePlayIcon)
                                .on('click', toggleVideoStatus)
                                .on('pause', updatePlayIcon)
                                .on('timeupdate', updateProgress);
                        }

                    }

                    if (dataMainCat == "videos") {
                        mediaControls('videoFile');
                    } else if (dataMainCat == "tones") {
                        mediaControls('audioFile');
                    }

                    gsap.from('.preview', {
                        duration: .5,
                        opacity: 0,
                        scale: .2,
                        delay: .5,
                        ease: "power4.inOut"
                    });
                }
            },
            error: (errorThrown) => {
                console.log(errorThrown);
            }
        })
    }

    let subCategoryLinkEvent = () => {
        $('.subCategory ul li a').on('click', (e) => {
            // remove active text color
            $('.subCategory ul li a').each(function () {
                $(this).css({ color: "#f6f6f6" });
            })
            // set active text color
            $(e.currentTarget).css({ color: "rgba(248, 245, 130, 1)" });

            const catId = $(e.currentTarget).siblings("input[name=catId]").val();
            const subCatId = $(e.currentTarget).siblings("input[name=subCatId]").val();
            const mainCat = $(e.currentTarget).parent().parent().attr('data-main-category');
            // show content
            showContent(catId, subCatId, mainCat);
        })
    }
    // click event content card
    let contentEvent = (dataMainCat) => {
        $('.content').on('click', (e) => {
            const contentId = $(e.currentTarget).children('input').val();
            showPreview(contentId, dataMainCat);
        })
    }

    $('.mainCategory ul li a').on('click', (e) => {
        const catActiveTL = gsap.timeline();
        // active background
        catActiveTL.to(".catActive", { duration: .5, y: 200, ease: "power4.inOut" })
            .to(".catActive", { duration: .1, x: `${$(e.currentTarget).outerWidth() * $(e.currentTarget).parent().index()}px` })
            .to(".catActive", { duration: .4, y: 0, ease: "power4.inOut" });

        let dataMainCat = $(e.currentTarget).parent().attr('data-main-cat');

        showSubCat(dataMainCat);
    });

    subCategoryLinkEvent();

    contentEvent($('.mainCategory ul li:first').attr('data-main-cat'));

})