<!DOCTYPE html>
<html lang="sr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="style/style.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous" defer></script>
    <script src="script/waitForImages.js" defer></script>
    <script src="script/main.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js" defer></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous" defer></script>
	<script src="script/script.js" defer></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <title>FunCity</title>
</head>
<body>
    <main class="mainContainer">
        <div class="headerBg"></div>
        <section class="scrollContainer">
            <section class="headerContainer">
                <section class="headerWrapper">
                <div class="logo">
                <img src="logo.png" alt="FunCity Logo">
                <h1>FunCity</h1>
                    <div class="lang">
					<span class='langson'>Језик:</span>
						<a href="#googtrans(en|en)" class="lang-select langson langson2" data-lang="en">English</a>
						<a href="#googtrans(en|sr)" class="lang-select langson langson2" data-lang="sr">Serbia</a>
					</div>
                </div>
                    <div class="subMenu">
                    </div>
                    <div class="subMenusContainer">
                        <ul class="subMenuLinksContainer"></ul>
                    </div>
                </section>
                <!-- <div class="headerWrapperBg"></div> -->
            </section>
            <section class="contentContainer">
            </section>
        </section>
        <footer class="footerContainer">
            <nav class="navContainer">
                <div class="menuButton" data-cat="games">
                    <i class="las la-chess"></i>
                </div>
                <div class="menuButton" data-cat="apps">
                    <i class="las la-box"></i>
                </div>
                <div class="menuButton" data-cat="tones">
                    <i class="las la-music"></i>
                </div>
                <div class="menuButton menuButtonActive" data-cat="videos">
                    <i class="las la-film"></i>
                </div>
                <div class="menuActive"></div>
            </nav>
        </footer>
    </main>
<svg class="svgBg1" width="519" height="518" viewBox="0 0 519 518" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M390.684 426.926C313.645 478.689 209.573 475.771 103.471 485.796C-2.10736 496.276 -109.681 520.188 -181.039 479.004C-252.922 437.365 -288.099 330.594 -286.931 229.601C-285.762 128.607 -247.758 33.3554 -185.303 -50.3485C-122.883 -134.542 -35.5565 -207.713 46.9312 -203.748C129.454 -199.294 207.173 -117.214 293.149 -43.5996C379.615 29.9803 473.848 95.1306 495.735 181.19C518.147 267.705 467.722 375.163 390.684 426.926Z" fill="#C9C8C8"/>
	</svg>
	<svg class="svgBg2" width="484" height="479" viewBox="0 0 484 479" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path d="M708.684 661.926C631.645 713.689 527.573 710.771 421.471 720.796C315.893 731.276 208.319 755.188 136.961 714.004C65.0778 672.365 29.9006 565.594 31.0692 464.601C32.2379 363.607 70.242 268.355 132.697 184.652C195.117 100.458 282.444 27.2872 364.931 31.2518C447.454 35.7061 525.173 117.786 611.149 191.4C697.615 264.98 791.848 330.131 813.735 416.19C836.147 502.705 785.722 610.163 708.684 661.926Z" fill="#C2BFBF"/>
	</svg>

	<!-- Translate Custom JS-->
	<script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }
	function triggerHtmlEvent(element, eventName) {
	  var event;
	  if (document.createEvent) {
		event = document.createEvent('HTMLEvents');
		event.initEvent(eventName, true, true);
		element.dispatchEvent(event);
	  } else {
		event = document.createEventObject();
		event.eventType = eventName;
		element.fireEvent('on' + event.eventType, event);
	  }
	}
	jQuery('.lang-select').click(function() {
	  var theLang = jQuery(this).attr('data-lang');
	  jQuery('.goog-te-combo').val(theLang);
	  //alert(jQuery(this).attr('href'));
	  window.location = jQuery(this).attr('href');
	  location.reload();

	});
</script>
<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</body>
</html>