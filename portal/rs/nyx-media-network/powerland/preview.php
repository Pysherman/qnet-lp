<?php
   session_start();
   include('connection/inc.php');
   include('connection/preview-header.php');
   ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"  xml:lang="en" translate="yes">
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />-->
      <title>Explorella | Download Store</title>
      <meta name="viewport" content="width=device-width,initial-scale=1.0">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/main.css">
      <script type="text/javascript" src="js/jquery.js"></script>
   </head>
   <body>
      <div class="header">
         <div class="logo">
            <a href="./"><img src="assets/logo.png" alt=""></a>
         </div>
         <div id='main-menu' class='main-menu'>
            <div class='container-menu'>
               <nav class='navigation'>
                  <span class='hamburger-menu'>
                  <span class='burger-1'></span>
                  <span class='burger-2'></span>
                  <span class='burger-3'></span>
                  </span>
                  <ul class='core-menu'>
                     <li class='menus'> <a href="./?varx=games"><img src='assets/<?php echo $btnImg; ?>' border="0"></a></li>
                     <li class='menus'> <a href="./?varx=videos"><img src='assets/<?php echo $vdoImg; ?>' border="0"></a></li>
                     <li class='menus'> <a href="./?varx=tones"><img src='assets/<?php echo $toneImg; ?>' border="0"></a></li>
                     <li class='menus'> <a href="./?varx=apps"><img src='assets/<?php echo $appImg; ?>' border="0"></a> </li>
                  </ul>
               </nav>
            </div>
         </div>
</div>
<main>
      <div class="main-content-preview">
         <div class="content-header"><span class="header_text">Vorschau & Screenshot</span><a href="<?php echo './?varx='.$cat.'#'.$sub; ?>">
            <span class='close cvid'>&times;</span></a>
         </div>
         <div class="content-item-preview">
            <?php
               if(isset($_GET['varx']))
               {
               	$prevSesID = $_GET['contid'];
               	$contenrdir = "https://s3-ap-southeast-1.amazonaws.com/qcnt/";
               	if($_GET['varx']=="")
               	{
               		header("location:index.php");
               	}
               	else
               	{
               		$getContent = $conn->query("SELECT id, title, description, file_name, original_file_name, mime,rate FROM cms.contents WHERE id=$prevSesID");
               		$data = array();
               		if($getContent)
               		{
               			while($items = mysqli_fetch_array($getContent))
               			{
               				$ContentID 		= $items['id'];
               				$itemTitle 		= $items['title'];
               				$description 	= $items['description'];
               				$contentRate	= $items['rate'];
               				$file_name 		= $items['file_name'];
               				$original_file_name = $items['original_file_name'];
               				$mime 			= $items['mime'];
               				$ext 			= pathinfo($file_name, PATHINFO_EXTENSION);
               				$filename = $contenrdir.$file_name;
               
               				$file = pathinfo($file_name, PATHINFO_FILENAME);
               
               				if($ext=="mp4" || $ext=="mp3")
               				{
               					$thumbimg = $file.'.png';
               					$preview ='<video width="100%"  controls preload="metadata" poster="'.$contenrdir.'content/'.$thumbimg.'">
               							<source src="'.$filename.'" type="video/mp4;codecs="avc1.42E01E, mp4a.40.2">
               							</video>';
               				}
               				else
               				{
               					$thumbimg = $file.'.png';
               					$preview ='<img src="'.$contenrdir.'content/'.$thumbimg.'" alt="'.$itemTitle.'">';
               				}
               			}
               		}
               	}
               }
               else
               {
               	header("location:index.php");
               }
               ?>
            <div class="content-item-child">
               <div class="section-title">
                  <?php														
                     if($ext=="apk" || $ext=="xapk")
                     {
                     $prevFile = $conn->query("SELECT file_name FROM cms.preview WHERE content_id='$ContentID'");
                     //if($prevFile)
                     if(mysqli_affected_rows($conn))
                     {
                     echo'
                     <div class="__item-preview">				
                     <div class="__item_screenshots">
                     <div style="margin:auto">';
                     while($PrevItems = mysqli_fetch_array($prevFile))
                     {
                     $scrFiles	= $PrevItems['file_name'];
                     $screenshot = $contenrdir.$scrFiles;
                     echo"<img src=\"".$screenshot."\" width=\"221\" height=\"191\" class=\"mySlides\">";
                     }
                     
                     // Next & Previous button 
                     echo"</div class=\"\">
                     <div class=\"back_next_holder\">
                     <button class=\"back_button\" onclick=\"plusDivs(-1)\"><img src=\"assets/btn_left.png\" width=\"19\" height=\"14\" /></button>
                     
                     <button class=\"next_button\" onclick=\"plusDivs(1)\"><img src=\"assets/btn_right.png\" width=\"19\" height=\"14\" /></button>
                     </div>
                     </div>
                     
                     
                     <script>
                     var slideIndex = 1;
                     showDivs(slideIndex);
                     
                     function plusDivs(n) {
                     showDivs(slideIndex += n);
                     }
                     function showDivs(n) {
                     var i;
                     var x = document.getElementsByClassName(\"mySlides\");
                     if (n > x.length) {slideIndex = 1}    
                     if (n < 1) {slideIndex = x.length}
                     for (i = 0; i < x.length; i++) {
                     x[i].style.display = \"none\";  
                     }
                     x[slideIndex-1].style.display = \"block\";  
                     }
                     </script>
                     
                     </div>
                     <div class=\"__item\">
                     <div class=\"__item_title\">$itemTitle</div>
                     	<!--img src=\"assets/$contentRate.png\" border=\"0\" /-->
                     	<div class=\"__item_description_holder\">
                           <div class=\"__item-description\">stripslashes($description);</div>
                           <div class=\"btn-holder\"><a href=\"$filename\" class=\"btn\">HERUNTERLADEN</a></div>
                     </div>
                     </div> 
                     
                     
                     ";
                     }
                     }else{
                     	echo "<div class=\"__item-preview\">  $preview </div>
                     	<div class=\"__item\">
                     	<div class=\"__item_title\">$itemTitle</div>
                     	<!--img src=\"assets/$contentRate.png\" border=\"0\" /-->
                     	<div class=\"__item_description_holder\">
                     		<div class=\"__item-description\"> stripslashes($description)
                        </div>
                        <div class=\"btn-holder\"><a href=\"$filename\" class=\"btn\">HERUNTERLADEN</a></div>
                     </div>
                     
                     </div>";
                     }
                     
                     ?>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script src="js/script.js"></script>
   </body>
</html>