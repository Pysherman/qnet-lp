<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="style/style.css" />
    
    <script src="script/jquery.js" defer></script>
    <script src="script/gsap/gsap.min.js" defer></script>
    <script src="script/gsap/ScrollToPlugin.min.js" defer></script>
    <script src="script/script.js" defer></script>
    <title>GAMEopedia | Download</title>
</head>
<body>
    <?php require("backend/connect.php"); ?>
    <?php
        function getSubCat($mysqli, $cat){
            $catName = "";
            $catCondition = "";
            if($cat === "Videos"){
                $catCondition = "and (b.sub_category='Asian' or b.sub_category='Blonde' or b.sub_category='Cosplay' or b.sub_category='European')";
                $catName = filter_var($cat, FILTER_SANITIZE_SPECIAL_CHARS);
            }else if($cat === "Wallpaper"){
                $catCondition = "and (b.sub_category='Asian' or b.sub_category='Cosplay' or b.sub_category='PornStar')";
                $catName = filter_var($cat, FILTER_SANITIZE_SPECIAL_CHARS);
            }

            $query = "SELECT a.id, a.category, b.id AS sc_id, b.sub_category FROM cms.categories a, cms.sub_categories b WHERE a.id = b.category_id $catCondition AND category like ?";

            $catName = "{$catName}%";
            
            $stmt = $mysqli->stmt_init();
            $stmt->prepare($query);
            $bindParam = $stmt->bind_param("s", $catName);
            if($bindParam){
                $stmt->execute();
                $subCat = [];
                $result = $stmt->get_result();
                while($data = $result->fetch_assoc()){
                    $catId = $data['id'];
                    $category = "";
                    $subCatId = $data['sc_id'];
                    $subCategory = $data['sub_category'];

                    if($data['category'] === "Games-apk"){
                        $category = "Games";
                    }

                    $dataAssoc = ["catId" => $catId, "modeifiedCategoryName" => $category, "subId" => $subCatId, "subCategory" => $subCategory, "originalCategoryName" => $data['category']];
                    array_push($subCat, $dataAssoc);
                }

                return $subCat;
                $stmt->close();
            }
        }

        $subCats = getSubCat($mysqli, isset($_GET['cat']) ? $_GET['cat'] : "Videos");

        function getContents($mysqli, $subCats){
            $mainData = [];

            foreach($subCats as $subCat){
                $query = "SELECT id, title, description, file_name, original_file_name, mime, sub_category_id FROM cms.contents WHERE id!=1 AND category_id = ? AND sub_category_id = ? ORDER BY id DESC LIMIT 20";

                $stmt = $mysqli->stmt_init();
                $stmt->prepare($query);
                $stmt->bind_param("ii", $subCat['catId'], $subCat['subId']);
                $stmt->execute();
                $stmt->store_result();

                $result = $stmt->num_rows();

                $data = [];

                if($result > 0){
                    $stmt->bind_result($id, $title, $description, $iconFileName, $contentFileName, $contentFileMime, $subCategoryId);
                    while($stmt->fetch()){
                        array_push($data, ["contentId" => $id, "title" => $title, "description" => $description, "icon" => $iconFileName, "fileName" => $contentFileName, "category" => $subCat['originalCategoryName'], "subCategoryId" => $subCategoryId]);
                    }
                }

                array_push($mainData, $data);

            }

            return $mainData;
        }

        $contents = getContents($mysqli, $subCats);

        function getSubCatName($mysqli, $subId){
            $query = "SELECT sub_category FROM cms.sub_categories WHERE id = ?";
            $stmt = $mysqli->stmt_init();
            $stmt->prepare($query);
            $stmt->bind_param("i", $subId);
            $stmt->execute();
            $stmt->bind_result($subName);
            while($stmt->fetch()){
                return $subName;
            }
            $stmt->close();
        }
    ?>
    <header class="headerWrap">
        <div class="headerCont">
            <h1>sexyLADYBOY</h1>
            <div class="menuCatCont">
                <div class="menuCat">
                    <a href="?cat=Videos" class="catLink">Videos</a>
                    <a href="?cat=Wallpaper" class="catLink">Wallpaper</a>
                </div>
            </div>
        </div>
    </header>
    <section class="indicatorWrap">
        <section class="catHeaderWrap">
            <span class="h3Bg" id="h3Bg"><?php echo ucwords(str_replace("-", " ", $subCats[0]['subCategory'])); ?></span>
        </section>
        <section class="catIconsWrap" style="opacity:0">
            <ul>
                <?php
                    $subCatProps = [
                        "Action" => ["iconName" => "crosshairs", "bgColor" => "#003F88", "headingColor" => "#004EA8", "darkColor" => "#00244F"],
                        "Asian" => ["iconName" => "female", "bgColor" => "#CB4022", "headingColor" => "#F13910", "darkColor" => "#8C1D05"],
                        "Cosplay" => ["iconName" => "female", "bgColor" => "#028226", "headingColor" => "#04982E", "darkColor" => "#025C1B"],
                        "PornStar" => ["iconName" => "female", "bgColor" => "#4701A1", "headingColor" => "#5D04CD", "darkColor" => "#21004B"],
                        "European" => ["iconName" => "female", "bgColor" => "#880062", "headingColor" => "#A60479", "darkColor" => "#4F0239"],
                        "Strategy" => ["iconName" => "chess", "bgColor" => "#021D23", "headingColor" => "#023D4A", "darkColor" => "#365E67"],
                        "Blonde" => ["iconName" => "female", "bgColor" => "#D78F05", "headingColor" => "#EB9C06", "darkColor" => "#845700"],
                        "virtual-reality" => ["iconName" => "vr-cardboard", "bgColor" => "#BF0B00", "headingColor" => "#DE1206", "darkColor" => "#760A03"]                    
                    ];

                    foreach($subCats as $subCat):
                ?>
                    <li><i class="fas fa-<?php echo $subCatProps[$subCat['subCategory']]["iconName"]; ?>" 
                            data-bgColor="<?php echo $subCatProps[$subCat['subCategory']]["bgColor"]; ?>"
                            data-headingColor="<?php echo $subCatProps[$subCat['subCategory']]["headingColor"]; ?>"
                            data-darkColor="<?php echo $subCatProps[$subCat['subCategory']]["darkColor"]; ?>"
                        ></i></li>
                <?php
                    endforeach;
                ?>
            </ul>
        </section>
    </section>
    <section class="mainWrap">
        <section class="subCatWrap">
            <section class="subCatNav">
                <div class="subCatList">
                <?php 
                    foreach($subCats as $subCat):
                ?>
                    <div class="subCatLink">
                        <input type="hidden" value="<?php echo $subCat['subId']; ?>" />
                        <span><?php echo ucwords(str_replace("-", " ", $subCat['subCategory'])); ?></span>
                    </div>
                <?php
                    endforeach;
                ?>
                </div>
                <div class="subCatIndi" id="subCatIndi">
                    <span class="subCatCircle" id="subCatCircle"></span>
                    <span class="subCatLine" id="subCatLine"></span>
                </div>
            </section>
        </section>
        <section class="contentWrap">
            <section class="contentList">
                <?php
                    $contentDir = "https://s3-ap-southeast-1.amazonaws.com/qcnt-portal/portal";
                    foreach($contents as $content):
                ?>
                    <div class="contentDivider">
                        <?php
                            if(empty($content)){ echo "<h3>👉 COMING SOON 👈</h3> ";}
                        ?>
                <?php
                        foreach($content as $cont):
                            $cat = strtolower($cont['category']);
                            $subCat = strtolower(getSubCatName($mysqli, $cont['subCategoryId']));
                            $contentName = str_replace(" ", "+", strtolower($cont['title']));
                            $iconName = pathinfo($cont['icon'], PATHINFO_FILENAME);
                            $ext = pathinfo($cont['icon'], PATHINFO_EXTENSION);
                            $fileName = str_replace(" ", "+", $cont['fileName']);

                            $thumbNail = "";
                            switch($cat){
                                case "videos":
                                    $thumbNail = "<video width='100%' height='100%' preload='metadata'><source src='https://s3-ap-southeast-1.amazonaws.com/qcnt/{$cont['icon']}#t=10' type='video/mp4'></video>";
                                    break;
                                case "wallpaper":
                                    $thumbNail = "<img src='https://s3-ap-southeast-1.amazonaws.com/qcnt/content/{$iconName}.jpg' />";
                                    break;
                            }
                ?>
                            <div class="content">
                                <?php echo $thumbNail; ?>
                                <p><?php echo $cont['title']; ?></p>
                                <div class="ctaBtns">
                                    <input type="hidden" value="<?php echo $cat; ?>" />
                                    <input type="hidden" value="<?php echo $subCat; ?>" />
                                    <input type="hidden" value="<?php echo $cont['contentId']; ?>" />
                                    <span class="detailsCta">DETAILS</span>
                                    <a href="https://s3-ap-southeast-1.amazonaws.com/qcnt/content/<?php echo $iconName.'.'.$ext; ?>">DOWNLOAD</a>
                                </div>
                            </div>
                <?php
                        endforeach;
                ?>
                    </div>
                <?php
                    endforeach;
                ?>
            </section>
        </section>
    </section>
    <section class="bgWrapper">
        <?php
            foreach($subCats as $subCat):
        ?>
            <div class="bg"></div>
        <?php
            endforeach;
        ?>
    </section>
</body>
</html>