<?php
    require('connect.php');

    if(isset($_POST['id'])){
        $id = filter_var($_POST['id'], FILTER_SANITIZE_SPECIAL_CHARS);

        $stmt = $mysqli->stmt_init();
        $stmt->prepare("SELECT id, title, description, file_name, original_file_name FROM cms.contents WHERE id = ?");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->num_rows();

        $stmt->bind_result($id, $title, $description, $iconName, $contentName);

        $data = [];

        while($stmt->fetch()){
            array_push($data, [
                "title" => $title,
                "description" => $description,
                "iconName" => pathinfo($iconName, PATHINFO_FILENAME),
                "ext" => pathinfo($iconName, PATHINFO_EXTENSION),
                "full" => $iconName,
                "contentName" => $contentName
            ]);
        }

        echo json_encode($data);
        $stmt->close();
    }
?>