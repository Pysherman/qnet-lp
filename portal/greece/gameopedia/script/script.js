$(window).ready(() => {
    class Backgrounds {
        constructor() {
            this.bg = $('.bg')
            this.bgWrapper = $('.bgWrapper')
            this.headerWrapH1 = $('.headerWrap h1')
        }

        setColors(icons) {
            this.bg.eq(0).addClass("activeBg")
            this.bg.each((i, elem) => $(elem).css({ backgroundColor: icons.eq(i).attr("data-bgColor") }))
        }

        updateELements(ind, icons, h3Bg, contentDivider) {
            setTimeout(() => {
                this.headerWrapH1.css({ color: icons.eq(ind).attr("data-darkColor") });
            }, 600)

            setTimeout(() => {
                h3Bg.css({ color: icons.eq(ind).attr("data-headingColor") })
            }, 400)

            icons.eq(ind).css({ color: "#fff" });

            setTimeout(() => {
                icons.not(".activeIcon").css({ color: icons.eq(ind).attr("data-darkColor") });
            }, 100)
        }

        changeBackground(curentSub) {

            let scrollVal = this.bg.eq($(curentSub).index()).outerWidth() * $(curentSub).index();
            this.bg.each((i, elem) => $(elem).removeClass("activeBg"))
            this.bg.eq($(curentSub).index()).addClass("activeBg")
            // this.bgWrapper.scrollLeft(scrollVal)
            gsap.to(this.bgWrapper, { duration: 2, scrollTo: { y: 0, x: scrollVal }, ease: "power4.out" })
        }

        windowResizeBackground() {
            $(window).on('resize', () => {
                this.bg.each((i, elem) => {
                    if ($(elem).hasClass("activeBg")) this.bgWrapper.scrollLeft($(elem).outerWidth() * i)
                })
            })
        }
    }

    class Contents extends Backgrounds {
        constructor() {
            super()
            this.contentDivider = $('.contentDivider');
            this.contentList = $('.contentList')
            this.detailsCta = $('.detailsCta')
            this.isDown = []
            this.startX = []
            this.scrollLeft = []
        }

        xScrollEventScreenshot(elem) {
            let down = false;
            let sX = 0;
            let scroll = 0;

            elem
                .on('mousedown', function (e) {
                    down = true;
                    sX = e.pageX - $(this).offset().left;
                    scroll = $(this).scrollLeft()
                    $(this).css({ cursor: "grab" })
                })
                .on('mousemove', function (e) {
                    e.preventDefault()
                    if (!down) return
                    let x = e.pageX - $(this).offset().left
                    let walk = (x - sX) * 1.2
                    $(this).scrollLeft(scroll - walk).css({ cursor: "grabbing" })
                })
                .on('mouseup', function (e) {
                    down = false
                    $(this).css({ cursor: "default" })
                })
                .on('mouseleave', function (e) {
                    down = false
                    $(this).css({ cursor: "default" })
                })
                .on('touchstart', function (e) {
                    down = true;
                    sX = e.pageX - $(this).offset().left;
                    scroll = $(this).scrollLeft()
                    $(this).css({ cursor: "grab" })
                })
                .on('touchmove', function (e) {
                    e.preventDefault()
                    if (!down) return
                    let x = e.pageX - $(this).offset().left
                    let walk = (x - sX) * 1.2
                    $(this).scrollLeft(scroll - walk).css({ cursor: "grabbing" })
                })
                .on('touchend', function (e) {
                    down = false
                    $(this).css({ cursor: "default" })
                })
                .on('touchcancel', function (e) {
                    down = false
                    $(this).css({ cursor: "default" })
                })
        }

        fetchData(contentId, subCat, cat, contentDividerInd, contentInd, icons) {
            const dataForm = new FormData
            dataForm.append("id", contentId)

            $.ajax({
                type: 'POST',
                url: 'backend/query.php',
                data: dataForm,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: (data, textStatus, xhr) => {
                    if (xhr.status === 200) {
                        let tl = gsap.timeline();

                        const { contentName, description, iconName, title, screenshots } = data[0]
                        const contentDir = "https://s3-ap-southeast-1.amazonaws.com/qcnt-portal/portal"

                        gsap.to(this.contentDivider.eq(contentDividerInd).children(), {
                            opacity: 0, scale: .3, y: 70, duration: .7, ease: "power4.out", stagger: { amount: .7, from: contentInd, grid: [3, 7], ease: "none" },
                            onComplete: () => {
                                this.contentDivider.eq(contentDividerInd).css({ display: "block", overflowX: "initial" }).children().css({ display: "none" })

                                const contentFolderName = title.replaceAll(" ", "+");

                                let images = "";

                                $.each(screenshots, (i, img) => images += `<img src="${contentDir}/${cat}/${subCat}/${contentFolderName.toLowerCase()}/screenshots/${img}" alt="${title}-screen-${i}" draggable="false" />`)

                                this.contentDivider.eq(contentDividerInd).prepend(`
                                <div class="previewCont">
                                    <div class="closeBtnCont">
                                        <span class="closeBtn">Close</span>
                                    </div>
                                    <div class="upperDetailsCont">
                                        <div class="thumbPreviewCont">
                                            <img src="${contentDir}/${cat}/${subCat}/${contentFolderName.toLowerCase()}/${iconName}.png" alt="${title}" />
                                            <span class="h4bg">${title}</span>
                                        </div>
                                        <div class="nameCtaPreviewCont">
                                            <p>${title}</p>
                                            <a href="${contentDir}/${cat}/${subCat}/${contentFolderName.toLowerCase()}/${contentName}" class="ctaPreviewDownload">DOWNLOAD</a>
                                        </div>
                                    </div>
                                    <div class="lowerDetailsCont">
                                        <div class="descriptionPreviewCont">
                                            <p>${description}</p>
                                        </div>
                                        <div class="screenshotsPreviewCont">
                                            ${images}
                                        </div>
                                    </div>
                                </div>
                            `)

                                this.contentDivider.eq(contentDividerInd).find('.closeBtn').css({ background: icons.eq(contentDividerInd).attr('data-darkcolor') })
                                this.contentDivider.eq(contentDividerInd).find('.ctaPreviewDownload').css({ background: icons.eq(contentDividerInd).attr('data-darkcolor') })
                                this.contentDivider.eq(contentDividerInd).find('.h4bg').css({ color: icons.eq(contentDividerInd).attr('data-headingcolor') })

                                this.xScrollEventScreenshot($('.screenshotsPreviewCont'))

                                tl.fromTo(this.contentDivider.eq(contentDividerInd).find('.previewCont'), { opacity: 0, scale: .3, y: 70 }, { opacity: 1, scale: 1, y: 0, duration: .7, ease: "power4.out" })
                                    .fromTo(this.contentDivider.eq(contentDividerInd).find('.h4bg'), { opacity: 0, x: "-100px" }, { opacity: 1, x: "40px", duration: 4, ease: "power4.out" }, "-=.4")

                                this.closeContentPreview(this.contentDivider.eq(contentDividerInd).find('.closeBtn'), contentDividerInd)
                            }
                        })

                    }
                },
                error: (err) => console.log(err)
            })
        }

        closeContentPreview(closeBtn, contentDividerInd) {
            closeBtn.on('click', (e) => {
                const tl = gsap.timeline()
                tl.to(this.contentDivider.eq(contentDividerInd).find('.previewCont'), {
                    opacity: 0, scale: .3, y: 70, duration: .7, ease: "power4.out",
                    onComplete: () => {
                        this.contentDivider.eq(contentDividerInd).find('.previewCont').remove();
                        this.contentDivider.eq(contentDividerInd).children().css({ display: "block" })
                        this.contentDivider.eq(contentDividerInd).css({ display: "grid", overflowX: "hidden" })
                    }
                })
                    .to(this.contentDivider.eq(contentDividerInd).children(), { opacity: 1, scale: 1, y: 0, duration: .7, ease: "power4.out", stagger: { amount: .7, from: "start", ease: "none" } })
            })
        }

        showContentPreview(icons) {
            this.detailsCta.on('click', (e) => {
                const contentId = $(e.currentTarget).prev().val();
                const subCat = $(e.currentTarget).prev().prev().val();
                const cat = $(e.currentTarget).prev().prev().prev().val();
                const contentDividerInd = $(e.currentTarget).parents('.contentDivider').index();
                const contentInd = $(e.currentTarget).parents('.content').index();

                this.fetchData(contentId, subCat, cat, contentDividerInd, contentInd, icons)
            })
        }

        setDefaultContentsValue() {
            this.contentDivider.each((i, elem) => {
                this.isDown.push(false)
                this.startX.push(0)
                this.scrollLeft.push(0)
            })
        }

        resetContentXScrollPosition() {
            // this.contentDivider.each((i, elem) => gsap.to(elem, {duration:2, scrollTo: {y: 0, x: 0}, ease:"power4.out"}))
            this.contentDivider.each((i, elem) => $(elem).scrollLeft(0))
        }

        yScrollEventContentDiv(curentSub) {
            let scrollVal = this.contentDivider.eq($(curentSub).index()).outerHeight(true) * $(curentSub).index();
            gsap.to(this.contentList, { duration: 2, scrollTo: { y: scrollVal, x: 0 }, ease: "power4.out", onComplete: () => this.resetContentXScrollPosition() })
        }

        xScrollEventContentDiv() {
            this.contentDivider.each((i, elem) => {
                if ($(elem).children().length > 7) {
                    $(elem)
                        .on('mousedown', (e) => {
                            this.isDown[i] = true;
                            this.startX[i] = e.pageX - $(e.currentTarget).offset().left
                            this.scrollLeft[i] = $(e.currentTarget).scrollLeft()
                            $(e.currentTarget).css({ cursor: "grab" })
                        })
                        .on('mousemove', (e) => {
                            e.preventDefault()
                            if (!this.isDown[i]) return
                            let x = e.pageX - $(e.currentTarget).offset().left
                            let walk = (x - this.startX[i]) * 1.2
                            $(e.currentTarget).scrollLeft(this.scrollLeft[i] - walk).css({ cursor: "grabbing" })
                        })
                        .on('mouseup', (e) => {
                            this.isDown[i] = false
                            $(e.currentTarget).css({ cursor: "default" })
                        })
                        .on('mouseleave', (e) => {
                            this.isDown[i] = false
                            $(e.currentTarget).css({ cursor: "default" })
                        })
                        .on('touchstart', (e) => {
                            this.isDown[i] = true;
                            this.startX[i] = e.pageX - $(e.currentTarget).offset().left
                            this.scrollLeft[i] = $(e.currentTarget).scrollLeft()
                            $(e.currentTarget).css({ cursor: "grab" })
                        })
                        .on('touchmove', (e) => {
                            e.preventDefault()
                            if (!this.isDown[i]) return
                            let x = e.pageX - $(e.currentTarget).offset().left
                            let walk = (x - this.startX[i]) * 1.2
                            $(e.currentTarget).scrollLeft(this.scrollLeft[i] - walk).css({ cursor: "grabbing" })
                        })
                        .on('touchcancel', (e) => {
                            this.isDown[i] = false
                            $(e.currentTarget).css({ cursor: "default" })
                        })
                        .on('touchend', (e) => {
                            this.isDown[i] = false
                            $(e.currentTarget).css({ cursor: "default" })
                        })
                }
            })
        }

        setBgColorContents(icons) {
            this.contentDivider.each((i, elem) => {
                if ($(elem).children(":first-child").is("h3")) {
                    $(elem).children().css({ background: "transparent" })
                } else {
                    $(elem).children().css({ background: icons.eq(i).attr("data-darkColor") })
                }
            })
        }

    }

    class SubCategories extends Contents {
        constructor() {
            super()
            this.subCatLink = $('.subCatLink')
            this.subCatLine = $('#subCatLine')
            this.subCatIndi = $('#subCatIndi')
            this.subCatCircle = $('#subCatCircle')
            this.lineWidth = (this.subCatLink.length - 1) * 4;
            this.icons = $('.catIconsWrap ul li i');
            this.h3Bg = $('#h3Bg')
            this.subCatWrap = $('.subCatWrap')
        }

        navXScrollEvent() {
            let down = false;
            let sX = 0;
            let scroll = 0;

            this.subCatWrap
                .on('mousedown', function (e) {
                    down = true;
                    sX = e.pageX - $(this).offset().left;
                    scroll = $(this).scrollLeft()
                    $(this).css({ cursor: "grab" })
                })
                .on('mousemove', function (e) {
                    e.preventDefault()
                    if (!down) return
                    let x = e.pageX - $(this).offset().left
                    let walk = (x - sX) * 1.2
                    $(this).scrollLeft(scroll - walk).css({ cursor: "grabbing" })
                })
                .on('mouseup', function (e) {
                    down = false
                    $(this).css({ cursor: "default" })
                })
                .on('mouseleave', function (e) {
                    down = false
                    $(this).css({ cursor: "default" })
                })
                .on('touchstart', function (e) {
                    down = true;
                    sX = e.pageX - $(this).offset().left;
                    scroll = $(this).scrollLeft()
                    $(this).css({ cursor: "grab" })
                })
                .on('touchmove', function (e) {
                    e.preventDefault()
                    if (!down) return
                    let x = e.pageX - $(this).offset().left
                    let walk = (x - sX) * 1.2
                    $(this).scrollLeft(scroll - walk).css({ cursor: "grabbing" })
                })
                .on('touchend', function (e) {
                    down = false
                    $(this).css({ cursor: "default" })
                })
                .on('touchcancel', function (e) {
                    down = false
                    $(this).css({ cursor: "default" })
                })
        }

        onload() {
            let tl = gsap.timeline();

            this.setDefaultContentsValue()

            this.xScrollEventContentDiv();

            this.setBgColorContents(this.icons)

            this.subCatLink.each((i, elem) => this.lineWidth += $(elem).outerWidth(true));

            this.subCatIndi.width(this.lineWidth)
            this.icons.eq(0).addClass("activeIcon")

            tl.fromTo(this.subCatLine, { width: 0 }, { width: this.lineWidth, duration: .5, ease: "power4.out" })
                .to(this.subCatCircle, { x: `${(this.subCatLink.eq(0).outerWidth() - this.subCatCircle.width()) / 2}px`, duration: .5, ease: "power4.out" }, "-=.45")
                .fromTo(this.h3Bg, { opacity: 0, x: "-100px" }, { opacity: 1, x: "-10px", duration: 4, ease: "power4.out" }, "-=1")

            this.setColors(this.icons)

            this.showContentPreview(this.icons);

            this.windowResizeBackground();
        }

        clickEvent() {

            const animation = (xValue, currentElem, ind) => {
                let tl = gsap.timeline();

                this.icons.each((i, elem) => $(elem).removeClass('activeIcon'))
                this.icons.eq(ind).addClass('activeIcon');

                tl.to(this.subCatCircle, { x: `${xValue}px`, duration: .5, ease: "power4.out" })
                    .to(this.h3Bg, {
                        opacity: 0, x: "-100px", duration: 1, ease: "power4.out",
                        onComplete: () => {
                            this.h3Bg.text($(currentElem).text())

                            gsap.to(this.h3Bg, { opacity: 1, x: "-10px", duration: 1, ease: "power4.out" })
                        }
                    }, "-=1")
            }

            this.subCatLink.on('click', (e) => {
                let distance = 0;

                this.yScrollEventContentDiv(e.currentTarget)

                this.changeBackground(e.currentTarget)

                this.setDefaultContentsValue()

                this.updateELements($(e.currentTarget).index(), this.icons, this.h3Bg)

                this.subCatLink.each((i, elem) => {

                    distance += ($(elem).outerWidth(true))

                    if (i === $(e.currentTarget).index()) {
                        if ($(e.currentTarget).index() === (this.subCatLink.length - 1)) {
                            let xValue = ((distance + (i * 4)) - ($(e.currentTarget).outerWidth() / 2)) - 4;

                            animation(xValue, e.currentTarget, i)
                        } else {
                            let xValue = ((distance + (i * 4)) - ($(e.currentTarget).outerWidth() / 2)) - (this.subCatCircle.width() + 6)

                            animation(xValue, e.currentTarget, i)
                        }
                        return false;
                    }
                })

            })
        }
    }

    const subCategories = new SubCategories();

    subCategories.onload();
    subCategories.clickEvent();
    subCategories.navXScrollEvent();
})